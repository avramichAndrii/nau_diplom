﻿using ErrorHandling;
using Menu.API.Client.Clients;
using Menu.API.Contracts.Requests;
using Menu.API.Contracts.Responses;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Infrastructure.HttpClientAbstractions;
using Xunit;

namespace Menu.API.Client.Tests
{
    public class MenuClientTests
    {
        private const string TestUrl = "http://google.com/";
        private const string BadRequestMessage = "something bad happened";
        private MenuClient PrepareClientForOkResult(object responseObject)
        {
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            var mockedResponse = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(JsonConvert.SerializeObject(responseObject), Encoding.UTF8, "application/json")
            };
            handlerMock.Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(mockedResponse)
               .Verifiable();

            var httpClient = new HttpClient(handlerMock.Object)
            {
                BaseAddress = new Uri(TestUrl)
            };
            return new MenuClient(httpClient);
        }
        private MenuClient PrepareClientForBadResult()
        {
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            var mockedResponse = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.BadRequest,
                Content = new StringContent(BadRequestMessage)
            };
            handlerMock.Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(mockedResponse)
               .Verifiable();

            var httpClient = new HttpClient(handlerMock.Object);
            return new MenuClient(httpClient);
        }

        #region Menu
        [Fact]
        public async Task GetMenu_WhenItExists_ShouldReturnSingle()
        {
            // Arrange
            var responseMenu = new MenuResponse()
            {
                Id = Guid.NewGuid(),
                MenuName = "Salads",
                MenuInfo = "some info",
                ProviderCompanyId = Guid.NewGuid(),
                DishList = new List<DishResponse>(),
            };
            //var responseMenu = new MenuResponse(Guid.NewGuid(), "Salads", "some info", Guid.NewGuid(), new List<DishModel>(), TimeSpan.FromMinutes(10));

            var testedClient = PrepareClientForOkResult(responseMenu);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            var response = await testedClient.GetMenuByIdAsync(Guid.NewGuid());



            // Assert

            Assert.NotNull(response);
            Assert.Equal(responseMenu.MenuName, response.MenuName);
            Assert.Equal(responseMenu.MenuInfo, response.MenuInfo);
        }



        [Fact]
        public async Task GetAllMenues_ShouldReturnReadOnlyCollection()
        {
            // Arrange
            var responseCollection = new GetAllMenusResponse(
                new List<MenuResponse>
                {new MenuResponse()
            {
                Id = Guid.NewGuid(),
                MenuName = "Salads",
                MenuInfo = "some info",
                ProviderCompanyId = Guid.NewGuid(),
                DishList = new List<DishResponse>(),
            },new MenuResponse()
            {
                Id = Guid.NewGuid(),
                MenuName = "Salads",
                MenuInfo = "some info",
                ProviderCompanyId = Guid.NewGuid(),
                DishList = new List<DishResponse>(),
            },new MenuResponse()
            {
                Id = Guid.NewGuid(),
                MenuName = "Salads",
                MenuInfo = "some info",
                ProviderCompanyId = Guid.NewGuid(),
                DishList = new List<DishResponse>(),
            }
        });

            var testedClient = PrepareClientForOkResult(responseCollection);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            var result = await testedClient.GetMenusAsync();

            // Assert
            var response = Assert.IsType<GetAllMenusResponse>(result);
            Assert.NotNull(response);
            Assert.Equal(responseCollection.MenusCollection.Count, response.MenusCollection.Count);
        }

        [Fact]
        public async Task GetAllMenuesByProvider_ShouldReturnReadOnlyCollection()
        {
            // Arrange
            var responseCollection = new GetAllMenusResponse(
                new List<MenuResponse>
                {
                   new MenuResponse()
            {
                Id = Guid.NewGuid(),
                MenuName = "Salads",
                MenuInfo = "some info",
                ProviderCompanyId = Guid.NewGuid(),
                DishList = new List<DishResponse>(),
            },new MenuResponse()
            {
                Id = Guid.NewGuid(),
                MenuName = "Salads",
                MenuInfo = "some info",
                ProviderCompanyId = Guid.NewGuid(),
                DishList = new List<DishResponse>(),
            },new MenuResponse()
            {
                Id = Guid.NewGuid(),
                MenuName = "Salads",
                MenuInfo = "some info",
                ProviderCompanyId = Guid.NewGuid(),
                DishList = new List<DishResponse>(),
            }});

            var testedClient = PrepareClientForOkResult(responseCollection);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            var result = await testedClient.GetByProviderAsync(Guid.NewGuid());

            // Assert
            var response = Assert.IsType<GetAllMenusResponse>(result);
            Assert.NotNull(response);
            Assert.Equal(responseCollection.MenusCollection.Count, response.MenusCollection.Count);
        }
        [Fact]
        public async Task GetMenu_WhenItNotExists_CausesBadRequest()
        {
            // Arrange

            var testedClient = PrepareClientForBadResult();
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            var thrownException = await Assert.ThrowsAsync<ClientErrorHttpResponseException>
                (() => testedClient.GetMenuByIdAsync(Guid.NewGuid()));


            Assert.Equal(HttpStatusCode.BadRequest, thrownException.StatusCode);
        }

        [Fact]
        public async Task CreateMenu_ShouldReturnCreated_WhenIsValid()
        {
            // Arrange
            Guid generatedId = Guid.NewGuid();
            var ValidMenu = new MenuResponse()
            {
                Id = Guid.NewGuid(),
                MenuName = "Salads",
                MenuInfo = "some info",
                ProviderCompanyId = Guid.NewGuid(),
                DishList = new List<DishResponse>(),
            };
            var testedClient = PrepareClientForOkResult(ValidMenu);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            var request = new MenuRequest()
            {
                MenuName = "Cakes",
                MenuInfo = "some info"
            };
            var result = await testedClient.CreateMenuAsync(request);

            // Assert

            Assert.NotNull(result);
            Assert.Equal(ValidMenu.Id, result.Id);
            Assert.Equal(ValidMenu.MenuName, result.MenuName);
            Assert.Equal(ValidMenu.MenuInfo, result.MenuInfo);
        }

        [Fact]
        public async Task CreateMenu_CausesBadRequest_WnenNotValid()
        {
            // Arrange
            var testedClient = PrepareClientForBadResult();
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            var request = new MenuRequest()
            {
                MenuName = "test",
                MenuInfo = "testing"
            };
            var thrownException = await Assert.ThrowsAsync<ClientErrorHttpResponseException>
              (() => testedClient.CreateMenuAsync(request));

            // Assert
            Assert.Equal(HttpStatusCode.BadRequest, thrownException.StatusCode);
        }

        [Fact]
        public async Task UpdateMenu_WhenItExists_ShouldReturnUpdated_WhenUsingValidData()
        {
            // Arrange
            Guid idForUpdate = Guid.NewGuid();
            var ValidMenu = new MenuResponse()
            {
                Id = Guid.NewGuid(),
                MenuName = "Salads",
                MenuInfo = "some info",
                ProviderCompanyId = Guid.NewGuid(),
                DishList = new List<DishResponse>(),
            };
            var testedClient = PrepareClientForOkResult(ValidMenu);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            var bodyForUpdate = new MenuRequest()
            {
                MenuName = "Salad",
                MenuInfo = "Some info again"
            };

            var result = await testedClient.UpdateMenuAsync(idForUpdate, bodyForUpdate);

            // Assert

            Assert.NotNull(result);
            Assert.Equal(ValidMenu.Id, result.Id);
            Assert.Equal(ValidMenu.MenuName, result.MenuName);
            Assert.Equal(ValidMenu.MenuInfo, result.MenuInfo);
        }


        #endregion


        #region Dish
        [Fact]
        public async Task GetDish_WhenItExists_ShouldReturnSingle()
        {
            // Arrange
            var responseDish = new DishResponse
            {
                Id = Guid.NewGuid(),
                DishName = "hamburger",
                Fats = 1,
                Amount = 1,
                CaloricContent = 1,
                Carbohydrates = 1,
                Category = "some category",
                Currency = "UAH",
                DishComposition = new List<string>() { "first ingredient", "second ingredient" },
                MenuID = Guid.NewGuid(),
                PreparationTime = TimeSpan.FromMinutes(10).ToString(),
                Protein = 1
            };

            var testedClient = PrepareClientForOkResult(responseDish);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            var result = await testedClient.GetDishByIdAsync(Guid.NewGuid());

            // Assert

            Assert.NotNull(result);
            Assert.Equal(responseDish.DishName, result.DishName);
            Assert.Equal(responseDish.Category, result.Category);

        }



        [Fact]
        public async Task CreateDish_ShouldReturnCreated_WhenIsValid()
        {
            // Arrange
            Guid generatedId = Guid.NewGuid();
            var ValidDish = new DishResponse
            {
                Id = generatedId,
                DishName = "hamburger",
                Fats = 1,
                Amount = 1,
                CaloricContent = 1,
                Carbohydrates = 1,
                Category = "some category",
                Currency = "UAH",
                DishComposition = new List<string>() { "first ingredient", "second ingredient" },
                MenuID = Guid.NewGuid(),
                PreparationTime = TimeSpan.FromMinutes(10).ToString(),
                Protein = 1
            };
            var testedClient = PrepareClientForOkResult(ValidDish);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            var request = new DishRequest()
            {
                DishId = generatedId,
                DishName = "salad",
                Fats = 1,
                Amount = 1,
                CaloricContent = 1,
                Carbohydrates = 1,
                Category = "some category",
                Currency = "UAH",
                DishComposition = new List<string>() { "first ingredient", "second ingredient" },
                MenuID = Guid.NewGuid(),
                PreparationTime = TimeSpan.FromMinutes(10).ToString(),
                Protein = 1
            };
            var result = await testedClient.CreateDishAsync(request);

            // Assert

            Assert.NotNull(result);
            Assert.Equal(ValidDish.Id, result.Id);
            Assert.Equal(ValidDish.DishName, result.DishName);
            Assert.Equal(ValidDish.Category, result.Category);
        }

        [Fact]
        public async Task UpdateDish_WhenItExists_ShouldReturnUpdated_WhenUsingValidData()
        {
            // Arrange
            Guid idForUpdate = Guid.NewGuid();
            var ValidDish = new DishResponse
            {
                Id = idForUpdate,
                DishName = "salad",
                Fats = 1,
                Amount = 1,
                CaloricContent = 1,
                Carbohydrates = 1,
                Category = "some category",
                Currency = "UAH",
                DishComposition = new List<string>() { "first ingredient", "second ingredient" },
                MenuID = Guid.NewGuid(),
                PreparationTime = TimeSpan.FromMinutes(10).ToString(),
                Protein = 1
            };
            var testedClient = PrepareClientForOkResult(ValidDish);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            var bodyForUpdate = new DishRequest()
            {
                DishName = "hamburger",
                Fats = 1,
                Amount = 1,
                CaloricContent = 1,
                Carbohydrates = 1,
                Category = "some category",
                Currency = "UAH",
                DishComposition = new List<string>() { "first ingredient", "second ingredient" },
                MenuID = Guid.NewGuid(),
                PreparationTime = TimeSpan.FromMinutes(10).ToString(),
                Protein = 1
            };

            var result = await testedClient.UpdateDishAsync(idForUpdate, bodyForUpdate);

            // Assert

            Assert.NotNull(result);
            Assert.Equal(ValidDish.Id, result.Id);
            Assert.Equal(ValidDish.DishName, result.DishName);
            Assert.Equal(ValidDish.Category, result.Category);
        }
        [Fact]
        public async Task CreateDish_CausesBadRequest_WnenNotValid()
        {
            // Arrange
            var testedClient = PrepareClientForBadResult();
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            var request = new DishRequest()
            {
                DishName = "salad",
                Fats = 1,
                Amount = 1,
                CaloricContent = 1,
                Carbohydrates = 1,
                Category = "some category",
                Currency = "UAH",
                DishComposition = new List<string>() { "first ingredient", "second ingredient" },
                MenuID = Guid.NewGuid(),
                PreparationTime = TimeSpan.FromMinutes(10).ToString(),
                Protein = 1
            };
            var thrownException = await Assert.ThrowsAsync<ClientErrorHttpResponseException>(
                () => testedClient.CreateDishAsync(request)
            );
            Assert.Equal(HttpStatusCode.BadRequest, thrownException.StatusCode);
        }

        #endregion
        #region marketplace
        [Fact]
        public async Task GetAlloffers_ShouldReturnReadOnlyCollection()
        {
            // Arrange
            var responseCollection = new GetAllMenusResponse(
                new List<MenuResponse>
                {new MenuResponse()
            {
                Id = Guid.NewGuid(),
                MenuName = "Salads",
                MenuInfo = "some info",
                ProviderCompanyId = Guid.NewGuid(),
                DishList = new List<DishResponse>(),
            },new MenuResponse()
            {
                Id = Guid.NewGuid(),
                MenuName = "Salads",
                MenuInfo = "some info",
                ProviderCompanyId = Guid.NewGuid(),
                DishList = new List<DishResponse>(),
            },new MenuResponse()
            {
                Id = Guid.NewGuid(),
                MenuName = "Salads",
                MenuInfo = "some info",
                ProviderCompanyId = Guid.NewGuid(),
                DishList = new List<DishResponse>(),
            }
        });

            var testedClient = PrepareClientForOkResult(responseCollection);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            var result = await testedClient.GetOffersAsync();

            // Assert
            var response = Assert.IsType<GetAllMenusResponse>(result);
            Assert.NotNull(response);
            Assert.Equal(responseCollection.MenusCollection.Count, response.MenusCollection.Count);
        }

        #endregion
    }
}
