﻿using Company.API.Contracts.Request;
using Company.API.Contracts.Response;
using Infrastructure.HttpClientAbstractions;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Company.API.Client.Tests
{
    public class CompanyClientTests
    {
        private const string TestUrl = "https://localhost:5000/";
        private const string BadRequestMessage = "something bad happened";

        /// <summary>
        /// Injects into CompanyClient HttpClient with mocked HttpMessageHandler that returns OkResult with responseObject.
        /// </summary>
        /// <param name="responseObject"></param>
        /// <returns>Prepared CompanyClient object.</returns>
        private CompanyClient PrepareClientForOkResult(object responseObject)
        {
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            var mockedResponse = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(JsonConvert.SerializeObject(responseObject), Encoding.UTF8, "application/json")
            };
            handlerMock.Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(mockedResponse);

            var httpClient = new HttpClient(handlerMock.Object);
            return new CompanyClient(httpClient);
        }

        /// <summary>
        /// Injects into CompanyClient HttpClient with mocked HttpMessageHandler that returns BadRequestResult.
        /// </summary>
        /// <param name="responseObject"></param>
        /// <returns>Prepared CompanyClient object.</returns>
        private CompanyClient PrepareClientForBadRequestResult()
        {
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            var mockedResponse = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.BadRequest,
                Content = new StringContent(BadRequestMessage)
            };
            handlerMock.Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(mockedResponse);

            var httpClient = new HttpClient(handlerMock.Object);
            return new CompanyClient(httpClient);
        }

        #region Company tests
        [Fact()]
        public async Task GetCompanies_ShouldReturnCollection()
        {
            // Arrange
            var responseCollection = new GetCompaniesResponse(
                new List<CompanyResponse>
                {
                    new CompanyResponse { Name = "IBM" },
                    new CompanyResponse { Name = "Apple" },
                    new CompanyResponse { Name = "HP" }
                });
            var testedClient = PrepareClientForOkResult(responseCollection);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            var actualResponse = await testedClient.GetCompaniesAsync();

            // Assert
            Assert.NotNull(actualResponse);
            Assert.NotNull(actualResponse.CompaniesCollection);
            Assert.Equal(responseCollection.CompaniesCollection.Count, actualResponse.CompaniesCollection.Count);
        }

        [Fact()]
        public async Task GetCompany_WhenItExists_ShouldReturnSingle()
        {
            // Arrange
            var responseCompany = new CompanyResponse
            {
                Name = "Apple",
                Description = "Think different"
            };
            var testedClient = PrepareClientForOkResult(responseCompany);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            var actualResponse = await testedClient.GetCompanyAsync(Guid.NewGuid());

            // Assert
            Assert.NotNull(actualResponse);
            Assert.Equal(responseCompany.Name, actualResponse.Name);
            Assert.Equal(responseCompany.Description, actualResponse.Description);
        }

        [Fact()]
        public async Task GetCompany_WhenItNotExists_ThrowsException()
        {
            // Arrange
            var testedClient = PrepareClientForBadRequestResult();
            testedClient.BaseUri = new Uri(TestUrl);

            //Act & Assert
            var thrownException = await Assert.ThrowsAsync<ClientErrorHttpResponseException>(
                () => testedClient.GetCompanyAsync(Guid.NewGuid())
            );
            Assert.Equal(HttpStatusCode.BadRequest, thrownException.StatusCode);
        }

        [Fact()]
        public async Task CreateValidCompany_ShouldReturnCreated()
        {
            // Arrange
            Guid generatedId = Guid.NewGuid();
            var createdCompany = new CompanyResponse
            {
                CompanyId = generatedId,
                Name = "Apple",
                Description = "Think different"
            };
            var testedClient = PrepareClientForOkResult(createdCompany);
            testedClient.BaseUri = new Uri(TestUrl);
            
            // Act
            var request = new CompanyRequest()
            {
                Name = "Apple",
                Description = "Think Different"
            };
            var actualResponse = await testedClient.CreateCompanyAsync(request);

            // Assert
            Assert.NotNull(actualResponse);
            Assert.Equal(createdCompany.CompanyId, actualResponse.CompanyId);
            Assert.Equal(createdCompany.Name, actualResponse.Name);
            Assert.Equal(createdCompany.Description, actualResponse.Description);
        }

        [Fact()]
        public async Task CreateInvalidCompany_ThrowsException()
        {
            // Arrange
            var testedClient = PrepareClientForBadRequestResult();
            testedClient.BaseUri = new Uri(TestUrl);

            // Act & Assert
            var thrownException = await Assert.ThrowsAsync<ClientErrorHttpResponseException>(
                () => testedClient.GetCompanyAsync(Guid.NewGuid())
            );
            Assert.Equal(HttpStatusCode.BadRequest, thrownException.StatusCode);
        }

        [Fact()]
        public async Task UpdateCompanyByValidData_WhenItExists_ShouldReturnUpdated()
        {
            // Arrange
            Guid idForUpdate = Guid.NewGuid();
            var updatedCompany = new CompanyResponse
            {
                CompanyId = idForUpdate,
                Name = "Apple",
                Description = "Think different"
            };
            var testedClient = PrepareClientForOkResult(updatedCompany);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            var bodyForUpdate = new CompanyRequest()
            {
                Name = "Nike",
                Description = "Just Do It"
            };
            var actualResponse = await testedClient.UpdateCompanyAsync(idForUpdate, bodyForUpdate);

            // Assert
            Assert.NotNull(actualResponse);
            Assert.Equal(updatedCompany.CompanyId, actualResponse.CompanyId);
            Assert.Equal(updatedCompany.Name, actualResponse.Name);
            Assert.Equal(updatedCompany.Description, actualResponse.Description);
        }

        [Fact()]
        public async Task UpdateCompany_ByInvalidDataOrWhenItNotExists_ThrowsException()
        {
            // Arrange
            var testedClient = PrepareClientForBadRequestResult();
            testedClient.BaseUri = new Uri(TestUrl);

            // Act & Assert
            var request = new CompanyRequest()
            {
                Name = string.Empty,
                Description = "Think Different"
            };
            var thrownException = await Assert.ThrowsAsync<ClientErrorHttpResponseException>(
                () => testedClient.UpdateCompanyAsync(Guid.NewGuid(), request)
            );
            Assert.Equal(HttpStatusCode.BadRequest, thrownException.StatusCode);
        }

        [Fact()]
        public async Task DeleteCompany_WhenItExists_ShouldReturnSingle()
        {
            // Arrange
            Guid requestedId = Guid.NewGuid();
            var removedCompanyResponse = new DeleteCompanyResponse
            {
                CompanyId = requestedId
            };
            var testedClient = PrepareClientForOkResult(removedCompanyResponse);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            var actualResponse = await testedClient.DeleteCompanyAsync(requestedId);

            // Assert
            Assert.NotNull(actualResponse);
            Assert.Equal(removedCompanyResponse.CompanyId, actualResponse.CompanyId);
        }

        [Fact()]
        public async Task DeleteCompany_WhenItNotExists_ThrowsException()
        {
            // Arrange
            var testedClient = PrepareClientForBadRequestResult();
            testedClient.BaseUri = new Uri(TestUrl);

            // Act & Assert
            var thrownException = await Assert.ThrowsAsync<ClientErrorHttpResponseException>(
                () => testedClient.DeleteCompanyAsync(Guid.NewGuid())
            );
            Assert.Equal(HttpStatusCode.BadRequest, thrownException.StatusCode);
        }
        #endregion

        #region Contract tests
        [Fact()]
        public async Task GetContracts_ShouldReturnCollection()
        {
            // Arrange
            var responseCollection = new GetContractsResponse(
                new List<ContractResponse>
                {
                        new ContractResponse { ContractNumber = "1A" },
                        new ContractResponse { ContractNumber = "2A" },
                        new ContractResponse { ContractNumber = "3A" }
                });
            var testedClient = PrepareClientForOkResult(responseCollection);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            var actualResponse = await testedClient.GetContractsAsync();

            // Assert
            Assert.NotNull(actualResponse);
            Assert.NotNull(actualResponse.ContractResponses);
            Assert.Equal(responseCollection.ContractResponses.Count, actualResponse.ContractResponses.Count);
        }

        [Fact()]
        public async Task GetContract_WhenItExists_ShouldReturnSingle()
        {
            // Arrange
            var responseContract = new ContractResponse
            {
                ContractNumber = "1A",
                ConsumerCompanyId = Guid.NewGuid(),
                ProviderCompanyId = Guid.NewGuid(),
                MenuId = Guid.NewGuid(),
                ContractStatus = "Active"
            };
            var testedClient = PrepareClientForOkResult(responseContract);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            var actualResponse = await testedClient.GetContractByIdAsync(Guid.NewGuid());

            // Assert
            Assert.NotNull(actualResponse);
            Assert.Equal(responseContract.ContractNumber, actualResponse.ContractNumber);
            Assert.Equal(responseContract.ContractStatus, actualResponse.ContractStatus);
            Assert.Equal(responseContract.ConsumerCompanyId, actualResponse.ConsumerCompanyId);
            Assert.Equal(responseContract.ProviderCompanyId, actualResponse.ProviderCompanyId);
            Assert.Equal(responseContract.MenuId, actualResponse.MenuId);
        }

        [Fact()]
        public async Task GetContract_WhenItNotExists_ThrowsException()
        {
            // Arrange
            var testedClient = PrepareClientForBadRequestResult();
            testedClient.BaseUri = new Uri(TestUrl);

            //Act & Assert
            var thrownException = await Assert.ThrowsAsync<ClientErrorHttpResponseException>(
                () => testedClient.GetContractByIdAsync(Guid.NewGuid())
            );
            Assert.Equal(HttpStatusCode.BadRequest, thrownException.StatusCode);
        }

        [Fact()]
        public async Task GetContractsByCompanyTypeAndId_WhenItExists_ShouldReturnCollection()
        {
            // Arrange
            Guid firstConsumerId = Guid.NewGuid();
            Guid secondConsumerId = Guid.NewGuid();
            Guid providerId = Guid.NewGuid();
            var testContractsCollection = new GetContractsResponse(
                new List<ContractResponse>
                {
                    new ContractResponse {ContractNumber = "101", ConsumerCompanyId = firstConsumerId, 
                        ProviderCompanyId = providerId},
                    new ContractResponse {ContractNumber = "202", ConsumerCompanyId = secondConsumerId, 
                        ProviderCompanyId = providerId}
                });
            var testClient = PrepareClientForOkResult(testContractsCollection);
            testClient.BaseUri = new Uri(TestUrl);
            
            // Act
            var actualResponse = await testClient.GetContractsByCompanyTypeAndIdAsync(providerId, "Provider");
            
            // Assert
            Assert.NotNull(actualResponse);
            Assert.NotNull(actualResponse.ContractResponses);
            Assert.Equal(testContractsCollection.ContractResponses.Count, actualResponse.ContractResponses.Count);
        }

        [Fact()]
        public async Task GetContractsByCompanyTypeAndId_WhenItDoesNotExist_ThrowsException()
        {
            // Arrange
            var testClient = PrepareClientForBadRequestResult();
            testClient.BaseUri = new Uri(TestUrl);
            
            // Act & Assert
            var thrownException = await Assert.ThrowsAsync<ClientErrorHttpResponseException>(
                () => testClient.GetContractsByCompanyTypeAndIdAsync(Guid.NewGuid(), "Provider"));
            Assert.Equal(HttpStatusCode.BadRequest, thrownException.StatusCode);
        }

        [Fact()]
        public async Task GetContractsByStatusAndCompanyTypeAndId()
        {
            // Arrange
            Guid firstConsumerId = Guid.NewGuid();
            Guid secondConsumerId = Guid.NewGuid();
            Guid providerId = Guid.NewGuid();
            string testContractStatus = "Changed";
            var testContractsCollection = new GetContractsResponse(
                new List<ContractResponse>
                {
                    new ContractResponse {
                        ContractNumber = "101", 
                        ConsumerCompanyId = firstConsumerId, 
                        ProviderCompanyId = providerId, 
                        ContractStatus = testContractStatus},
                    new ContractResponse {
                        ContractNumber = "202", 
                        ConsumerCompanyId = secondConsumerId, 
                        ProviderCompanyId = providerId, 
                        ContractStatus = testContractStatus}
                });
            var testClient = PrepareClientForOkResult(testContractsCollection);
            testClient.BaseUri = new Uri(TestUrl);
            
            // Act
            var actualResponse = await testClient.GetContractsByStatusAndCompanyTypeAndIdAsync
                    (providerId, "Provider", testContractStatus);

            // Assert
            Assert.NotNull(actualResponse);
            Assert.NotNull(actualResponse.ContractResponses);
            Assert.Equal(testContractsCollection.ContractResponses.Count, actualResponse.ContractResponses.Count);
        }

        [Fact()]
        public async Task CreateValidContract_ShouldReturnCreated()
        {
            // Arrange
            Guid generatedId = Guid.NewGuid();
            Guid providerCompanyId = Guid.NewGuid();
            Guid consumerCompanyId = Guid.NewGuid();
            Guid menuId = Guid.NewGuid();

            var createdContract = new ContractResponse
            {
                Id = generatedId,
                ContractNumber = "1A",
                ConsumerCompanyId = consumerCompanyId,
                ProviderCompanyId = providerCompanyId,
                MenuId = menuId,
                ContractStatus = "Active"
            };
            var testedClient = PrepareClientForOkResult(createdContract);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            var request = new ContractRequest()
            {
                ContractNumber = "1A",
                ConsumerCompanyId = consumerCompanyId,
                ProviderCompanyId = providerCompanyId,
                MenuId = menuId,
                ContractStatus = "Active"
            };
            var actualResponse = await testedClient.CreateContractAsync(request);

            // Assert
            Assert.NotNull(actualResponse);
            Assert.Equal(createdContract.Id, actualResponse.Id);
            Assert.Equal(createdContract.ContractNumber, actualResponse.ContractNumber);
            Assert.Equal(createdContract.ConsumerCompanyId, actualResponse.ConsumerCompanyId);
            Assert.Equal(createdContract.ProviderCompanyId, actualResponse.ProviderCompanyId);
            Assert.Equal(createdContract.MenuId, actualResponse.MenuId);
            Assert.Equal(createdContract.ContractStatus, actualResponse.ContractStatus);
        }

        [Fact()]
        public async Task CreateInvalidContract_ThrowsException()
        {
            // Arrange
            Guid testConsumerId = Guid.NewGuid();
            Guid testProviderId = Guid.NewGuid();
            var testClient = PrepareClientForBadRequestResult();
            testClient.BaseUri = new Uri(TestUrl);
            
            // Act & Arrange
            var testRequest = new ContractRequest
            {
                ContractNumber = "505",
                ConsumerCompanyId = testConsumerId,
                ProviderCompanyId = testProviderId,
                ContractStatus = "Active"
            };

            var thrownException = await Assert.ThrowsAsync<ClientErrorHttpResponseException>(
                () => testClient.CreateContractAsync(testRequest));
            Assert.Equal(HttpStatusCode.BadRequest, thrownException.StatusCode);
        }

        [Fact()]
        public async Task UpdateContractByValidData_WhenItExists_ShouldReturnUpdated()
        {
            // Arrange
            Guid idForUpdate = Guid.NewGuid();
            var updatedContract = new ContractResponse
            {
                Id = idForUpdate,
                ContractNumber = "1A",
                ConsumerCompanyId = Guid.NewGuid(),
                ProviderCompanyId = Guid.NewGuid(),
                MenuId = Guid.NewGuid(),
                ContractStatus = "Active"
            };
            var testedClient = PrepareClientForOkResult(updatedContract);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            var bodyForUpdate = new ContractRequest()
            {
                ContractStatus = "Changed"
            };
            var actualResponse = await testedClient.UpdateContractAsync(idForUpdate, bodyForUpdate);

            // Assert
            Assert.NotNull(actualResponse);
            Assert.Equal(updatedContract.Id, actualResponse.Id);
            Assert.Equal(updatedContract.ContractStatus, actualResponse.ContractStatus);
        }

        [Fact()]
        public async Task UpdateContract_WhenItDoesNotExist_ThrowsException()
        {
            // Arrange
            var testConsumerId = Guid.NewGuid();
            var testProviderId = Guid.NewGuid();
            var testClient = PrepareClientForBadRequestResult();
            testClient.BaseUri = new Uri(TestUrl);
            
            // Act & Assert
            var testRequest = new ContractRequest
            {
                ContractNumber = "505",
                ConsumerCompanyId = testConsumerId,
                ProviderCompanyId = testProviderId,
                ContractStatus = "Active"
            };
            var thrownException = await Assert.ThrowsAsync<ClientErrorHttpResponseException>(
                () => testClient.UpdateContractAsync(Guid.NewGuid(), testRequest));
            Assert.Equal(HttpStatusCode.BadRequest, thrownException.StatusCode);
        }
        #endregion
    }
}
