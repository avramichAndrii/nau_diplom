using Company.API.Contracts.Request;
using Company.API.Validation;
using System.Collections.Generic;
using Xunit;

namespace Company.API.Tests.Validation
{
    public class CompanyRequestValidationTests
    {
        const int TooLongStringLength = 1501;
        [Theory, MemberData(nameof(TestCases))]
        public void TestCompanyRequestValidation(CompanyRequest request, bool expectedResult)
        {
            // Arrange
            var requestValidator = new CompanyRequestValidator();
            // Act
            bool actualResult = requestValidator.Validate(request).IsValid;
            // Assert
            Assert.Equal(expectedResult, actualResult);
        }

        public static IEnumerable<object[]> TestCases
        {
            get
            {
                yield return new object[]
                {
                    new CompanyRequest()
                    {
                        Name = "Apple",
                        Description = "Think Different"
                    }, true
                };
                yield return new object[]
                {
                    new CompanyRequest()
                    {
                        Name = null,
                        Description = "Think Different"
                    }, false
                };
                yield return new object[]
                {
                    new CompanyRequest()
                    {
                        Name = "",
                        Description = "Think Different"
                    }, false
                };
                yield return new object[]
                {
                    new CompanyRequest()
                    {
                        Name = new string('z', TooLongStringLength),
                        Description = "Think Different"
                    }, false
                };
                yield return new object[]
                {
                    new CompanyRequest()
                    {
                        Name = "Apple",
                        Description = new string('z', TooLongStringLength)
                    }, false
                };
            }
        }
    }
}
