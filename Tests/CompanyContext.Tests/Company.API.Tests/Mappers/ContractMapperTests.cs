using Company.API.Contracts.Request;
using Company.API.Mappers;
using Company.Domain;
using System;
using System.Collections.Generic;
using Xunit;

namespace Company.API.Tests.Mappers
{
    public class ContractMapperTests
    {
        [Theory]
        [InlineData("571", "3c76f0be-5d7a-47a7-b4c3-2e961bf618cd", "993031ba-cc10-4c33-bfb1-215f535471e2", "cb012d83-588d-4177-9f6f-6f356351fda0", "Active")]
        [InlineData("", "3c76f0be-5d7a-47a7-b4c3-2e961bf618cd", "993031ba-cc10-4c33-bfb1-215f535471e2", "cb012d83-588d-4177-9f6f-6f356351fda0", "Active")]
        [InlineData("571", "00000000-0000-0000-0000-000000000000", "993031ba-cc10-4c33-bfb1-215f535471e2", "cb012d83-588d-4177-9f6f-6f356351fda0", "Active")]
        [InlineData("571", "3c76f0be-5d7a-47a7-b4c3-2e961bf618cd", "00000000-0000-0000-0000-000000000000", "cb012d83-588d-4177-9f6f-6f356351fda0", "Active")]
        [InlineData("571", "3c76f0be-5d7a-47a7-b4c3-2e961bf618cd", "993031ba-cc10-4c33-bfb1-215f535471e2", "00000000-0000-0000-0000-000000000000", "Active")]
        public void TestMappingFromRequestToModel(string contractNumber,
            Guid providerCompanyId, Guid consumerCompanyId, Guid menuId, string contractStatus)
        {
            //Arrange
            var request = new ContractRequest()
            {
                ContractNumber = contractNumber,
                ProviderCompanyId = providerCompanyId,
                ConsumerCompanyId = consumerCompanyId,
                MenuId = menuId,
                ContractStatus = contractStatus
            };

            //Act
            var model = request.MapFromRequest();

            //Assert
            Assert.Equal(request.ContractNumber, model.ContractNumber);
            Assert.Equal(request.ProviderCompanyId, model.ProviderCompanyId);
            Assert.Equal(request.ConsumerCompanyId, model.ConsumerCompanyId);
            Assert.Equal(request.ContractNumber, model.ContractNumber);
            Assert.Equal(request.MenuId, model.MenuId);
            Assert.Equal(request.ContractStatus, model.ContractStatus.ToString());
        }

        public static IEnumerable<object[]> TestData
        {
            get
            {
                yield return new object[] { "562", Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), "Active" };
                yield return new object[] { "", Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), "Active" };
                yield return new object[] { "562", null, Guid.NewGuid(), Guid.NewGuid(), "Active" };
                yield return new object[] { "562", Guid.NewGuid(), null, Guid.NewGuid(), "Active" };
                yield return new object[] { "562", Guid.NewGuid(), Guid.NewGuid(), null, "Active" };

            }
        }

        [Theory, MemberData(nameof(TestData))]
        public void TestMappingFromModelToResponse(string contractNumber,
            Guid providerCompanyId, Guid consumerCompanyId, Guid menuId, string contractStatus)
        {
            //Arrange
            var model = new ContractModel()
            {
                ContractNumber = contractNumber,
                ProviderCompanyId = providerCompanyId,
                ConsumerCompanyId = consumerCompanyId,
                MenuId = menuId,
                ContractStatus = Enum.Parse<ContractStatus>(contractStatus)
            };

            //Act
            var result = model.MapToResponse();

            //Assert
            Assert.Equal(model.ContractNumber, result.ContractNumber);
            Assert.Equal(model.ProviderCompanyId, result.ProviderCompanyId);
            Assert.Equal(model.ConsumerCompanyId, result.ConsumerCompanyId);
            Assert.Equal(model.MenuId, result.MenuId);
            Assert.Equal(model.ContractStatus.ToString(), result.ContractStatus);
        }

    }
}
