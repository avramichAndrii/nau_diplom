using Company.API.Contracts.Request;
using Company.API.Mappers;
using Company.Domain;
using System;
using System.Collections.Generic;
using Xunit;

namespace Company.API.Tests.Mappers
{
    public class CompanyMapperTests
    {
        [Theory]
        [InlineData("Apple", "Think Different", "Consumer")]
        [InlineData(null, "Think Different", "Consumer")]
        [InlineData("Apple", "", "Consumer")]
        public void TestMappingToModelFromRequest(string name, string description, string typeOfCompany)
        {
            // Arrange
            var request = new CompanyRequest()
            {
                Name = name,
                Description = description,
                TypeOfCompany = typeOfCompany
            };
            // Act
            var model = request.MapFromRequest();
            // Assert
            Assert.Equal(request.Name, model.Name);
            Assert.Equal(request.Description, model.Description);
            Assert.Equal(request.TypeOfCompany, model.TypeOfCompany.ToString());
        }

        [Theory, MemberData(nameof(TestCases))]
        public void TestMappingFromModelToResponse(Guid id, string name, string description, string typeOfCompany)
        {
            // Arrange
            var model = new CompanyModel(id)
            {
                Name = name,
                Description = description,
                TypeOfCompany = Enum.Parse<CompanyType>(typeOfCompany)
            };
            // Act
            var response = model.MapToResponse();
            // Assert
            Assert.Equal(model.Id, response.CompanyId);
            Assert.Equal(model.Name, response.Name);
            Assert.Equal(model.Description, response.Description);
            Assert.Equal(model.TypeOfCompany.ToString(), response.TypeOfCompany);
        }

        public static IEnumerable<object[]> TestCases
        {
            get
            {
                yield return new object[] { Guid.NewGuid(), "Apple", "Think Different", "Consumer" };
                yield return new object[] { Guid.NewGuid(), "", "Think Different", "Consumer" };
                yield return new object[] { Guid.NewGuid(), "Apple", null, "Consumer" };
            }
        }
    }
}
