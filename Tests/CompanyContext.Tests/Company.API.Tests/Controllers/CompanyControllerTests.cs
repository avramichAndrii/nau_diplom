using Company.API.Contracts.Request;
using Company.API.Contracts.Response;
using Company.API.Controllers;
using Company.Domain;
using Company.Domain.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xunit;

namespace Company.API.Tests.Controllers
{
    public class CompanyControllerTests
    {
        [Fact]
        public async Task GetAllCompanies_WhenCollectionIsEmpty()
        {
            // Arrange
            var mockService = new Mock<ICompanyDomainServices>();
            mockService.Setup(ms => ms.GetAllAsync())
                .ReturnsAsync(new List<CompanyModel>());
            var controller = new CompanyController(mockService.Object);

            // Act
            var result = await controller.GetAllCompanies();

            // Assert
            var list = Assert.IsType<GetCompaniesResponse>(result);
            Assert.Empty(list.CompaniesCollection);
        }

        [Fact]
        public async Task GetCompaniesCount_WhenCollectionIsNotEmpty_ShouldReturn3()
        {
            // Arrange
            var mockService = new Mock<ICompanyDomainServices>();
            var testModelsCollection = new ReadOnlyCollection<CompanyModel>(
                new List<CompanyModel>
                {
                    new CompanyModel { Name = "IBM" },
                    new CompanyModel { Name = "Apple" },
                    new CompanyModel { Name = "HP" }
                });
            mockService.Setup(ms => ms.GetAllAsync())
                .ReturnsAsync(testModelsCollection);
            var controller = new CompanyController(mockService.Object);

            // Act
            var result = await controller.GetAllCompanies();

            // Assert
            var list = Assert.IsType<GetCompaniesResponse>(result);
            Assert.Equal(testModelsCollection.Count, list.CompaniesCollection.Count);
        }

        [Fact]
        public async Task GetSingleCompany_WhenItExists()
        {
            // Arrange
            var mockService = new Mock<ICompanyDomainServices>();

            Guid requestedId = Guid.NewGuid();
            mockService.Setup(ms => ms.GetByIdAsync(requestedId)).ReturnsAsync(
                new CompanyModel(requestedId)
                {
                    Name = "Apple",
                    Description = "Think Different"
                }
            );

            var controller = new CompanyController(mockService.Object);

            // Act
            var result = await controller.GetCompany(requestedId);

            // Assert
            var response = Assert.IsType<CompanyResponse>(result.Value);
            Assert.Equal(requestedId, response.CompanyId);
        }

        [Fact]
        public async Task GetSingleCompany_WhenItDoesNotExist_CausesBadRequest()
        {
            // Arrange
            var mockService = new Mock<ICompanyDomainServices>();

            Guid requestedId = Guid.NewGuid();
            mockService.Setup(ms => ms.GetByIdAsync(requestedId)).Throws<CompanyNotFoundDomainException>();

            var controller = new CompanyController(mockService.Object);

            // Act
            var result = await controller.GetCompany(requestedId);

            // Assert
            var objectResult = Assert.IsType<BadRequestObjectResult>(result.Result);
        }

        [Fact]
        public async Task CreateValidCompany()
        {
            // Arrange
            var mockService = new Mock<ICompanyDomainServices>();

            Guid generatedId = Guid.NewGuid();
            var modelToCreate = new CompanyModel(generatedId)
            {
                Name = "Apple",
                Description = "Think Different",
                TypeOfCompany = CompanyType.Provider
            };
            mockService.Setup(ms => ms.CreateAsync(It.IsAny<CompanyModel>()))
                .ReturnsAsync(() => modelToCreate);

            var controller = new CompanyController(mockService.Object);

            // Act
            var request = new CompanyRequest()
            {
                Name = "Apple",
                Description = "Think Different",
                TypeOfCompany = CompanyType.Provider.ToString()
            };
            var result = await controller.CreateCompany(request);

            // Assert
            var response = Assert.IsType<CompanyResponse>(result.Value);
            Assert.Equal(generatedId, response.CompanyId);
        }

        [Fact]
        public async Task UpdateCompany_WhenItExists()
        {
            // Arrange
            var mockService = new Mock<ICompanyDomainServices>();

            Guid requestedId = Guid.NewGuid();
            mockService.Setup(ms => ms.UpdateAsync(requestedId, It.IsAny<CompanyModel>()))
                .ReturnsAsync(() => new CompanyModel(requestedId)
                {
                    Name = "Apple",
                    Description = "Think Different",
                    TypeOfCompany = CompanyType.Provider
                });
            var controller = new CompanyController(mockService.Object);

            // Act
            var request = new CompanyRequest()
            {
                Name = "Apple",
                Description = "Think Different",
                TypeOfCompany = CompanyType.Provider.ToString()
            };
            var result = await controller.UpdateCompany(requestedId, request);

            // Assert
            var response = Assert.IsType<CompanyResponse>(result.Value);
            Assert.Equal(requestedId, response.CompanyId);
            Assert.Equal(request.Name, response.Name);
            Assert.Equal(request.Description, response.Description);
        }

        [Fact]
        public async Task UpdateCompany_WhenItDoesNotExist_CausesBadRequest()
        {
            // Arrange
            var mockService = new Mock<ICompanyDomainServices>();

            Guid requestedId = Guid.NewGuid();
            mockService.Setup(ms => ms.UpdateAsync(requestedId, It.IsAny<CompanyModel>()))
                .Throws<CompanyNotFoundDomainException>();
            var controller = new CompanyController(mockService.Object);

            // Act
            var request = new CompanyRequest()
            {
                Name = "Apple",
                Description = "Think Different",
                TypeOfCompany = CompanyType.Provider.ToString()
            };
            var result = await controller.UpdateCompany(requestedId, request);

            // Assert
            var objectResult = Assert.IsType<BadRequestObjectResult>(result.Result);
        }

        [Fact]
        public async Task RemoveCompany_WhenItExists()
        {
            // Arrange
            var mockService = new Mock<ICompanyDomainServices>();

            Guid requestedId = Guid.NewGuid();
            mockService.Setup(ms => ms.DeleteAsync(requestedId)).ReturnsAsync(
                new CompanyModel(requestedId)
                {
                    Name = "Apple",
                    Description = "Think Different"
                }
            );

            var controller = new CompanyController(mockService.Object);

            // Act
            var result = await controller.RemoveCompany(requestedId);

            // Assert
            var response = Assert.IsType<DeleteCompanyResponse>(result.Value);
            Assert.Equal(requestedId, response.CompanyId);
        }

        [Fact]
        public async Task RemoveCompany_WhenItDoesNotExist_CausesBadRequest()
        {
            // Arrange
            var mockService = new Mock<ICompanyDomainServices>();

            Guid requestedId = Guid.NewGuid();
            mockService.Setup(ms => ms.DeleteAsync(requestedId)).Throws<CompanyNotFoundDomainException>();

            var controller = new CompanyController(mockService.Object);

            // Act
            var result = await controller.RemoveCompany(requestedId);

            // Assert
            var objectResult = Assert.IsType<BadRequestObjectResult>(result.Result);
        }
    }
}

