using Company.API.Contracts.Request;
using Company.API.Contracts.Response;
using Company.API.Controllers;
using Company.Domain;
using Company.Domain.Exceptions;
using EmailNotifier.Application.EmailNotificationService.Interfaces;
using EmailNotifier.EmailNotification;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xunit;

namespace Company.API.Tests.Controllers
{
    public class ContractControllerTests
    {
        [Fact]
        public async Task GetAllContracts_WhenCollectionIsNotEmpty_ShouldReturn4()
        {
            //Arrange
            var testData = new ReadOnlyCollection<ContractModel>(
                new List<ContractModel>
                {
                    new ContractModel{ ContractNumber = "541"},
                    new ContractModel{ ContractNumber = "874"},
                    new ContractModel{ ContractNumber = "985"},
                    new ContractModel{ ContractNumber = "235"}
                });

            var mockService = new Mock<IContractDomainService>();
            mockService.Setup(ms => ms.GetAllContractsAsync())
                .ReturnsAsync(testData);
            var mockMailSender = new Mock<IMailSender>();
            var mockNotificationService = new Mock<ICompanyEmailNotificationService>();
            var controller = new ContractController(mockService.Object, mockMailSender.Object, mockNotificationService.Object);

            //Act
            var check = await controller.GetAllContracts();

            //Assert
            var result = Assert.IsType<GetContractsResponse>(check);
            Assert.Equal(testData.Count, result.ContractResponses.Count);

        }

        [Fact]
        public async Task GetAllContracts_WhenCollectionIsEmpty()
        {
            //Arrange
            var mockService = new Mock<IContractDomainService>();
            mockService.Setup(ms => ms.GetAllContractsAsync())
                .ReturnsAsync(new List<ContractModel>());
            var mockMailSender = new Mock<IMailSender>();
            var mockNotificationService = new Mock<ICompanyEmailNotificationService>();
            var controller = new ContractController(mockService.Object, mockMailSender.Object, mockNotificationService.Object);

            //Act
            var check = await controller.GetAllContracts();

            //Assert
            var result = Assert.IsType<GetContractsResponse>(check);
            Assert.Empty(result.ContractResponses);

        }

        [Fact]
        public async Task GetContractById_WhenItExists()
        {
            //Arrange
            Guid testId = Guid.NewGuid();
            var mockService = new Mock<IContractDomainService>();
            mockService.Setup(ms => ms.GetContractByIdAsync(testId))
                .ReturnsAsync(
                    new ContractModel(testId)
                    {
                        ContractNumber = "563"
                    });
            var mockMailSender = new Mock<IMailSender>();
            var mockNotificationService = new Mock<ICompanyEmailNotificationService>();
            var controller = new ContractController(mockService.Object, mockMailSender.Object, mockNotificationService.Object);

            //Act
            var check = await controller.GetContractById(testId);

            //Assert
            var result = Assert.IsType<ContractResponse>(check.Value);
            Assert.Equal(testId, result.Id);
        }

        [Fact]
        public async Task GetContractById_WhenItNotExists()
        {
            //Assert
            Guid testData = Guid.NewGuid();
            var mockService = new Mock<IContractDomainService>();
            mockService.Setup(ms => ms.GetContractByIdAsync(testData))
                .Throws<ContractNotFoundDomainException>();
            var mockMailSender = new Mock<IMailSender>();
            var mockNotificationService = new Mock<ICompanyEmailNotificationService>();
            var controller = new ContractController(mockService.Object, mockMailSender.Object, mockNotificationService.Object);

            //Act
            var check = await controller.GetContractById(testData);

            //Assert
            var result = Assert.IsType<BadRequestObjectResult>(check.Result);

        }

        [Fact]
        public async Task GetAllContractsByCompanyTypeAndId_WhenItExists_ShouldReturn2()
        {
            //Arrange
            Guid firstConsumerId = Guid.NewGuid();
            Guid secondConsumerId = Guid.NewGuid();
            Guid supplierId = Guid.NewGuid();
            CompanyType testCompanyType = CompanyType.Provider;
            var testCompaniesCollection = new ReadOnlyCollection<CompanyModel>(
                new List<CompanyModel>
                {
                    new CompanyModel(firstConsumerId){Name = "Apple", TypeOfCompany = CompanyType.Consumer},
                    new CompanyModel(secondConsumerId){ Name = "Adidas", TypeOfCompany = CompanyType.Consumer},
                    new CompanyModel(supplierId){ Name = "PerfectFood", TypeOfCompany = CompanyType.Provider}
                });
            var testContracts = new ReadOnlyCollection<ContractModel>(
                new List<ContractModel>
                {
                    new ContractModel { ContractNumber = "574", ProviderCompanyId = supplierId,
                        ConsumerCompanyId = firstConsumerId},
                    new ContractModel{ ContractNumber = "123", ProviderCompanyId = supplierId,
                        ConsumerCompanyId = secondConsumerId}
                });
            var mockService = new Mock<IContractDomainService>();
            mockService.Setup(ms => ms.GetByCompanyTypeAndIdAsync(supplierId, testCompanyType))
                .ReturnsAsync(testContracts);
            var mockMailSender = new Mock<IMailSender>();
            var mockNotificationService = new Mock<ICompanyEmailNotificationService>();
            var controller = new ContractController(mockService.Object, mockMailSender.Object, mockNotificationService.Object);

            //Act
            var check = await controller.GetAllByCompanyTypeAndId(supplierId, testCompanyType.ToString());

            //Assert
            var result = Assert.IsType<GetContractsResponse>(check);
            Assert.Equal(testContracts.Count, result.ContractResponses.Count);
        }

        [Fact]
        public async Task GetAllContractsByCompanyTypeAndId_WhenItNotExists()
        {
            //Arrange
            Guid testId = Guid.NewGuid();
            var testCompanyType = CompanyType.Provider;
            var mockService = new Mock<IContractDomainService>();
            mockService.Setup(ms => ms.GetByCompanyTypeAndIdAsync(testId, testCompanyType))
                .ReturnsAsync(new List<ContractModel>());
            var mockMailSender = new Mock<IMailSender>();
            var mockNotificationService = new Mock<ICompanyEmailNotificationService>();
            var controller = new ContractController(mockService.Object, mockMailSender.Object, mockNotificationService.Object);

            //Act
            var check = await controller.GetAllByCompanyTypeAndId(testId, testCompanyType.ToString());

            //Assert
            var result = Assert.IsType<GetContractsResponse>(check);
            Assert.Empty(result.ContractResponses);
        }

        [Fact]
        public async Task GetAllContractsByStatusAndCompanyTypeAndId_WhenItExists_ShouldReturn2()
        {
            //Arrange
            Guid firstConsumerId = Guid.NewGuid();
            Guid secondConsumerId = Guid.NewGuid();
            Guid supplierId = Guid.NewGuid();
            ContractStatus testContractStatus = ContractStatus.Active;
            CompanyType testCompanyType = CompanyType.Provider;
            var testCompaniesCollection = new ReadOnlyCollection<CompanyModel>(
                new List<CompanyModel>
                {
                    new CompanyModel(firstConsumerId){Name = "Apple", TypeOfCompany = CompanyType.Consumer},
                    new CompanyModel(secondConsumerId){ Name = "Adidas", TypeOfCompany = CompanyType.Consumer},
                    new CompanyModel(supplierId){ Name = "PerfectFood", TypeOfCompany = CompanyType.Provider}
                });
            var testContractsCollection = new ReadOnlyCollection<ContractModel>(
                new List<ContractModel>
                {
                    new ContractModel { ContractNumber = "574", ProviderCompanyId = supplierId,
                        ConsumerCompanyId = firstConsumerId, ContractStatus = testContractStatus},
                    new ContractModel{ ContractNumber = "123", ProviderCompanyId = supplierId,
                        ConsumerCompanyId = secondConsumerId, ContractStatus = testContractStatus}
                });
            var mockService = new Mock<IContractDomainService>();
            mockService.Setup(ms =>
                    ms.GetByStatusAndCompanyTypeAndIdAsync(supplierId, testCompanyType, testContractStatus))
                .ReturnsAsync(testContractsCollection);
            var mockMailSender = new Mock<IMailSender>();
            var mockNotificationService = new Mock<ICompanyEmailNotificationService>();
            var controller = new ContractController(mockService.Object, mockMailSender.Object, mockNotificationService.Object);

            //Act
            var check = await controller.GetAllByStatusAndCompanyTypeAndId
            (supplierId, testCompanyType.ToString(), testContractStatus.ToString());

            //Assert
            var result = Assert.IsType<GetContractsResponse>(check);
            Assert.Equal(testContractsCollection.Count, result.ContractResponses.Count);

        }

        [Fact]
        public async Task GetAllContractsByStatusAndCompanyTypeAndId_WhenItNotExists()
        {
            //Arrange
            Guid testId = Guid.NewGuid();
            CompanyType testCompanyType = CompanyType.Provider;
            ContractStatus testContractStatus = ContractStatus.Active;
            var mockService = new Mock<IContractDomainService>();
            mockService.Setup(ms => ms.GetByStatusAndCompanyTypeAndIdAsync
                (testId, testCompanyType, testContractStatus)).ReturnsAsync(new List<ContractModel>());
            var mockMailSender = new Mock<IMailSender>();
            var mockNotificationService = new Mock<ICompanyEmailNotificationService>();
            var controller = new ContractController(mockService.Object, mockMailSender.Object, mockNotificationService.Object);

            //Act
            var check = await controller.GetAllByStatusAndCompanyTypeAndId
                (testId, testCompanyType.ToString(), testContractStatus.ToString());

            //Asert
            var result = Assert.IsType<GetContractsResponse>(check);
            Assert.Empty(result.ContractResponses);
        }

        [Fact]
        public async Task CreateValidContract()
        {
            // Arrange
            var mockService = new Mock<IContractDomainService>();

            Guid testId = Guid.NewGuid();
            Guid testConsumerCompanyId = Guid.NewGuid();
            Guid testProviderCompanyId = Guid.NewGuid();

            var testModelContract = new ContractModel(testId)
            {
                ContractNumber = "789",
                ConsumerCompanyId = testConsumerCompanyId,
                ProviderCompanyId = testProviderCompanyId,
                ContractStatus = ContractStatus.Proposed
            };
            mockService.Setup(ms => ms.CreateContractAsync(It.IsAny<ContractModel>()))
                .ReturnsAsync(() => testModelContract);
            var mockMailSender = new Mock<IMailSender>();
            var mockNotificationService = new Mock<ICompanyEmailNotificationService>();
            var controller = new ContractController(mockService.Object, mockMailSender.Object, mockNotificationService.Object);

            // Act
            var request = new ContractRequest()
            {
                ContractNumber = "789",
                ConsumerCompanyId = testConsumerCompanyId,
                ProviderCompanyId = testProviderCompanyId,
                ContractStatus = ContractStatus.Proposed.ToString()
            };
            var check = await controller.CreateContract(request);

            // Assert
            var result = Assert.IsType<ContractResponse>(check.Value);
            Assert.Equal(testId, result.Id);
        }

        [Fact]
        public async Task UpdateContract_WhenItExists()
        {
            // Arrange
            Guid testId = Guid.NewGuid();
            Guid testConsumerCompanyId = Guid.NewGuid();
            Guid testProviderCompanyId = Guid.NewGuid();

            var mockService = new Mock<IContractDomainService>();
            var testModelContract = new ContractModel(testId)
            {
                ContractNumber = "111",
                ConsumerCompanyId = testConsumerCompanyId,
                ProviderCompanyId = testProviderCompanyId,
                ContractStatus = ContractStatus.Proposed
            };
            mockService.Setup(ms => ms.UpdateContractAsync(testId, It.IsAny<ContractModel>()))
                .ReturnsAsync(testModelContract);
            mockService.Setup(ms => ms.GetContractByIdAsync(testId))
                .ReturnsAsync(
                    new ContractModel(testId)
                    {
                        ContractNumber = "563"
                    });
            var mockMailSender = new Mock<IMailSender>();
            var mockNotificationService = new Mock<ICompanyEmailNotificationService>();
            var controller = new ContractController(mockService.Object, mockMailSender.Object, mockNotificationService.Object);

            // Act
            var request = new ContractRequest()
            {
                ContractNumber = "111",
                ConsumerCompanyId = testConsumerCompanyId,
                ProviderCompanyId = testProviderCompanyId,
                ContractStatus = ContractStatus.Proposed.ToString()
            };
            var check = await controller.UpdateContract(testId, request);

            // Assert
            var result = Assert.IsType<ContractResponse>(check.Value);
            Assert.Equal(testId, result.Id);
            Assert.Equal(request.ContractNumber, result.ContractNumber);
            Assert.Equal(request.ContractStatus, result.ContractStatus);
            Assert.Equal(request.ConsumerCompanyId, result.ConsumerCompanyId);
            Assert.Equal(request.ProviderCompanyId, result.ProviderCompanyId);
        }

        [Fact]
        public async Task UpdateContract_WhenItDoesNotExist_CauseBadRequest()
        {
            // Arrange
            Guid testId = Guid.NewGuid();
            var mockService = new Mock<IContractDomainService>();
            mockService.Setup(ms => ms.UpdateContractAsync(testId, It.IsAny<ContractModel>()))
                .Throws<ContractNotFoundDomainException>();
            mockService.Setup(ms => ms.GetContractByIdAsync(testId))
                .ReturnsAsync(
                    new ContractModel(testId)
                    {
                        ContractNumber = "563"
                    });
            var mockMailSender = new Mock<IMailSender>();
            var mockNotificationService = new Mock<ICompanyEmailNotificationService>();
            var controller = new ContractController(mockService.Object, mockMailSender.Object, mockNotificationService.Object);

            // Act
            var request = new ContractRequest()
            {
                ContractNumber = "111",
                ConsumerCompanyId = new Guid(),
                ProviderCompanyId = new Guid(),
                ContractStatus = ContractStatus.Proposed.ToString()
            };
            var check = await controller.UpdateContract(testId, request);

            // Assert
            var result = Assert.IsType<BadRequestObjectResult>(check.Result);
        }
    }
}
