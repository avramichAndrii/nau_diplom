using Company.Domain;
using Company.Domain.Exceptions;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Company.DomainServices.Tests
{
    public class CompanyServiceTests
    {
        [Fact]
        public async Task GetAllCompanies_WhenCollectionIsEmpty()
        {
            //Arrange
            var mockRepository = new Mock<ICompanyRepository>();
            mockRepository.Setup(n => n.GetAllAsync())
                .ReturnsAsync(new List<CompanyModel>());
            var service = new CompanyDomainService(mockRepository.Object);
            
            //Act
            var check = await service.GetAllAsync();

            //Assert
            var result = Assert.IsType<List<CompanyModel>>(check);
            Assert.Empty(result);
        }

        [Fact]
        public async Task GetCompaniesCount_WhenCollectionIsNotEmpty_ShouldReturn3()
        {
            //Arrange
            var mockRepository = new Mock<ICompanyRepository>();
            var testModelsCollection = new List<CompanyModel>
            {
                new CompanyModel { Name = "Apple", Description = "Think different" },
                new CompanyModel { Name = "Lenovo", Description = "Different is better" },
                new CompanyModel { Name = "Asus", Description = "In search of incredible" }
            };
            mockRepository
                .Setup(mR => mR.GetAllAsync()).ReturnsAsync(testModelsCollection);
            var service = new CompanyDomainService(mockRepository.Object);
            
            //Act
            var check = await service.GetAllAsync();

            //Assert
            var result = Assert.IsType<List<CompanyModel>>(check);
            Assert.Equal(testModelsCollection.Count, result.Count);
        }

        [Fact]
        public async Task GetSingleCompany_WhenItExists()
        {
            //Arrange
            var mockRepository = new Mock<ICompanyRepository>();
            Guid requestId = Guid.NewGuid();
            mockRepository.Setup(mR => mR.GetByIdAsync(requestId))
                .ReturnsAsync(new CompanyModel(requestId)
                {
                    Name = "Asus",
                    Description = "In search of incredible"
                });
            var service = new CompanyDomainService(mockRepository.Object);

            //Act
            var check = await service.GetByIdAsync(requestId);

            //Assert
            var result = Assert.IsType<CompanyModel>(check);
            Assert.Equal(requestId, result.Id);
        }


        [Fact]
        public async Task GetSingleCompany_WhenItDoesNotExist_CausesException()
        {
            //Arrange
            var mockRepository = new Mock<ICompanyRepository>();
            Guid requestId = Guid.NewGuid();
            mockRepository
                .Setup(mR => mR.GetByIdAsync(requestId)).Throws<CompanyNotFoundDomainException>();
            var service = new CompanyDomainService(mockRepository.Object);

            //Act & Assert
            await Assert.ThrowsAsync<CompanyNotFoundDomainException>(() => service.GetByIdAsync(requestId));
        }


        [Fact]
        public async Task CreateValidCompany()
        {
            //Arrange
            var mockRepository = new Mock<ICompanyRepository>();
            Guid id = Guid.NewGuid();
            var testModel = new CompanyModel(id)
            {
                Name = "Asus",
                Description = "In search of incredible"
            };
            mockRepository.Setup(mR => mR.CreateAsync(testModel))
                .ReturnsAsync(() => testModel);
            var service = mockRepository.Object;

            //Act
            var check = await service.CreateAsync(testModel);

            //Assert
            mockRepository.Verify(mR => mR.CreateAsync(testModel), Times.Once);
            var result = Assert.IsType<CompanyModel>(check);
            Assert.Equal(id, result.Id);
        }

        [Fact]
        public async Task UpdateCompany_WhenItExists()
        {
            //Arrange
            var mockRepository = new Mock<ICompanyRepository>();
            Guid id = Guid.NewGuid();
            mockRepository.Setup(mR => mR.UpdateAsync(id, It.IsAny<CompanyModel>()))
                .ReturnsAsync(() => new CompanyModel(id)
                {
                    Name = "Asus",
                    Description = "In search of incredible"
                });
            var service = new CompanyDomainService(mockRepository.Object);

            //Act
            var modelToUpdate = new CompanyModel
            {
                Name = "Asus",
                Description = "In search of incredible"
            };
            var check = await service.UpdateAsync(id, modelToUpdate);

            //Assert
            mockRepository.Verify(mR => mR.UpdateAsync(id, modelToUpdate), Times.AtLeastOnce);
            var result = Assert.IsType<CompanyModel>(check);
            Assert.Equal(id, result.Id);
            Assert.Equal(check.Name, result.Name);
            Assert.Equal(check.Description, result.Description);
        }

        [Fact]
        public async Task UpdateCompany_WhenItDoesNotExist_CausesException()
        {
            //Arrange
            var mockRepository = new Mock<ICompanyRepository>();
            Guid id = Guid.NewGuid();
            mockRepository
                .Setup(mR => mR.UpdateAsync(id, It.IsAny<CompanyModel>()))
                .Throws<CompanyNotFoundDomainException>();
            var service = new CompanyDomainService(mockRepository.Object);
            
            //Act
            var point = new CompanyModel()
            {
                Name = "Asus", Description = "In search of incredible"
            };
            
            //Assert
            await Assert.ThrowsAsync<CompanyNotFoundDomainException>(() => service.UpdateAsync(id, point));

        }

        [Fact]
        public void RemoveCompany_WhenItExists()
        {
            //Arrange
            var mockRepository = new Mock<ICompanyRepository>();
            Guid id = Guid.NewGuid();
            mockRepository.Setup(mR => mR.DeleteAsync(id))
                .ReturnsAsync(new CompanyModel(id)
                {
                    Name = "Asus",
                    Description = "In search of incredible"
                });
            var service = new CompanyDomainService(mockRepository.Object);
            
            //Act
            var check = service.DeleteAsync(id);

            //Assert
            mockRepository.Verify(mR => mR.DeleteAsync(id), Times.Once);
            var result = Assert.IsType<CompanyModel>(check.Result);
            Assert.Equal(id, result.Id);
        }

        [Fact]
        public void RemoveCompany_WhenItDoesNotExist_CausesException()
        {
            //Arrange
            var mockRepository = new Mock<ICompanyRepository>();
            Guid id = Guid.NewGuid();
            mockRepository.Setup(mR => mR.DeleteAsync(id))
                .Throws<CompanyNotFoundDomainException>();
            var service = new CompanyDomainService(mockRepository.Object);
            
            //Act & Assert
            Assert.ThrowsAsync<CompanyNotFoundDomainException>(() => service.DeleteAsync(id));
        }
    }
}