﻿using Company.Domain;
using Company.Domain.Exceptions;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;


namespace Company.DomainServices.Tests
{
    public class ContractServiceTests
    {
        [Fact]
        public async Task GetAllContracts_WhenCollectionIsEmpty()
        {
            //Arrange
            var mockRepository = new Mock<IContractRepository>();
            mockRepository.Setup(n => n.GetAllContractsAsync())
                .ReturnsAsync(new List<ContractModel>());
            var service = new ContractDomainService(mockRepository.Object);

            //Act
            var check = await service.GetAllContractsAsync();

            //Assert
            var result = Assert.IsType<List<ContractModel>>(check);
            Assert.Empty(result);
        }

        [Fact]
        public async Task GetContactsCount_WhenCollectionIsNotEmpty_ShouldReturn3()
        {
            //Arrange
            var mockRepository = new Mock<IContractRepository>();
            var testModelsCollection = new List<ContractModel>
            {
                new ContractModel { ContractNumber = "1A", ContractStatus = ContractStatus.Active, ConsumerCompanyId = new Guid(), ProviderCompanyId = new Guid(), MenuId = new Guid()},
                new ContractModel { ContractNumber = "2A", ContractStatus = ContractStatus.Active, ConsumerCompanyId = new Guid(), ProviderCompanyId = new Guid(), MenuId = new Guid() },
                new ContractModel { ContractNumber = "3A", ContractStatus = ContractStatus.Active, ConsumerCompanyId = new Guid(), ProviderCompanyId = new Guid(), MenuId = new Guid() }
            };
            mockRepository
                .Setup(mR => mR.GetAllContractsAsync()).ReturnsAsync(testModelsCollection);
            var service = new ContractDomainService(mockRepository.Object);

            //Act
            var check = await service.GetAllContractsAsync();

            //Assert
            var result = Assert.IsType<List<ContractModel>>(check);
            Assert.Equal(testModelsCollection.Count, result.Count);
        }

        [Fact]
        public async Task GetSingleContract_WhenItExists()
        {
            //Arrange
            var mockRepository = new Mock<IContractRepository>();
            Guid requestId = Guid.NewGuid();
            mockRepository.Setup(mR => mR.GetContractByIdAsync(requestId))
                .ReturnsAsync(new ContractModel(requestId)
                {
                    ContractNumber = "1A",
                    ContractStatus = ContractStatus.Active,
                    ConsumerCompanyId = new Guid(),
                    ProviderCompanyId = new Guid(),
                    MenuId = new Guid()
                });
            var service = new ContractDomainService(mockRepository.Object);

            //Act
            var check = await service.GetContractByIdAsync(requestId);

            //Assert
            var result = Assert.IsType<ContractModel>(check);
            Assert.Equal(requestId, result.Id);
        }


        [Fact]
        public async Task GetSingleContract_WhenItDoesNotExist_CausesException()
        {
            //Arrange
            var mockRepository = new Mock<IContractRepository>();
            Guid requestId = Guid.NewGuid();
            mockRepository
                .Setup(mR => mR.GetContractByIdAsync(requestId)).Throws<CompanyNotFoundDomainException>();
            var service = new ContractDomainService(mockRepository.Object);

            //Act & Assert
            await Assert.ThrowsAsync<CompanyNotFoundDomainException>(() => service.GetContractByIdAsync(requestId));
        }


        [Fact]
        public async Task CreateValidContract()
        {
            //Arrange
            var mockRepository = new Mock<IContractRepository>();
            Guid id = Guid.NewGuid();
            var testModel = new ContractModel(id)
            {
                ContractNumber = "1A",
                ContractStatus = ContractStatus.Active,
                ConsumerCompanyId = new Guid(),
                ProviderCompanyId = new Guid(),
                MenuId = new Guid()
            };
            mockRepository.Setup(mR => mR.CreateContractAsync(testModel))
                .ReturnsAsync(() => testModel);
            var service = new ContractDomainService(mockRepository.Object);

            //Act
            var check = await service.CreateContractAsync(testModel);

            //Assert
            mockRepository.Verify(mR => mR.CreateContractAsync(testModel), Times.Once);
            var result = Assert.IsType<ContractModel>(check);
            Assert.Equal(id, result.Id);
        }

        [Fact]
        public async Task UpdateContract_WhenItExists()
        {
            //Arrange
            var mockRepository = new Mock<IContractRepository>();
            Guid id = Guid.NewGuid();
            mockRepository.Setup(mR => mR.UpdateContractAsync(id, It.IsAny<ContractModel>()))
                .ReturnsAsync(() => new ContractModel(id)
                {
                    ContractNumber = "1A",
                    ContractStatus = ContractStatus.Active,
                    ConsumerCompanyId = new Guid(),
                    ProviderCompanyId = new Guid(),
                    MenuId = new Guid()
                });
            var service = new ContractDomainService(mockRepository.Object);

            //Act
            var modelToUpdate = new ContractModel
            {
                ContractStatus = ContractStatus.Changed
            };
            var check = await service.UpdateContractAsync(id, modelToUpdate);

            //Assert
            mockRepository.Verify(mR => mR.UpdateContractAsync(id, modelToUpdate), Times.AtLeastOnce);
            var result = Assert.IsType<ContractModel>(check);
            Assert.Equal(id, result.Id);
            Assert.Equal(check.ContractNumber, result.ContractNumber);
            Assert.Equal(check.ContractStatus, result.ContractStatus);
            Assert.Equal(check.ConsumerCompanyId, result.ConsumerCompanyId);
            Assert.Equal(check.ProviderCompanyId, result.ProviderCompanyId);
            Assert.Equal(check.MenuId, result.MenuId);
        }

        [Fact]
        public async Task UpdateContract_WhenItDoesNotExist_CausesException()
        {
            //Arrange
            var mockRepository = new Mock<IContractRepository>();
            Guid id = Guid.NewGuid();
            mockRepository
                .Setup(mR => mR.UpdateContractAsync(id, It.IsAny<ContractModel>()))
                .Throws<CompanyNotFoundDomainException>();
            var service = new ContractDomainService(mockRepository.Object);

            //Act
            var point = new ContractModel()
            {
                ContractNumber = "1A",
                ContractStatus = ContractStatus.Active,
                ConsumerCompanyId = new Guid(),
                ProviderCompanyId = new Guid(),
                MenuId = new Guid()
            };

            //Assert
            await Assert.ThrowsAsync<CompanyNotFoundDomainException>(() => service.UpdateContractAsync(id, point));

        }
    }
}
