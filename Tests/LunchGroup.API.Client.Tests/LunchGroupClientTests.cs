using LunchGroup.API.Contracts;
using LunchGroup.API.Contracts.Requests;
using LunchGroup.API.Contracts.Responses;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace LunchGroup.API.Client.Tests
{
    
    public class LunchGroupClientTests
    {
        #region General Moqs
        private const string TestUrl = "http://google.com/";
        private const string BadRequestMessage = "something bad happened";
        private LunchGroupClient PrepareClient200(object responseObject)
        {
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            var mockedResponse = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(JsonConvert.SerializeObject(responseObject), Encoding.UTF8, "application/json")
            };
            handlerMock.Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(mockedResponse);


            var httpClient = new HttpClient(handlerMock.Object)
            {
                BaseAddress = new Uri(TestUrl)
            };

            return new LunchGroupClient(httpClient);

        }
        #endregion

        #region Tests variables

        #region OkRequests
        private readonly SetActiveFlagRequest _okSetActiveFlagRequest;
        private readonly CreateLunchGroupRequest _okCreateRequest;
        private readonly UpdateLunchGroupRequest _okUpdateLunchGroupRequest;
        #endregion

        #region OkResponses
        private readonly CreateLunchGroupResponse _okCreateLunchGroupResponse;
        private readonly GetLunchGroupsCollectionResponse _okGetLunchGroupsCollectionResponse;
        private readonly GetLunchGroupResponse _okGetLunchGroupResponse;
        private readonly GetTimetableResponse _okGetTimetableResponse;
        #endregion

        #region neutral
        private readonly Guid _companyGuid;
        private readonly Guid _groupGuid;
        private readonly Guid _managerGuid;
        private readonly Guid[] _workersGuids;
        #endregion

        #endregion

        public LunchGroupClientTests()
        {
            //general
            _companyGuid = new Guid("2146e721-630a-4cf6-9bb5-9a28caf9bbc1");
            _groupGuid = new Guid("08d820f1-6015-4cf3-825f-4640673166c6");
            _managerGuid = new Guid("08d818c1-7147-4a12-81dc-efb2a8ab7c00");
            _workersGuids = new Guid[] { new Guid("2146e721-630a-4cf6-9bb5-9a28caf9bbc3") };

            //requests
            _okSetActiveFlagRequest = new SetActiveFlagRequest { IsActive = true };
            _okCreateRequest = new CreateLunchGroupRequest
            {
                SubmitDeadline = DateTime.Parse("2020-06-26"),
                PeriodStart = DateTime.Parse("2020-06-29"),
                Periodicity = 7,
                Weekends = new string[] { "Saturday", "Sunday" },
                Name = "Test",
                Members = _workersGuids,
                CompanyId = _companyGuid,
                ManagerId = _managerGuid,
                Address = new AddressRequest
                {
                    City = "Praga",
                    Street = "Gavela",
                    BuildingNumber = "8",
                    Office = "3",
                }
            };
            _okUpdateLunchGroupRequest = new UpdateLunchGroupRequest
            {
                SubmitDeadline = DateTime.Parse("2020-06-26"),
                PeriodStart = DateTime.Parse("2020-06-29"),
                Periodicity = 7,
                Weekends = new string[] { "Saturday", "Sunday" },
                Name = "Test",
                Members = _workersGuids,
                Address = new AddressRequest
                {
                    City = "Praga",
                    Street = "Gavela",
                    BuildingNumber = "8",
                    Office = "3",
                }
            };

            //responses
            _okCreateLunchGroupResponse = new CreateLunchGroupResponse
            {
                Id = _groupGuid,
                SubmitDeadline = "2020-06-26",
                PeriodStart = "2020-06-29",
                Periodicity = 7,
                Weekends = new string[] { "Saturday", "Sunday" },
                Name = "Test",
                Members = _workersGuids,
                CompanyId = _companyGuid,
                ManagerId = _managerGuid,
                Address = new AddressResponse
                {
                    City = "Praga",
                    Street = "Gavela",
                    BuildingNumber = "8",
                    Office = "3",
                }
            };
            _okGetLunchGroupResponse = new GetLunchGroupResponse
            {
                Id = _groupGuid,
                IsActive = true,
                SubmitDeadline = DateTime.Parse("2020-06-26"),
                PeriodStart = DateTime.Parse("2020-06-29"),
                Periodicity = 7,
                Weekends = new string[] { "Saturday", "Sunday" },
                Name = "Test",
                Members = _workersGuids,
                CompanyId = _companyGuid,
                ManagerId = _managerGuid,
                Address = new AddressResponse
                {
                    City = "Praga",
                    Street = "Gavela",
                    BuildingNumber = "8",
                    Office = "3",
                }
            };
            _okGetLunchGroupsCollectionResponse = new GetLunchGroupsCollectionResponse(new List<GetLunchGroupResponse> { _okGetLunchGroupResponse });
            _okGetTimetableResponse = new GetTimetableResponse(DateTime.Parse("2020-06-26"),new List<DateTime>
            {
            DateTime.Parse("2020-06-26"),
            DateTime.Parse("2020-06-27"),
            DateTime.Parse("2020-06-28"),
            DateTime.Parse("2020-06-29"),
            DateTime.Parse("2020-06-30"),
            DateTime.Parse("2020-07-01"),
            DateTime.Parse("2020-07-02")
            });

        }

        [Fact]
        public async Task Create_ShouldReturnGroup()
        {
            // Arrange
            LunchGroupClient testedClient = PrepareClient200(_okCreateLunchGroupResponse);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            CreateLunchGroupResponse response = await testedClient.CreateLunchGroupAsync(_okCreateRequest);

            // Assert
            Assert.NotNull(response);
            Assert.Equal(_okCreateLunchGroupResponse, response);
        }

        [Fact]
        public async Task GetAllByCompany_ShouldReturnGroups()
        {
            // Arrange
            LunchGroupClient testedClient = PrepareClient200(_okGetLunchGroupsCollectionResponse);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            GetLunchGroupsCollectionResponse response = await testedClient.GetAllLunchGroupsByCompanyAsync(_companyGuid);

            // Assert
            Assert.NotNull(response);
            Assert.True(CompareService.IsCollectionsEqual(_okGetLunchGroupsCollectionResponse.LunchGroupsCollection, response.LunchGroupsCollection));
        }

        [Fact]
        public async Task GetById_ShouldReturnGroup()
        {
            // Arrange
            LunchGroupClient testedClient = PrepareClient200(_okGetLunchGroupResponse);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            GetLunchGroupResponse response = await testedClient.GetLunchGroupByIdAsync(_groupGuid);

            // Assert
            Assert.NotNull(response);
            Assert.Equal(_okGetLunchGroupResponse, response);
        }

        [Fact]
        public async Task GetByManagerId_ShouldReturnGroups()
        {
            // Arrange
            LunchGroupClient testedClient = PrepareClient200(_okGetLunchGroupsCollectionResponse);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            GetLunchGroupsCollectionResponse response = await testedClient.GetAllLunchGroupsByCompanyAsync(_managerGuid);

            // Assert
            Assert.NotNull(response);
            Assert.True(CompareService.IsCollectionsEqual(_okGetLunchGroupsCollectionResponse.LunchGroupsCollection, response.LunchGroupsCollection));
        }

        [Fact]
        public async Task GetTimetable_ShouldReturnTimetable()
        {
            // Arrange
            LunchGroupClient testedClient = PrepareClient200(_okGetTimetableResponse);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            GetTimetableResponse response = await testedClient.GetTimetableAsync(_groupGuid);

            // Assert
            Assert.NotNull(response);
            Assert.Equal(_okGetTimetableResponse, response);
        }

        [Fact]
        public async Task GetWorkerGroups_ShouldReturnSingleGroup()
        {
            // Arrange
            LunchGroupClient testedClient = PrepareClient200(_okGetLunchGroupResponse);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            GetLunchGroupResponse response = await testedClient.GetWorkerLunchGroupAsync(_workersGuids[0]);

            // Assert
            Assert.NotNull(response);
            Assert.Equal(_okGetLunchGroupResponse, response);
        }

        [Fact]
        public async Task UpdateGroup_ShouldReturnGroupId()
        {
            // Arrange
            LunchGroupClient testedClient = PrepareClient200(_groupGuid);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            Guid response = await testedClient.UpdateLunchGroupAsync(_okUpdateLunchGroupRequest, _groupGuid);

            // Assert
            Assert.Equal(_groupGuid.ToString(), response.ToString());
        }
    }
}
