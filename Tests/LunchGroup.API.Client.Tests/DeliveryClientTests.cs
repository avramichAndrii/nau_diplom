﻿using LunchGroup.API.Contracts.Requests;
using LunchGroup.API.Contracts.Responses;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace LunchGroup.API.Client.Tests
{
    public class DeliveryClientTests
    {
        private const string TestUrl = "http://google.com/";
        private const string BadRequestMessage = "something bad happened";
        private LunchGroupClient PrepareClientForOkResult(object responseObject)
        {
            Mock<HttpMessageHandler> handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            HttpResponseMessage mockedResponse = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(JsonConvert.SerializeObject(responseObject), Encoding.UTF8, "application/json")
            };
            handlerMock.Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(mockedResponse)
               .Verifiable();

            HttpClient httpClient = new HttpClient(handlerMock.Object)
            {
                BaseAddress = new Uri(TestUrl)
            };
            LunchGroupClient testClient = new LunchGroupClient(httpClient);
            testClient.BaseUri = new Uri(TestUrl);
            return testClient;
        }

        private LunchGroupClient PrepareClientForBadResult()
        {
            Mock<HttpMessageHandler> handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            HttpResponseMessage mockedResponse = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.BadRequest,
                Content = new StringContent(BadRequestMessage)
            };
            handlerMock.Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(mockedResponse)
               .Verifiable();

            HttpClient httpClient = new HttpClient(handlerMock.Object)
            {
                BaseAddress = new Uri(TestUrl)
            };
            return new LunchGroupClient(httpClient);
        }


        [Fact]
        public async Task GetAllDeliveries_ShouldReturnReadOnlyCollection()
        {
            // Arrange
            GetAllDeliveriesResponse responseCollection = new GetAllDeliveriesResponse(
                new List<DeliveryResponse>
                {
            new DeliveryResponse()
            {
              DeliveryId = Guid.NewGuid(),
              DeliveryTime = DateTime.Now,
              GroupOrderId = Guid.NewGuid(),
              Status = "Done"
            },
            new DeliveryResponse()
            {
               DeliveryId = Guid.NewGuid(),
              DeliveryTime = DateTime.Now,
              GroupOrderId = Guid.NewGuid(),
              Status = "Done"
            },
            new DeliveryResponse()
            {
            DeliveryId = Guid.NewGuid(),
              DeliveryTime = DateTime.Now,
              GroupOrderId = Guid.NewGuid(),
              Status = "Done"
            }
        });

            LunchGroupClient testedClient = PrepareClientForOkResult(responseCollection);

            // Act
            GetAllDeliveriesResponse result = await testedClient.GetAllDeliveriesAsync();

            // Assert
            GetAllDeliveriesResponse response = Assert.IsType<GetAllDeliveriesResponse>(result);
            Assert.NotNull(response);
            Assert.Equal(responseCollection.DeliveryCollection.Count, response.DeliveryCollection.Count);
        }

        [Fact]
        public async Task GetDelivery_ByDeliveryId_WhenItExists_ShouldReturnSingle()
        {
            // Arrange
            DeliveryResponse responseDelivery = new DeliveryResponse
            {
                DeliveryId = Guid.NewGuid(),
                DeliveryTime = DateTime.Now,
                GroupOrderId = Guid.NewGuid(),
                Status = "Created"

            };

            LunchGroupClient testedClient = PrepareClientForOkResult(responseDelivery);

            // Act
            DeliveryResponse result = await testedClient.GetDeliveryByIdAsync(Guid.NewGuid());

            // Assert

            Assert.NotNull(result);
            Assert.Equal(responseDelivery.Status, result.Status);
            Assert.Equal(responseDelivery.DeliveryTime, result.DeliveryTime);

        }

        [Fact]
        public async Task GetDelivery_ByGroupOrder_WhenItExists_ShouldReturnSingle()
        {
            // Arrange
            DeliveryResponse responseDelivery = new DeliveryResponse
            {
                DeliveryId = Guid.NewGuid(),
                DeliveryTime = DateTime.Now,
                GroupOrderId = Guid.NewGuid(),
                Status = "Created"

            };

            LunchGroupClient testedClient = PrepareClientForOkResult(responseDelivery);

            // Act
            DeliveryResponse result = await testedClient.GetDeliveryByGroupOrderIdAsync(Guid.NewGuid());

            // Assert

            Assert.NotNull(result);
            Assert.Equal(responseDelivery.Status, result.Status);
            Assert.Equal(responseDelivery.DeliveryTime, result.DeliveryTime);

        }



        [Fact]
        public async Task CreateDelivery_ShouldReturnCreated_WhenIsValid()
        {
            // Arrange
            Guid generatedId = Guid.NewGuid();
            DeliveryResponse validDelivery = new DeliveryResponse
            {
                DeliveryId = generatedId,
                DeliveryTime = DateTime.Now,
                GroupOrderId = Guid.NewGuid(),
                Status = "Created"

            };
            LunchGroupClient testedClient = PrepareClientForOkResult(validDelivery);

            // Act
            CreateDeliveryRequest request = new CreateDeliveryRequest
            {
                GroupOrderId = generatedId
            };
            DeliveryResponse result = await testedClient.CreateDeliveryAsync(request);

            // Assert

            Assert.NotNull(result);
            Assert.Equal(validDelivery.DeliveryId, result.DeliveryId);
            Assert.Equal(validDelivery.DeliveryTime, result.DeliveryTime);
            Assert.Equal(validDelivery.Status, result.Status);
        }

        [Fact]
        public async Task UpdateDelivery_WhenItExists_ShouldReturnUpdated_WhenUsingValidData()
        {
            // Arrange
            Guid idForUpdate = Guid.NewGuid();
            DeliveryResponse validDelivery = new DeliveryResponse
            {
                DeliveryId = idForUpdate,
                DeliveryTime = DateTime.Now,
                GroupOrderId = Guid.NewGuid(),
                Status = "Created"

            };
            LunchGroupClient testedClient = PrepareClientForOkResult(validDelivery);

            // Act
            UpdateDeliveryRequest bodyForUpdate = new UpdateDeliveryRequest
            {
                DeliveryId = idForUpdate,
                DeliveryTime = DateTime.Now,
                GroupOrderId = Guid.NewGuid(),
                Status = "Created"
            };

            DeliveryResponse result = await testedClient.UpdateDeliveryAsync(idForUpdate, bodyForUpdate);

            // Assert

            Assert.NotNull(result);
            Assert.Equal(validDelivery.DeliveryId, result.DeliveryId);
            Assert.Equal(validDelivery.DeliveryTime, result.DeliveryTime);
            Assert.Equal(validDelivery.Status, result.Status);
        }


        [Fact()]
        public async Task DeleteDelivery_WhenItExists_ShouldReturnSingle()
        {
            // Arrange
            Guid requestedId = Guid.NewGuid();
            DeliveryResponse validDelivery = new DeliveryResponse
            {
                DeliveryId = requestedId,
                DeliveryTime = DateTime.Now,
                GroupOrderId = Guid.NewGuid(),
                Status = "Created"

            };
            LunchGroupClient testedClient = PrepareClientForOkResult(validDelivery);

            // Act
            DeliveryResponse actualResponse = await testedClient.DeleteDeliveryAsync(requestedId);

            // Assert
            Assert.NotNull(actualResponse);
            Assert.Equal(validDelivery.DeliveryId, actualResponse.DeliveryId);
        }
    }
}
