﻿using System;
using Xunit;

namespace Domain.Core.Tests
{
    public class EntityTests
    {
        private class TestEntity1 : Entity<Guid>
        {
            public string OtherData { get; set; }

            public TestEntity1(Guid id) : base(id) { }

            public TestEntity1() { }

            public void SetId(Guid id) { Id = id; }
        }

        private class TestEntity2 : Entity<Guid>
        {
            public string OtherData { get; set; }

            public TestEntity2(Guid id) : base(id) { }
        }

        [Fact]
        public void EqualityCheckTest()
        {
            // Arrange
            Guid id1 = Guid.NewGuid();
            Guid id2 = Guid.NewGuid();

            TestEntity1 testEntity11 = new TestEntity1(id1) { OtherData = "I am test entity 11" };
            TestEntity1 testEntity12 = new TestEntity1(id1) { OtherData = "I am test entity 12" };
            TestEntity1 testEntity13 = new TestEntity1(id2) { OtherData = "I am test entity 13" };
            TestEntity2 testEntity21 = new TestEntity2(id2) { OtherData = "I am test entity 21" };

            // Act


            // Assert
            Assert.True(testEntity11 == testEntity12);  // same id same type
            Assert.False(testEntity11 != testEntity12);
            Assert.True(testEntity11.Equals(testEntity12));
            Assert.True(testEntity11 != testEntity13);  // same type different ids
            Assert.False(testEntity11 == testEntity13);
            Assert.False(testEntity11.Equals(testEntity13));
            Assert.True(testEntity21 != testEntity13);  // same id different types
            Assert.False(testEntity21 == testEntity13);
            Assert.False(testEntity11.Equals(testEntity13));

            Assert.False(testEntity11 == null); // null check
            Assert.True(testEntity11 != null);
            Assert.False(testEntity11.Equals(null));
        }
        
        [Fact]
        public void SetIdTwiceTest()
        {
            // Arrange
            TestEntity1 testEntity11 = new TestEntity1(Guid.NewGuid()) { OtherData = "I am test entity 11" };
            TestEntity1 testEntity12 = new TestEntity1() { OtherData = "I am test entity 12" };
            Guid id = Guid.NewGuid();

            // Act
            Action act1 = () => testEntity11.SetId(Guid.NewGuid());
            Action act2 = () => testEntity12.SetId(Guid.Empty);
            testEntity12.SetId(id);

            // Assert
            var exception1 = Assert.Throws<EntityIdAlreadySetException>(act1);
            var exception2 = Assert.Throws<EntityIdAlreadySetException>(act2);
            Assert.Equal("Entity id should be set only once per object lifecycle", exception1.Message);
            Assert.Equal("Entity id should be set only once per object lifecycle", exception2.Message);
            Assert.Equal(id, testEntity12.Id);
        }
    }
}
