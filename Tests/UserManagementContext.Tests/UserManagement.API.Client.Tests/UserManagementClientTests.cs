using System;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UserManagement.API.Client.Client;
using UserManagement.API.Contracts.Requests;
using UserManagement.API.Contracts.Responses;
using UserManagement.API.Contracts.Routs;
using UserManagement.Application.Helpers;
using UserManagement.Domain.Models;
using Xunit;

namespace UserManagement.API.Client.Tests
{
    public class UserManagementClientTests
    {
        private readonly Mock<HttpMessageHandler> _handlerMock;

        #region General Moqs
        private const string TestUrl = "https://localhost:5001/";
        private const string BadRequestMessage = "something bad happened";

        public UserManagementClientTests()
        {
            _handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
        }

        private void SetUpHandler(object responseObject)
        {
            var mockedResponse = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(JsonConvert.SerializeObject(responseObject), Encoding.UTF8, "application/json")
            };
            _handlerMock.Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>()
                )
                .ReturnsAsync(mockedResponse);
        }
        private UserManagementClient PrepareClient200(object responseObject)
        {
           
            var mockedResponse = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(JsonConvert.SerializeObject(responseObject), Encoding.UTF8, "application/json")
            };
            _handlerMock.Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(mockedResponse);


            var httpClient = new HttpClient(_handlerMock.Object);

            return new UserManagementClient(httpClient);

        }
        private UserManagementClient PrepareClient400()
        {
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            var mockedResponse = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.BadRequest,
                Content = new StringContent(BadRequestMessage)
            };
            handlerMock.Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(mockedResponse)
               .Verifiable();

            var httpClient = new HttpClient(handlerMock.Object);

            return new UserManagementClient(httpClient);
        }
        #endregion

        [Fact]
        public async Task GetUsers_ShouldReturnCollectionAsync()
        {
            // Arrange
            var responseCollection = new UsersCollectionResponse(
                new List<UserResponse> {
                    //Guids are differents
                    new UserResponse {Email="test@gmail.com",FirstName="TestName",LastName="LastTest",
                        Id= new Guid("08d8115b-d9e4-4c57-874e-811f7969db4d"),Login="testLogin" },
                    new UserResponse {Email="test@gmail.com",FirstName="TestName",LastName="LastTest",
                        Id= new Guid("08d8115b-d9e4-4c57-874e-811f7969db4a"),Login="testLogin2" }
                });
            var testedClient = PrepareClient200(responseCollection);
            testedClient.BaseUri = new Uri(TestUrl);
        
            // Act

            var responce = await testedClient.GetAllAsync();
            
            // Assert
            Assert.Equal(responseCollection.UsersCollection.Count, responce.UsersCollection.Count);
        
        }
        
        [Fact]
        public async Task GetUsersByCompanyAndRole_ShouldReturnCollectionAsync()
        {
            // Arrange
            Guid companyId = Guid.NewGuid();
            const string Worker = nameof(Worker);
            var responseCollection = new UsersCollectionResponse(
                new List<UserResponse> {
                    new UserResponse
                    {
                        Email = "test@gmail.com",
                        FirstName = "TestName",
                        LastName = "LastTest",
                        CompanyId = companyId,
                        Id = new Guid("08d8115b-d9e4-4c57-874e-811f7969db4d"),
                        Login = "testLogin",
                        Role = Worker
                    },
                    new UserResponse
                    {
                        Email = "test@gmail.com",
                        FirstName = "TestName",
                        LastName = "LastTest",
                        CompanyId = companyId,
                        Id = new Guid("08d8115b-d9e4-4c57-874e-811f7969db4a"),
                        Login = "testLogin2",
                        Role = Worker
                    }
                });
            var testedClient = PrepareClient200(responseCollection);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            var responce = await testedClient.GetAllByCompanyAndRoleAsync(companyId, Worker);

            // Assert
            Assert.Equal(responseCollection.UsersCollection.Count, responce.UsersCollection.Count);

        }

        [Fact]
        public async Task Authorize_ShouldReturnGuid()
        {
            // Arrange
            var request = new AuthenticateRequest { Login = "testLogin", Password = "qwerty1234" };
            User user = new User
            {
                Login = "newTestLogin",
                CompanyId = Guid.NewGuid(),
                Email = "test@gmail.com",
                FirstName = "newtestFirstName",
                LastName = "newtestLastName",
                Role = "Worker"
            };
            Guid token = Guid.NewGuid();
            AuthenticationSettings authSettings = new AuthenticationSettings() { Secret = "Our secret for tests is really big", JwtLifetime = "1" };
            RefreshTokenSettings refreshTokenSettings = new RefreshTokenSettings { Lifetime = "1" };

            var response = new AuthenticateModel
            {
                AccessToken = JWTTokenProvider.GenerateAccessToken(authSettings, user),
                RefreshToken = token,
                ExpiresAt = DateTime.Now.AddDays(Int32.Parse(refreshTokenSettings.Lifetime))
            };
            
            var testedClient = PrepareClient200(response);
            testedClient.BaseUri = new Uri(TestUrl);
        
            // Act
            var actualResponse = await testedClient.AuthenticateAsync(request);
            
            //Assert 
            Assert.Equal(response.AccessToken, actualResponse.AccessToken);
        }
        
        [Fact]
        public async Task Register_ShouldReturnModel()
        {
            // Arrange
            var response = new RegisterRequest
            {
                Email = "test@gmail.com",
                FirstName = "TestName",
                LastName = "LastTest",
                Login = "testLogin",
                Password = "qwerty1234",
                Role = "Worker"
        
            };
            var testedClient = PrepareClient200(response);
            testedClient.BaseUri = new Uri(TestUrl);
        
            // Act
            var ActualResponse = (await testedClient.RegisterAsync(response));
        
            // Assert
            Assert.Equal(response.Email, ActualResponse.Email);
            Assert.Equal(response.Login, ActualResponse.Login);
        }
        
        [Fact]
        public async Task GetById_ShouldReturnUser()
        {
            // Arrange
            Guid id = new Guid("08d8115b-d9e4-4c57-874e-811f7969db4d");
            var response = new UserResponse
            {
                Email = "test@gmail.com",
                FirstName = "TestName",
                LastName = "LastTest",
                Id = id,
                Login = "testLogin"
            };
            
            var testedClient = PrepareClient200(response);
            testedClient.BaseUri = new Uri(TestUrl);
        
            // Act
            var ActualResponse = await testedClient.GetByIdAsync(id);

            //Assert
            Assert.Equal(response.Email, ActualResponse.Email);
            Assert.Equal(response.Login, ActualResponse.Login);
        }
        
        [Fact]
        public async Task PutById_ShouldReturnUser()
        {
            // Arrange
            Guid id = new Guid("08d8115b-d9e4-4c57-874e-811f7969db4d");
            var request = new UpdateRequest
            {
                Email = "test@gmail.com",
                FirstName = "TestName",
                LastName = "LastTest",
                Login = "testLogin2"
            };
            var response = new UserResponse
            {
                Email = "test@gmail.com",
                FirstName = "TestName",
                LastName = "LastTest",
                Id = id,
                Login = "testLogin"
            };
        
            var testedClient = PrepareClient200(response);
            testedClient.BaseUri = new Uri(TestUrl);
        
            // Act
            var ActualResponse = await testedClient.UpdateByIdAsync(id, request);

            //Assert
            Assert.Equal(response.Email, ActualResponse.Email);
            Assert.Equal(response.Login, ActualResponse.Login);
        }

        [Fact]
        public async Task DeleteById_ShouldReturnUser()
        {
            // Arrange
            var response = new Guid("08d8115b-d9e4-4c57-874e-811f7969db4d");

            var testedClient = PrepareClient200(response);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            var ActualResponse = await testedClient.DeleteByIdAsync(response);

            //Assert
            Assert.Equal(response, ActualResponse);
        }

        [Fact]
        public async Task RefreshTokenTest ()
        {
            // Arrange
            User user = new User
            {
                Login = "newTestLogin",
                CompanyId = Guid.NewGuid(),
                Email = "test@gmail.com",
                FirstName = "newtestFirstName",
                LastName = "newtestLastName",
                Role = "Worker"
            };
            Guid token = Guid.NewGuid();
            AuthenticationSettings authSettings = new AuthenticationSettings() { Secret = "Our secret for tests is really big", JwtLifetime = "1" };
            RefreshTokenSettings refreshTokenSettings = new RefreshTokenSettings { Lifetime = "1" };

            var response = new AuthenticateModel
            {
                AccessToken = JWTTokenProvider.GenerateAccessToken(authSettings, user),
                RefreshToken = token,
                ExpiresAt = DateTime.Now.AddDays(Int32.Parse(refreshTokenSettings.Lifetime))
            };

            var testedClient = PrepareClient200(response);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            var actualResponse = await testedClient.RefreshTokenAsync(token);

            //Assert 
            Assert.Equal(response.AccessToken, actualResponse.AccessToken);
            Assert.Equal(response.ExpiresAt, actualResponse.ExpiresAt);
            Assert.Equal(response.RefreshToken, actualResponse.RefreshToken);

        }
        [Fact]
        public async Task ForgotPasswordTest()
        {
            // Arrange
            ForgotPasswordRequest forgotPasswordRequest = new ForgotPasswordRequest()
                {Email = "karyna.dorosh@gmail.com"};

            var response = new HandleForgottenPasswordResponse(){Email = forgotPasswordRequest.Email};
            var testedClient = PrepareClient200(response);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            var actualResponse = await testedClient.HandleForgottenPassword(forgotPasswordRequest);
            Assert.Equal(forgotPasswordRequest.Email, actualResponse.Email);
        }
        [Fact]
        public async Task ResetPasswordTest()
        {
            // Arrange
            User user = new User
            {
                Login = "newTestLogin",
                CompanyId = Guid.NewGuid(),
                Email = "test@gmail.com",
                FirstName = "newtestFirstName",
                LastName = "newtestLastName",
                Role = "Worker"
            };
            var passwordResetSettings = new PasswordResetSettings
            {
                JwtLifetime = "1",
                Secret = "our secure secret for reset password",
                Audience = "http://localhsot/4200/",
                Issuer = "http://localhsot/5001/"
            };
            ResetPasswordRequest resetPasswordRequest = new ResetPasswordRequest()
            {
                ConfirmPassword = "qwerty123",
                Password = "qwerty123",
                Token = JWTTokenProvider.GenerateResetToken(passwordResetSettings, user)
            };
            UserResponse userResponse = new UserResponse()
            {
                CompanyId = user.CompanyId,
                Email = user.Email,
                FirstName = user.FirstName,
                Id = Guid.NewGuid(),
                LastName = user.LastName,
                Login = user.Login,
                Role = user.Role
            };
            var testedClient = PrepareClient200(userResponse);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            var actualResponse = await testedClient.ResetPasswordAsync(resetPasswordRequest);

            //Assert
            Assert.Equal(user.Email, actualResponse.Email);

        }
        [Fact]
        public async Task UpdatePassword_ShouldReturnUser()
        {
            // Arrange
            Guid id = new Guid();
            var request = new UpdatePasswordRequest
            {
                OldPassword = "veryoldpassword",
                NewPassword = "Newpassword"
            };
            var response = new UserResponse
            {
                Email = "usermail@gmail.com",
                FirstName = "UserName",
                LastName = "UserSurname",
                Id = id,
                Login = "userlogin"
            };

            var testedClient = PrepareClient200(response);
            testedClient.BaseUri = new Uri(TestUrl);

            // Act
            var ActualResponse = await testedClient.UpdatePasswordAsync(id, request);

            //Assert
            Assert.Equal(response.Email, ActualResponse.Email);
        }
    }
}
