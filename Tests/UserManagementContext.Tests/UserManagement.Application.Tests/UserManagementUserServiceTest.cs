using EmailNotifier.EmailNotification;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Threading.Tasks;
using UserManagement.API.Contracts.Requests;
using UserManagement.Application.Helpers;
using UserManagement.Application.Services;
using UserManagement.Domain.Models;
using UserManagement.Domain.Repositories;
using Xunit;

namespace UserManagement.Application.Tests
{
    public class UserManagementUserServiceTest
    {
        private readonly UserService _userService;
        private readonly Mock<IUserRepository> _userRepoMock = new Mock<IUserRepository>();
        private readonly Mock<IOptions<AuthenticationSettings>> _authMock = new Mock<IOptions<AuthenticationSettings>>();
        private readonly Mock<IOptions<PasswordResetSettings>> _resetMock = new Mock<IOptions<PasswordResetSettings>>();
        private readonly Mock<IMailSender> _mailSenderMock = new Mock<IMailSender>();
        private readonly Mock<IOptions<WebOptions>> _webMock = new Mock<IOptions<WebOptions>>();
        private readonly Mock<IOptions<RefreshTokenSettings>> _refreshTokenMock = new Mock<IOptions<RefreshTokenSettings>>();



        public UserManagementUserServiceTest()
        {
            WebOptions webOptions = new WebOptions() { FrontendAppUrl = "http://localhost:4200/" };
            RefreshTokenSettings refreshTokenSettings = new RefreshTokenSettings { Lifetime = "1"};

            AuthenticationSettings authSettings = new AuthenticationSettings() { Secret = "Our secret for tests is really big", JwtLifetime = "1" };
            PasswordResetSettings resetSettings = new PasswordResetSettings() { Secret = "Our secret password", JwtLifetime = "1",  Audience= "http://localhost:4200/", Issuer = "http://localhost:5001/" };

            _resetMock.Setup(ap => ap.Value).Returns(resetSettings);
            _authMock.Setup(ap => ap.Value).Returns(authSettings);
            _webMock.Setup(ap => ap.Value).Returns(webOptions);
            _refreshTokenMock.Setup(ap => ap.Value).Returns(refreshTokenSettings);

            _mailSenderMock.Setup(x => x.SendEmailAsync("test@gmail.com", "subject", "content"));
            _userService = new UserService(_userRepoMock.Object, _mailSenderMock.Object, _authMock.Object, _resetMock.Object, _webMock.Object, _refreshTokenMock.Object);
        }

        [Fact]
        public void GetByIdAnsyncWhenUserExistsTest()
        {
            //Arrange
            var userId = Guid.NewGuid();
            User userTest = new User { Login = "testLogin", CompanyId = Guid.NewGuid(), Email = "test@gmail.com" };
            userTest.SetId(userId);
            _userRepoMock.Setup(x => x.GetByIdAsync(userId)).ReturnsAsync(userTest);

            //Act     
            var user = _userService.GetByIdAsync(userId);

            //Assert
            Assert.Equal(userId, user.Result.Id);
            _userRepoMock.Verify(uRM => uRM.GetByIdAsync(userId), Times.Once);

        }

        [Fact]
        public void GetByIdAsyncReturnNothingWhenUserDoesNotExistTest()
        {
            // Arrange
            _userRepoMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>()))
                .Throws<UserDomainException>();

            // Act & Assert
            Assert.ThrowsAsync<UserDomainException>(() => _userService.GetByIdAsync(new Guid()));
            _userRepoMock.Verify(uRM => uRM.GetByIdAsync(It.IsAny<Guid>()), Times.Once);
        }

        [Theory]
        [InlineData("password")]
        public void CreateAsyncTestWithNormalPasswordTest(string password)
        {
            //Arrange
            Guid testId = Guid.NewGuid();

            User userTest = new User { Login = "testLogin", CompanyId = Guid.NewGuid(), Email = "test@gmail.com" };
            userTest.SetId(testId);
            _userRepoMock.Setup(x => x.CreateAsync(userTest))
                .ReturnsAsync(() =>
                {
                    return userTest;
                });

            //Act
            var user = _userService.CreateAsync(userTest, password);

            //Assert
            var result = Assert.IsType<User>(user.Result);
            Assert.Equal(testId, result.Id);
            _userRepoMock.Verify(uRM => uRM.CreateAsync(userTest), Times.Once);
        }

        [Fact]
        public void UpdateLoginFirstnameLastnameTest()
        {
            //Arrange
            Guid testId = Guid.NewGuid();

            User updateUserInfo = new User
            {
                Login = "testLogin",
                CompanyId = Guid.NewGuid(),
                Email = "test@gmail.com",
                FirstName = "testFirstName",
                LastName = "testLastName"
            };
            updateUserInfo.SetId(testId);
            User updateUserResult = new User
            {
                Login = "newTestLogin",
                CompanyId = Guid.NewGuid(),
                Email = "test@gmail.com",
                FirstName = "newtestFirstName",
                LastName = "newtestLastName"
            };
            updateUserResult.SetId(testId);

            _userRepoMock.Setup(x => x.UpdateAsync(updateUserInfo))
               .ReturnsAsync(() => updateUserResult);

            //Act
            var result = _userService.UpdateAsync(updateUserInfo);

            //Assert
            Assert.Equal(updateUserResult.Login, result.Result.Login);
            Assert.Equal(updateUserResult.LastName, result.Result.LastName);
            Assert.Equal(updateUserResult.FirstName, result.Result.FirstName);
            _userRepoMock.Verify(uRM => uRM.UpdateAsync(updateUserInfo), Times.Once);
        }

        [Fact]
        public void DeleteAsyncNormalTest()
        {
            // Arrange
            Guid testId = Guid.NewGuid();

            User userTest = new User { Login = "testLogin", CompanyId = Guid.NewGuid(), Email = "test@gmail.com" };
            userTest.SetId(testId);
            _userRepoMock.Setup(x => x.DeleteAsync(testId))
                .ReturnsAsync(() => userTest);

            // Act
            var user = _userService.DeleteAsync(testId);

            // Assert
            Assert.Equal(user.Result.Id, userTest.Id);
            _userRepoMock.Verify(uRM => uRM.DeleteAsync(testId), Times.Once);
        }

        [Fact]
        public void DeleteAsyncReturnsNullWhenNotDeletedTest()
        {
            // Arrange
            Guid testId = Guid.NewGuid();

            User userTest = new User { Login = "testLogin", CompanyId = Guid.NewGuid(), Email = "test@gmail.com" };
            userTest.SetId(testId);
            _userRepoMock.Setup(x => x.DeleteAsync(testId))
                .ReturnsAsync(() => null);

            // Act
            var user = _userService.DeleteAsync(testId);

            // Assert
            Assert.Null(user.Result);
            _userRepoMock.Verify(uRM => uRM.DeleteAsync(testId), Times.Once);
        }

        [Fact]
        public async Task ForgotPasswordTest()
        {
            // Arrange
            string email = "test@gmail.com";
            User userTest = new User
            {
                Login = "newTestLogin",
                CompanyId = Guid.NewGuid(),
                Email = email,
                FirstName = "newtestFirstName",
                LastName = "newtestLastName",
                Role = "Worker"
            };
            _userRepoMock.Setup(x => x.GetByEmailAsync(email)).ReturnsAsync(userTest);

            // Act
            await _userService.HandleForgottenPassword(email);

            // Assert
            _userRepoMock.Verify(uRM => uRM.GetByEmailAsync(email), Times.Once);
        }

        [Fact]
        public async Task ForgotPasswordTest_ShouldThrowException()
        {
            // Arrange

            string email = "test@gmail.com";
            _userRepoMock.Setup(x => x.GetByEmailAsync(It.IsAny<string>())).ReturnsAsync(() => null);

            // Act 
            // Assert
            await Assert.ThrowsAsync<UserDomainException>(() => _userService.HandleForgottenPassword(email));
            _userRepoMock.Verify(uRM => uRM.GetByEmailAsync(It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public async Task ResetPasswordTest()
        {
            //Arrange            
            Guid generatedId = Guid.NewGuid();

            PasswordResetSettings resetSettings = new PasswordResetSettings() { 
                Secret = "Our secret password", 
                JwtLifetime = "1", 
                Audience = "http://localhost:4200/",
                Issuer = "http://localhost:5001/" 
            };

            User user = new User
            {
                Login = "newTestLogin",
                CompanyId = Guid.NewGuid(),
                Email = "test@gmail.com",
                FirstName = "newtestFirstName",
                LastName = "newtestLastName",
                Role = "Worker"
            };
            user.SetId(generatedId);
             _userRepoMock.Setup(m => m.GetByIdAsync(generatedId)).ReturnsAsync(user);
            var token = JWTTokenProvider.GenerateResetToken(resetSettings, user);
            string password = "newPassword";

            //Act
            var result = await _userService.ResetPassword(token, password);

            //Assert
            _userRepoMock.Verify(mS => mS.GetByIdAsync(generatedId), Times.Once);
        }

        [Fact]
        public async Task UpdatePasswordTest()
        {
            //Arrange            
            Guid generatedId = Guid.NewGuid();            

            User user = new User
            {
                Login = "newTestLogin",
                CompanyId = Guid.NewGuid(),
                Email = "test@gmail.com",
                FirstName = "newtestFirstName",
                LastName = "newtestLastName",
                Role = "Manager"                
            };
            user.SetId(generatedId); 
            Cryptography.CreatePasswordHash("OldPassword", out byte[] passwordHash, out byte[] passwordSalt);
            user.SetPasswordItems(passwordHash, passwordSalt);

            _userRepoMock.Setup(m => m.GetByIdAsync(generatedId)).ReturnsAsync(user);

            string oldPassword = "OldPassword";
            string newPassword = "NewPassword";

            //Act
            var result = await _userService.UpdatePasswordAsync(oldPassword, newPassword, generatedId);

            //Assert
            _userRepoMock.Verify(mS => mS.GetByIdAsync(generatedId), Times.Once);
        }

        [Fact]
        public async void RefreshTokenTest()
        {
            //Arrange            
            Guid token = Guid.NewGuid();
            RefreshToken refreshToken = new RefreshToken()
            {
                Token = token,
                UserId = Guid.NewGuid(),
                ExpiresAt = DateTime.Now.AddDays(Int32.Parse(_refreshTokenMock.Object.Value.Lifetime))
            };
            User userTest = new User
            {
                Login = "newTestLogin",
                CompanyId = Guid.NewGuid(),
                Email = "test@gmail.com",
                FirstName = "newtestFirstName",
                LastName = "newtestLastName",
                Role = "Worker"
            };
            _userRepoMock.Setup(ur => ur.GetRefreshTokenAsync(token)).ReturnsAsync(refreshToken);
            _userRepoMock.Setup(ur => ur.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(userTest);

            var authenticateModel = new AuthenticateModel
            {
                AccessToken = JWTTokenProvider.GenerateAccessToken(_authMock.Object.Value, userTest),
                RefreshToken = refreshToken.Token,
                ExpiresAt = refreshToken.ExpiresAt
            };

            //Act
            var result = await _userService.RefreshTokenAsync(token);

            //Assert
            Assert.NotEqual(new Guid(), result.RefreshToken);
            Assert.Equal(authenticateModel.AccessToken, result.AccessToken);
            Assert.Equal(authenticateModel.ExpiresAt.Date, result.ExpiresAt.Date);
            _userRepoMock.Verify(ur => ur.GetRefreshTokenAsync(token), Times.Once);
            _userRepoMock.Verify(ur => ur.GetByIdAsync(It.IsAny<Guid>()), Times.Once);
        }

        [Fact]
        public async void AuthenticateTest()
        {
            //Arrange            
            Guid token = Guid.NewGuid();
            User userTest = new User
            {
                Login = "newTestLogin",
                CompanyId = Guid.NewGuid(),
                Email = "test@gmail.com",
                FirstName = "newtestFirstName",
                LastName = "newtestLastName",
                Role = "Worker"
            };

            Cryptography.CreatePasswordHash("password", out byte[] passwordHash, out byte[] passwordSalt);
            userTest.SetPasswordItems(passwordHash, passwordSalt);
            _userRepoMock.Setup(ur => ur.GetByLoginAsync(userTest.Login)).ReturnsAsync(userTest);

            var authenticateModel = new AuthenticateModel
            {
                AccessToken = JWTTokenProvider.GenerateAccessToken(_authMock.Object.Value, userTest),
                RefreshToken = token,
                ExpiresAt = DateTime.Now.AddDays(Int32.Parse(_refreshTokenMock.Object.Value.Lifetime))
            };

            //Act
            var result = await _userService.AuthenticateAsync(userTest.Login, "password");

            //Assert
            Assert.Equal(authenticateModel.AccessToken, result.AccessToken);
            Assert.NotEqual(new Guid(), result.RefreshToken);
            _userRepoMock.Verify(ur => ur.GetByLoginAsync(userTest.Login), Times.Once);
        }
    }
}
