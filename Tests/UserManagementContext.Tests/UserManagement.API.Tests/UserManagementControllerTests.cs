using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using UserManagement.API.Contracts.Requests;
using UserManagement.API.Contracts.Responses;
using UserManagement.API.Controllers;
using UserManagement.Application.Helpers;
using UserManagement.Domain.Models;
using UserManagement.Domain.Services;
using Xunit;

namespace UserManagement.API.Tests
{
    public class UserManagementControllerTests
    {
        private const string Worker = nameof(Worker);
        private readonly Guid _companyId = Guid.NewGuid();
        #region testdata
        private IReadOnlyCollection<User> GetTestModels()
        {
            var userId = Guid.NewGuid();
            User userTest1 = new User { Login = "testLogin1", CompanyId = _companyId, Email = "test@gmail.com", Role = Worker };
            User userTest2 = new User { Login = "testLogin2", CompanyId = _companyId, Email = "test@gmail.com", Role = Worker };

            List<User> sample = new List<User>();
            sample.Add(userTest1);
            sample.Add(userTest2);
            return new ReadOnlyCollection<User>(sample);
        }

        private IReadOnlyCollection<RegisterRequest> GetRegisterTestModels()
        {
            RegisterRequest userTest1 = new RegisterRequest { FirstName = "Test1", LastName = "Last1", Login = "testLogin1", Password = "qwerty1234", Role = Role.Manager };
            RegisterRequest userTest2 = new RegisterRequest { FirstName = "Test2", LastName = "Last2", Login = "testLogin10", Password = "qwerty1234", Role = Role.Manager };
            RegisterRequest FakeUser = new RegisterRequest { FirstName = "dublicate", LastName = "dublicate", Login = "testLogin2", Password = "qwerty1234", Role = Role.Manager };

            List<RegisterRequest> sample = new List<RegisterRequest>
            {
                userTest1,
                userTest2,
                FakeUser
            };
            return new ReadOnlyCollection<RegisterRequest>(sample);
        }

        private IReadOnlyCollection<UserResponse> GetResponseTestModels()
        {
            var userId = Guid.NewGuid();
            UserResponse userTest1 = new UserResponse { Id = userId, FirstName = "Test1", LastName = "Last1", Login = "testLogin1", Email = "test@gmail.com" };
            UserResponse userTest2 = new UserResponse { Id = userId, FirstName = "Test2", LastName = "Last2", Login = "testLogin2", Email = "test2@gmail.com" };

            List<UserResponse> sample = new List<UserResponse>
            {
                userTest1,
                userTest2
            };
            return new ReadOnlyCollection<UserResponse>(sample);

        }

        #endregion
        [Fact]
        public async Task GetAllAsyncTest()
        {
            //Arrange
            var mockMapper = new Mock<IMapper>();
            mockMapper.Setup(m => m.Map<IReadOnlyCollection<UserResponse>>(It.IsAny<IReadOnlyCollection<User>>())).Returns(GetResponseTestModels());

            var mockService = new Mock<IUserService>();
            mockService.Setup(m => m.GetAllAsync()).Returns(Task.FromResult(GetTestModels()));

            var controller = new UserController(mockService.Object, mockMapper.Object);

            //Act
            //Actually variable result has inner result of GetResponseTestModels()
            var result = await controller.GetAllAsync();

            IActionResult actionResult = result.Result;
            var okResult = result.Result as OkObjectResult;
            var list = okResult.Value as UsersCollectionResponse;

            //Assert
            Assert.Equal(GetResponseTestModels().Count, list.UsersCollection.Count);
            mockService.Verify(mS => mS.GetAllAsync(), Times.Once);
        }

        [Fact]
        public async Task GetUsersByCompanyAndRole_ShouldReturnCollection()
        {
            //Arrange
            var mockMapper = new Mock<IMapper>();
            mockMapper.Setup(m => m.Map<IReadOnlyCollection<UserResponse>>(It.IsAny<IReadOnlyCollection<User>>())).Returns(GetResponseTestModels());

            var mockService = new Mock<IUserService>();
            mockService.Setup(m => m.GetAllByCompanyAndRoleAsync(_companyId, Worker)).Returns(Task.FromResult(GetTestModels()));

            var controller = new UserController(mockService.Object, mockMapper.Object);

            //Act
            //Actually variable result has inner result of GetResponseTestModels()
            var result = await controller.GetAllByCompanyAndRoleAsync(_companyId, Worker);

            IActionResult actionResult = result.Result;
            var okResult = result.Result as OkObjectResult;
            var list = okResult.Value as UsersCollectionResponse;

            //Assert
            Assert.Equal(GetResponseTestModels().Count, list.UsersCollection.Count);
            mockService.Verify(mS => mS.GetAllByCompanyAndRoleAsync(_companyId, Worker), Times.Once);
        }

        [Fact]
        public async Task RegistrationTest()
        {
            //Arrange
            var mockMapper = new Mock<IMapper>();
            mockMapper.Setup(m => m.Map<User>(It.IsAny<RegisterRequest>())).Returns(GetTestModels().Last());

            var mockService = new Mock<IUserService>();
            mockService.Setup(m => m.CreateAsync(It.IsAny<User>(), It.IsAny<string>())).Returns(Task.FromResult<User>(GetTestModels().Last()));

            var controller = new UserController(mockService.Object, mockMapper.Object);

            RegisterRequest user = GetRegisterTestModels().Last();

            //Act
            var result = await controller.RegisterAsync(user);

            //Assert
            Assert.IsType<OkObjectResult>(result);
            mockService.Verify(mS => mS.CreateAsync(It.IsAny<User>(), It.IsAny<string>()), Times.Once);
            mockMapper.Verify(mM => mM.Map<User>(It.IsAny<RegisterRequest>()), Times.Once);
        }

        [Fact]
        public async Task GetByIdAsync_ExistingEntity_Test()
        {
            // Arrange
            var mockService = new Mock<IUserService>();
            var mockMapper = new Mock<IMapper>();

            Guid requestedId = Guid.NewGuid();
            var firstName = "James";
            var lastName = "Wilson";

            var user = new User() { FirstName = firstName, LastName = lastName };
            user.SetId(requestedId);
            var mappedUser = new UserResponse() { Id = requestedId, FirstName = firstName, LastName = lastName, Login = "login", Email = "test@gmail.com" };

            mockService.Setup(x => x.GetByIdAsync(requestedId)).ReturnsAsync(user);
            mockMapper.Setup(x => x.Map<UserResponse>(user)).Returns(mappedUser);

            var controller = new UserController(mockService.Object, mockMapper.Object);

            // Act
            var result = await controller.GetByIdAsync(requestedId);

            // Assert
            Assert.IsType<OkObjectResult>(result.Result);
            var response = result.Result as OkObjectResult;
            var value = response.Value as UserResponse;
            Assert.Equal(firstName, value.FirstName);
            Assert.Equal(lastName, value.LastName);
            mockService.Verify(mS => mS.GetByIdAsync(requestedId), Times.Once);
            mockMapper.Verify(mM => mM.Map<UserResponse>(user), Times.Once);
        }

        [Fact]
        public async Task AuthenticateTest()
        {
            //Arrange
            var mockService = new Mock<IUserService>();
            Guid token = Guid.NewGuid();
            AuthenticationSettings authSettings = new AuthenticationSettings() { Secret = "Our secret for tests is really big", JwtLifetime = "1" };
            RefreshTokenSettings refreshTokenSettings = new RefreshTokenSettings { Lifetime = "1" };
            User userTest = new User
            {
                Login = "newTestLogin",
                CompanyId = Guid.NewGuid(),
                Email = "test@gmail.com",
                FirstName = "newtestFirstName",
                LastName = "newtestLastName",
                Role = "Worker"
            };

            var authenticateModel = new AuthenticateModel
            {
                AccessToken = JWTTokenProvider.GenerateAccessToken(authSettings, userTest),
                RefreshToken = token,
                ExpiresAt = DateTime.Now.AddDays(Int32.Parse(refreshTokenSettings.Lifetime))
            };
            mockService.Setup(m => m.AuthenticateAsync(It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult<AuthenticateModel>(authenticateModel));
            var mockMapper = new Mock<IMapper>();
            mockMapper.Setup(m => m.Map<AuthenticateResponse>(It.IsAny<AuthenticateModel>())).Returns(new AuthenticateResponse()
            {
                ExpiresAt = authenticateModel.ExpiresAt,
                AccessToken = authenticateModel.AccessToken,
                RefreshToken = authenticateModel.RefreshToken
            });
            var controller = new UserController(mockService.Object, mockMapper.Object);
            
            //Act
            var result = await controller.AuthenticateAsync(new AuthenticateRequest() { Login = "Test1", Password = "qwerty1234" });

            //Assert
            Assert.IsType<OkObjectResult>(result);
            if (result is OkObjectResult response)
            {
                var value = response.Value as AuthenticateResponse;
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }
                Assert.Equal(authenticateModel.ExpiresAt, value.ExpiresAt);
                Assert.Equal(authenticateModel.AccessToken, value.AccessToken);
                Assert.Equal(authenticateModel.RefreshToken, value.RefreshToken);
            }
            mockService.Verify(mS => mS.AuthenticateAsync(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }
        [Fact]
        public async Task ForgotPasswordTest()
        {
            //Arrange
            var mockService = new Mock<IUserService>();
            ForgotPasswordRequest forgotPasswordRequqest = new ForgotPasswordRequest
            {
                Email = "test@gmail.com"
            };
            mockService.Setup(m => m.HandleForgottenPassword(forgotPasswordRequqest.Email)).Returns(Task.FromResult(forgotPasswordRequqest.Email));

            var controller = new UserController(mockService.Object, null);

            //Act
            var result = await controller.HandleForgottenPasswordAsync(forgotPasswordRequqest);

            //Assert
            Assert.IsType<OkObjectResult>(result.Result);
            if (result.Result is OkObjectResult response)
            {
                var value = response.Value as HandleForgottenPasswordResponse;
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }
                Assert.Equal(forgotPasswordRequqest.Email, value.Email);
            }
            mockService.Verify(mS => mS.HandleForgottenPassword(forgotPasswordRequqest.Email), Times.Once);
        }

        [Fact]
        public async Task UpdateTest()
        {
            //Arrange
            var mockMapper = new Mock<IMapper>();
            mockMapper.Setup(m => m.Map<User>(It.IsAny<UpdateRequest>())).Returns(GetTestModels().Last());

            var mockService = new Mock<IUserService>();
            mockService.Setup(m => m.UpdateAsync(It.IsAny<User>())).Returns(Task.FromResult(GetTestModels().Last()));

            var controller = new UserController(mockService.Object, mockMapper.Object);

            //Act
            var result = await controller.UpdateAsync(Guid.NewGuid(),new UpdateRequest() { 
                FirstName ="Tester",LastName="LastName",Login="TestLogin", Email="test@gmail.com"
            });

            //Assert
            Assert.IsType<OkObjectResult>(result.Result);
            mockService.Verify(mS => mS.UpdateAsync(It.IsAny<User>()), Times.Once);
            mockMapper.Verify(mM => mM.Map<User>(It.IsAny<UpdateRequest>()), Times.Once);
        }

        [Fact]
        public async Task DeleteTest()
        {
            //Arrange
            var mockService = new Mock<IUserService>();
            mockService.Setup(m => m.DeleteAsync(It.IsAny<Guid>())).Returns(Task.FromResult(GetTestModels().Last()));

            var controller = new UserController(mockService.Object, null);

            //Act
            var result = await controller.DeleteAsync(Guid.NewGuid());
            
            //Assert
            Assert.IsType<OkObjectResult>(result.Result);
            mockService.Verify(mS => mS.DeleteAsync(It.IsAny<Guid>()), Times.Once);
        }

        [Fact]
        public async Task ResetPasswordTest()
        {
            //Arrange
            var mockService = new Mock<IUserService>();
            var passwordResetSettings = new PasswordResetSettings
            {
                JwtLifetime = "1",
                Secret = "our secure secret for reset password",
                Audience = "http://localhsot/4200/",
                Issuer = "http://localhsot/5001/"
            };
            User user = new User
            {
                Login = "newTestLogin",
                CompanyId = Guid.NewGuid(),
                Email = "test@gmail.com",
                FirstName = "newtestFirstName",
                LastName = "newtestLastName",
                Role = "Worker"
            };
            UserResponse userResponse = new UserResponse()
            {
                Login = "newTestLogin",
                CompanyId = Guid.NewGuid(),
                Email = "test@gmail.com",
                FirstName = "newtestFirstName",
                LastName = "newtestLastName",
                Role = "Worker",
                Id = Guid.NewGuid()
            };
            var mockMapper = new Mock<IMapper>();
            mockMapper.Setup(m => m.Map<UserResponse>(It.IsAny<User>())).Returns(userResponse);

            var token = JWTTokenProvider.GenerateResetToken(passwordResetSettings, user);
            ResetPasswordRequest resetPasswordRequest = new ResetPasswordRequest
            {
                Token = token,
                Password = "newPassword",
                ConfirmPassword = "newPassword"
            };
            mockService.Setup(m => m.ResetPassword(resetPasswordRequest.Token, resetPasswordRequest.Password)).Returns(Task.FromResult(user));

            var controller = new UserController(mockService.Object, mockMapper.Object);

            //Act
            var result = await controller.ResetPasswordAsync(resetPasswordRequest);

            //Assert
            Assert.IsType<OkObjectResult>(result.Result);
            mockService.Verify(mS => mS.ResetPassword(resetPasswordRequest.Token, resetPasswordRequest.Password), Times.Once);
        }

        [Fact]
        public async Task UpdatePasswordTest()
        {
            //Arrange
            var mockService = new Mock<IUserService>();
            User user = new User
            {
                Login = "newTestLogin",
                CompanyId = Guid.NewGuid(),
                Email = "test@gmail.com",
                FirstName = "newtestFirstName",
                LastName = "newtestLastName",
                Role = "Worker"
            };
            UpdatePasswordRequest updatePasswordRequest = new UpdatePasswordRequest
            {
                OldPassword = "oldPassword",
                NewPassword = "newPassword"
            };
            Guid id = new Guid();
            mockService.Setup(m => m.UpdatePasswordAsync(updatePasswordRequest.OldPassword, updatePasswordRequest.NewPassword, id)).Returns(Task.FromResult(user));

            var controller = new UserController(mockService.Object, null);

            //Act
            var result = await controller.UpdatePasswordAsync(id, updatePasswordRequest);

            //Assert
            mockService.Verify(mS => mS.UpdatePasswordAsync(updatePasswordRequest.OldPassword, updatePasswordRequest.NewPassword, id), Times.Once);
        }

        [Fact]
        public async void RefreshTokenTests()
        {
            //Arrange
            var mockService = new Mock<IUserService>();
            Guid token = Guid.NewGuid();
            User userTest = new User
            {
                Login = "newTestLogin",
                CompanyId = Guid.NewGuid(),
                Email = "test@gmail.com",
                FirstName = "newtestFirstName",
                LastName = "newtestLastName",
                Role = "Worker"
            };

            AuthenticationSettings authSettings = new AuthenticationSettings() { Secret = "Our secret for tests is really big", JwtLifetime = "1" };
            RefreshTokenSettings refreshTokenSettings = new RefreshTokenSettings { Lifetime = "1" };
            var authenticateModel = new AuthenticateModel
            {
                AccessToken = JWTTokenProvider.GenerateAccessToken(authSettings, userTest),
                RefreshToken = token,
                ExpiresAt = DateTime.Now.AddDays(Int32.Parse(refreshTokenSettings.Lifetime))
            };
            mockService.Setup(m => m.RefreshTokenAsync(token)).Returns(Task.FromResult(authenticateModel));
            var mockMapper = new Mock<IMapper>();
            mockMapper.Setup(m => m.Map<AuthenticateResponse>(It.IsAny<AuthenticateModel>())).Returns(new AuthenticateResponse()
            {
                ExpiresAt = authenticateModel.ExpiresAt,
                AccessToken = authenticateModel.AccessToken,
                RefreshToken = authenticateModel.RefreshToken
            });

            var controller = new UserController(mockService.Object, mockMapper.Object);

            //Act
            var result = await controller.RefreshTokenAsync(token);
            Assert.IsType<OkObjectResult>(result);
            if (result is OkObjectResult response)
            {
                var value = response.Value as AuthenticateResponse;
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }
                Assert.Equal(authenticateModel.ExpiresAt, value.ExpiresAt);
                Assert.Equal(authenticateModel.AccessToken, value.AccessToken);
                Assert.Equal(authenticateModel.RefreshToken, value.RefreshToken);
            }
            //Assert
            mockService.Verify(mS => mS.RefreshTokenAsync(token), Times.Once);
        }

    }
}

