using System.Collections.Generic;
using UserManagement.API.Contracts.Requests;
using UserManagement.API.Validators;
using UserManagement.Domain.Models;
using Xunit;

namespace UserManagement.API.Tests
{
    // TODO: add role validations tests
    public class ValidationTests
    {
        [Theory, MemberData(nameof(TestCasesForRegistration))]
        public void RegisterRequestsTest(RegisterRequest request, bool expectedResult)
        {
            // Arrange
            var registerRequestValidator = new RegisterRequestValidator();

            // Act
            var validationResult = registerRequestValidator.Validate(request);
            bool actualResult = validationResult.IsValid;

            // Assert
            Assert.Equal(expectedResult, actualResult);
        }

        public static IEnumerable<object[]> TestCasesForRegistration
        {
            get
            {
                yield return new object[]
                {
                    new RegisterRequest()
                    {
                        Login = "BestWorker",
                        FirstName = "James",
                        LastName = "Wilson",
                        Password = "012345678",
                        Email = "test@gmail.com",
                        Role = Role.Worker
                    }, true
                };
                yield return new object[]
                {
                    new RegisterRequest()
                    {
                        Login = "MyPasswordIsTooShort",
                        FirstName = "James",
                        LastName = "Wilson",
                        Password = "1234",
                        Email = "test@gmail.com",
                        Role = Role.Worker
                    }, false
                };
                yield return new object[]
                {
                    new RegisterRequest()
                    {
                        Login = "Log",
                        FirstName = "James",
                        LastName = "Wilson",
                        Password = "MyLoginIsTooShort",
                        Email = "test@gmail.com",
                        Role = Role.Worker
                    }, false
                };
                yield return new object[]
                {
                    new RegisterRequest()
                    {
                        Login = "BestWorker",
                        FirstName = "James",
                        LastName = "Wilson",
                        Email = "test@gmail.com",
                        Password = "",
                        Role =Role.Worker
                    }, false
                };
                yield return new object[]
                {
                    new RegisterRequest()
                    {
                        Login = "BestWorker",
                        FirstName = "James",
                        LastName = "",
                        Password = "12345678",
                        Email = "test@gmail.com",
                        Role =Role.Worker
                    }, false
                };
                yield return new object[]
                {
                    new RegisterRequest()
                    {
                        Login = "BestWorker",
                        FirstName = "",
                        LastName = "123",
                        Password = "12345678",
                        Email = "test@gmail.com",
                        Role =Role.Worker
                    }, false
                };
                yield return new object[]
                {
                    new RegisterRequest()
                    {
                        Login = "",
                        FirstName = "James",
                        LastName = "123",
                        Password = "12345678",
                        Email = "test@gmail.com",
                        Role =Role.Worker
                    }, false
                };
                yield return new object[]
                {
                    new RegisterRequest()
                    {
                        Login = "  ",
                        FirstName = "James",
                        LastName = "123",
                        Password = "12345678",
                        Email = "test@gmail.com",
                        Role =Role.Worker
                    }, false
                };
                yield return new object[]
                {
                    new RegisterRequest()
                    {
                        Login = "BestWorker",
                        LastName = "123",
                        Password = "12345678",
                        Email = "test@gmail.com",
                        Role =Role.Worker
                    }, false
                };
                yield return new object[]
                {
                    new RegisterRequest()
                    {
                        Login = "BestWorker",
                        FirstName = "123",
                        Password = "12345678",
                        Email = "test@gmail.com",
                        Role = Role.Worker
                    }, false
                };
            }
        }

        [Theory, MemberData(nameof(TestCasesForUpdate))]
        public void UpdateRequestsTest(UpdateRequest request, bool expectedResult)
        {
            // Arrange
            var updateRequestValidator = new UpdateRequestValidator();

            // Act
            var validationResult = updateRequestValidator.Validate(request);
            bool actualResult = validationResult.IsValid;

            // Assert
            Assert.Equal(expectedResult, actualResult);
        }

        public static IEnumerable<object[]> TestCasesForUpdate
        {
            get
            {
                yield return new object[]
                {
                    new UpdateRequest()
                    {
                        Login = "BestWorker",
                        FirstName = "James",
                        LastName = "Wilson",
                        Email = "test@gmail.com",
                    }, true
                };
                yield return new object[]
                {
                    new UpdateRequest()
                    {
                        Login = "Log",
                        FirstName = "James",
                        LastName = "Wilson",
                        Email = "test@gmail.com",
                    }, false
                };
                yield return new object[]
                {
                    new UpdateRequest()
                    {
                        Login = "BestWorker",
                        FirstName = "James",
                        LastName = "Wilson",
                        Email = "test@gmail.com"
                    }, true
                };
                yield return new object[]
                {
                    new UpdateRequest()
                    {
                        Login = "BestWorker",
                        FirstName = "James",
                        LastName = "",
                        Email = "test@gmail.com"
                    }, false
                };
                yield return new object[]
                {
                    new UpdateRequest()
                    {
                        Login = "BestWorker",
                        FirstName = "",
                        LastName = "123",
                        Email = "test@gmail.com"
                    }, false
                };
                yield return new object[]
                {
                    new UpdateRequest()
                    {
                        Login = "",
                        FirstName = "James",
                        LastName = "123",
                        Email = "test@gmail.com"
                    }, false
                };
                yield return new object[]
                {
                    new UpdateRequest()
                    {
                        Login = "  ",
                        FirstName = "James",
                        LastName = "123",
                        Email = "test@gmail.com"
                    }, false
                };
            }
        }
        public static IEnumerable<object[]> TestCasesForResetModel
        {
            get
            {
                yield return new object[]
                {
                    new ResetPasswordRequest()
                    {
                        ConfirmPassword = "newpassword",
                        Password = "newpassword",
                        Token = "token"
                    }, true
                };
                yield return new object[]
                {
                     new ResetPasswordRequest()
                    {
                        ConfirmPassword = "newpassword",
                        Password = "",
                        Token = "token"
                    }, false
                };
                yield return new object[]
                {
                     new ResetPasswordRequest()
                    {
                        ConfirmPassword = "",
                        Password = "newpassword",
                        Token = "token"
                    }, false
                };
                yield return new object[]
                {
                     new ResetPasswordRequest()
                    {
                        ConfirmPassword = "newpassword",
                        Password = "NEWPASSWORD",
                        Token = "token"
                    }, false 
                };
                yield return new object[]
                {
                     new ResetPasswordRequest()
                    {
                        ConfirmPassword = "newpassword",
                        Password = "newpassword",
                        Token = ""
                    }, false
                };
                yield return new object[]
                {
                     new ResetPasswordRequest()
                    {
                        ConfirmPassword = "newpassword",
                        Password = "newpassword",
                        Token = null
                    }, false
                };
                yield return new object[]
                {
                     new ResetPasswordRequest()
                    {
                        ConfirmPassword = "",
                        Password = "",
                        Token = ""
                    }, false 
                };
                yield return new object[]
               {
                     new ResetPasswordRequest()
                    {
                        ConfirmPassword = "qwe",
                        Password = "qwe",
                        Token = "token"
                    }, false
               };
            }
        }

        [Theory, MemberData(nameof(TestCasesForResetModel))]
        public void ResetPasswordVAlidationTest(ResetPasswordRequest request, bool expectedResult)
        {
            // Arrange
            var resetPasswordValidator = new ResetPasswordValidator();

            // Act
            var validationResult = resetPasswordValidator.Validate(request);
            bool actualResult = validationResult.IsValid;

            // Assert
            Assert.Equal(expectedResult, actualResult);
        }
        public static IEnumerable<object[]> TestCasesForUpdateModel
        {
            get
            {
                yield return new object[]
                {
                    new UpdatePasswordRequest()
                    {
                        OldPassword="itwasyouroldpass",
                        NewPassword="123456789"
                    }, true
                };
                yield return new object[]
                {
                     new UpdatePasswordRequest()
                    {
                        OldPassword="",
                        NewPassword="123456789"
                    }, false
                };
                yield return new object[]
                {
                     new UpdatePasswordRequest()
                    {
                        OldPassword="123456789",
                        NewPassword=""
                    }, false
                };               
            }
        }

        [Theory, MemberData(nameof(TestCasesForUpdateModel))]
        public void UpdatePasswordVAlidationTest(UpdatePasswordRequest request, bool expectedResult)
        {
            // Arrange
            var updatePasswordValidator = new UpdatePasswordValidator();

            // Act
            var validationResult = updatePasswordValidator.Validate(request);
            bool actualResult = validationResult.IsValid;

            // Assert
            Assert.Equal(expectedResult, actualResult);
        }
    }
}
