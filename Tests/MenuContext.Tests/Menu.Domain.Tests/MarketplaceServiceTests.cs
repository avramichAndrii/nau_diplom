﻿using Menu.Domain.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xunit;

namespace Menu.Domain.Tests
{
    public class MarketplaceServiceTests
    {
        [Fact]
        public async Task GetByIdAsync_ShouldReturnOffer_WhenOfferExistsTest()
        {
            //Arrange
            var menuRepoMock = new Mock<IMenuRepository>();
            var testId = Guid.NewGuid();
            var offersTest = new List<MenuModel>
            {
                new MenuModel(Guid.NewGuid(),"Cakes", "Ordinary menu", testId, new byte[456], true) ,
                new MenuModel(Guid.NewGuid(),"Sushi", "East menu", testId, new byte[456], true)
            };
        
            menuRepoMock.Setup(x => x.GetByProviderIdFromMarketplaceAsync(testId))
                                                         .ReturnsAsync(offersTest);
            var marketplaceService = new MarketplaceService(menuRepoMock.Object);

            //Act
            var result = await marketplaceService.GetByProviderIdAsync(testId);

            //Assert
            Assert.Equal(offersTest.Count, result.Count);
        }

        [Fact]
        public async Task GetByIdAsync_ShouldReturnNull_WhenOfferNotExistTest()
        {
            //Arrange
            Guid testId = Guid.NewGuid();
            Task<IReadOnlyCollection<MenuModel>> taskIReadOnly = Task.FromResult<IReadOnlyCollection<MenuModel>>(new List<MenuModel>().AsReadOnly());
            var menuRepoMock = new Mock<IMenuRepository>();
            menuRepoMock.Setup(x => x.GetByProviderIdFromMarketplaceAsync(testId)).Returns(taskIReadOnly);
            var marketplaceService = new MarketplaceService(menuRepoMock.Object);

            //Act
            var result = await marketplaceService.GetByProviderIdAsync(testId);
            Assert.Empty(result);

        }
        [Fact]
        public async Task AddOnMarketplaceAsync_ShouldReturnMenuModel()
        {
            //Arrange
            Guid testId = Guid.NewGuid();
            var menuRepoMock = new Mock<IMenuRepository>();
            var offerTest = new MenuModel(testId, menuName: "Junk food", menuInfo: "more eat, more fat", providerCompanyId: Guid.NewGuid(), new byte[234], isInMarketplace: true);

            menuRepoMock.Setup(expression: x => x.UpdateIsInMarketplaceAsync(testId, false)).ReturnsAsync(value: offerTest);
            var marketplaceService = new MarketplaceService(menuRepository: menuRepoMock.Object);

            //Act
            MenuModel result = await marketplaceService.UpdateIsInMarketplaceAsync(menuId: testId, isInMarketplace: false);

            //Assert
            Assert.True(result.IsInMarketplace);
            Assert.Equal(testId, result.Id);

        }
        [Fact]
        public async Task AddOnMarketplaceAsync_ShouldThrowException()
        {
            //Arrange
            Guid testId = Guid.NewGuid();
            var menuRepoMock = new Mock<IMenuRepository>();

            menuRepoMock.Setup(x => x.UpdateIsInMarketplaceAsync(testId, true)).Returns(() => Task.FromResult<MenuModel>(null));
            var marketplaceService = new MarketplaceService(menuRepository: menuRepoMock.Object);

            //Act
            var result = await marketplaceService.UpdateIsInMarketplaceAsync(menuId: testId, isInMarketplace: true);

            //Assert
            Assert.Null(result);
        }
        #region testinfo
        private IReadOnlyCollection<MenuModel> TestModels()
        {
            return new ReadOnlyCollection<MenuModel>(new List<MenuModel>
            {
                new MenuModel(Guid.NewGuid(),"Cakes", "Ordinary menu", Guid.NewGuid(), new byte[256], true) ,
                new MenuModel(Guid.NewGuid(),"Sushi", "East menu", Guid.NewGuid(), new byte[345], true)
            });
        }
        #endregion
        [Fact]
        public async Task GetAll_ShouldReturn2EntitiesTest()
        {
            // Arrange
            var menuRepoMock = new Mock<IMenuRepository>();
            menuRepoMock.Setup(ms => ms.GetAllOffersAsync())
                .ReturnsAsync(TestModels());
            var service = new MarketplaceService(menuRepoMock.Object);

            // Act
            var result = await service.GetAllOffersAsync();

            // Assert
            Assert.Equal(TestModels().Count, result.Count);
        }

        [Fact]
        public async Task GetAllAsync_ShouldReturnEmptyListTest()
        {
            // Arrange
            var menuRepoMock = new Mock<IMenuRepository>();
            menuRepoMock.Setup(ms => ms.GetAllOffersAsync())
                        .ReturnsAsync(new List<MenuModel>());
            var service = new MarketplaceService(menuRepoMock.Object);

            // Act
            IReadOnlyCollection<MenuModel> result = await service.GetAllOffersAsync();

            // Assert
            Assert.Empty(result);
        }
    }
}
