﻿using Menu.Domain;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Menu.Domain.Tests
{
    public class MenuModelTests
    {
        [Theory]
        [MemberData(nameof(InternalModelTestsData.TestData), MemberType = typeof(InternalModelTestsData))]
        public void GetPreparationTime_ShouldCalculateCorrectly_WhenDishesExistsOrNot(MenuModel menuModel,TimeSpan testPreparationTime)
        {
            //Arrange

            //Act
            TimeSpan preparationTime = menuModel.GetMenuPreparationTime();
            //Assert
            Assert.Equal(preparationTime, testPreparationTime);
        }
    }
}
