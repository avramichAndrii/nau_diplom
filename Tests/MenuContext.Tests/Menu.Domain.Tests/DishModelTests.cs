﻿using Menu.Domain;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace Menu.Domain.Tests
{
    public class DishTests
    {
        Random rnd = new Random();
        [Fact]
        public void SetCorrectPrice()
        {
            MenuModel model = new MenuModel(Guid.NewGuid(),"","", Guid.NewGuid(), null);
            DishModel testDish = new DishModel(model.Id, "", "", TimeSpan.FromMinutes(24), new List<string>(), Guid.NewGuid(), null);
            int price = rnd.Next();
            testDish.SetPrice(price, "My currency");
            Assert.Equal(price, testDish.Price.Amount);
        }
        
        [Fact]
        public void SetCorrectPFC()
        {
            MenuModel model = new MenuModel(Guid.NewGuid(),"", "", Guid.NewGuid(), null);
            DishModel testDish = new DishModel(model.Id, "", "", TimeSpan.FromMinutes(24), new List<string>(), Guid.NewGuid(), null);
            int caloric = rnd.Next();
            int carbohydrates = rnd.Next();
            int fats = rnd.Next();
            int proteins = rnd.Next();

            testDish.SetNutritionalValue(caloric, carbohydrates, fats, proteins);
            Assert.Equal(caloric, testDish.DishPFC.CaloricContent);
            Assert.Equal(carbohydrates, testDish.DishPFC.Carbohydrates);
            Assert.Equal(fats, testDish.DishPFC.Fats);
            Assert.Equal(proteins, testDish.DishPFC.Proteins);
        }

    }
}
