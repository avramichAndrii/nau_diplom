﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Menu.Domain.Tests
{
    public class InternalModelTestsData
    {
        public static readonly MenuModel MenuModelWithoutDishes = new MenuModel(Guid.NewGuid(), "HappyMeal", "hamburgers", Guid.NewGuid(), null);
    
        public static List<TimeSpan> Times { get; set; }
        public static MenuModel MenuModelWithDishes {
            get
            {
                MenuModel menuModel = new MenuModel(Guid.NewGuid(), "HappyMeal", "hamburgers", Guid.NewGuid(), null);
                Random rnd = new Random();
                Times = new List<TimeSpan>()
                {
                    new TimeSpan(0, 0, rnd.Next(0, 60), 0, 0),
                    new TimeSpan(0, 0, rnd.Next(0, 60), 0, 0),
                    new TimeSpan(0, 0, rnd.Next(0, 60), 0, 0)
                };
                menuModel.DishList = new List<DishModel>()
                {
                    new DishModel(Guid.NewGuid(), "", "", Times[0], new List<string>(), Guid.NewGuid(), null),
                    new DishModel(Guid.NewGuid(), "", "", Times[1], new List<string>(), Guid.NewGuid(), new byte[7894]),
                    new DishModel(Guid.NewGuid(), "", "", Times[2], new List<string>(), Guid.NewGuid(), new byte[841])
                };
                return menuModel;
            } 
        }

        public static IEnumerable<object[]> TestData
        {
            get
            {
                yield return new object[]
                {
                    MenuModelWithoutDishes,
                    TimeSpan.Zero
                };
                yield return new object[]
                {
                    MenuModelWithDishes,
                    Times.Max()
                };
            }
        }
    }
}
