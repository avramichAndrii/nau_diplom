using Menu.API;
using Menu.API.Contracts.Requests;
using Menu.API.Contracts.Responses;
using Menu.API.Controllers;
using Menu.Domain;
using Moq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Menu.Controllers.Test
{
    public class DishControllerTest
    {
        private readonly NutritionalValue TestPFC = new NutritionalValue(1, 1, 1, 1);
        private readonly Price TestPrice = new Price(12, "USD");

        
        
        [Theory,MemberData(nameof(TestDish))]
        public async Task GetById_ExistingEntity_Test(string name,string category,TimeSpan time,List<string> ingridients)
        {
            // Arrange
            var mockService = new Mock<IDishDomainService>();

            Guid requestedId = Guid.NewGuid();
            Guid MenuGuid = Guid.NewGuid();
            string testName = "Cakes";

            var modelToCreate = new DishModel(requestedId, name, category,time, ingridients, MenuGuid, null);
            modelToCreate.SetPrice(TestPrice.Amount, TestPrice.Currency);
            modelToCreate.SetNutritionalValue(TestPFC.CaloricContent, TestPFC.Carbohydrates, TestPFC.Fats, TestPFC.Proteins);


            mockService.Setup(x => x.GetByIdAsync(requestedId)).ReturnsAsync(modelToCreate);

            var controller = new DishController(null, mockService.Object);

            // Act
            var result = await controller.GetAsync(requestedId);

            // Assert
            var response = Assert.IsType<DishResponse>(result.Value);
            Assert.Equal(testName, response.DishName);
        }
        public static IEnumerable<object[]> TestDish
        {
            get
            {
                yield return new object[] { "Cakes", "category", TimeSpan.FromSeconds(1), new List<string>(){"some ingredients"} };
            }
        }

        [Fact]
        public async Task GetAll_ShouldReturn_LengthOfTestModels_Items()
        {
            // Arrange
            var mockService = new Mock<IDishDomainService>();
            mockService.Setup(ms => ms.GetAllAsync()).ReturnsAsync(GetTestModels());

            var controller = new DishController(null, mockService.Object);

            // Act
            var result = await controller.GetAll();

            // Assert
            var list = result.Value;
            Assert.Equal(GetTestModels().Count, list.DishesCollection.Count);
        }
        #region testdata
        private IReadOnlyCollection<DishModel> GetTestModels()
        {
            var temp = Guid.NewGuid();
            List<string> dishComposition = new List<string>()
            {
                "some ingredients"
            };
            var FirstDishTest = new DishModel(Guid.NewGuid(), "Cakes", "category", TimeSpan.FromSeconds(1), dishComposition, temp, null);
            FirstDishTest.SetPrice(TestPrice.Amount, TestPrice.Currency);
            FirstDishTest.SetNutritionalValue(TestPFC.CaloricContent, TestPFC.Carbohydrates, TestPFC.Fats, TestPFC.Proteins);
            var SecondDishTest = new DishModel(Guid.NewGuid(), "Pizza", "Category", TimeSpan.FromSeconds(2), dishComposition, temp, null);
            SecondDishTest.SetPrice(TestPrice.Amount, TestPrice.Currency);
            SecondDishTest.SetNutritionalValue(TestPFC.CaloricContent, TestPFC.Carbohydrates, TestPFC.Fats, TestPFC.Proteins);
            List<DishModel> sample = new List<DishModel>
            {
                FirstDishTest,
                SecondDishTest
            };
            return new ReadOnlyCollection<DishModel>(sample);
        }
        #endregion

        [Fact]
        public async Task Get_All_ReturnsNull_WhenEmpty()
        {
            // Arrange
            var mockService = new Mock<IDishDomainService>();
            mockService.Setup(ms => ms.GetAllAsync())
                .ReturnsAsync(new List<DishModel>());
            var controller = new DishController(null, mockService.Object);

            // Act
            var result = await controller.GetAll();

            // Assert
            var list = result.Value;
            
            Assert.Empty(list.DishesCollection);
        }

        [Fact]
        public async Task Create_ShouldPass_WhenResponseNameAndTestNameEqual()
        {
            // Arrange
            var mockService = new Mock<IDishDomainService>();
            Guid generatedId = Guid.NewGuid();
            Guid MenuId = Guid.NewGuid();
            List<string> dishComposition = new List<string>()
            {
                "some ingredients"
            };

            var modelToCreate = new DishModel(Guid.NewGuid(),"Cakes", "category", TimeSpan.FromSeconds(1), dishComposition, MenuId, new byte[254]);
            modelToCreate.SetPrice(TestPrice.Amount, TestPrice.Currency);
            modelToCreate.SetNutritionalValue(TestPFC.CaloricContent, TestPFC.Carbohydrates, TestPFC.Fats, TestPFC.Proteins);

            var testname = "test";
            mockService.Setup(x => x.CreateAsync(It.IsAny<DishModel>()))
                .ReturnsAsync(() =>
                {
                    modelToCreate.DishName = testname;

                    return modelToCreate;
                });

            var controller = new DishController(null, mockService.Object);

            // Act
            var request = new DishRequest()
            {
                DishId = generatedId,
                DishName = testname,
                Fats = 1,
                Amount = 1,
                CaloricContent = 1,
                Carbohydrates = 1,
                Category = "some category",
                Currency = "UAH",
                DishComposition = new List<string>() { "first ingredient", "second ingredient" },
                MenuID = Guid.NewGuid(),
                PreparationTime = TimeSpan.FromMinutes(10).ToString(),
                Protein = 1
            };
            var result = await controller.CreateDishAsync(request);

            // Assert
            var response = result.Value;
            Assert.Equal(testname, response.DishName);
        }



        [Fact]
        public async Task Update_RequestNameShouldBeDifferent_From_BaseName()
        {
            // Arrange
            var mockService = new Mock<IDishDomainService>();
            Guid requestedId = Guid.NewGuid();
            List<string> dishComposition = new List<string>()
            {
                "some ingredients"
            };

            var modelToCreate = new DishModel(requestedId, "Cakes", "category", TimeSpan.FromSeconds(1), dishComposition, requestedId, new byte[458]);
            modelToCreate.SetPrice(TestPrice.Amount, TestPrice.Currency);
            modelToCreate.SetNutritionalValue(TestPFC.CaloricContent, TestPFC.Carbohydrates, TestPFC.Fats, TestPFC.Proteins);


            mockService.Setup(ms => ms.UpdateAsync(requestedId, It.IsAny<DishModel>()))
                .ReturnsAsync(modelToCreate);
            var controller = new DishController(null, mockService.Object);
            // Act
            var request = new DishRequest()
            {
                DishId = requestedId,
                DishName = "burgers",
                Fats = 1,
                Amount = 1,
                CaloricContent = 1,
                Carbohydrates = 1,
                Category = "category",

                Currency = "UAH",
                DishComposition = new List<string>() { "first ingredient", "second ingredient" },
                MenuID = Guid.NewGuid(),
                PreparationTime = TimeSpan.FromMinutes(10).ToString(),
                Protein = 1


            };

            var result = await controller.UpdateDishAsync(requestedId, request);

            // Assert
            var response = result.Value;

            Assert.NotEqual(request.DishName, response.DishName);
            Assert.Equal(request.Category, response.Category);
        }
    }
}

