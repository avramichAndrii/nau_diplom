using Menu.API;
using Menu.API.Contracts.Requests;
using Menu.API.Contracts.Responses;
using Menu.Domain;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xunit;

namespace Menu.Controllers.Test
{
    public class MarketplaceControllerTest
    {
        #region testinfo
        private IReadOnlyCollection<MenuModel> TestModels()
        {
            return new ReadOnlyCollection<MenuModel>(new List<MenuModel>
            {
                new MenuModel(Guid.NewGuid(),"Cakes", "Ordinary menu", Guid.NewGuid(), new byte[678], true) ,
                new MenuModel(Guid.NewGuid(),"Sushi", "East menu", Guid.NewGuid(), new byte[567], true)
            });
        }
        #endregion
        [Fact]
        public async Task GetAll_ShouldReturn2EntitiesTest()
        {
            // Arrange
            var mockService = new Mock<IMarketplaceService>();
            mockService.Setup(ms => ms.GetAllOffersAsync())
                                      .ReturnsAsync(TestModels());
            var controller = new MarketplaceController(mockService.Object);

            // Act
            ActionResult<GetAllMenusResponse> result = await controller.GetAllAsync();

            // Assert
            var response = result.Result as OkObjectResult;
            if (response?.Value is List<MenuResponse> list)
            {
                Assert.Equal(TestModels().Count, list.Count);
            }
        }

        [Fact]
        public async Task GetAllAsync_ShouldReturnEmptyListTest()
        {
            // Arrange
            var mockService = new Mock<IMarketplaceService>();
            mockService.Setup(ms => ms.GetAllOffersAsync())
                                      .ReturnsAsync(new List<MenuModel>());
            var controller = new MarketplaceController(mockService.Object);

            // Act
            ActionResult<GetAllMenusResponse> result = await controller.GetAllAsync();

            // Assert
            Assert.IsType<OkObjectResult>(result.Result);
            if (result.Result is OkObjectResult response)
            {
                var value = response.Value as GetAllMenusResponse;
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }
                Assert.Empty(value.MenusCollection);
            }
        }

        [Fact]
        public async Task GetByProviderIdOnMarketplaceAsync_ShouldReturnOneOfferTest()
        {
            // Arrange
            Guid testId = Guid.NewGuid();
            var mockService = new Mock<IMarketplaceService>();
            var offersTest = new List<MenuModel>
            {
                new MenuModel(Guid.NewGuid(),"Cakes", "Ordinary menu", testId, null, true) ,
                new MenuModel(Guid.NewGuid(),"Sushi", "East menu", testId, null, true)
            };
            mockService.Setup(ms => ms.GetByProviderIdAsync(testId))
                                      .ReturnsAsync(offersTest);
            var controller = new MarketplaceController(mockService.Object);

            // Act
            var result = await controller.GetByProviderIdAsync(testId);

            //Assert

            Assert.IsType<OkObjectResult>(result.Result);
            if (result.Result is OkObjectResult response)
            {
                var value = response.Value as GetAllMenusResponse;
                if (value != null)
                {
                    Assert.Equal(offersTest.Count, value.MenusCollection.Count);
                }
            }
        }
        [Fact]
        public async Task GetByIdAsync_ShouldThrowExceptionTest()
        {
            Guid testId = Guid.NewGuid();
            var mockService = new Mock<IMarketplaceService>();
            mockService.Setup(ms => ms.GetByProviderIdAsync(testId)).Throws<MenuDomainException>();

            var controller = new MarketplaceController(mockService.Object);

            // Act
            ActionResult<MenuResponse> result = await controller.GetByProviderIdAsync(testId);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result.Result);
        }

        [Fact]
        public async Task UpdateIsInMarketplaceAsync_ShouldReturnMenuModelTest()
        {
            Guid testId = Guid.NewGuid();
            var mockService = new Mock<IMarketplaceService>();
            mockService.Setup(ms => ms.UpdateIsInMarketplaceAsync(testId, true))
                .ReturnsAsync(new MenuModel(testId, "Sushi", "tasty and healthy", Guid.NewGuid(), null, true));
            var controller = new MarketplaceController(mockService.Object);
            MarketplacePutRequest req = new MarketplacePutRequest()
            {
                IsInMarketplace = true
            };
            // Act
            ActionResult<MenuResponse> result = await controller.UpdateIsInMarketplaceAsync(testId,req);

            // Assert
            Assert.IsType<OkObjectResult>(result.Result);
            if (result.Result is OkObjectResult response)
            {
                var value = response.Value as MenuResponse;
                if (value != null)
                {
                    Assert.Equal(testId, value.Id);
                    Assert.True(value.IsInMarketplace);
                }
            }
        }

        [Fact]
        public async Task UpdateIsInMarketplaceAsync_ShouldThrowExceptionTest()
        {
            Guid testId = Guid.NewGuid();
            var mockService = new Mock<IMarketplaceService>();
            mockService.Setup(ms => ms.UpdateIsInMarketplaceAsync(testId, true))
                                                             .Throws<MenuDomainException>();
            var controller = new MarketplaceController(mockService.Object);
            MarketplacePutRequest req = new MarketplacePutRequest()
            {
                IsInMarketplace = true
            };
            // Act
            ActionResult<MenuResponse> result = await controller.UpdateIsInMarketplaceAsync(testId,req);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result.Result);

        }
    }
}

