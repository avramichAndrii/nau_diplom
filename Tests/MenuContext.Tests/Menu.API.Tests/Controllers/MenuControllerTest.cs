﻿using Menu.API;
using Menu.API.Contracts.Requests;
using Menu.API.Contracts.Responses;
using Menu.Domain;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Menu.Controllers.Test
{
    public class MenuControllerTest
    {
        [Theory,MemberData(nameof(TestMenu))]
        public async Task GetById_ExistingEntity_Test(string name,string info,Guid guid)
        {
            // Arrange
            var mockService = new Mock<IMenuDomainService>();

            Guid requestedId = Guid.NewGuid();
            string testName = "Cakes";
            mockService.Setup(x => x.GetByIdAsync(requestedId)).ReturnsAsync(
                new MenuModel(guid, name, info, guid, null));
            var controller = new MenuController(null, mockService.Object);

            // Act
            var result = await controller.GetAsync(requestedId);

            // Assert
            var response = Assert.IsType<MenuResponse>(result.Value);
            Assert.Equal(testName, response.MenuName);
        }
        public static IEnumerable<object[]> TestMenu
        {
            get
            {
                yield return new object[] { "Cakes", "Ordinary menu",Guid.NewGuid()};
            }
        }
        [Fact]
        public async Task GetAll_ShouldReturn3Entities_test()
        {
            // Arrange
            var mockService = new Mock<IMenuDomainService>();
            mockService.Setup(ms => ms.GetAllAsync()).ReturnsAsync(GetTestModels());

            var controller = new MenuController(null, mockService.Object);

            // Act
            var result = await controller.GetAllAsync();

            // Assert
            var list = result.Value;
            Assert.Equal(3, list.MenusCollection.Count);
        }
        #region testinfo
        private IReadOnlyCollection<MenuModel> GetTestModels()
        {
            return new ReadOnlyCollection<MenuModel>(new List<MenuModel>
            {
                new MenuModel(Guid.NewGuid(),"Cakes", "Ordinary menu", Guid.NewGuid(), null) ,
                new MenuModel(Guid.NewGuid(),"Ice-cream", "Ordinary menu", Guid.NewGuid(), null) ,
                new MenuModel(Guid.NewGuid(),"Soup", "Ordinary menu", Guid.NewGuid(), null)
            });
        }
        #endregion
        [Fact]
        public async Task GetAllAsync_WhenListisEmpty_returns0()
        {
            // Arrange
            var mockService = new Mock<IMenuDomainService>();
            mockService.Setup(ms => ms.GetAllAsync())
                .ReturnsAsync(new List<MenuModel>());
            var controller = new MenuController(null, mockService.Object);

            // Act
            var result = await controller.GetAllAsync();

            // Assert
            var list = result.Value;

            Assert.Equal(0,list.MenusCollection.Count);
        }

        [Fact]
        public async Task Create_ShouldReturnTrue_WhenResponseAndTestStringAreEqual()
        {
            // Arrange
            var mockService = new Mock<IMenuDomainService>();
            Guid generatedId = Guid.NewGuid();
            Guid RequestProviderId = Guid.NewGuid();

            var modelToCreate = new MenuModel(Guid.NewGuid(),"", "cakes", Guid.NewGuid(), null);

            var teststring = "test";
            mockService.Setup(x => x.CreateAsync(It.IsAny<MenuModel>()))
                .ReturnsAsync(() =>
                {
                    modelToCreate.MenuName = teststring;

                    return modelToCreate;
                });

            var controller = new MenuController(null, mockService.Object);

            // Act
            var request = new MenuRequest()
            {
                MenuName = "cakes",
                MenuInfo = "some info",
                ProviderCompanyId = RequestProviderId
            };
            var result = await controller.CreateMenuAsync(request);

            // Assert
            var response = result.Value;
            Assert.Equal(teststring, response.MenuName);
        }

        

        [Fact]
        public async Task Update_RequestNameShouldBeDifferent_From_BaseName()
        {
            // Arrange
            var mockService = new Mock<IMenuDomainService>();

            Guid requestedId = Guid.NewGuid();
            mockService.Setup(ms => ms.UpdateAsync(requestedId, It.IsAny<MenuModel>()))
                .ReturnsAsync(() => new MenuModel(Guid.NewGuid(),"cakes", "some info", Guid.NewGuid(), null));
            var controller = new MenuController(null, mockService.Object);

            // Act
            var request = new MenuRequest()
            {
                MenuName = "burgers",
                MenuInfo = "some info",
                ProviderCompanyId = requestedId
            };
            var result = await controller.UpdateMenuAsync(requestedId, request);

            // Assert
            var response = result.Value;

            Assert.Equal(request.MenuInfo, response.MenuInfo);
            Assert.NotEqual(request.MenuName, response.MenuName);
        }
    }
}

