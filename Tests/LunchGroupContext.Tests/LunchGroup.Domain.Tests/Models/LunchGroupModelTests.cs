﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace LunchGroup.Domain.Models.Tests
{
    public class LunchGroupModelTests
    {
        [Theory, MemberData(nameof(AvailableDatesCases))]
        public void GetAvailableDatesTest(LunchGroupModel lgModel, int numberOfAvailableDates, DateTime lastDate)
        {
            // Act
            IReadOnlyCollection<DateTime> dates = lgModel.GetAvailableDates();
            // Assert
            Assert.Equal(dates.Count, numberOfAvailableDates);
            Assert.Equal(dates.Last(), lastDate);
        }

        [Theory, MemberData(nameof(TimetableCases))]
        public void UpdateTimetableTest(LunchGroupModel lgModel, DateTime newDeadline, DateTime newPeriodStart)
        {
            // Act
            lgModel.UpdateTimetable();
            // Assert
            Assert.Equal(lgModel.SubmitDeadline, newDeadline);
            Assert.Equal(lgModel.PeriodStart, newPeriodStart);
        }

        public static IEnumerable<object[]> AvailableDatesCases
        {
            get
            {
                yield return new object[]
                {
                    new LunchGroupModel
                    {
                        PeriodStart = new DateTime(2020, 6, 29),
                        Periodicity = 7,
                        Weekends = new List<DayOfWeek>(){ DayOfWeek.Sunday }
                    }, 6, new DateTime(2020, 7, 4)
                };
                yield return new object[]
                {
                    new LunchGroupModel
                    {
                        PeriodStart = new DateTime(2020, 6, 29),
                        Periodicity = 7,
                        Weekends = new List<DayOfWeek>(){ DayOfWeek.Saturday, DayOfWeek.Sunday }
                    }, 5, new DateTime(2020, 7, 3)
                };
                yield return new object[]
                {
                    new LunchGroupModel
                    {
                        PeriodStart = new DateTime(2020, 6, 29),
                        Periodicity = 7,
                        Weekends = new List<DayOfWeek>(){ DayOfWeek.Tuesday, DayOfWeek.Friday, DayOfWeek.Sunday }
                    }, 4, new DateTime(2020, 7, 4)
                };
                yield return new object[]
                {
                    new LunchGroupModel
                    {
                        PeriodStart = new DateTime(2020, 6, 29),
                        Periodicity = 7,
                        Weekends = new List<DayOfWeek>(){ DayOfWeek.Monday, DayOfWeek.Tuesday, DayOfWeek.Friday, DayOfWeek.Saturday }
                    }, 3, new DateTime(2020, 7, 5)
                };
                yield return new object[]
                {
                    new LunchGroupModel
                    {
                        PeriodStart = new DateTime(2020, 6, 29),
                        Periodicity = 1,
                        Weekends = new List<DayOfWeek>(){ DayOfWeek.Monday }
                    }, 1, new DateTime(2020, 6, 30)
                };
                yield return new object[]
                {
                    new LunchGroupModel
                    {
                        PeriodStart = new DateTime(2020, 6, 29),
                        Periodicity = 1,
                        Weekends = new List<DayOfWeek>(){ DayOfWeek.Sunday }
                    }, 1, new DateTime(2020, 6, 29)
                };
            }
        }
        public static IEnumerable<object[]> TimetableCases
        {
            get
            {
                yield return new object[]
                {
                    new LunchGroupModel
                    {
                        SubmitDeadline = new DateTime(2020, 6, 26),
                        PeriodStart = new DateTime(2020, 6, 29),
                        Periodicity = 7,
                        Weekends = new List<DayOfWeek>(){ }
                    }, new DateTime(2020, 7, 3), new DateTime(2020, 7, 6)
                };
                yield return new object[]
                {
                    new LunchGroupModel
                    {
                        SubmitDeadline = new DateTime(2020, 6, 25),
                        PeriodStart = new DateTime(2020, 6, 26),
                        Periodicity = 1,
                        Weekends = new List<DayOfWeek>(){ DayOfWeek.Sunday }
                    }, new DateTime(2020, 6, 26), new DateTime(2020, 6, 27)
                };
                yield return new object[]
                {
                    new LunchGroupModel
                    {
                        SubmitDeadline = new DateTime(2020, 6, 26),
                        PeriodStart = new DateTime(2020, 6, 27),
                        Periodicity = 1,
                        Weekends = new List<DayOfWeek>(){ DayOfWeek.Sunday }
                    }, new DateTime(2020, 6, 27), new DateTime(2020, 6, 29)
                };
                yield return new object[]
                {
                    new LunchGroupModel
                    {
                        SubmitDeadline = new DateTime(2020, 6, 26),
                        PeriodStart = new DateTime(2020, 6, 27),
                        Periodicity = 1,
                        Weekends = new List<DayOfWeek>(){ DayOfWeek.Sunday, DayOfWeek.Monday }
                    }, new DateTime(2020, 6, 27), new DateTime(2020, 6, 30)
                };
                yield return new object[]
                {
                    new LunchGroupModel
                    {
                        SubmitDeadline = new DateTime(2020, 6, 25),
                        PeriodStart = new DateTime(2020, 6, 26),
                        Periodicity = 1,
                        Weekends = new List<DayOfWeek>(){ DayOfWeek.Saturday, DayOfWeek.Sunday, DayOfWeek.Monday }
                    }, new DateTime(2020, 6, 26), new DateTime(2020, 6, 30)
                };
            }
        }
    }
}