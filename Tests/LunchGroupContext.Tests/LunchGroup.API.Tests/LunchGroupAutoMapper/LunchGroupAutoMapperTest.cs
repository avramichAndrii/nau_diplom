﻿using AutoMapper;
using LunchGroup.API.Contracts.Responses;
using LunchGroup.API.Mapping;
using LunchGroup.Domain.Models;
using System;
using System.Collections.Generic;
using Xunit;

namespace LunchGroup.API.Tests.DeliveryMapper
{
    public class LunchGroupAutoMapperTest
    {
        [Fact]
        public void MapModelToGetLunchGroupResponseTest()
        {
            // Arrange
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new LunchGroupAutoMapper());
            });
            var mapper = mockMapper.CreateMapper();
            testModelGet.Address = _addressModel;
            testResponseGet.Address = _addressResponse;

            // Act
            var actualResponse = mapper.Map<GetLunchGroupResponse>(testModelGet);

            // Assert
            Assert.Equal(testResponseGet.Address.City, actualResponse.Address.City);
            Assert.Equal(testResponseGet.Address.Street, actualResponse.Address.Street);
            Assert.Equal(testResponseGet.Address.BuildingNumber, actualResponse.Address.BuildingNumber);
            Assert.Equal(testResponseGet.Address.Office, actualResponse.Address.Office);

            Assert.Equal(testResponseGet.CompanyId, actualResponse.CompanyId);
            Assert.Equal(testResponseGet.Id, actualResponse.Id);
            Assert.Equal(testResponseGet.IsActive, actualResponse.IsActive);
            Assert.Equal(testResponseGet.Members, actualResponse.Members);
            Assert.Equal(testResponseGet.Name, actualResponse.Name);
            Assert.Equal(testResponseGet.Periodicity, actualResponse.Periodicity);
            Assert.Equal(testResponseGet.PeriodStart, actualResponse.PeriodStart);
            Assert.Equal(testResponseGet.SubmitDeadline, actualResponse.SubmitDeadline);
            Assert.Equal(testResponseGet.Weekends, actualResponse.Weekends);
        }

        [Fact]
        public void MapModelToGetTimetableResponseTest()
        {
            // Arrange
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new LunchGroupAutoMapper());
            });
            var mapper = mockMapper.CreateMapper();

            // Act
            var actualResponse = mapper.Map<GetTimetableResponse>(testModelGetTimetable);

            // Assert
            Assert.Equal(testResponseGetTimetable.SubmitDeadline, actualResponse.SubmitDeadline);
            Assert.Equal(testResponseGetTimetable.AvailableDates, actualResponse.AvailableDates);
        }

        #region Samples
        private static readonly Guid groupId = Guid.NewGuid();
        private static readonly Guid companyId = Guid.NewGuid();
        private static readonly Guid memberId1 = Guid.NewGuid();
        private static readonly Guid memberId2 = Guid.NewGuid();
        private static readonly DateTime testDeadline = new DateTime(2020, 7, 2);
        private static readonly DateTime testPeriodStart = new DateTime(2020, 7, 6);

        private static readonly LunchGroupModel testModelGet =
            new LunchGroupModel(groupId)
            {
                SubmitDeadline = testDeadline,
                PeriodStart = testPeriodStart,
                Weekends = new List<DayOfWeek> { DayOfWeek.Sunday },
                Periodicity = 7,
                Name = "TestName",
                GroupMembers = new List<LunchGroupMember>
                {
                    new LunchGroupMember{ LunchGroupModelId = groupId, MemberId = memberId1 },
                    new LunchGroupMember{ LunchGroupModelId = groupId, MemberId = memberId2 },
                },
                CompanyId = companyId,
                IsActive = true
            };
        private static readonly GetLunchGroupResponse testResponseGet = 
            new GetLunchGroupResponse
            {
                Id = groupId,
                SubmitDeadline = testDeadline,
                PeriodStart = testPeriodStart,
                Weekends = new string[] { "Sunday" },
                Periodicity = 7,
                Name = "TestName",
                Members = new Guid[] { memberId1, memberId2 },
                CompanyId = companyId,
                IsActive = true
            };

        private static readonly LunchGroupModel testModelGetTimetable =
            new LunchGroupModel
            {
                SubmitDeadline = testDeadline,
                PeriodStart = testPeriodStart,
                Periodicity = 7,
                Weekends = new List<DayOfWeek> { DayOfWeek.Thursday, DayOfWeek.Friday, DayOfWeek.Saturday, DayOfWeek.Sunday }
            };
        private static readonly GetTimetableResponse testResponseGetTimetable =
            new GetTimetableResponse(testDeadline,
                new List<DateTime>
                {
                    new DateTime(2020, 7, 6),
                    new DateTime(2020, 7, 7),
                    new DateTime(2020, 7, 8)
                });

        private static readonly AddressModel _addressModel =
            new AddressModel
            {
                City = "Kyiv",
                Street = "Vatslava Gavela",
                BuildingNumber = "8",
                Office = "3"
            };
        private static readonly AddressResponse _addressResponse =
            new AddressResponse
            {
                City = "Kyiv",
                Street = "Vatslava Gavela",
                BuildingNumber = "8",
                Office = "3"
            };
        #endregion
    }
}
