﻿using LunchGroup.API.Contracts.Requests;
using LunchGroup.API.Mapping;
using LunchGroup.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace LunchGroup.API.Tests.DeliveryMapper
{
    public class DeliveryMapperTest
    {

        public static IEnumerable<object[]> TestCases
        {
            get
            {
                yield return new object[] { Guid.NewGuid(), Guid.NewGuid(), DateTime.Now, StatusModel.Done };
                yield return new object[] { Guid.NewGuid(), Guid.NewGuid(), DateTime.Now, StatusModel.Submitted };
            }
        }
        [Theory, MemberData(nameof(TestCases))]
        public void MapFromRequestTest(Guid id, Guid orderId, DateTime date, StatusModel status)
        {
            // Arrange
            var request = new UpdateDeliveryRequest()
            {
                GroupOrderId = orderId,
                DeliveryTime = date,
                Status = status.ToString()

            };
            // Act
            var model = request.MapFromRequest();
            // Assert
            Assert.Equal(request.GroupOrderId, model.GroupOrderId);
            Assert.Equal(request.DeliveryTime, model.DeliveryTime);
            Assert.Equal(request.Status, model.Status.ToString());
        }
        [Theory, MemberData(nameof(TestCases))]
        public void MapToResponseTest(Guid id, Guid orderId, DateTime date, StatusModel status)
        {
            // Arrange
            var model = new DeliveryModel(id, orderId, date, status);
            // Act
            var response = model.MapToResponse();
            // Assert

            Assert.Equal(model.Status.ToString(), response.Status);
            Assert.Equal(model.DeliveryTime, response.DeliveryTime);
        }
        [Theory, MemberData(nameof(TestCases))]
        public void MapToAllResponseTest(Guid id, Guid orderId, DateTime date, StatusModel status)
        {
            // Arrange
            List<DeliveryModel> list = new List<DeliveryModel>
            {
                new DeliveryModel(id, orderId, date, status)
            };

            // Act
            var response = list.MapToCollection();
            // Assert

            Assert.Equal(list.Count, response.DeliveryCollection.Count);
            Assert.Equal(list.FirstOrDefault().Id, response.DeliveryCollection.FirstOrDefault().DeliveryId);
        }
    }

}
