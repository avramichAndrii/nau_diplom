using GroupOrder.Application.Services;
using GroupOrder.Domain.Models;
using GroupOrder.Domain.Repositories;
using GroupOrder.Domain.Services;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace GroupOrder.Application.Tests
{
    public class CartServiceTests
    {
        private readonly ICartService _cartService;
        private readonly Mock<ICartRepository> _cartRepoMock = new Mock<ICartRepository>();

        public CartServiceTests()
        {
            _cartService = new CartService(_cartRepoMock.Object);
        }

        [Fact]
        public async Task GetByWorkerIdAsync_ShouldReturnCart_WhenOCartExists()
        {
            //Arrange            
            Guid workerId = Guid.NewGuid();
            CartModel cart = new CartModel(Guid.NewGuid(), "Info for cart 1", Guid.NewGuid(), workerId, null);
            _cartRepoMock.Setup(x => x.GetCartByWorkerIdAsync(workerId))
                         .ReturnsAsync(cart);

            //Act
            var workerCart = await _cartService.GetCartByWorkerId(workerId);

            //Assert
            Assert.NotNull(workerCart);
        }        
        
         [Fact]
         public async Task CreateAsync_ShouldReturnNewCartModel()
         {
             //Arrange         
             Guid testId = Guid.NewGuid();
             CartModel cart = new CartModel(testId, "Info for cart 1", Guid.NewGuid(), Guid.NewGuid(), null);
           
             //Act
             _cartRepoMock.Setup(x => x.CreateAsync(cart))
                           .ReturnsAsync(() => cart);
             var order = await _cartService.Create(cart);

             //Assert
             var result = Assert.IsType<CartModel>(cart);
             Assert.Equal(testId, result.Id);
         }     
    }
}
