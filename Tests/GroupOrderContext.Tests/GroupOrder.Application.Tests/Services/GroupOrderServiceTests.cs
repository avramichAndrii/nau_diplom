using GroupOrder.Application.Services;
using GroupOrder.Domain.Exceptions;
using GroupOrder.Domain.Models;
using GroupOrder.Domain.Repositories;
using GroupOrder.Domain.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace GroupOrder.Application.Tests
{
    public class GroupOrderServiceTests
    {
            private readonly IGroupOrderService _groupOrderService;
            private readonly Mock<IGroupOrderRepository> _groupOrderRepoMock = new Mock<IGroupOrderRepository>();

            public GroupOrderServiceTests()
            {
                _groupOrderService = new GroupOrderService(_groupOrderRepoMock.Object);
            }

            [Fact]
            public async Task GetByIdAsync_ShouldReturnGroupOrder_WhenGroupOrderExists()
            {
                //Arrange
                var testId = Guid.NewGuid();
                GroupOrderModel groupOrderTest = new GroupOrderModel(testId, Guid.NewGuid(), Guid.NewGuid(), DateTime.Today);
                _groupOrderRepoMock.Setup(x => x.GetByIdAsync(testId)).ReturnsAsync(groupOrderTest);

                //Act
                var groupOrder = await _groupOrderService.GetById(testId);

                //Assert
                Assert.Equal(testId, groupOrder.Id);
            }

            [Fact]
            public async Task GetByIdAsync_ShouldReturnNothing_WhenGroupOrderDoesNotExist()
            {
                // Arrange
                _groupOrderRepoMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>()))
                    .ReturnsAsync(() => null);

                // Act
                var groupOrder = await _groupOrderService.GetById(Guid.NewGuid());

                // Assert
                Assert.Null(groupOrder);
            }

            [Fact]
            public async Task GetByProviderIdAsync_ShouldReturnGroupOrder_WhenGroupOrderExists()
            {
                //Arrange
                var providerId = Guid.NewGuid();
                List<GroupOrderModel> groupOrderList = new List<GroupOrderModel>
            {
                new GroupOrderModel(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), DateTime.Today),
                new GroupOrderModel(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), DateTime.Today),
                new GroupOrderModel(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), DateTime.Today)
            };
                _groupOrderRepoMock.Setup(x => x.GetAllByProviderIdAsync(providerId)).ReturnsAsync(groupOrderList);

                //Act
                var groupOrders = await _groupOrderService.GetAllByProviderId(providerId);

                //Assert
                Assert.NotEmpty(groupOrders);
            }

            [Fact]
            public async Task GetByProviderIdAsync_ShouldReturnNothing_WhenGroupOrderDoesNotExist()
            {
                // Arrange
                _groupOrderRepoMock.Setup(x => x.GetAllByProviderIdAsync(It.IsAny<Guid>()))
                    .ReturnsAsync(() => null);

                // Act
                var groupOrder = await _groupOrderService.GetAllByProviderId(Guid.NewGuid());

                // Assert
                Assert.Null(groupOrder);
            }

            [Fact]
            public async Task CreateAsync_ShouldReturnNewGroupOrderModel()
            {
                //Arrange
                Guid testId = Guid.NewGuid();
                var testGroupOrder = new GroupOrderModel(testId, Guid.NewGuid(), Guid.NewGuid(), DateTime.Today);

                //Act
                _groupOrderRepoMock.Setup(x => x.CreateAsync(testGroupOrder))
                    .ReturnsAsync(() => testGroupOrder);
                var groupOrder = await _groupOrderService.Create(testGroupOrder);

                //Assert
                var result = Assert.IsType<GroupOrderModel>(groupOrder);
                Assert.Equal(testId, result.Id);
            }

            [Fact]
            public async Task GetGroupOrderByLunchGroupIdAsync_ShouldReturnGroupOrder_WhenGroupOrderExists()
            {
                //Arrange
                var lunchGroupId = Guid.NewGuid();
                List<GroupOrderModel> groupOrderList = new List<GroupOrderModel>
                {
                    new GroupOrderModel(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), DateTime.Today),
                    new GroupOrderModel(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), DateTime.Today),
                    new GroupOrderModel(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), DateTime.Today)
                };

                _groupOrderRepoMock.Setup(x => x.GetAllByLunchGroupIdAsync(lunchGroupId)).ReturnsAsync(groupOrderList);

                //Act     
                var groupOrders = await _groupOrderService.GetAllByLunchGroupId(lunchGroupId);

                //Assert
                Assert.NotEmpty(groupOrders);
            }

            [Fact]
            public async Task GetGroupOrderByLunchGroupIdAsync_ShouldReturnNothing_WhenGroupOrderDoesNotExist()
            {
                // Arrange
                _groupOrderRepoMock.Setup(x => x.GetAllByLunchGroupIdAsync(It.IsAny<Guid>()))
                    .ReturnsAsync(() => null);

                // Act
                var groupOrder = await _groupOrderService.GetAllByLunchGroupId(Guid.NewGuid());

                // Assert
                Assert.Null(groupOrder);
            }
    }
}
