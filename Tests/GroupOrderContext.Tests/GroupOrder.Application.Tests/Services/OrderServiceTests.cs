using GroupOrder.Application.Services;
using GroupOrder.Domain.Exceptions;
using GroupOrder.Domain.Models;
using GroupOrder.Domain.Repositories;
using GroupOrder.Domain.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace GroupOrder.Application.Tests
{
    public class OrderServiceTests
    {
        private readonly IOrderService _orderService;
        private readonly Mock<IOrderRepository> _orderRepoMock = new Mock<IOrderRepository>();
        private readonly Mock<IGroupOrderService> _groupOrderServiceMock = new Mock<IGroupOrderService>();

        public OrderServiceTests()
        {
            _orderService = new OrderService(_orderRepoMock.Object, _groupOrderServiceMock.Object);
        }

        [Fact]
        public async Task GetByIdAsync_ShouldReturnOrder_WhenOrderExists()
        {
            //Arrange
            Guid generatedId = Guid.NewGuid();            
            OrderModel orderTest = new OrderModel(generatedId, DateTime.Now, "Info", Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), null);
            _orderRepoMock.Setup(x => x.GetByIdAsync(generatedId))
                          .ReturnsAsync(orderTest);

            //Act
            var groupOrder = await _orderService.GetById(generatedId);

            //Assert
            Assert.Equal(generatedId, groupOrder.Id);
        }

        [Fact]
        public async Task GetByIdAsync_ShouldReturnNothing_WhenOrderDoesNotExist()
        {
           // Arrange
           _orderRepoMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>()))
                         .ReturnsAsync(() => null);

           //Act
           var order = await _orderService.GetById(Guid.NewGuid());

           //Assert
           Assert.Null(order);
        }

        [Fact]
        public async Task GetByProviderIdAsync_ShouldReturnOrder_WhenOrderExists()
        {
            //Arrange            
            Guid providerId = Guid.NewGuid();           
            List<Guid> dishes = new List<Guid>()
            {
                Guid.NewGuid()
            };
            List<OrderModel> orderList = new List<OrderModel>
            {
                new OrderModel(Guid.NewGuid(), DateTime.Now, "Info for order 1", providerId, Guid.NewGuid(), Guid.NewGuid(), dishes),
                new OrderModel(Guid.NewGuid(), DateTime.Now, "Info for order 2", providerId, Guid.NewGuid(), Guid.NewGuid(), dishes),
                new OrderModel(Guid.NewGuid(), DateTime.Now, "Info for order 3", providerId, Guid.NewGuid(), Guid.NewGuid(), dishes)
            };
            _orderRepoMock.Setup(x => x.GetAllByProviderIdAsync(providerId))
                          .ReturnsAsync(orderList);

            //Act
            var orders = await _orderService.GetAllByProviderId(providerId);

            //Assert
            Assert.NotEmpty(orders);
        }
        
        [Fact]
        public async Task GetByProviderIdAsync_ShouldReturnNothing_WhenOrderDoesNotExist()
        {
            //Arrange
            _orderRepoMock.Setup(x => x.GetAllByProviderIdAsync(It.IsAny<Guid>()))
                          .ReturnsAsync(() => null);

            //Act
            var order = await _orderService.GetAllByProviderId(Guid.NewGuid());

            //Assert
            Assert.Null(order);
        }

        [Fact]
        public async Task GetByWorkerIdAsync_ShouldReturnOrder_WhenOrderExists()
        {
            //Arrange            
            Guid workerId = Guid.NewGuid();
            List<Guid> dishes = new List<Guid>()
            {
                Guid.NewGuid()
            };
            List<OrderModel> orderList = new List<OrderModel>
            {
                new OrderModel(Guid.NewGuid(), DateTime.Now, "Info for order 1", Guid.NewGuid(), workerId, Guid.NewGuid(), dishes),
                new OrderModel(Guid.NewGuid(), DateTime.Now, "Info for order 2", Guid.NewGuid(), workerId, Guid.NewGuid(), dishes),
                new OrderModel(Guid.NewGuid(), DateTime.Now, "Info for order 3", Guid.NewGuid(), workerId, Guid.NewGuid(), dishes)
            };
            _orderRepoMock.Setup(x => x.GetAllByWorkerIdAsync(workerId))
                          .ReturnsAsync(orderList);

            //Act
            var orders = await _orderService.GetAllByWorkerId(workerId);

            //Assert
            Assert.NotEmpty(orders);
        }

        [Fact]
        public async Task GetByWorkerIdAsync_ShouldReturnNothing_WhenOrderDoesNotExist()
        {
            //Arrange
            _orderRepoMock.Setup(x => x.GetAllByWorkerIdAsync(It.IsAny<Guid>()))
                          .ReturnsAsync(() => null);

            //Act
            var order = await _orderService.GetAllByWorkerId(Guid.NewGuid());

            //Assert
            Assert.Null(order);
        }
    }
}
