﻿using GroupOrder.Application.Services;
using GroupOrder.Domain.Models;
using GroupOrder.Domain.Repositories;
using GroupOrder.Domain.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace GroupOrder.Application.Tests
{
    public class OrderTemplateServiceTests
    {
        private Mock<IOrderTemplateRepository> _orderTemplateRepoMock = new Mock<IOrderTemplateRepository>();
        private IOrderTemplateService _orderTemplateService;

        public OrderTemplateServiceTests()
        {
            _orderTemplateService = new OrderTemplateService(_orderTemplateRepoMock.Object);
        }

        [Fact]
        public async Task GetByIdAsync_ShouldReturnOrderTemplate_WhenOrderTemplateExists()
        {
            //Arrange
            Guid orderTemplateId = Guid.NewGuid();
            OrderTemplateModel orderTemplate = new OrderTemplateModel(orderTemplateId, "my friday template", Guid.NewGuid(), Guid.NewGuid(), new List<Guid>());

            _orderTemplateRepoMock.Setup(repo => repo.GetByIdAsync(orderTemplateId)).ReturnsAsync(orderTemplate);

            //Act     
            var returnedTemplate = await _orderTemplateService.GetById(orderTemplateId);

            //Assert
            Assert.Equal(orderTemplateId, returnedTemplate.Id);
            _orderTemplateRepoMock.Verify(repo => repo.GetByIdAsync(orderTemplateId), Times.Once);
        }

        [Fact]
        public async Task GetByIdAsync_ShouldReturnNothing_WhenOrderTemplateDoesNotExist()
        {
            // Arrange
            _orderTemplateRepoMock.Setup(repo => repo.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(() => null);

            // Act
            var template = await _orderTemplateService.GetById(Guid.NewGuid());

            // Assert
            Assert.Null(template);
            _orderTemplateRepoMock.Verify(repo => repo.GetByIdAsync(It.IsAny<Guid>()), Times.Once);
        }


        [Fact]
        public async Task CreateAsync_ShouldReturnNewOrderTemplateModel_Test()
        {
            // Arrange
            Guid modelId = Guid.NewGuid();
            var orderTemplateModel = new OrderTemplateModel(modelId, "my wednesday meat template", Guid.NewGuid(), Guid.NewGuid(), new List<Guid>());
            _orderTemplateRepoMock.Setup(repo => repo.CreateAsync(orderTemplateModel)).ReturnsAsync(() => orderTemplateModel);

            // Act
            var orderTemplate = await _orderTemplateService.Create(orderTemplateModel);

            // Assert
            var result = Assert.IsType<OrderTemplateModel>(orderTemplate);
            Assert.Equal(modelId, result.Id);
            _orderTemplateRepoMock.Verify(repo => repo.CreateAsync(orderTemplateModel), Times.Once);
        }

        [Fact]
        public async Task DeleteAsync_VerifyCall_Test()
        {
            // Arrange
            Guid deletedTemplateId = Guid.NewGuid();
            _orderTemplateRepoMock.Setup(repo => repo.DeleteAsync(deletedTemplateId));

            // Act
            await _orderTemplateService.Delete(deletedTemplateId);

            // Assert
            _orderTemplateRepoMock.Verify(repo => repo.DeleteAsync(deletedTemplateId), Times.Once);
        }

        [Fact]
        public async Task UpdateAsync_VerifyCall_Test()
        {
            // Arrange
            Guid deletedTemplateId = Guid.NewGuid();
            _orderTemplateRepoMock.Setup(repo => repo.DeleteAsync(deletedTemplateId));

            // Act
            await _orderTemplateService.Delete(deletedTemplateId);

            // Assert
            _orderTemplateRepoMock.Verify(repo => repo.DeleteAsync(deletedTemplateId), Times.Once);
        }

        [Fact]
        public async Task GetAllAsync_ShouldReturnList_VerifyCall_Test()
        {
            // Arrange
            _orderTemplateRepoMock.Setup(repo => repo.GetAllAsync()).ReturnsAsync(() => GetOrderTemplateTestModels());

            // Act
            var result = await _orderTemplateService.GetAll();

            // Assert
            Assert.Equal(GetOrderTemplateTestModels().Count, result.Count);
            _orderTemplateRepoMock.Verify(repo => repo.GetAllAsync(), Times.Once);
        }

        private List<OrderTemplateModel> GetOrderTemplateTestModels()
        {
            return new List<OrderTemplateModel>
            {
                new OrderTemplateModel(Guid.NewGuid(), "template A", Guid.NewGuid(), Guid.NewGuid(), new List<Guid>()),
                new OrderTemplateModel(Guid.NewGuid(), "template B", Guid.NewGuid(), Guid.NewGuid(), new List<Guid>()),
                new OrderTemplateModel(Guid.NewGuid(), "template C", Guid.NewGuid(), Guid.NewGuid(), new List<Guid>())
            };
        }
    }
}
