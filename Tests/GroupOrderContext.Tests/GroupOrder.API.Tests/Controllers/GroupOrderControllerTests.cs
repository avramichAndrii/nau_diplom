using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GroupOrder.API.Contracts.Requests;
using GroupOrder.API.Contracts.Responses;
using GroupOrder.API.Controllers;
using GroupOrder.Domain.Models;
using GroupOrder.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace GroupOrder.API.Tests.Controllers
{
    public class GroupOrderControllerTests
    {
        private readonly Mock<IGroupOrderService> _groupOrderMockService = new Mock<IGroupOrderService>();
        private readonly GroupOrderController _groupOrderController;

        public GroupOrderControllerTests()
        {
            _groupOrderController = new GroupOrderController(_groupOrderMockService.Object);
        }

        [Fact]
        public async Task GetByIdAsync_ExistingEntity()
        {
            // Arrange
            Guid requestedId = Guid.NewGuid();
            _groupOrderMockService.Setup(x => x.GetById(requestedId))
                                  .ReturnsAsync(
                                   new GroupOrderModel(requestedId, Guid.NewGuid(), Guid.NewGuid(), DateTime.Today));
            // Act
            var result = await _groupOrderController.GetByIdAsync(requestedId);

            // Assert
            Assert.IsType<OkObjectResult>(result.Result);
            if (result.Result is OkObjectResult response)
            {
                var value = response.Value as GroupOrderResponse;
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }
                Assert.Equal(requestedId, value.Id);
            }
            _groupOrderMockService.Verify(service => service.GetById(requestedId), Times.Once);
        }

        [Fact]
        public async Task GetAllAsync_ShouldReturnList()
        {
            // Arrange
            _groupOrderMockService.Setup(ms => ms.GetAll()).ReturnsAsync(GetTestModels());

            // Act
            var result = await _groupOrderController.GetAllAsync();

            // Assert
            Assert.IsType<OkObjectResult>(result.Result);
            if (result.Result is OkObjectResult response)
            {
                var value = response.Value as GetAllGroupOrdersResponse;
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }
                Assert.Equal(GetTestModels().Count, value.GroupOrderCollection.Count);
                _groupOrderMockService.Verify(service => service.GetAll(), Times.Once);
            }
        }

        [Fact]
        public async Task GetByProviderIdAsync_ShouldReturnList()
        {
            // Arrange
            Guid providerId = Guid.NewGuid();
            _groupOrderMockService.Setup(service => service.GetAllByProviderId(providerId)).ReturnsAsync(GetTestModels);

            // Act
            var result = await _groupOrderController.GetAllByProviderIdAsync(providerId);

            // Assert
            Assert.IsType<OkObjectResult>(result.Result);
            if (result.Result is OkObjectResult response)
            {
                var value = response.Value as GetAllGroupOrdersResponse;
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }
                Assert.Equal(GetTestModels().Count, value.GroupOrderCollection.Count);
            }
            _groupOrderMockService.Verify(service => service.GetAllByProviderId(providerId), Times.Once);
        }

        [Fact]
        public async Task GetByLunchGroupIdAsync_ShouldReturnList()
        {
            // Arrange
            Guid lunchGroupId = Guid.NewGuid();
            _groupOrderMockService.Setup(service => service.GetAllByLunchGroupId(lunchGroupId)).ReturnsAsync(GetTestModels);

            // Act
            var result = await _groupOrderController.GetAllByLunchGroupIdAsync(lunchGroupId);

            // Assert
            Assert.IsType<OkObjectResult>(result.Result);
            if (result.Result is OkObjectResult response)
            {
                var value = response.Value as GetAllGroupOrdersResponse;
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }
                Assert.Equal(GetTestModels().Count, value.GroupOrderCollection.Count);
            }
            _groupOrderMockService.Verify(service => service.GetAllByLunchGroupId(lunchGroupId), Times.Once);
        }

        #region testinfo
        private List<GroupOrderModel> GetTestModels()
        {
            return new List<GroupOrderModel>
            {
            new GroupOrderModel(Guid.NewGuid(),Guid.NewGuid(), Guid.NewGuid(), DateTime.Today),
            new GroupOrderModel(Guid.NewGuid(),Guid.NewGuid(), Guid.NewGuid(), DateTime.Today),
            new GroupOrderModel(Guid.NewGuid(),Guid.NewGuid(), Guid.NewGuid(), DateTime.Today)
            };
        }
        #endregion

        [Fact]
        public async Task GetAllAsync_WhenNoGroupOrders_ReturnsEmptyList()
        {
            // Arrange
            _groupOrderMockService.Setup(service => service.GetAll())
                                  .ReturnsAsync(new List<GroupOrderModel>());

            // Act
            var result = await _groupOrderController.GetAllAsync();

            // Assert
            Assert.IsType<OkObjectResult>(result.Result);
            if (result.Result is OkObjectResult response)
            {
                var value = response.Value as GetAllGroupOrdersResponse;
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }
                Assert.Empty(value.GroupOrderCollection);
            }
            _groupOrderMockService.Verify(service => service.GetAll(), Times.Once);

        }

        [Fact]
        public async Task Create_ShouldReturnGroupModel()
        {
            // Arrange
            Guid generatedId = Guid.NewGuid();
            Guid providerId = Guid.NewGuid();
            Guid lunchGroupId = Guid.NewGuid();

            var request = new GroupOrderRequest(providerId, lunchGroupId, DateTime.Today.AddDays(-2));
            _groupOrderMockService.Setup(service => service.Create(It.IsAny<GroupOrderModel>())).ReturnsAsync(() => 
                new GroupOrderModel(generatedId, providerId, lunchGroupId, DateTime.Today.AddDays(-2)));

            //Act
            var result = await _groupOrderController.CreateAsync(request);

            // Assert
            Assert.IsType<OkObjectResult>(result.Result);
            if (result.Result is OkObjectResult response)
            {
                var value = response.Value as GroupOrderResponse;
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }

                Assert.Equal(generatedId, value.Id);
            }
            _groupOrderMockService.Verify(service => service.Create(It.IsAny<GroupOrderModel>()), Times.Once);
        }

        [Fact]
        public async Task DeleteAsync_ShouldCallService()
        {
            //Arrange
            Guid testId = Guid.NewGuid();
            _groupOrderMockService.Setup(x => x.Delete(testId));

            // Act
            await _groupOrderController.DeleteAsync(testId);

            // Assert
            _groupOrderMockService.Verify(x => x.Delete(testId), Times.Once);
        }
    }
}
