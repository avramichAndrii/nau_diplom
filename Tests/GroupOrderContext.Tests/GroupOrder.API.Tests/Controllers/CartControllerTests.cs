using GroupOrder.API.Contracts.Requests;
using GroupOrder.API.Contracts.Responses;
using GroupOrder.API.Controllers;
using GroupOrder.Domain.Models;
using GroupOrder.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace GroupOrder.API.Tests.Controllers
{
    public class CartControllerTests
    {
        private readonly Mock<ICartService> _cartMockService = new Mock<ICartService>();
        private readonly CartController _cartController;
        private Guid workerId = Guid.NewGuid(); 
        private List<CartModel> GetCartTestModels()
        {
            return new List<CartModel>
            {
                new CartModel(Guid.NewGuid(), "Information for cart 1", Guid.NewGuid(), workerId, new List<Guid>{Guid.NewGuid(), Guid.NewGuid()}),
                new CartModel(Guid.NewGuid(), "Information for cart 2", Guid.NewGuid(), Guid.NewGuid(), new List<Guid>{Guid.NewGuid(), Guid.NewGuid()}),
                new CartModel(Guid.NewGuid(), "Information for cart 3", Guid.NewGuid(), Guid.NewGuid(), new List<Guid>{Guid.NewGuid(), Guid.NewGuid()}),
                new CartModel(Guid.NewGuid(), "Information for cart 4", Guid.NewGuid(), Guid.NewGuid(), new List<Guid>{Guid.NewGuid(), Guid.NewGuid()})
            };
        }

        public CartControllerTests()
        {
            _cartController = new CartController(_cartMockService.Object);
        }

        [Fact]
        public async Task Create_ShouldReturnCartModel()
        {
            // Arrange
            Guid generatedId = Guid.NewGuid();
            Guid providerId = Guid.NewGuid();
            Guid workerId = Guid.NewGuid();
            List<Guid> dishes = new List<Guid>()
            {
                Guid.NewGuid()
            };
            var request = new CartRequest(generatedId, dishes, "Info",providerId, workerId);
            _cartMockService.Setup(service => service.Create(It.IsAny<CartModel>()))
                            .ReturnsAsync(() => new CartModel(generatedId, "Info", providerId, workerId, dishes));
            
            //Act
            var result = await _cartController.CreateAsync(request);

            // Assert
            Assert.IsType<OkObjectResult>(result.Result);
            if (result.Result is OkObjectResult response)
            {
                var value = response.Value as CartResponse;
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }

                Assert.Equal(generatedId, value.Id);
            }
            _cartMockService.Verify(service => service.Create(It.IsAny<CartModel>()), Times.Once);
        }

        [Fact]
        public async Task Update_RequestInformationShouldBeDifferent_FromBaseInformation()
        {
            // Arrange
            Guid requestedId = Guid.NewGuid();
            Guid providerId = Guid.NewGuid();
            Guid workerId = Guid.NewGuid();
            List<Guid> dishes = new List<Guid>()
            {
                Guid.NewGuid()
            };
            var UpdatedModel = new CartModel(requestedId, "New information", providerId, workerId, dishes);

            _cartMockService.Setup(service => service.Update(It.IsAny<CartModel>()))
                            .ReturnsAsync(UpdatedModel);
            // Act
            var request = new CartRequest(requestedId, dishes, "Old information", providerId, workerId );


            var result = await _cartController.UpdateAsync(request);

            // Assert
            var response = result.Value;

            Assert.NotEqual(request.AdditionalInfo, response.AdditionalInfo);
            _cartMockService.Verify(service => service.Update(It.IsAny<CartModel>()), Times.Once);
        }

        [Fact]
        public async Task Delete_ShouldCallService()
        {
            //Arrange
            Guid testId = Guid.NewGuid();
            _cartMockService.Setup(service => service.Delete(testId));

            // Act
            await _cartController.DeleteAsync(testId);

            // Assert
            _cartMockService.Verify(service => service.Delete(testId), Times.Once);
        }

        [Fact]
        public async Task GetByWorkerIdAsync_ShouldReturnModel()
        {
            // Arrange
            CartModel model = GetCartTestModels().Select(model => model)
                                                 .Where(model => model.WorkerId == workerId)
                                                 .FirstOrDefault();

            _cartMockService.Setup(service => service.GetCartByWorkerId(workerId))
                            .ReturnsAsync(model);

            // Act
            var result = await _cartController.GetByWorkerIdAsync(workerId);

            // Assert
            Assert.IsType<OkObjectResult>(result.Result);
            if (result.Result is OkObjectResult response)
            {
                var value = response.Value as CartResponse;
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }
                Assert.Equal(model.AdditionalInfo, value.AdditionalInfo);
            }
            _cartMockService.Verify(service => service.GetCartByWorkerId(workerId), Times.Once);
        }       
    }
}

