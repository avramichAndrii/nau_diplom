﻿using GroupOrder.API.Contracts.Requests;
using GroupOrder.API.Contracts.Responses;
using GroupOrder.API.Controllers;
using GroupOrder.Domain.Models;
using GroupOrder.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace GroupOrder.API.Tests.Controllers
{
    public class OrderTemplateControllerTests
    {
        private readonly Mock<IOrderTemplateService> _orderTemplateServiceMock = new Mock<IOrderTemplateService>();
        private readonly OrderTemplateController _orderTemplateController;
        private readonly Guid _testMenuId = Guid.NewGuid();
        private readonly Guid _testWorkerId = Guid.NewGuid();

        public OrderTemplateControllerTests()
        {
            _orderTemplateController = new OrderTemplateController(_orderTemplateServiceMock.Object);
        }

        private List<OrderTemplateModel> GetOrderTemplateTestModels()
        {
            return new List<OrderTemplateModel>
            {
                new OrderTemplateModel(Guid.NewGuid(), "template A", Guid.NewGuid(), _testMenuId, new List<Guid>()),
                new OrderTemplateModel(Guid.NewGuid(), "template B", _testWorkerId, _testMenuId, new List<Guid>()),
                new OrderTemplateModel(Guid.NewGuid(), "template C", _testWorkerId, Guid.NewGuid(), new List<Guid>())
            };
        }

        [Fact]
        public async Task GetAllAsync_ShouldReturnList_Test()
        {
            // Arrange
            _orderTemplateServiceMock.Setup(service => service.GetAll()).ReturnsAsync(GetOrderTemplateTestModels());

            // Act
            var result = await _orderTemplateController.GetAllAsync();

            // Assert
            Assert.IsType<OkObjectResult>(result.Result);
            if (result.Result is OkObjectResult response)
            {
                var value = response.Value as GetAllOrderTemplatesResponse;
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }
                Assert.Equal(GetOrderTemplateTestModels().Count, value.OrderTemplatesCollection.Count);
            }
            _orderTemplateServiceMock.Verify(service => service.GetAll(), Times.Once);
        }

        [Fact]
        public async Task Create_ShouldReturnNewModel_Test()
        {
            // Arrange
            string name = "template 0";
            Guid orderTemplateId = Guid.NewGuid();
            Guid workerId = Guid.NewGuid();
            Guid menuId = Guid.NewGuid();

            var request = new OrderTemplateRequest() { Name = name, WorkerId = workerId };
            _orderTemplateServiceMock.Setup(service => service
                                                       .Create(It.IsAny<OrderTemplateModel>()))
                                                       .ReturnsAsync(() =>
                                                       new OrderTemplateModel(orderTemplateId, name, workerId, menuId, new List<Guid>()));

            //Act
            var result = await _orderTemplateController.CreateAsync(request);

            // Assert
            Assert.IsType<OkObjectResult>(result.Result);
            if (result.Result is OkObjectResult response)
            {
                var value = response.Value as OrderTemplateResponse;
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }

                Assert.Equal(orderTemplateId, value.Id);
            }
            _orderTemplateServiceMock.Verify(service => service.Create(It.IsAny<OrderTemplateModel>()), Times.Once);
        }

        [Fact]
        public async Task DeleteAsync_ShouldReturnId_Test()
        {
            //Arrange
            Guid deletedTemplateId = Guid.NewGuid();
            _orderTemplateServiceMock.Setup(service => service.Delete(deletedTemplateId));

            // Act
            var result = await _orderTemplateController.DeleteAsync(deletedTemplateId);

            // Assert
            _orderTemplateServiceMock.Verify(service => service.Delete(deletedTemplateId), Times.Once);
        }

        [Fact]
        public async Task Update_RequestNameShouldBeDifferent_FromBaseName_Test()
        {
            // Arrange
            Guid requestedId = Guid.NewGuid();
            string baseName = "my monday template";
            _orderTemplateServiceMock.Setup(service => service.UpdateName(It.IsAny<OrderTemplateModel>()))
                                                              .ReturnsAsync(() => 
                                                              new OrderTemplateModel(requestedId, baseName, Guid.NewGuid(), Guid.NewGuid(), new List<Guid>()));

            // Act
            var request = new OrderTemplateUpdateNameRequest()
            {
                Name = "my friday template"
            };
            var result = await _orderTemplateController.UpdateNameAsync(requestedId, request);

            // Assert
            Assert.IsType<OkObjectResult>(result.Result);
            if (result.Result is OkObjectResult response)
            {
                var value = response.Value as OrderTemplateResponse;
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }
                Assert.Equal(requestedId, value.Id);
                Assert.NotEqual(request.Name, value.Name);
            }
            _orderTemplateServiceMock.Verify(service => service.UpdateName(It.IsAny<OrderTemplateModel>()), Times.Once);
        }
        
        [Fact]
        public async Task GetByIdAsync_ExistingEntity_Test()
        {
            // Arrange
            Guid requestedId = Guid.NewGuid();
            _orderTemplateServiceMock.Setup(service => service.GetById(requestedId))
                                     .ReturnsAsync(
                                     new OrderTemplateModel(requestedId, "my favourite template", Guid.NewGuid(), Guid.NewGuid(), new List<Guid>()));
            
            // Act
            var result = await _orderTemplateController.GetByIdAsync(requestedId);

            // Assert
            Assert.IsType<OkObjectResult>(result.Result);
            if (result.Result is OkObjectResult response)
            {
                var value = response.Value as OrderTemplateResponse;
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }
                Assert.Equal(requestedId, value.Id);
            }
            _orderTemplateServiceMock.Verify(service => service.GetById(requestedId), Times.Once);
        }

        [Fact]
        public async Task GetAllByMenuIdAsync_ExistingEntity_Test()
        {
            // Arrange
            var appropriateModels = GetOrderTemplateTestModels()
                                    .Select(model => model)
                                    .Where(model => model.ProviderId == _testMenuId)
                                    .ToList();

            _orderTemplateServiceMock.Setup(service => service.GetAllByProviderId(_testMenuId))
                                     .ReturnsAsync(appropriateModels);
            
            // Act
            var result = await _orderTemplateController.GetAllByProviderIdAsync(_testMenuId);

            // Assert
            Assert.IsType<OkObjectResult>(result.Result);
            if (result.Result is OkObjectResult response)
            {
                var value = response.Value as GetAllOrderTemplatesResponse;
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }
                Assert.Equal(appropriateModels.Count, value.OrderTemplatesCollection.Count);
            }
            _orderTemplateServiceMock.Verify(service => service.GetAllByProviderId(_testMenuId), Times.Once);
        }

        [Fact]
        public async Task GetByAllWorkerIdAsync_ExistingEntity_Test()
        {
            // Arrange
            var appropriateModels = GetOrderTemplateTestModels()
                                    .Select(model => model)
                                    .Where(model => model.WorkerId == _testWorkerId)
                                    .ToList();

            _orderTemplateServiceMock.Setup(service => service.GetAllByWorkerId(_testWorkerId))
                                     .ReturnsAsync(appropriateModels);

            // Act
            var result = await _orderTemplateController.GetAllByWorkerIdAsync(_testWorkerId);

            // Assert
            Assert.IsType<OkObjectResult>(result.Result);
            if (result.Result is OkObjectResult response)
            {
                var value = response.Value as GetAllOrderTemplatesResponse;
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }
                Assert.Equal(appropriateModels.Count, value.OrderTemplatesCollection.Count);
            }
            _orderTemplateServiceMock.Verify(service => service.GetAllByWorkerId(_testWorkerId), Times.Once);
        }
    }
}
