using GroupOrder.API.Contracts.Requests;
using GroupOrder.API.Contracts.Responses;
using GroupOrder.API.Controllers;
using GroupOrder.Domain.Models;
using GroupOrder.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace GroupOrder.API.Tests.Controllers
{
    public class OrderControllerTests
    {
        private readonly Mock<IOrderService> _orderMockService = new Mock<IOrderService>();
        private readonly OrderController _orderController;
        private Guid workerId = Guid.NewGuid();
        private Guid providerId = Guid.NewGuid();
        private List<OrderModel> GetOrdersTestModels()
        {
            return new List<OrderModel>
            {
                new OrderModel(Guid.NewGuid(), DateTime.Now, "Information for order 1", Guid.NewGuid(), workerId, Guid.NewGuid(), new List<Guid>{Guid.NewGuid(), Guid.NewGuid()}),
                new OrderModel(Guid.NewGuid(), DateTime.Now, "Information for order 2", providerId, Guid.NewGuid(), Guid.NewGuid(), new List<Guid>{Guid.NewGuid(), Guid.NewGuid()}),
                new OrderModel(Guid.NewGuid(), DateTime.Now, "Information for order 3", Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), new List<Guid>{Guid.NewGuid(), Guid.NewGuid()}),
                new OrderModel(Guid.NewGuid(), DateTime.Now, "Information for order 4", providerId, workerId, Guid.NewGuid(), new List<Guid>{Guid.NewGuid(), Guid.NewGuid()})            
            };
        }

        public OrderControllerTests()
        {
            _orderController = new OrderController(_orderMockService.Object);
        }

        [Fact]
        public async Task GetById_ExistingEntity_Test()
        {
            // Arrange
            List<Guid> dishes = new List<Guid>()
            {
                Guid.NewGuid()
            };
            Guid requestedId = Guid.NewGuid();
            _orderMockService.Setup(x => x.GetById(requestedId))
                             .ReturnsAsync(new OrderModel(requestedId, DateTime.Now, "info", Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), dishes));
            // Act
            var result = await _orderController.GetByIdAsync(requestedId);

            // Assert
            Assert.IsType<OkObjectResult>(result.Result);
            if (result.Result is OkObjectResult response)
            {
                var value = response.Value as OrderResponse;
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }
                Assert.Equal(requestedId, value.Id);
            }
            _orderMockService.Verify(service => service.GetById(requestedId), Times.Once);

        }

        public static IEnumerable<object[]> TestOrder
        {
            get
            {
                yield return new object[] { "Some additional information for order", DateTime.Now, new List<Guid>() { Guid.NewGuid() } };
            }
        }

        [Fact]
        public async Task GetAll_ShouldReturn_LengthOfTestModels_Items()
        {            
            // Arrange
            _orderMockService.Setup(ms => ms.GetAll())
                             .ReturnsAsync(GetTestModels());

            // Act
            var result = await _orderController.GetAllAsync();

            // Assert
            Assert.IsType<OkObjectResult>(result.Result);
            if (result.Result is OkObjectResult response)
            {
                var value = response.Value as GetAllOrdersResponse;
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }
                Assert.Equal(GetTestModels().Count, value.OrdersCollection.Count);
            }
            _orderMockService.Verify(service => service.GetAll(), Times.Once);
        }
        #region testdata
        private List<OrderModel> GetTestModels()
        {
            List<Guid> dishes = new List<Guid>()
            {
                Guid.NewGuid()
            };
            return new List<OrderModel>
            {
                new OrderModel(Guid.NewGuid(), DateTime.Now, "Information for 1 order", Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), dishes),
                new OrderModel(Guid.NewGuid(), DateTime.Now, "Information for 2 order", Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), dishes),
                new OrderModel(Guid.NewGuid(), DateTime.Now, "Information for 3 order", Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), dishes),
            };
        }
        #endregion

        [Fact]
        public async Task GetAll_ReturnsEmptyList_WhenEmpty()
        {
            // Arrange
            _orderMockService.Setup(ms => ms.GetAll())
                             .ReturnsAsync(new List<OrderModel>());

            // Act
            var result = await _orderController.GetAllAsync();

            // Assert
            Assert.IsType<OkObjectResult>(result.Result);
            if (result.Result is OkObjectResult response)
            {
                var value = response.Value as GetAllOrdersResponse;
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }
                Assert.Empty(value.OrdersCollection);
            }
            _orderMockService.Verify(service => service.GetAll(), Times.Once);

        }

        [Fact]
        public async Task Create_ShouldReturnCartModel()
        {
            // Arrange
            Guid generatedId = Guid.NewGuid();
            Guid providerId = Guid.NewGuid();
            Guid workerId = Guid.NewGuid();
            Guid groupOrderId = Guid.NewGuid();
            Guid lunchGroupId = Guid.NewGuid();
            List<Guid> dishes = new List<Guid>()
            {
                Guid.NewGuid()
            };
            var request = new OrderRequest(generatedId, dishes,DateTime.Now, "Info",providerId, workerId, groupOrderId, lunchGroupId);
            _orderMockService.Setup(x => x.Create(It.IsAny<OrderModel>(), lunchGroupId)).ReturnsAsync(() =>
                new OrderModel(generatedId, DateTime.Now, "Info", providerId, workerId, groupOrderId, dishes));
            
            //Act
            var result = await _orderController.CreateAsync(request);

            // Assert
            Assert.IsType<OkObjectResult>(result.Result);
            if (result.Result is OkObjectResult response)
            {
                var value = response.Value as OrderResponse;
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }

                Assert.Equal(generatedId, value.Id);
                _orderMockService.Verify(service => service.Create(It.IsAny<OrderModel>(), lunchGroupId), Times.Once);

            }
        }

        [Fact]
        public async Task Update_RequestInformatoinShouldBeDifferent_FromBaseInformation()
        {
            // Arrange
            var request = new OrderRequest(Guid.NewGuid(), null, DateTime.Now, "Old information", Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid());
            _orderMockService.Setup(ms => ms.Update(It.IsAny<OrderModel>()))
                       .ReturnsAsync(()=> new OrderModel(Guid.NewGuid(), DateTime.Now, "New information", Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), null));
            
            // Act
            var result = await _orderController.UpdateOrderAsync(request);

            // Assert
            if (result.Result is OkObjectResult response)
            {
                var value = response.Value as OrderResponse;
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }
                Assert.NotEqual(request.AdditionalInfo, value.AdditionalInfo);
                _orderMockService.Verify(service => service.Update(It.IsAny<OrderModel>()), Times.Once);
            }
        }

        [Fact]
        public async Task Delete_ShouldCallService()
        {
            //Arrange
            Guid testId = Guid.NewGuid();
            _orderMockService.Setup(x => x.Delete(testId));

            // Act
            await _orderController.DeleteAsync(testId);

            // Assert
            _orderMockService.Verify(x => x.Delete(testId), Times.Once);
        }

        [Fact]
        public async Task GetByProviderIdAsync_ShouldReturnList()
        {
            // Arrange
            List<OrderModel> model = GetOrdersTestModels().Select(model => model)
                                                   .Where(model => model.ProviderId == providerId)
                                                   .ToList();

            _orderMockService.Setup(service => service.GetAllByProviderId(providerId))
                             .ReturnsAsync(model);

            // Act
            var result = await _orderController.GetByProviderIdAsync(providerId);

            // Assert
            Assert.IsType<OkObjectResult>(result.Result);
            if (result.Result is OkObjectResult response)
            {
                var value = response.Value as GetAllOrdersResponse;
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }
                Assert.Equal(model.Count, value.OrdersCollection.Count);
            }
            _orderMockService.Verify(service => service.GetAllByProviderId(providerId), Times.Once);
        }

        [Fact]
        public async Task GetByWorkerIdAsync_ShouldReturnList()
        {
            // Arrange
            List<OrderModel> model = GetOrdersTestModels().Select(model => model)
                                                          .Where(model => model.WorkerId == workerId)
                                                          .ToList();

            _orderMockService.Setup(service => service.GetAllByWorkerId(workerId))
                             .ReturnsAsync(model);

            // Act
            var result = await _orderController.GetAllByWorkerIdAsync(workerId);

            // Assert
            Assert.IsType<OkObjectResult>(result.Result);
            if (result.Result is OkObjectResult response)
            {
                var value = response.Value as GetAllOrdersResponse;
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }
                Assert.Equal(model.Count, value.OrdersCollection.Count);
            }
            _orderMockService.Verify(service => service.GetAllByWorkerId(workerId), Times.Once);
        }
    }
}

