﻿using System;
using System.Globalization;

namespace Domain.Core
{
    public class EntityIdAlreadySetException : Exception
    {
        public EntityIdAlreadySetException() : base() { }

        public EntityIdAlreadySetException(string message) : base(message) { }

        public EntityIdAlreadySetException(string message, params object[] args)
            : base(string.Format(CultureInfo.CurrentCulture, message, args)) { }
    }
}