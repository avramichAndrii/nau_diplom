﻿namespace Domain.Core
{
    public abstract class Entity<EntityId>
    {
        private EntityId _id;
        public EntityId Id
        {
            get => _id;
            protected set
            {
                if (Equals(value, default(EntityId)) ||
                    !Equals(_id, default(EntityId)))
                {
                    throw new EntityIdAlreadySetException("Entity id should be set only once per object lifecycle");
                }

                _id = value;
            }
        }

        public Entity() { }

        public Entity(EntityId id)
        {
            Id = id;
        }

        public static bool operator ==(Entity<EntityId> e1, object e2)
        {
            if (ReferenceEquals(e1, null))
            {
                return e2 == null;
            }

            Entity<EntityId> e2AsEntity = e2 as Entity<EntityId>;
            if (e2AsEntity == null) return false;
            return e1.GetType() == e2.GetType() && e1.Id.Equals(e2AsEntity.Id);
        }

        public static bool operator !=(Entity<EntityId> e1, object e2)
        {
            return !(e1 == e2);
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj.GetType() != this.GetType()) return false;

            Entity<EntityId> objAsEntity = obj as Entity<EntityId>;

            return Id.Equals(objAsEntity.Id);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}