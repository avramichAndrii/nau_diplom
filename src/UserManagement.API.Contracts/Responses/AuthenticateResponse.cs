﻿using System;

namespace UserManagement.API.Contracts.Responses
{
    public class AuthenticateResponse
    {
        public string AccessToken { get; set; }
        public Guid RefreshToken { get; set; }
        public DateTime ExpiresAt { get; set; }

    }
}
