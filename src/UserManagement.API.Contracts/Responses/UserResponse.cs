﻿using System;

namespace UserManagement.API.Contracts.Responses
{
    public class UserResponse
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public Guid CompanyId { get; set; }
    }
}
