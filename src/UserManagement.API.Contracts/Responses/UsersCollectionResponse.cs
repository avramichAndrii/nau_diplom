﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace UserManagement.API.Contracts.Responses
{
    public class UsersCollectionResponse
    {
        public IReadOnlyCollection<UserResponse> UsersCollection { get; private set; }
   
        public UsersCollectionResponse(IList<UserResponse> usersCollection)
        {
            UsersCollection = new ReadOnlyCollection<UserResponse>(usersCollection);
        }
    }
}
