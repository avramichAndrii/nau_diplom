﻿namespace UserManagement.API.Contracts.Responses
{
    public class HandleForgottenPasswordResponse
    {
        public string Email { get; set; }
    }
}