using System.ComponentModel.DataAnnotations;

namespace UserManagement.API.Contracts.Requests
{
    public class AuthenticateRequest
    {
        [Required]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }
    }
}