using System.ComponentModel.DataAnnotations;

namespace UserManagement.API.Contracts.Requests
{ 
  public class UpdateRequest
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Login { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public string CompanyId { get; set; }
    }
}