﻿using System.ComponentModel.DataAnnotations;

namespace UserManagement.API.Contracts.Requests
{
    public class UpdatePasswordRequest
    {
        [Required]
        [MinLength(8)]
        public string OldPassword { get; set; }

        [Required]
        [MinLength(8)]
        public string NewPassword { get; set; }
    }
}
