﻿using System.ComponentModel.DataAnnotations;

namespace UserManagement.API.Contracts.Requests
{
    public class ForgotPasswordRequest
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
