﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UserManagement.API.Contracts.Requests
{
    public class ValidateResetTokenRequest
    {
        [Required]
        public Guid UserId { get; set; }
        public Guid Token { get; set; }
    }
}