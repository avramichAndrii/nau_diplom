﻿using System;

namespace UserManagement.API.Contracts.Requests
{
    public class SetCompanyRequest
    {
        public Guid CompanyId { get; set; }
    }
}
