﻿namespace UserManagement.API.Contracts.Routs
{
    public static class UserManagementRouting
    {
        public const string BaseControllerUri = "user/";
        public const string BasePureControllerUri = "user";
        public const string AuthenticateUri =  "authenticate";
        public const string RegisterUri =  "register";
        public const string UsersUri = "users";
        public const string ChangePasswordUri = "change-password/";
        public const string CompanyUri = "company";
        public const string RoleUri = "role";
        public const string RefreshUri = "refresh-token/";
        public const string HandleForgottenPasswordUri = "forgot-password"; 
        public const string ResetPasswordUri = "reset-password";
    }
}
