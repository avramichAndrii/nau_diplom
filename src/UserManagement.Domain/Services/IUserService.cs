﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UserManagement.Domain.Models;

namespace UserManagement.Domain.Services
{
    public interface IUserService
    {
        Task<AuthenticateModel> AuthenticateAsync(string login, string password);
        Task<IReadOnlyCollection<User>> GetAllAsync();
        Task<IReadOnlyCollection<User>> GetAllByCompanyAndRoleAsync(Guid companyId, string role);
        Task<User> GetByIdAsync(Guid id);
        Task<User> CreateAsync(User user, string password);
        Task<User> UpdateAsync(User user);
        Task<User> DeleteAsync(Guid id);
        Task<string> HandleForgottenPassword(string email);
        Task<User> ResetPassword(string token, string password);
        Task SetCompanyAsync(Guid userId, Guid companyId);
        Task<User> UpdatePasswordAsync(string oldPassword, string newPassword, Guid id);
        Task<User> SetPasswordAsync(User user, string password);
        Task<AuthenticateModel> RefreshTokenAsync(Guid token);
    }
}
