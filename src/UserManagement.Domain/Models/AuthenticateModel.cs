﻿using System;

namespace UserManagement.Domain.Models
{
    public class AuthenticateModel
    {
        public string AccessToken { get; set; }
        public Guid RefreshToken { get; set; }
        public DateTime ExpiresAt { get; set; }
    }
}
