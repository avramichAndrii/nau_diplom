﻿namespace UserManagement.Domain.Models
{
    public static class Role
    {
        public const string Admin = nameof(Admin);
        public const string Worker = nameof(Worker);
        public const string Manager = nameof(Manager);
        public const string Provider = nameof(Provider);
    }
}
