using Domain.Core;
using System;

namespace UserManagement.Domain.Models
{
    public class User : Entity<Guid>
    {
        public string Login { get; set; }

        public string Email { get; set; }

        public byte[] PasswordHash { get; private set; }

        public byte[] PasswordSalt { get; private set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Guid CompanyId { get; set; }

        public string Role { get; set; }

        public User() { }

        public User(string login, string email, string firstName,
                    string lastName, Guid companyId, string role,
                    byte[] passwordHash, byte[] passwordSalt) : base (Guid.NewGuid())
        {
            Login = login;
            Email = email;
            FirstName = firstName;
            LastName = lastName;
            CompanyId = companyId;
            Role = role;
            PasswordHash = passwordHash;
            PasswordSalt = passwordSalt;
        }

        public void SetPasswordItems(byte[] passwordHash, byte[] passwordSalt)
        {
            PasswordHash = passwordHash;
            PasswordSalt = passwordSalt;
        }

        public void SetId(Guid id)
        {
            Id = id;
        }
    }
}