﻿using Domain.Core;
using System;

namespace UserManagement.Domain.Models
{
    public class RefreshToken : Entity<Guid>
    {
        public Guid UserId { get; set; }
        public Guid Token { get; set; }
        public DateTime ExpiresAt { get; set; }
    }
}