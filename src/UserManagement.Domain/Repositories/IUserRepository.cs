﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UserManagement.Domain.Models;

namespace UserManagement.Domain.Repositories
{
    public interface IUserRepository
    {
        Task<User> UpdateAsync(User userModel);
        Task<User> CreateAsync(User userModel);
        Task<User> DeleteAsync(Guid userId);
        Task<IReadOnlyCollection<User>> GetAllAsync();
        Task<IReadOnlyCollection<User>> GetAllByCompanyAndRoleAsync(Guid companyId, string role);
        Task<User> GetByIdAsync(Guid userId);
        Task<User> GetByLoginAsync(string userLogin);
        Task<User> GetByEmailAsync(string userEmail);
        Task SetCompanyAsync(Guid userId, Guid companyId);
        Task<User> UpdatePasswordAsync(User newUser);
        Task<RefreshToken> GetRefreshTokenAsync(Guid token);
        Task<RefreshToken> CreateRefreshTokenAsync(RefreshToken token);
        Task DeleteRefreshTokenAsync(RefreshToken token);
    }
}
