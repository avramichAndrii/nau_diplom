﻿namespace Gateway.Application.Helpers.AssignToken
{
    public class AssignmentSettings
    {
        public string Secret { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public string JwtLifetime { get; set; }
    }
}
