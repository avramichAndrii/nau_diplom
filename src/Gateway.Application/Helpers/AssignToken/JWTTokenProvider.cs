﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace Gateway.Application.Helpers.AssignToken
{
    public static class JWTTokenProvider
    {
        private const string EmailClaimType = "email";
        private const string NameIdClaimType = "nameid";
        private const string RoleClaimType = "role";
        private const string InvalidClaimsMessage = "Token claims has an invalid data format";

        public static string GenerateAssignToken(AssignmentSettings regSettings,
            string email, string role, Guid companyId)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(regSettings.Secret);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Email, email),
                    new Claim(ClaimTypes.NameIdentifier, companyId.ToString()),
                    new Claim(ClaimTypes.Role, role)
                }),
                Issuer = regSettings.Issuer,
                IssuedAt = DateTime.UtcNow,
                Audience = regSettings.Audience,
                Expires = DateTime.UtcNow.AddMinutes(int.Parse(regSettings.JwtLifetime)),
                NotBefore = DateTime.UtcNow,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public static bool ValidateAssignToken(AssignmentSettings regSettings, string token)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(regSettings.Secret));

            var tokenHandler = new JwtSecurityTokenHandler();
            try
            {
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidIssuer = regSettings.Issuer,
                    ValidAudience = regSettings.Audience,
                    IssuerSigningKey = securityKey,
                    ClockSkew = new TimeSpan(0)
                }, out SecurityToken validatedToken);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public static void GetClaims(string token,
            out string email, out string role, out Guid companyId)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var securityToken = tokenHandler.ReadToken(token) as JwtSecurityToken;

            try
            {
                email = securityToken.Claims.Single(claim => claim.Type == EmailClaimType).Value;
                role = securityToken.Claims.Single(claim => claim.Type == RoleClaimType).Value;
                string strCompanyId = securityToken.Claims.Single(claim => claim.Type == NameIdClaimType).Value;
                companyId = Guid.Parse(strCompanyId);
            }
            catch
            {
                throw new FormatException(InvalidClaimsMessage);
            }
        }
    }
}
