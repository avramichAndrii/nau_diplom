﻿using Company.API.Contracts.Request;
using Company.API.Contracts.Response;
using System;
using System.Threading.Tasks;

namespace Gateway.Application.Abstractions
{
    public interface ICompanyRedirectorService
    {
        #region company

        public Task<GetCompaniesResponse> GetCompaniesAsync();
        public Task<CompanyResponse> GetCompanyAsync(Guid id);
        public Task<GetCompaniesResponse> GetCompaniesByTypeAsync(string type);
        public Task<CompanyResponse> CreateCompanyAsync(CompanyRequest request);
        public Task<CompanyResponse> UpdateCompanyAsync(Guid id, CompanyRequest request);
        public Task<DeleteCompanyResponse> RemoveCompanyAsync(Guid id);

        #endregion

        #region contract

        Task<GetContractsResponse> GetContractsAsync();
        Task<ContractResponse> GetContractByIdAsync(Guid contractId);
        Task<GetContractsResponse> GetContractsByCompanyTypeAndIdAsync(Guid companyId, string companyType);
        Task<GetContractsResponse> GetContractsByStatusAndCompanyTypeAndIdAsync
            (Guid companyId, string type, string status);
        Task<ContractResponse> CreateContractAsync(ContractRequest request);
        Task<ContractResponse> UpdateContractAsync(Guid contractId, ContractRequest request);

        #endregion

    }
}
