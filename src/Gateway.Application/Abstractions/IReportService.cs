﻿using System;
using System.Threading.Tasks;

namespace Gateway.Application.Abstractions
{
    public interface IReportService
    {
        public Task<byte[]> GetFileAsync(Guid userId, DateTime startDate, DateTime endDate, Guid companyId = default, Guid lunchGroupId = default);
        public Task SendFileAsync(Guid userId, DateTime startDate, DateTime endDate, Guid companyId = default, Guid lunchGroupId = default);
        public Task<byte[]> CreateProviderTableAndReturnCreatedFilePath();
    }
}
