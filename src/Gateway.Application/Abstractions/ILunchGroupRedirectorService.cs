﻿using LunchGroup.API.Contracts.Requests;
using LunchGroup.API.Contracts.Responses;
using System;
using System.Threading.Tasks;

namespace Gateway.Application.Services
{
    public interface ILunchGroupRedirectorService
    {
        Task<CreateLunchGroupResponse> CreateLunchGroupAsync(CreateLunchGroupRequest lunchGroupRequest);
        Task<GetLunchGroupsCollectionResponse> GetAllLunchGroupsByCompanyAsync(Guid companyId);
        Task<GetLunchGroupResponse> GetLunchGroupByIdAsync(Guid groupId);
        Task<GetLunchGroupsCollectionResponse> GetLunchGroupsByManagerAsync(Guid managerId);
        Task<GetTimetableResponse> GetTimetableAsync(Guid groupId);
        Task<GetLunchGroupResponse> GetWorkerLunchGroupAsync(Guid workerId);
        Task SetActiveFlagAsync(Guid groupId, SetActiveFlagRequest request);
        Task<Guid> UpdateLunchGroupAsync(UpdateLunchGroupRequest lunchGroupToUpdate, Guid id);
        Task UpdateTimetableAsync(Guid groupId);

        Task<GetAllDeliveriesResponse> GetAllDeliveriesAsync();
        Task<DeliveryResponse> GetDeliveryByIdAsync(Guid deliveryId);
        Task<DeliveryResponse> GetDeliveryByGroupOrderIdAsync(Guid orderId);
        Task<DeliveryResponse> CreateDeliveryAsync(Guid deliveryId, CreateDeliveryRequest request);
        Task<DeliveryResponse> UpdateDeliveryAsync(Guid deliveryId, UpdateDeliveryRequest request);
        Task<DeliveryResponse> DeleteDeliveryAsync(Guid deliveryId);
    }
}