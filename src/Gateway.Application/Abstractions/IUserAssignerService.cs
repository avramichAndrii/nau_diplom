﻿using System;
using System.Threading.Tasks;
using UserManagement.API.Contracts.Requests;
using UserManagement.API.Contracts.Responses;

namespace Gateway.Application.Abstractions
{
    public interface IUserAssignerService
    {
        Task SendInvitation(Guid inviterId, string invitedUserRole, string workerEmail);
        Task<UserResponse> RegisterWorker(string token, RegisterRequest request);
    }
}