﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UserManagement.API.Contracts.Requests;
using UserManagement.API.Contracts.Responses;

namespace Gateway.Application
{
    public interface IUserManagementRedirectorService
    {
        Task<AuthenticateResponse> AuthenticateAsync(AuthenticateRequest model);
        Task<Guid> DeleteByIdAsync(Guid Id);
        Task<UserResponse> GetByIdAsync(Guid Id);
        Task<UserResponse> UpdateByIdAsync(Guid Id, UpdateRequest model);
        Task SetCompanyForUserAsync(Guid id, SetCompanyRequest request);
        Task<UserResponse> RegisterAsync(RegisterRequest model);
        Task<UsersCollectionResponse> GetUsersAsync();
        Task UpdatePasswordAsync(Guid Id, UpdatePasswordRequest model);
        Task<UsersCollectionResponse> GetUsersByCompanyAndRoleAsync(Guid companyId, string role);
        Task<HandleForgottenPasswordResponse> HandleForgottenPassword(ForgotPasswordRequest forgotPasswordRequest);
        Task<AuthenticateResponse> RefreshToken(Guid token);
        Task<UserResponse> ResetPassword(ResetPasswordRequest request);
    }
}
