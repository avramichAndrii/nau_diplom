﻿using Company.API.Contracts.Response;
using GroupOrder.API.Contracts.Responses;
using LunchGroup.API.Contracts.Responses;
using Menu.API.Contracts.Responses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gateway.Application.Abstractions.Reports
{
    public interface IReportAggregatorWorkerService
    {
        public Task<string> getProviderCompanyAsync(Guid providerId);
        public Task<List<OrderResponse>> GetOrdersAsync(Guid userId, DateTime startDate, DateTime endDate);
        public Task<string> GetUserInformationAsync(Guid userId);

    }
}
