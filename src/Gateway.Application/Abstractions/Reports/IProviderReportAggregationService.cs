﻿using GroupOrder.API.Contracts.Responses;
using LunchGroup.API.Contracts.Responses;
using Menu.API.Contracts.Responses;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Gateway.Application.Abstractions.Reports
{
    public interface IProviderReportAggregationService
    {
        public string getProviderCompanyName(Guid providerId);
        public string[] GetMenusPrice(Guid providerId);
        public Guid GetCompanyId(Guid providerId);
        public IReadOnlyCollection<GroupOrderResponse> getGroupOrders(Guid providerId);
        public Guid ConsumerGuid(string companyName);
        public Task<GetAllMenusResponse> GetMenuNames(Guid providerId);
        public Task<List<OrderResponse>> GetOrdersAsync(Guid userId);
       
      
    }
}
