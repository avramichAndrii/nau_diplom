﻿using System;
using System.Threading.Tasks;

namespace Gateway.Application.Abstractions.Reports
{
    public interface IReportGeneratorService
    {
        public Task<byte[]> GenerateReportAsync(Guid userId, DateTime startDate, DateTime endDate);
    }
}
