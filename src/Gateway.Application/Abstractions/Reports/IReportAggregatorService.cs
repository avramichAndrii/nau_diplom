﻿using Company.API.Contracts.Response;
using GroupOrder.API.Contracts.Responses;
using LunchGroup.API.Contracts.Responses;
using Menu.API.Contracts.Responses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UserManagement.API.Contracts.Responses;

namespace Gateway.Application.Abstractions.Reports
{
    public interface IReportAggregatorService
    {
        public Task<UserResponse> GetUserDataAsync(Guid userId);
        public Task<string> GetCompanyNameAsync(Guid companyId);
        public Task<IReadOnlyCollection<GetLunchGroupResponse>> GetLunchGroupsDataAsync(Guid userId);
        public Task<string> GetLunchGroupNameAsync(Guid lunchGroupId);
        public Task<decimal> GetDishesCostAsync(IReadOnlyCollection<Guid> dishes);
        public Task<decimal> GetGroupOrderSummaryCostAsync(GroupOrderResponse groupOrder);
        public Task<IReadOnlyCollection<GroupOrderResponse>> GetGroupOrdersDataAsync(
            IReadOnlyCollection<Guid> lunchGroupIds,
            DateTime startDate, DateTime endDate);
        public IReadOnlyCollection<OrderResponse> GetOrdersData(IReadOnlyCollection<GroupOrderResponse> groupOrders);
        public Task<IReadOnlyCollection<CompanyResponse>> GetProviderCompaniesDataAsync(IReadOnlyCollection<GroupOrderResponse> groupOrders);
        public Task<IReadOnlyCollection<MenuResponse>> GetMenuesDataAsync(IReadOnlyCollection<GroupOrderResponse> groupOrders);
        public HashSet<string> GetMenuNamesByGroupOrder(GroupOrderResponse groupOrder, List<MenuResponse> menues);
        public HashSet<string> GetMenuNamesByOrder(OrderResponse order, List<MenuResponse> menues);
    }
}
