﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Gateway.Application.Abstractions.Reports
{
    public interface IDetailedProviderReport
    {
        public Task<byte[]> GenerateProviderDetailedReportAsync(Guid userId,string companyName, DateTime startDate, DateTime endDate);
    }
}
