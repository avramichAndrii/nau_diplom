﻿using System;
using System.Threading.Tasks;

namespace Gateway.Application.Abstractions.Reports
{
    public interface IReportSenderService
    {
        public Task<string> SendReportFileAsync(Guid userId, byte[] reportContent);
    }
}
