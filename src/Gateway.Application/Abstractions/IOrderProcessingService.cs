﻿using GroupOrder.API.Contracts.Requests;
using GroupOrder.API.Contracts.Responses;
using LunchGroup.API.Contracts.Responses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gateway.Application.Services
{
    public interface IOrderProcessingService
    {
        Task<OrderResponse> CreateOrderByWorker(OrderRequest createOrderRequest);
        Task<IReadOnlyCollection<DeliveryResponse>> CreateDeliveriesByManager(Guid lunchGroupId);
    }
}