﻿using GroupOrder.API.Contracts.Requests;
using GroupOrder.API.Contracts.Responses;
using System;
using System.Threading.Tasks;

namespace Gateway.Application.Abstractions
{
    public interface IGroupOrderRedirectorService
    {
        #region GroupOrder
        public Task<GetAllGroupOrdersResponse> GetGroupOrdersAsync();
        public Task<GroupOrderResponse> GetGroupOrderByIdAsync(Guid groupOrderId);
        public Task<GetAllGroupOrdersResponse> GetGroupOrdersByProviderIdAsync(Guid providerId);
        public Task<GetAllGroupOrdersResponse> GetAllByLunchGroupsIdAsync(Guid lunchGroupId);
        public Task<GroupOrderResponse> CreateGroupOrderAsync(GroupOrderRequest request);
        public Task<DeleteGroupOrderResponse> DeleteGroupOrderAsync(Guid groupOrderId);

        #endregion

        #region Order
        public Task<GetAllOrdersResponse> GetOrdersAsync();
        public Task<OrderResponse> GetOrderByIdAsync(Guid orderId);
        public Task<GetAllOrdersResponse> GetOrdersByProviderIdAsync(Guid providerId);
        public Task<GetAllOrdersResponse> GetOrdersByWorkerIdAsync(Guid workerId);
        public Task<OrderResponse> CreateOrderAsync(OrderRequest request);
        public Task<DeleteOrderResponse> DeleteOrderAsync(Guid orderId);
        public Task<OrderResponse> UpdateOrderAsync(OrderRequest request);
        
        #endregion
         
        #region OrderTemplate
         
        public Task<GetAllOrderTemplatesResponse> GetOrderTemplatesAsync();
        public Task<OrderTemplateResponse> GetOrderTemplateByIdAsync(Guid orderTemplateId);
        public Task<GetAllOrderTemplatesResponse> GetOrderTemplatesByWorkerIdAsync(Guid workerId);
        public Task<GetAllOrderTemplatesResponse> GetOrderTemplatesByProviderIdAsync(Guid providerId);
        public Task<OrderTemplateResponse> CreateOrderTemplateAsync(OrderTemplateRequest request);
        public Task DeleteOrderTemplateAsync(Guid orderTemplateId);
        public Task UpdateOrderTemplateNameAsync(Guid id, OrderTemplateUpdateNameRequest request);
        public Task UpdateOrderTemplateDishesAsync(Guid id, OrderTemplateUpdateDishesRequest request);

        #endregion

        #region Cart
        public Task<CartResponse> GetCartByIdAsync(Guid cartId);
        public Task<CartResponse> GetCartByWorkerIdAsync(Guid workerId);
        public Task<CartResponse> CreateCartAsync(CartRequest request);
        public Task DeleteCartAsync(Guid cartId);
        public Task<CartResponse> UpdateCartAsync(CartRequest request);

        #endregion
    }
}