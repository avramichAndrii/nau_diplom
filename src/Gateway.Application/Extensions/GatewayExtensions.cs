﻿using Gateway.Application.Abstractions;
using Gateway.Application.Abstractions.Reports;
using Gateway.Application.Services;
using Gateway.Application.Services.Reports;
using Microsoft.Extensions.DependencyInjection;

namespace Gateway.Application.Extensions
{
    public static class GatewayExtensions
    {
        public static void RegisterGateway(this IServiceCollection collection)
        {
            collection.AddScoped<IUserManagementRedirectorService, UserManagementRedirectorService>();
            collection.AddScoped<ICompanyRedirectorService, CompanyRedirectorService>();
            collection.AddScoped<IMenuRedirectorService, MenuRedirectorService>();
            collection.AddScoped<IGroupOrderRedirectorService, GroupOrderRedirectorService>();
            collection.AddScoped<IUserAssignerService, UserAssignerService>();
            collection.AddScoped<ILunchGroupRedirectorService, LunchGroupRedirectorService>();
            collection.AddScoped<IOrderProcessingService, OrderProcessingService>();
            collection.AddScoped<IReportAggregatorService, ManagerReportAggregatorService>();
            collection.AddScoped<IReportGeneratorService, ManagerReportGeneratorService>();
            collection.AddScoped<IFilteredReportGeneratorService, ManagerFilteredReportGeneratorService>();
            collection.AddScoped<IReportSenderService, ReportSenderService>();
            collection.AddScoped<IReportService, ReportService>();
            collection.AddScoped<IReportAggregatorWorkerService, WorkerReportAggregatorService>();
            collection.AddScoped<IReportGeneratorService, WorkerReportGeneratorService>();
            collection.AddScoped<IProviderReportAggregationService, ProviderReportAggregatorService>();
            collection.AddScoped<IReportGeneratorService, ProviderReportGeneratorService>();
            collection.AddScoped<IDetailedProviderReport, ProviderReportGeneratorService>();
        }
    }
}