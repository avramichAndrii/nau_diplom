﻿using Gateway.Application.Abstractions;
using Infrastructure.HttpClientAbstractions.Interfaces;
using OfficeOpenXml;
using System;
using System.Threading.Tasks;
using UserManagement.API.Client.ClientInterface;

namespace Gateway.Application.Services.Reports
{
    public class ReportService : IReportService
    {
        private readonly IUserManagementClient _userManagementClient;

        public ReportService(IClientFactory clientFactory)
        {
            _userManagementClient = clientFactory.GetClient<IUserManagementClient>();
            //put another clients here
        }

        public Task<byte[]> GetFileAsync(Guid userId, DateTime startDate, DateTime endDate, Guid companyId = default, Guid lunchGroupId = default)
        {
            throw new NotImplementedException();
        }

        public Task SendFileAsync(Guid userId, DateTime startDate, DateTime endDate, Guid companyId = default, Guid lunchGroupId = default)
        {
            throw new NotImplementedException();
        }

        public Task<byte[]> CreateProviderTableAndReturnCreatedFilePath()
        {
            var tmp = _userManagementClient.GetAllAsync();
            //string path = "test1.xlsx";// to change add your path here
            //System.IO.File.Delete(path);
            //System.IO.FileInfo spreadsheetInfo = new System.IO.FileInfo(path);
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage excel = new ExcelPackage();
            var userCollectionTable = excel.Workbook.Worksheets.Add("Users");
            userCollectionTable.Cells["A1"].Value = "Name";
            userCollectionTable.Cells["B1"].Value = "LastName";
            userCollectionTable.Cells["C1"].Value = "Role";
            userCollectionTable.Cells["A1:B1"].Style.Font.Bold = true;
            //filling table with data
            int currentRow = 2;
            foreach (var user in tmp.Result.UsersCollection)
            {
                userCollectionTable.Cells["A" + currentRow.ToString()].Value = user.FirstName;
                userCollectionTable.Cells["B" + currentRow.ToString()].Value = user.LastName;
                userCollectionTable.Cells["C" + currentRow.ToString()].Value = user.Role;
                currentRow++;
            }
            userCollectionTable.View.FreezePanes(2, 1);
            return excel.GetAsByteArrayAsync();
        }
    }
}
