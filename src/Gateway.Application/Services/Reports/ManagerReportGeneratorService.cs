﻿using Company.API.Contracts.Response;
using Gateway.Application.Abstractions.Reports;
using GroupOrder.API.Contracts.Responses;
using LunchGroup.API.Contracts.Responses;
using Menu.API.Contracts.Responses;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using UserManagement.API.Contracts.Responses;

namespace Gateway.Application.Services.Reports
{
    public class ManagerReportGeneratorService : IReportGeneratorService
    {
        private readonly IReportAggregatorService _reportAggregatorService;
        private const string DateFormat = "dd.MM.yyyy";

        public ManagerReportGeneratorService(IReportAggregatorService reportAggregatorService)
        {
            _reportAggregatorService = reportAggregatorService;
        }

        /// <summary>
        /// Aggregates data for specified parameters, constructs datatable and writes it to Excel worksheet.
        /// </summary>
        /// <param name="managerId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="lunchGroupId"></param>
        /// <returns>The task result contains a byte array which represents contents of generated file.</returns>
        public async Task<byte[]> GenerateReportAsync(Guid managerId, DateTime startDate, DateTime endDate)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using ExcelPackage excel = new ExcelPackage();
            var worksheet = excel.Workbook.Worksheets.Add("Report");

            int currentRow = 1;
            void AddHeader(string headerContent)
            {
                worksheet.Cells["A" + currentRow].Value = headerContent;
                worksheet.Cells[$"A{currentRow}:F{currentRow}"].Merge = true;
                worksheet.Row(currentRow).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                currentRow++;
            }

            //headers
            string reportPeriodHeader = $"Report period: {startDate.ToString(DateFormat)} - {endDate.ToString(DateFormat)}";
            AddHeader(reportPeriodHeader);
            UserResponse userResponse = await _reportAggregatorService.GetUserDataAsync(managerId);
            string companyName = await _reportAggregatorService.GetCompanyNameAsync(userResponse.CompanyId);
            string companyHeader = $"Company: {companyName}";
            AddHeader(companyHeader);
            string managerHeader = $"Manager: {userResponse.FirstName} {userResponse.LastName}";
            AddHeader(managerHeader);

            //report
            IReadOnlyCollection<GetLunchGroupResponse> lunchGroups =
                await _reportAggregatorService.GetLunchGroupsDataAsync(managerId);
            List<Guid> lunchGroupIds = lunchGroups.Select(lg => lg.Id).ToList();
            IReadOnlyCollection<GroupOrderResponse> groupOrders =
                await _reportAggregatorService.GetGroupOrdersDataAsync(lunchGroupIds, startDate, endDate);
            IReadOnlyCollection<CompanyResponse> providers =
                await _reportAggregatorService.GetProviderCompaniesDataAsync(groupOrders);
            IReadOnlyCollection<MenuResponse> menues =
                await _reportAggregatorService.GetMenuesDataAsync(groupOrders);

            DataTable dataTable = new DataTable();

            //headers
            dataTable.Columns.Add("Order date", typeof(string));
            dataTable.Columns.Add("LunchGroup name", typeof(string));
            dataTable.Columns.Add("LunchGroup address", typeof(string));
            dataTable.Columns.Add("Provider name", typeof(string));
            dataTable.Columns.Add("Menues", typeof(string));
            dataTable.Columns.Add("Order summ, UAH", typeof(decimal));

            //fill with data
            foreach (GroupOrderResponse groupOrder in groupOrders)
            {
                DateTime orderDate = groupOrder.Date;
                GetLunchGroupResponse lunchGroup = lunchGroups.Single(lg => lg.Id == groupOrder.LunchGroupId);
                string lunchGroupName = lunchGroup.Name;
                AddressResponse address = lunchGroup.Address;
                string lunchGroupAddress = string.Format("{0} city, {1} {2} str., office {3}",
                    address.City, address.BuildingNumber, address.Street, address.Office);
                CompanyResponse provider = providers.Single(p => p.CompanyId == groupOrder.ProviderId);
                string providerName = provider.Name;

                List<MenuResponse> menuesByProvider = menues.Where(m => m.ProviderCompanyId == groupOrder.ProviderId).ToList();
                HashSet<string> menuesInGroupOrder = _reportAggregatorService.GetMenuNamesByGroupOrder(groupOrder, menuesByProvider);
                string menuesList = string.Join(", ", menuesInGroupOrder).TrimEnd(',', ' ');
                decimal orderSumm = await _reportAggregatorService.GetGroupOrderSummaryCostAsync(groupOrder);

                dataTable.Rows.Add(orderDate.ToString(DateFormat), lunchGroupName, lunchGroupAddress, providerName, menuesList, orderSumm);
            }
            currentRow++;
            worksheet.Cells["A"+currentRow].LoadFromDataTable(dataTable, true);
            const int SingleColumnWidth = 20;
            for (int i = 1; i <= dataTable.Columns.Count; i++)
            {
                worksheet.Column(i).Width = SingleColumnWidth;
            }
            worksheet.Row(currentRow).Style.Font.Bold = true;
            return await excel.GetAsByteArrayAsync();
        }
    }
}
