﻿using Gateway.Application.Abstractions.Reports;
using Infrastructure.HttpClientAbstractions.Interfaces;
using Menu.API.Client.Clients;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gateway.Application.Services.Reports
{
    public class ProviderReportGeneratorService : IReportGeneratorService, IDetailedProviderReport
    {
        private readonly IProviderReportAggregationService _providerReportAggregatorService;
        private readonly IMenuClient _menuClient;

        public ProviderReportGeneratorService(IClientFactory clientFactory, IProviderReportAggregationService providerReportAggregatorService)
        {
            _providerReportAggregatorService = providerReportAggregatorService;
            _menuClient = clientFactory.GetClient<IMenuClient>();
        }

        public Task<byte[]> GenerateReportAsync(Guid providerId, DateTime start, DateTime end)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage excel = new ExcelPackage();

            string companyName = _providerReportAggregatorService.getProviderCompanyName(providerId);
            var menus = _providerReportAggregatorService.GetMenuNames(providerId).Result.MenusCollection;
            var menusPrices = _providerReportAggregatorService.GetMenusPrice(providerId);


            var providerOrdersReport = excel.Workbook.Worksheets.Add(companyName + " report" + DateTime.Now.ToString());
            providerOrdersReport.DefaultColWidth = 30;
            providerOrdersReport.Cells["A1"].Value = "Menu name";
            providerOrdersReport.Cells["B1"].Value = "Total cost of current menu";

            int currentRow = 2;
            int dishCounter = 0;
            foreach (var menu in menus)
            {
                providerOrdersReport.Cells["A" + currentRow.ToString()].Value = menu.MenuName;
                providerOrdersReport.Cells["B" + currentRow.ToString()].Value = menusPrices[dishCounter];
                dishCounter++;
                currentRow++;
            }

            providerOrdersReport.View.FreezePanes(2, 1);
            return excel.GetAsByteArrayAsync();
        }



        public Task<byte[]> GenerateProviderDetailedReportAsync(Guid providerId, string consumerCompanyName, DateTime startDate, DateTime endDate)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage excel = new ExcelPackage();

            string companyName = _providerReportAggregatorService.getProviderCompanyName(providerId);
            var menus = _providerReportAggregatorService.GetMenuNames(providerId).Result.MenusCollection;
            var menusPrices = _providerReportAggregatorService.GetMenusPrice(providerId);
      
         

            var providerOrdersReport = excel.Workbook.Worksheets.Add(companyName + " report" + DateTime.Now.ToString());
            providerOrdersReport.DefaultColWidth = 30;
            providerOrdersReport.Cells["A1"].Value = "Menu name";
            providerOrdersReport.Cells["B1"].Value = "Total cost of current menu";
            providerOrdersReport.Cells["C1"].Value = "Consumer company";
      

            int currentRow = 2;
            int dishCounter = 0;
        
                foreach (var menu in menus)
                {
                    providerOrdersReport.Cells["A" + currentRow.ToString()].Value = menu.MenuName;
                    providerOrdersReport.Cells["B" + currentRow.ToString()].Value = menusPrices[dishCounter];
                    providerOrdersReport.Cells["C" + currentRow.ToString()].Value = consumerCompanyName;
                    dishCounter++;
                    currentRow++;
                }
       

            providerOrdersReport.View.FreezePanes(2, 1);
            return excel.GetAsByteArrayAsync();
        }
    }
}
