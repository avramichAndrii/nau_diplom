﻿using Company.API.Client;
using Gateway.Application.Abstractions.Reports;
using GroupOrder.API.Client.Client;
using GroupOrder.API.Contracts.Responses;
using Infrastructure.HttpClientAbstractions.Interfaces;
using LunchGroup.API.Client;
using LunchGroup.API.Contracts.Responses;
using Menu.API.Client.Clients;
using Menu.API.Contracts.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagement.API.Client.ClientInterface;

namespace Gateway.Application.Services.Reports
{
    public class ProviderReportAggregatorService : IProviderReportAggregationService
    {
        private readonly IGroupOrderClient _groupOrderClient;
        private readonly ICompanyClient _companyClient;
        private readonly IUserManagementClient _userManagementClient;
        private readonly IMenuClient _menuClient;

        public ProviderReportAggregatorService(IClientFactory clientFactory)
        {
            _groupOrderClient = clientFactory.GetClient<IGroupOrderClient>();
            _companyClient = clientFactory.GetClient<ICompanyClient>();
            _userManagementClient = clientFactory.GetClient<IUserManagementClient>();
            _menuClient = clientFactory.GetClient<IMenuClient>();
        }

        public string getProviderCompanyName(Guid providerId)
        {
            var provider = _userManagementClient.GetByIdAsync(providerId);
            return _companyClient.GetCompanyAsync(provider.Result.CompanyId).Result.Name;
        }


        public string[] GetMenusPrice(Guid providerId)
        {
            var providerCompanyId = _userManagementClient.GetByIdAsync(providerId).Result.CompanyId;
            var menus = _menuClient.GetByProviderAsync(providerCompanyId);
            string[] arrayWithPrices = new string[menus.Result.MenusCollection.Count];
            var menusArray = menus.Result.MenusCollection.ToArray();
            for (int i = 0; i < arrayWithPrices.Length; i++)
            {
                arrayWithPrices[i] = PriceOfDishes(menusArray[i].Id);
            }

            return arrayWithPrices;
        }


        public string PriceOfDishes(Guid menuId)
        {
            decimal sum = 0;
            var dishList = _menuClient.GetDishesByMenuAsync(menuId).Result.DishesCollection;
            foreach (var dish in dishList)
            {
                sum += dish.Amount;
            }
            return Convert.ToInt32(sum).ToString();
        }

        public Task<GetAllMenusResponse> GetMenuNames(Guid providerId)
        {
            var providerCompanyId = _userManagementClient.GetByIdAsync(providerId).Result.CompanyId;
            var menus = _menuClient.GetByProviderAsync(providerCompanyId);
            return menus;
        }

        public IReadOnlyCollection<GroupOrderResponse> getGroupOrders(Guid providerId)
        {
            return _groupOrderClient.GetGroupOrdersAsync().Result.GroupOrderCollection;
            
        }

        public Guid GetCompanyId(Guid providerId)
        {
            return _userManagementClient.GetByIdAsync(providerId).Result.CompanyId;
        }

        public Guid ConsumerGuid(string companyName)
        {
            var company = _companyClient.GetCompaniesAsync().Result.CompaniesCollection.Where(x => x.Name == companyName);
            return company.First().CompanyId;
        }


        public async Task<List<OrderResponse>> GetOrdersAsync(Guid userId)
        {
            var orders = await _groupOrderClient.GetOrdersByProviderIdAsync(userId);
            return orders.OrdersCollection.ToList();
        }

     
       
    }
}
