﻿using Company.API.Client;
using Company.API.Contracts.Response;
using Gateway.Application.Abstractions.Reports;
using GroupOrder.API.Client.Client;
using GroupOrder.API.Contracts.Responses;
using Infrastructure.HttpClientAbstractions;
using Infrastructure.HttpClientAbstractions.Interfaces;
using LunchGroup.API.Client;
using LunchGroup.API.Contracts.Responses;
using Menu.API.Client.Clients;
using Menu.API.Contracts.Requests;
using Menu.API.Contracts.Responses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using UserManagement.API.Client.ClientInterface;
using UserManagement.API.Contracts.Responses;

namespace Gateway.Application.Services.Reports
{
    public class ManagerReportAggregatorService : IReportAggregatorService
    {
        private readonly IClientFactory _clientFactory;

        private const string NoDataMessage = "No data found by specified parameters";

        public ManagerReportAggregatorService(IClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<UserResponse> GetUserDataAsync(Guid userId)
        {
            return await _clientFactory.GetClient<IUserManagementClient>().GetByIdAsync(userId);
        }
        public async Task<string> GetCompanyNameAsync(Guid companyId)
        {
            var companyResponse = await _clientFactory.GetClient<ICompanyClient>().GetCompanyAsync(companyId);
            return companyResponse.Name;
        }
        public async Task<string> GetLunchGroupNameAsync(Guid lunchGroupId)
        {
            var lunchGroupResponse = 
                await _clientFactory.GetClient<ILunchGroupClient>().GetLunchGroupByIdAsync(lunchGroupId);
            return lunchGroupResponse.Name;
        }

        public async Task<IReadOnlyCollection<GetLunchGroupResponse>> GetLunchGroupsDataAsync(Guid managerId)
        {
            var lunchGroupsResponse = 
                await _clientFactory.GetClient<ILunchGroupClient>().GetLunchGroupsByManagerAsync(managerId);
            return lunchGroupsResponse.LunchGroupsCollection.ToList();
        }

        public async Task<decimal> GetDishesCostAsync(IReadOnlyCollection<Guid> dishes)
        {
            var dishCostResponse = await _clientFactory.GetClient<IMenuClient>().PostDishListCostAsync(
                new CalculateDishListPriceRequest(dishes));
            return dishCostResponse.Cost;
        }

        /// <summary>
        /// Calculates cost of all dishes in group order.
        /// </summary>
        /// <param name="groupOrder"></param>
        /// <returns>The task result contains a total cost of dishes.</returns>
        public async Task<decimal> GetGroupOrderSummaryCostAsync(GroupOrderResponse groupOrder)
        {
            decimal summaryCost = 0;
            foreach (var order in groupOrder.Orders)
            {
                var dishCostResponse = await _clientFactory.GetClient<IMenuClient>().PostDishListCostAsync(
                    new CalculateDishListPriceRequest(order.ChosenDishes));
                summaryCost += dishCostResponse.Cost;
            }
            return summaryCost;
        }

        /// <summary>
        /// Aggregates group orders for each id from lunchGroupIds,
        /// filters them by date and related delivery's status,
        /// and sorts by each property.
        /// </summary>
        /// <param name="lunchGroupIds"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>The task result contains an ordered and filtered collection of group order's data.</returns>
        /// <exception cref="ClientErrorHttpResponseException"></exception>
        public async Task<IReadOnlyCollection<GroupOrderResponse>> GetGroupOrdersDataAsync(
            IReadOnlyCollection<Guid> lunchGroupIds,
            DateTime startDate, DateTime endDate)
        {
            var groupOrderClient = _clientFactory.GetClient<IGroupOrderClient>();
            var allGroupOrders = new List<GroupOrderResponse>();
            //get group orders for lunch groups
            foreach (Guid lunchGroupId in lunchGroupIds)
            {
                var groupOrdersResponse = await groupOrderClient.GetGroupOrdersByLunchGroupsIdAsync(lunchGroupId);
                List<GroupOrderResponse> groupOrders = groupOrdersResponse.GroupOrderCollection.ToList();
                allGroupOrders.AddRange(groupOrders);
            }
            //get all done deliveries
            var deliveriesResponse = await _clientFactory.GetClient<ILunchGroupClient>().GetAllDeliveriesAsync();
            const string Done = nameof(Done);
            List<Guid> doneGroupOrders = deliveriesResponse.DeliveryCollection
                .Where(d => d.Status == Done)
                .Select(x => x.GroupOrderId)
                .ToList();
            //filter and sort group orders
            allGroupOrders = allGroupOrders
                .Where(go => go.Date >= startDate.Date && go.Date < endDate.AddDays(1).Date && doneGroupOrders.Contains(go.Id))
                .OrderBy(go => go.Date)
                .ThenBy(go => go.LunchGroupId)
                .ThenBy(go => go.ProviderId)
                .ToList();
            if (allGroupOrders.Count == 0)
            {
                throw new ClientErrorHttpResponseException(HttpStatusCode.NoContent, new { message = NoDataMessage });
            }
            return allGroupOrders;
        }

        /// <summary>
        /// Gets the ordered collection of orders for group orders collection.
        /// </summary>
        /// <param name="groupOrders"></param>
        /// <returns>The task result contains an ordered collection of orders.</returns>
        public IReadOnlyCollection<OrderResponse> GetOrdersData(
            IReadOnlyCollection<GroupOrderResponse> groupOrders)
        {
            List<OrderResponse> orders = new List<OrderResponse>();
            foreach (var groupOrder in groupOrders)
            {
                orders.AddRange(groupOrder.Orders);
            }
            return orders.OrderBy(o => o.Date)
                .ThenBy(o => o.WorkerId)
                .ToList();
        }

        /// <summary>
        /// Gets the collection of provider companies for each unique provider id from group orders collection.
        /// </summary>
        /// <param name="groupOrders"></param>
        /// <returns>The task result contains a collection of company's data.</returns>
        public async Task<IReadOnlyCollection<CompanyResponse>> GetProviderCompaniesDataAsync(IReadOnlyCollection<GroupOrderResponse> groupOrders)
        {
            var companyClient = _clientFactory.GetClient<ICompanyClient>();
            var providers = new List<CompanyResponse>();
            List<Guid> providerIds = groupOrders.Select(go => go.ProviderId).Distinct().ToList();
            foreach (Guid orderProviderId in providerIds)
            {
                var providerResponse = await companyClient.GetCompanyAsync(orderProviderId);
                providers.Add(providerResponse);
            }
            return providers;
        }

        /// <summary>
        /// Gets the collection of menues for each unique provider id from group orders collection.
        /// </summary>
        /// <param name="groupOrders"></param>
        /// <returns>The task result contains a collection of menu's data.</returns>
        public async Task<IReadOnlyCollection<MenuResponse>> GetMenuesDataAsync(IReadOnlyCollection<GroupOrderResponse> groupOrders)
        {
            var menuClient = _clientFactory.GetClient<IMenuClient>();
            var menues = new List<MenuResponse>();
            List<Guid> providerIds = groupOrders.Select(go => go.ProviderId).Distinct().ToList();
            foreach (Guid orderProviderId in providerIds)
            {
                var menusResponse = await menuClient.GetByProviderAsync(orderProviderId);
                menues.AddRange(menusResponse.MenusCollection);
            }
            return menues;
        }

        /// <summary>
        /// Gets the collection of unique menu names related with dishes in each group order.
        /// </summary>
        /// <param name="groupOrder"></param>
        /// <param name="menues"></param>
        /// <returns>The task result contains a collection of unique menu's names.</returns>
        public HashSet<string> GetMenuNamesByGroupOrder(GroupOrderResponse groupOrder, List<MenuResponse> menues)
        {
            var menuesInGroupOrder = new HashSet<string>();

            IReadOnlyCollection<OrderResponse> orders = groupOrder.Orders;
            List<Guid> dishesInGroupOrder = new List<Guid>();
            foreach (OrderResponse order in orders)
            {
                dishesInGroupOrder.AddRange(order.ChosenDishes);
            }
            dishesInGroupOrder = dishesInGroupOrder.Distinct().ToList();

            foreach (Guid dishId in dishesInGroupOrder)
            {
                MenuResponse menu = menues.Single(m => m.DishList.Select(x => x.Id).Contains(dishId));
                menuesInGroupOrder.Add(menu.MenuName);
            }
            return menuesInGroupOrder;
        }

        /// <summary>
        /// Gets the collection of unique menu names related with dishes in order.
        /// </summary>
        /// <param name="order"></param>
        /// <param name="menues"></param>
        /// <returns>The task result contains a collection of unique menu's names.</returns>
        public HashSet<string> GetMenuNamesByOrder(OrderResponse order, List<MenuResponse> menues)
        {
            var menuesInOrder = new HashSet<string>();

            foreach (Guid dishId in order.ChosenDishes)
            {
                MenuResponse menu = menues.Single(m => m.DishList.Select(x => x.Id).Contains(dishId));
                menuesInOrder.Add(menu.MenuName);
            }
            return menuesInOrder;
        }
    }
}
