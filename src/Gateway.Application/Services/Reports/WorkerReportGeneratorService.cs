﻿using Gateway.Application.Abstractions.Reports;
using GroupOrder.API.Contracts.Responses;
using Infrastructure.HttpClientAbstractions;
using Infrastructure.HttpClientAbstractions.Interfaces;
using Menu.API.Client.Clients;
using Menu.API.Contracts.Responses;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Gateway.Application.Services.Reports
{
    public class WorkerReportGeneratorService : IReportGeneratorService
    {
        private readonly IReportAggregatorWorkerService _reportAggregatorWorkerService;
        private readonly IMenuClient _menuClient;
        private const string EmptyOrderList = "Orders not found!";

        public WorkerReportGeneratorService(IClientFactory clientFactory, IReportAggregatorWorkerService reportAggregatorWorkerService)
        {
            _reportAggregatorWorkerService = reportAggregatorWorkerService;
            _menuClient = clientFactory.GetClient<IMenuClient>();
        }

        public async Task<byte[]> GenerateReportAsync(Guid workerId, DateTime startDate, DateTime endDate)
        {
            var userInformation = await _reportAggregatorWorkerService.GetUserInformationAsync(workerId);
            var orders = await _reportAggregatorWorkerService.GetOrdersAsync(workerId, startDate, endDate);
            if ((orders == null)||(orders.Count==0))
            {
                throw new ClientErrorHttpResponseException(HttpStatusCode.BadRequest, new { message = EmptyOrderList });
            }
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage excel = new ExcelPackage();
            var workerOrdersReport = excel.Workbook.Worksheets.Add("Report (" + userInformation + "):" + DateTime.Now.ToString());
            workerOrdersReport.DefaultColWidth = 20;
            workerOrdersReport.Cells["A1"].Value = "Date";
            workerOrdersReport.Cells["B1"].Value = "Provider";
            workerOrdersReport.Cells["C1"].Value = "Dishes";
            workerOrdersReport.Cells["D1"].Value = "Price";
            workerOrdersReport.Cells["E1"].Value = "Total";
            workerOrdersReport.Cells["A1:E1"].Style.Font.Bold = true;

            int currentRow = 2;
            foreach (var order in orders)
            {
                decimal total = 0;
                foreach (var dish in order.ChosenDishes)
                {
                    DishResponse dishInfo = _menuClient.GetDishByIdAsync(dish).GetAwaiter().GetResult();
                    workerOrdersReport.Cells["A" + currentRow.ToString()].Value = order.Date.ToString();
                    workerOrdersReport.Cells["B" + currentRow.ToString()].Value = await _reportAggregatorWorkerService.getProviderCompanyAsync(order.ProviderId); 
                    workerOrdersReport.Cells["C" + currentRow.ToString()].Value = dishInfo.DishName;
                    workerOrdersReport.Cells["D" + currentRow.ToString()].Value = dishInfo.Amount;
                    total += dishInfo.Amount;
                    currentRow++;
                }
                workerOrdersReport.Cells["E" + (currentRow - 1).ToString()].Value = total;
                workerOrdersReport.Cells["E:E"].Style.Font.Bold = true;
            }

            workerOrdersReport.View.FreezePanes(2, 1);
            return await excel.GetAsByteArrayAsync();
        }
    }
}
