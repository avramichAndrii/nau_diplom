﻿using Company.API.Client;
using Gateway.Application.Abstractions.Reports;
using GroupOrder.API.Client.Client;
using GroupOrder.API.Contracts.Responses;
using Infrastructure.HttpClientAbstractions.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using UserManagement.API.Client.ClientInterface;

namespace Gateway.Application.Services.Reports
{
    public class WorkerReportAggregatorService : IReportAggregatorWorkerService
    {
        private readonly IGroupOrderClient _groupOrderClient;
        private readonly ICompanyClient _companyClient;
        private readonly IUserManagementClient _userManagementClient;

        public WorkerReportAggregatorService(IClientFactory clientFactory)
        {
            _groupOrderClient = clientFactory.GetClient<IGroupOrderClient>();
            _companyClient = clientFactory.GetClient<ICompanyClient>();
            _userManagementClient = clientFactory.GetClient<IUserManagementClient>();
        }

        public async Task<string> getProviderCompanyAsync(Guid providerId)
        {
            var company = await _companyClient.GetCompanyAsync(providerId);
            return company.Name;
        }

        public async Task<List<OrderResponse>> GetOrdersAsync(Guid userId, DateTime startDate, DateTime endDate)
        {
            var orders = await _groupOrderClient.GetOrdersByWorkerIdAsync(userId);
            return orders.OrdersCollection.Where(order => ((order.Date <= endDate) && (order.Date >= startDate)))
                                          .OrderBy(o => o.Date)
                                          .ToList();
        }

        public async Task<string> GetUserInformationAsync(Guid userId)
        {
            var user = await _userManagementClient.GetByIdAsync(userId); ;
            return user.FirstName + "" + user.LastName;
        }
    }
}
