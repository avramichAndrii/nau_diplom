﻿using EmailNotifier.EmailNotification;
using Gateway.Application.Abstractions.Reports;
using Infrastructure.HttpClientAbstractions.Interfaces;
using System;
using System.Threading.Tasks;
using UserManagement.API.Client.ClientInterface;

namespace Gateway.Application.Services.Reports
{
    public class ReportSenderService : IReportSenderService
    {
        private readonly IMailSender _mailSender;
        private readonly IUserManagementClient _userManagementClient;

        public ReportSenderService(IClientFactory clientFactory, IMailSender mailSender)
        {
            _mailSender = mailSender;
            _userManagementClient = clientFactory.GetClient<IUserManagementClient>();
        }

        /// <summary>
        /// Gets user's data for specified id and sends mail with report contents in attachments.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="reportContent"></param>
        /// <returns>The task result contains a reciever's email.</returns>
        public async Task<string> SendReportFileAsync(Guid userId, byte[] reportContent)
        {
            var userResponse = await _userManagementClient.GetByIdAsync(userId);
            string userEmail = userResponse.Email;

            string subject = $"{userResponse.Role} Report";
            const string Body = "Your requested report is here!";
            const string FileName = "report.xlsx";
            const string ContentType = "plain/text";
            await _mailSender.SendEmailWithAttachmentAsync(userEmail, subject, Body, FileName, reportContent, ContentType);
            return userEmail;
        }
    }
}
