﻿using Gateway.Application.Abstractions;
using GroupOrder.API.Client.Client;
using GroupOrder.API.Contracts.Requests;
using GroupOrder.API.Contracts.Responses;
using System;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure.HttpClientAbstractions.Interfaces;

namespace Gateway.Application.Services
{
    public class GroupOrderRedirectorService : IGroupOrderRedirectorService
    {
        private readonly IGroupOrderClient _groupOrderClient;

        public GroupOrderRedirectorService(IClientFactory clientFactory)
        {
            _groupOrderClient = clientFactory.GetClient<IGroupOrderClient>();
        }

        #region GroupOrder
        public Task<GetAllGroupOrdersResponse> GetGroupOrdersAsync()
        {
            return _groupOrderClient.GetGroupOrdersAsync();
        }

        public Task<GroupOrderResponse> GetGroupOrderByIdAsync(Guid groupOrderId)
        {
            return _groupOrderClient.GetGroupOrderByIdAsync(groupOrderId);
        }

        public Task<GetAllGroupOrdersResponse> GetGroupOrdersByProviderIdAsync(Guid providerId)
        {
            return _groupOrderClient.GetAllByProviderIdAsync(providerId);
        }

        public Task<GetAllGroupOrdersResponse> GetAllByLunchGroupsIdAsync(Guid lunchGroupId)
        {
            return _groupOrderClient.GetGroupOrdersByLunchGroupsIdAsync(lunchGroupId);
        }

        public Task<GroupOrderResponse> CreateGroupOrderAsync(GroupOrderRequest request)
        {
            return _groupOrderClient.CreateGroupOrderAsync(request);
        }

        public Task<DeleteGroupOrderResponse> DeleteGroupOrderAsync(Guid groupOrderId)
        {
            return _groupOrderClient.DeleteGroupOrderAsync(groupOrderId);
        }
        #endregion

        #region Order
        public Task<GetAllOrdersResponse> GetOrdersAsync()
        {
            return _groupOrderClient.GetOrdersAsync();
        }

        public Task<OrderResponse> GetOrderByIdAsync(Guid orderId)
        {
            return _groupOrderClient.GetOrderByIdAsync(orderId);

        }

        public Task<GetAllOrdersResponse> GetOrdersByProviderIdAsync(Guid providerId)
        {
            return _groupOrderClient.GetOrdersByProviderIdAsync(providerId);

        }

        public Task<GetAllOrdersResponse> GetOrdersByWorkerIdAsync(Guid workerId)
        {
            return _groupOrderClient.GetOrdersByWorkerIdAsync(workerId);

        }

        public async Task<OrderResponse> CreateOrderAsync(OrderRequest request)
        {          
            return await _groupOrderClient.CreateOrderAsync(request);
        }

        public Task<DeleteOrderResponse> DeleteOrderAsync(Guid orderId)
        {
            return _groupOrderClient.DeleteOrderAsync(orderId);
        }

        public Task<OrderResponse> UpdateOrderAsync(OrderRequest request)
        {
            return _groupOrderClient.UpdateOrderAsync(request);
        }
        #endregion

        #region OrderTemplate

        public Task<GetAllOrderTemplatesResponse> GetOrderTemplatesAsync()
        {
            return _groupOrderClient.GetOrderTemplatesAsync();
        }

        public Task<OrderTemplateResponse> GetOrderTemplateByIdAsync(Guid orderTemplateId)
        {
            return _groupOrderClient.GetOrderTemplateByIdAsync(orderTemplateId);
        }

        public Task<GetAllOrderTemplatesResponse> GetOrderTemplatesByWorkerIdAsync(Guid workerId)
        {
            return _groupOrderClient.GetOrderTemplatesByWorkerIdAsync(workerId);
        }

        public Task<GetAllOrderTemplatesResponse> GetOrderTemplatesByProviderIdAsync(Guid providerId)
        {
            return _groupOrderClient.GetOrderTemplatesByProviderIdAsync(providerId);
        }

        public Task<OrderTemplateResponse> CreateOrderTemplateAsync(OrderTemplateRequest request)
        {
            return _groupOrderClient.CreateOrderTemplateAsync(request);
        }

        public Task DeleteOrderTemplateAsync(Guid orderTemplateId)
        {
            return _groupOrderClient.DeleteOrderTemplateAsync(orderTemplateId);
        }

        public Task UpdateOrderTemplateNameAsync(Guid id, OrderTemplateUpdateNameRequest request)
        {
            return _groupOrderClient.UpdateOrderTemplateNameAsync(id, request);
        }
        public Task UpdateOrderTemplateDishesAsync(Guid id, OrderTemplateUpdateDishesRequest request)
        {
            return _groupOrderClient.UpdateOrderTemplateDishesAsync(id, request);
        }
        #endregion

        #region Cart
        public Task<CartResponse> GetCartByIdAsync(Guid cartId)
        {
            return _groupOrderClient.GetCartByIdAsync(cartId);
        }

        public Task<CartResponse> GetCartByWorkerIdAsync(Guid workerId)
        {
            return _groupOrderClient.GetCartByWorkerIdAsync(workerId);
        }

        public Task<CartResponse> CreateCartAsync(CartRequest request)
        {
            return _groupOrderClient.CreateOrderTemplateAsync(request);
        }

        public Task DeleteCartAsync(Guid cartId)
        {
            return _groupOrderClient.DeleteCartAsync(cartId);
        }

        public Task<CartResponse> UpdateCartAsync(CartRequest request)
        {
            return _groupOrderClient.UpdateCartAsync(request);
        }

        #endregion

    }
}