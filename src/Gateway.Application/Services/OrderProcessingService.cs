﻿using GroupOrder.API.Client.Client;
using GroupOrder.API.Contracts.Requests;
using GroupOrder.API.Contracts.Responses;
using Infrastructure.HttpClientAbstractions;
using Infrastructure.HttpClientAbstractions.Interfaces;
using LunchGroup.API.Client;
using LunchGroup.API.Contracts.Requests;
using LunchGroup.API.Contracts.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Gateway.Application.Services
{
    public class OrderProcessingService : IOrderProcessingService
    {
        private readonly ILunchGroupClient _lunchGroupClient;
        private readonly IGroupOrderClient _groupOrderClient;

        private const string WrongDateMessage = "Specified date is not belongs to current ordering period.";
        private const string WaitDeadlineMessage = "Submit deadline has not been reached yet.";

        public OrderProcessingService(IClientFactory clientFactory)
        {
            _lunchGroupClient = clientFactory.GetClient<ILunchGroupClient>();
            _groupOrderClient = clientFactory.GetClient<IGroupOrderClient>();
        }

        /// <summary>
        /// Creates order from request, if it passed the validation logic.
        /// </summary>
        /// <param name="createOrderRequest"></param>
        /// <returns>The task result contains a response from client with an object of created order.</returns>
        /// <exception cref="ClientErrorHttpResponseException"></exception>
        public async Task<OrderResponse> CreateOrderByWorker(OrderRequest createOrderRequest)
        {
            //check request for lunch group and date
            GetLunchGroupResponse lunchGroup = await _lunchGroupClient.GetWorkerLunchGroupAsync(createOrderRequest.WorkerId);
            GetTimetableResponse availableDatesResponse = await _lunchGroupClient.GetTimetableAsync(lunchGroup.Id);
            if (!availableDatesResponse.AvailableDates.Contains(createOrderRequest.Date))
            {
                throw new ClientErrorHttpResponseException(HttpStatusCode.BadRequest, new { message = WrongDateMessage });
            }

            //create order
            createOrderRequest.LunchGroupId = lunchGroup.Id;
            return await _groupOrderClient.CreateOrderAsync(createOrderRequest);
        }

        /// <summary>
        /// Converts all lunch group's groupOrders to deliveries, if deadline is reached.
        /// </summary>
        /// <param name="lunchGroupId"></param>
        /// <returns>The task result contains a collection of created deliveries.</returns>
        /// <exception cref="ClientErrorHttpResponseException"></exception>
        public async Task<IReadOnlyCollection<DeliveryResponse>> CreateDeliveriesByManager(Guid lunchGroupId)
        {
            //check deadline
            GetLunchGroupResponse lunchGroup = await _lunchGroupClient.GetLunchGroupByIdAsync(lunchGroupId);
            GetTimetableResponse timetableResponse = await _lunchGroupClient.GetTimetableAsync(lunchGroupId);
            if (timetableResponse.SubmitDeadline > DateTime.Now)
            {
                throw new ClientErrorHttpResponseException(HttpStatusCode.BadRequest, new { message = WaitDeadlineMessage });
            }

            //create deliveries
            GetAllGroupOrdersResponse groupOrdersResponse =
                await _groupOrderClient.GetGroupOrdersByLunchGroupsIdAsync(lunchGroupId);
            var createdDeliveries = new List<DeliveryResponse>();
            foreach (DateTime orderingDate in timetableResponse.AvailableDates)
            {
                var groupOrderIdsByDate = groupOrdersResponse.GroupOrderCollection
                    .Where(groupOrder => groupOrder.Date == orderingDate)
                    .Select(groupOrder => groupOrder.Id);
                foreach (Guid groupOrderId in groupOrderIdsByDate)
                {
                    var createdDelivery = await _lunchGroupClient.CreateDeliveryAsync(new CreateDeliveryRequest
                    {
                        GroupOrderId = groupOrderId
                    });
                    createdDeliveries.Add(createdDelivery);
                }
            }

            //update deadline and period
            await _lunchGroupClient.UpdateTimetableAsync(lunchGroupId);
            return createdDeliveries;
        }
    }
}
