﻿using Company.API.Client;
using Company.API.Contracts.Response;
using EmailNotifier.EmailNotification;
using Gateway.Application.Abstractions;
using Gateway.Application.Helpers.AssignToken;
using Infrastructure.HttpClientAbstractions;
using Infrastructure.HttpClientAbstractions.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Net;
using System.Threading.Tasks;
using UserManagement.API.Client.ClientInterface;
using UserManagement.API.Contracts.Requests;
using UserManagement.API.Contracts.Responses;

namespace Gateway.Application.Services
{
    public class UserAssignerService : IUserAssignerService
    {
        private readonly ICompanyClient _companyClient;
        private readonly IUserManagementClient _userManagementClient;
        private readonly IMailSender _mailSender;
        private readonly AssignmentSettings _assignSettings;
        private readonly string _uiApplicationUri;

        private const string Worker = nameof(Worker);
        private const string MailSubject = "LunchMe register invitation";
        private const string MessageContentTemplate = 
            "In order to be registered in LunchMe system as a {0} of {1} company, please follow the link and submit form:\n";
        private const string WrongInviterRole = "Worker cannot send an invitation";
        private const string InvalidTokenMessage = "Token is not valid or expired";
        private const string EmailOrRoleMismatchMessage = "Email or role is not matching";

        public UserAssignerService(
            IClientFactory clientFactory,
            IMailSender mailSender,
            IConfiguration config,
            IOptions<AssignmentSettings> assignSettings)
        {
            _companyClient = clientFactory.GetClient<ICompanyClient>();
            _userManagementClient = clientFactory.GetClient<IUserManagementClient>();
            _mailSender = mailSender;
            _assignSettings = assignSettings.Value;
            _uiApplicationUri = config.GetSection("WebOptions")["FrontendAppUrl"];
        }

        /// <summary>
        /// Sends mail with register link which contains token with additional data.
        /// </summary>
        /// <param name="inviterId"></param>
        /// <param name="invitedUserRole"></param>
        /// <param name="invitedUserMail"></param>
        /// <returns>A task that represents an asynchronous operation.</returns>
        /// <exception cref="ClientErrorHttpResponseException"></exception>
        public async Task SendInvitation(Guid inviterId, string invitedUserRole, string invitedUserMail)
        {
            UserResponse inviter = await _userManagementClient.GetByIdAsync(inviterId);
            if (inviter.Role == Worker)
            {
                throw new ClientErrorHttpResponseException(HttpStatusCode.BadRequest, new { message = WrongInviterRole });
            }
            Guid inviterCompanyId = inviter.CompanyId;
            CompanyResponse company = await _companyClient.GetCompanyAsync(inviterCompanyId);
            string jwtToken = JWTTokenProvider.GenerateAssignToken(
                _assignSettings, invitedUserMail, invitedUserRole, inviterCompanyId);

            string mailBody = string.Format(MessageContentTemplate, invitedUserRole.ToLower(), company.Name) +
                $"{_uiApplicationUri}register/{jwtToken}";
            await _mailSender.SendEmailAsync(invitedUserMail, MailSubject, mailBody);
        }

        /// <summary>
        /// Gets data from given token and registers worker if data is valid.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="request"></param>
        /// <returns>Registered user data.</returns>
        /// <exception cref="ClientErrorHttpResponseException"></exception>
        public async Task<UserResponse> RegisterWorker(string token, RegisterRequest request)
        {
            if (!JWTTokenProvider.ValidateAssignToken(_assignSettings, token))
            {
                throw new ClientErrorHttpResponseException(HttpStatusCode.BadRequest, new { message = InvalidTokenMessage });
            }
            Guid invitedUserCompanyId;
            string invitedUserMail;
            string invitedUserRole;
            try
            {
                JWTTokenProvider.GetClaims(token, out invitedUserMail, out invitedUserRole, out invitedUserCompanyId);
            }
            catch (FormatException ex)
            {
                throw new ClientErrorHttpResponseException(HttpStatusCode.BadRequest, new { message = ex.Message });
            }
            await _companyClient.GetCompanyAsync(invitedUserCompanyId);
            if (request.Email == invitedUserMail && request.Role == invitedUserRole)
            {
                UserResponse registeredUserResponse = await _userManagementClient.RegisterAsync(request);
                await _userManagementClient.SetCompanyByUserIdAsync(registeredUserResponse.Id, invitedUserCompanyId);
                registeredUserResponse.CompanyId = invitedUserCompanyId;
                return registeredUserResponse;
            }
            else
            {
                throw new ClientErrorHttpResponseException(HttpStatusCode.BadRequest, new { message = EmailOrRoleMismatchMessage });
            }
        }
    }
}
