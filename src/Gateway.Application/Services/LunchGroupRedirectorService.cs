﻿using LunchGroup.API.Client;
using LunchGroup.API.Contracts.Requests;
using LunchGroup.API.Contracts.Responses;
using System;
using System.Threading.Tasks;
using Infrastructure.HttpClientAbstractions.Interfaces;

namespace Gateway.Application.Services
{
    class LunchGroupRedirectorService : ILunchGroupRedirectorService
    {
        private readonly ILunchGroupClient _lunchGroupClient;

        public LunchGroupRedirectorService(IClientFactory clientFactory)
        {
           _lunchGroupClient = clientFactory.GetClient<ILunchGroupClient>();
        }

        public Task<CreateLunchGroupResponse> CreateLunchGroupAsync(CreateLunchGroupRequest lunchGroupRequest)
        {
            return _lunchGroupClient.CreateLunchGroupAsync(lunchGroupRequest);
        }

        public Task<GetLunchGroupsCollectionResponse> GetAllLunchGroupsByCompanyAsync(Guid companyId)
        {
            return _lunchGroupClient.GetAllLunchGroupsByCompanyAsync(companyId);
        }

        public Task<GetLunchGroupResponse> GetLunchGroupByIdAsync(Guid groupId)
        {
            return _lunchGroupClient.GetLunchGroupByIdAsync(groupId);
        }

        public Task<GetLunchGroupsCollectionResponse> GetLunchGroupsByManagerAsync(Guid managerId)
        {
            return _lunchGroupClient.GetLunchGroupsByManagerAsync(managerId);
        }

        public Task<GetTimetableResponse> GetTimetableAsync(Guid groupId)
        {
            return _lunchGroupClient.GetTimetableAsync(groupId);
        }

        public Task<GetLunchGroupResponse> GetWorkerLunchGroupAsync(Guid workerId)
        {
            return _lunchGroupClient.GetWorkerLunchGroupAsync(workerId);
        }

        public Task SetActiveFlagAsync(Guid groupId, SetActiveFlagRequest request)
        {
            return _lunchGroupClient.SetActiveFlagAsync(groupId, request);
        }

        public Task<Guid> UpdateLunchGroupAsync(UpdateLunchGroupRequest lunchGroupToUpdate, Guid id)
        {
            return _lunchGroupClient.UpdateLunchGroupAsync(lunchGroupToUpdate, id);
        }

        public Task UpdateTimetableAsync(Guid groupId)
        {
            return _lunchGroupClient.UpdateTimetableAsync(groupId);
        }

        #region delivery
        public Task<GetAllDeliveriesResponse> GetAllDeliveriesAsync()
        {
            return _lunchGroupClient.GetAllDeliveriesAsync();
        }

        public Task<DeliveryResponse> GetDeliveryByIdAsync(Guid deliveryId)
        {
            return _lunchGroupClient.GetDeliveryByIdAsync(deliveryId);
        }

        public Task<DeliveryResponse> GetDeliveryByGroupOrderIdAsync(Guid orderId)
        {
            return _lunchGroupClient.GetDeliveryByGroupOrderIdAsync(orderId);
        }

        public Task<DeliveryResponse> CreateDeliveryAsync(Guid deliveryId, CreateDeliveryRequest request)
        {
            return _lunchGroupClient.CreateDeliveryAsync(request);
        }

        public Task<DeliveryResponse> UpdateDeliveryAsync(Guid deliveryId, UpdateDeliveryRequest request)
        {
            return _lunchGroupClient.UpdateDeliveryAsync(deliveryId, request);
        }

        public Task<DeliveryResponse> DeleteDeliveryAsync(Guid deliveryId)
        {
            return _lunchGroupClient.DeleteDeliveryAsync(deliveryId);
        }
        #endregion

    }

}
