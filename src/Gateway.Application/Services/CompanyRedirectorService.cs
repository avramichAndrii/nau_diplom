﻿using Company.API.Client;
using Company.API.Contracts.Request;
using Company.API.Contracts.Response;
using Gateway.Application.Abstractions;
using System;
using System.Threading.Tasks;
using Infrastructure.HttpClientAbstractions.Interfaces;

namespace Gateway.Application
{
    public class CompanyRedirectorService : ICompanyRedirectorService
    {
        private readonly ICompanyClient _companyClient;
        
        public CompanyRedirectorService(IClientFactory clientFactory)
        {
            _companyClient = clientFactory.GetClient<ICompanyClient>();
        }

        #region Company

        public Task<GetCompaniesResponse> GetCompaniesAsync()
        {
            return _companyClient.GetCompaniesAsync();
        }
        public Task<CompanyResponse> GetCompanyAsync(Guid id)
        {
            return _companyClient.GetCompanyAsync(id);
        }

        public Task<GetCompaniesResponse> GetCompaniesByTypeAsync(string type)
        {
            return _companyClient.GetCompaniesByTypeAsync(type);
        }

        public Task<CompanyResponse> CreateCompanyAsync(CompanyRequest request)
        {
            return _companyClient.CreateCompanyAsync(request);
        }

        public Task<CompanyResponse> UpdateCompanyAsync(Guid id, CompanyRequest request)
        {
            return _companyClient.UpdateCompanyAsync(id, request);
        }

        public Task<DeleteCompanyResponse> RemoveCompanyAsync(Guid id)
        {
            return _companyClient.DeleteCompanyAsync(id);
        }

        #endregion

        #region Contract

        public Task<GetContractsResponse> GetContractsAsync()
        {
            return _companyClient.GetContractsAsync();
        }

        public Task<ContractResponse> GetContractByIdAsync(Guid contractId)
        {
            return _companyClient.GetContractByIdAsync(contractId);
        }

        public Task<GetContractsResponse> GetContractsByCompanyTypeAndIdAsync(Guid companyId, string companyType)
        {
            return _companyClient.GetContractsByCompanyTypeAndIdAsync(companyId, companyType);
        }

        public Task<GetContractsResponse> GetContractsByStatusAndCompanyTypeAndIdAsync(Guid companyId, string type, string status)
        {
            return _companyClient.GetContractsByStatusAndCompanyTypeAndIdAsync(companyId, type, status);
        }

        public Task<ContractResponse> CreateContractAsync(ContractRequest request)
        {
            return _companyClient.CreateContractAsync(request);
        }

        public Task<ContractResponse> UpdateContractAsync(Guid contractId, ContractRequest request)
        {
            return _companyClient.UpdateContractAsync(contractId, request);
        }

        #endregion
    }
}
