﻿using System.Threading.Tasks;
using System;
using Infrastructure.HttpClientAbstractions.Interfaces;
using Microsoft.AspNetCore.Mvc;
using UserManagement.API.Client.ClientInterface;
using UserManagement.API.Contracts.Requests;
using UserManagement.API.Contracts.Responses;

namespace Gateway.Application
{
    public class UserManagementRedirectorService : IUserManagementRedirectorService
    {
        private readonly IUserManagementClient _userManagementClient;

        public UserManagementRedirectorService(IClientFactory clientFactory)
        {
            _userManagementClient = clientFactory.GetClient<IUserManagementClient>();
        }

        public Task<AuthenticateResponse> AuthenticateAsync(AuthenticateRequest model)
        {
            return _userManagementClient.AuthenticateAsync(model);
        }
        
        public Task<UsersCollectionResponse> GetUsersAsync()
        {
            return _userManagementClient.GetAllAsync();
        }
        public Task<UserResponse> RegisterAsync(RegisterRequest model)
        {
            return _userManagementClient.RegisterAsync(model);
        }
        
        public Task<UsersCollectionResponse> GetUsersByCompanyAndRoleAsync(Guid companyId, string role)
        {
            return _userManagementClient.GetAllByCompanyAndRoleAsync(companyId, role);
        }

        public async Task<UserResponse> GetByIdAsync(Guid id)
        {
            return await _userManagementClient.GetByIdAsync(id);
        }
        public async Task<UserResponse> UpdateByIdAsync(Guid id, UpdateRequest model)
        {
            return await _userManagementClient.UpdateByIdAsync(id, model);
        }

        public async Task SetCompanyForUserAsync(Guid id, SetCompanyRequest request)
        {
            await _userManagementClient.SetCompanyByUserIdAsync(id, request.CompanyId);
        }
        
        public async Task<Guid> DeleteByIdAsync(Guid id)
        {
            return await _userManagementClient.DeleteByIdAsync(id);
        }
        public Task UpdatePasswordAsync(Guid id, UpdatePasswordRequest model)
        {
            return _userManagementClient.UpdatePasswordAsync(id, model);
        }
        public Task<AuthenticateResponse> RefreshToken(Guid token)
        {
            return _userManagementClient.RefreshTokenAsync(token);
        }

        public Task<UserResponse> ResetPassword(ResetPasswordRequest request)
        {
            return _userManagementClient.ResetPasswordAsync(request);
        }

        public Task<HandleForgottenPasswordResponse> HandleForgottenPassword(ForgotPasswordRequest forgotPasswordRequest)
        {
            return _userManagementClient.HandleForgottenPassword(forgotPasswordRequest);
        }
    }
}
