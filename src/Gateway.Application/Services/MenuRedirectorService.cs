﻿using Company.API.Contracts.Response;
using Gateway.Application.Abstractions;
using Menu.API.Client.Clients;
using Menu.API.Contracts.Requests;
using Menu.API.Contracts.Responses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Infrastructure.HttpClientAbstractions.Interfaces;

namespace Gateway.Application.Services
{
    public class MenuRedirectorService : IMenuRedirectorService
    {
        private readonly IMenuClient _menuClient;

        public MenuRedirectorService(IClientFactory clientFactory)
        {
            _menuClient = clientFactory.GetClient<IMenuClient>();
        }
        
        #region menu
        public Task<GetAllMenusResponse> GetMenusAsync()
        {
            return _menuClient.GetMenusAsync();
        }
        public Task<MenuResponse> GetMenuByIdAsync(Guid menuId)
        {
            return _menuClient.GetMenuByIdAsync(menuId);
        }
        public Task<GetAllDishesResponse> GetDishesByMenuAsync(Guid menuId)
        {
            return _menuClient.GetDishesByMenuAsync(menuId);
        }
        public Task<GetAllMenusResponse> GetByProviderAsync(Guid providerId)
        {
            return _menuClient.GetByProviderAsync(providerId);
        }
        public Task<MenuResponse> CreateMenuAsync(MenuRequest request)
        {
            return _menuClient.CreateMenuAsync(request);
        }
        public Task<MenuResponse> UpdateMenuAsync(Guid menuId, MenuRequest request)
        {
            return _menuClient.UpdateMenuAsync(menuId, request);
        }
        public Task DeleteMenuAsync(Guid menuId)
        {
            return _menuClient.DeleteMenuAsync(menuId);
        }
        public async Task<GetAllMenusResponse> GetAllMenusByContractsAsync(GetContractsResponse contracts)
        {
            List<MenuResponse> menus = new List<MenuResponse>();
            foreach (var item in contracts.ContractResponses)
            {
                menus.Add(await _menuClient.GetMenuByIdAsync(item.MenuId));
            }
            return new GetAllMenusResponse(menus);
        }
        #endregion
        #region dish
        public Task<GetAllDishesResponse> GetDishesAsync()
        {
            return _menuClient.GetDishesAsync();
        }
        public Task<DishResponse> GetDishByIdAsync(Guid dishId)
        {
            return _menuClient.GetDishByIdAsync(dishId);
        }
        public Task<DishResponse> CreateDishAsync(DishRequest request)
        {
            return _menuClient.CreateDishAsync(request);
        }
        public Task<DishResponse> UpdateDishAsync(Guid dishId, DishRequest request)
        {
            return _menuClient.UpdateDishAsync(dishId, request);
        }
        public Task<DishResponse> UpdateDishIsActiveAsync(Guid dishId, UpdateDishIsActiveRequest request)
        {
            return _menuClient.UpdateDishIsActiveAsync(dishId, request);
        }
        public Task DeleteDishAsync(Guid dishId)
        {
            return _menuClient.DeleteDishAsync(dishId);
        }

        #endregion
        #region marketplace
        public Task<GetAllMenusResponse> GetOffersAsync()
        {
            return _menuClient.GetOffersAsync();
        }
        public Task<MenuResponse> GetOffersByIdAsync(Guid providerId)
        {
            return _menuClient.GetByProviderOffersIdAsync(providerId);
        }

        public Task<MenuResponse> UpdateIsInMarketplaceAsync(Guid menuId, MarketplacePutRequest request)
        {
            return _menuClient.UpdateIsInMarketplaceAsync(menuId, request);
        }

        #endregion
    }
}

