﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Menu.Domain
{
    public interface IDishDomainService
    {
        Task<DishModel> CreateAsync(DishModel dishModel);
        Task<IReadOnlyCollection<DishModel>> GetAllAsync();
        Task<DishModel> UpdateAsync(Guid dishId,DishModel dishModel);
        Task<DishModel> UpdateDishIsActiveAsync(Guid dishId, bool isActive);
        Task DeleteAsync(Guid dishId);
        Task<DishModel> GetByIdAsync(Guid dishId);
        public Task<decimal> GetDishListCostAsync(IReadOnlyCollection<Guid> dishIds);
    }
}
