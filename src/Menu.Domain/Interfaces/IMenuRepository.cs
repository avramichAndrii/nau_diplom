﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Menu.Domain
{
    public interface IMenuRepository
    {
        Task<MenuModel> CreateAsync(MenuModel menuModel);
        Task<IReadOnlyCollection<MenuModel>> GetAllAsync();
        Task<IReadOnlyCollection<MenuModel>> GetByProviderCompanyIdAsync(Guid providerCompanyId);
        Task<MenuModel> GetByIdAsync(Guid menuId);
        Task<IReadOnlyCollection<DishModel>> GetDishesByMenuIdAsync(Guid menuId);
        Task<MenuModel> UpdateAsync(Guid menuId, MenuModel menuModel);
        Task DeleteAsync(Guid menuId);

        Task<IReadOnlyCollection<MenuModel>> GetAllOffersAsync();
        Task<IReadOnlyCollection<MenuModel>> GetByProviderIdFromMarketplaceAsync(Guid providerId);
        Task<MenuModel> UpdateIsInMarketplaceAsync(Guid menuId, bool isInMarketplace);

    }
}

