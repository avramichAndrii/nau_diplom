﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Menu.Domain
{
    public interface IMarketplaceService
    {
        Task<IReadOnlyCollection<MenuModel>> GetAllOffersAsync();
        Task<IReadOnlyCollection<MenuModel>> GetByProviderIdAsync(Guid providerId);
        Task<MenuModel> UpdateIsInMarketplaceAsync(Guid menuId, bool isInMarketplace);
    }
}
