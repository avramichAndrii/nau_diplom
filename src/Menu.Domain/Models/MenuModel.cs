﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Core;

namespace Menu.Domain
{
    public class MenuModel : Entity<Guid>
    { 
        public string MenuName { get; set; }
        public string MenuInfo { get; set; }
        public Guid ProviderCompanyId { get; set; }
        public List<DishModel> DishList { get; set; }
        public bool IsInMarketplace { get; set; }
        public byte[] ImageData { get; set; }

        private MenuModel()
        {
        }

        public MenuModel(Guid id, string menuName, string menuInfo, Guid providerCompanyId, byte[] imageData, bool isInMarketplace = false) : base(id)
        {
            try
            {
                MenuName = menuName;
                MenuInfo = menuInfo;
                ProviderCompanyId = providerCompanyId;
                ImageData = imageData;
                DishList = new List<DishModel>();
                IsInMarketplace = isInMarketplace;
            }
            catch (FormatException ex)
            {
                throw new InvalidDataMenuDomainException(ex.Message);
            }
        }
        public TimeSpan GetMenuPreparationTime()
        {
            TimeSpan preparationTime;
            if (DishList != null && DishList.Count>0)
            {
                preparationTime = DishList.Max(dish => dish.PreparationTime);
            }
            else preparationTime = TimeSpan.Zero;
            return preparationTime;
        }
    }
}
