﻿using System;
using System.Globalization;

namespace Menu.Domain
{
    public class MenuDomainException : Exception
    {
        public MenuDomainException() : base() { }

        public MenuDomainException(string message) : base(message) { }

        public MenuDomainException(string message, params object[] args)
            : base(String.Format(CultureInfo.CurrentCulture, message, args)) { }
    }
}
