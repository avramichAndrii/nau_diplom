﻿using System;
using System.Globalization;

namespace Menu.Domain
{
    public class InvalidDataMenuDomainException : MenuDomainException
    {
        public InvalidDataMenuDomainException() : base() { }

        public InvalidDataMenuDomainException(string message) : base(message) { }

        public InvalidDataMenuDomainException(string message, params object[] args)
            : base(String.Format(CultureInfo.CurrentCulture, message, args)) { }
    }
}
