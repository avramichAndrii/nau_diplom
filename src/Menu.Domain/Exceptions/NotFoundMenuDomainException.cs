﻿using System;
using System.Globalization;

namespace Menu.Domain
{
    public class NotFoundMenuDomainException : MenuDomainException
    {
        public NotFoundMenuDomainException() : base() { }

        public NotFoundMenuDomainException(string message) : base(message) { }

        public NotFoundMenuDomainException(string message, params object[] args)
            : base(String.Format(CultureInfo.CurrentCulture, message, args)) { }
    }
}
