﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Menu.Domain.Services
{
    public class MarketplaceService : IMarketplaceService
    {
        private readonly IMenuRepository _menuRepository;

        public MarketplaceService(IMenuRepository menuRepository)
        {
            _menuRepository = menuRepository;
        }

        public Task<IReadOnlyCollection<MenuModel>> GetAllOffersAsync()
        {
            return _menuRepository.GetAllOffersAsync();
        }

        public Task<IReadOnlyCollection<MenuModel>> GetByProviderIdAsync(Guid providerId)
        {
            return _menuRepository.GetByProviderIdFromMarketplaceAsync(providerId);
        }

        public Task<MenuModel> UpdateIsInMarketplaceAsync(Guid menuId, bool isInMarketplace)
        {
            return _menuRepository.UpdateIsInMarketplaceAsync(menuId, isInMarketplace);
        }

    }
}
