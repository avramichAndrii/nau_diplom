﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Menu.Domain.Services
{
    public class DishDomainService : IDishDomainService
    {
        private readonly IDishRepository _dishRepository;
        public DishDomainService(IDishRepository dishRepository)
        {
            _dishRepository = dishRepository;
        }

      
        public  Task<DishModel> CreateAsync(DishModel dishModel)
        {
            return  _dishRepository.CreateAsync(dishModel);
        }

        public Task DeleteAsync(Guid dishId)
        {
            return _dishRepository.DeleteAsync(dishId);
        }

        public Task<IReadOnlyCollection<DishModel>> GetAllAsync()
        {
            return _dishRepository.GetAllAsync();
        }

        public Task<DishModel> GetByIdAsync(Guid dishId)
        {
            return _dishRepository.GetByIdAsync(dishId);
        }

        public Task<DishModel> UpdateAsync(Guid dishId, DishModel dishModel)
        {
            return _dishRepository.UpdateAsync(dishId,dishModel);
        }

        public Task<DishModel> UpdateDishIsActiveAsync(Guid dishId, bool isActive)
        {
            return _dishRepository.UpdateDishIsActiveAsync(dishId, isActive);
        }

        public Task<decimal> GetDishListCostAsync(IReadOnlyCollection<Guid> dishIds)
        {
            decimal totalCost = 0;

            foreach (Guid dishId in dishIds)
            {
                var dish = GetByIdAsync(dishId);
                totalCost += dish.GetAwaiter().GetResult().Price.Amount;
            }

            return Task.FromResult(totalCost);
        }

    }
}
