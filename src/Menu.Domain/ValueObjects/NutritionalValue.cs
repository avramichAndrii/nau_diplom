﻿namespace Menu.Domain
{
    public class NutritionalValue
    {
        public double Proteins { get; set; }
        public double Fats { get; set; }
        public double Carbohydrates { get; set; }
        public double CaloricContent { get; set; }
        public NutritionalValue(double proteins, double fats, double carbohydrates, double caloricContent)
        {
            Proteins = proteins;
            Fats = fats;
            Carbohydrates = carbohydrates;
            CaloricContent = caloricContent;
        }
    }
}
