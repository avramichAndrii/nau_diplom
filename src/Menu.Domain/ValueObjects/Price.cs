﻿namespace Menu.Domain
{
    public class Price
    {
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public Price(decimal amount, string currency)
        {
            Amount = amount;
            Currency = currency;
        }

    }
}
