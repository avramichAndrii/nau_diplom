﻿using Microsoft.Extensions.DependencyInjection;
using Menu.Domain.Services;


namespace Menu.Domain
{
    public static class MenuDomainExtensions
    {
        public static void RegisterMenuDomain(this IServiceCollection collection)
        {
            collection.AddScoped<IMenuDomainService, MenuDomainService>();
            collection.AddScoped<IDishDomainService, DishDomainService>();
            collection.AddScoped<IMarketplaceService, MarketplaceService>();
        }

    }
}
