using EmailNotifier.EmailNotification;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Threading.Tasks;

namespace EmailNotifier
{   
    public class EmailSender : IMailSender
    {
        private readonly SendGridEmailOptions _emailOptions;
        
        public EmailSender(IOptionsMonitor<SendGridEmailOptions> emailOptions) 
        {
            _emailOptions = emailOptions.CurrentValue;
        }

        public async Task SendEmailAsync(string toEmail, string subject, string content)
        {
            var apiKey = _emailOptions.ApiKey;
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress(_emailOptions.FromEmail, "LunchMe notification");
            var to = new EmailAddress(toEmail);
            var message = MailHelper.CreateSingleEmail(from, to, subject, content, content);
            var response = await client.SendEmailAsync(message);
        }

        public async Task SendEmailWithAttachmentAsync(string toEmail, string subject, string content, 
            string fileName, byte[] fileContent, string fileContentType)
        {
            var apiKey = _emailOptions.ApiKey;
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress(_emailOptions.FromEmail, "LunchMe notification");
            var to = new EmailAddress(toEmail);
            var message = MailHelper.CreateSingleEmail(from, to, subject, content, content);
            message.AddAttachment(fileName, Convert.ToBase64String(fileContent), fileContentType);
            await client.SendEmailAsync(message);
        }
    }
}