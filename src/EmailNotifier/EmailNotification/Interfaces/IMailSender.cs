using System;
using System.Threading.Tasks;

namespace EmailNotifier.EmailNotification
{
    public interface IMailSender
    {
        Task SendEmailAsync(string toEmail, string subject, string content);
        Task SendEmailWithAttachmentAsync(string toEmail, string subject, string content, string fileName, byte[] fileContent, string contentType);
    }
}