namespace EmailNotifier
{
    public class SendGridEmailOptions
    {
        public string ApiKey { get; set; }
        public string FromEmail { get; set; }
    }
}