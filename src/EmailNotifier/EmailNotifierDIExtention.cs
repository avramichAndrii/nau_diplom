using EmailNotifier.EmailNotification;
using Microsoft.Extensions.DependencyInjection;


namespace EmailNotifier
{
    public static class EmailNotifierDIExtention
    {
        public static void RegisterEmailNotifier(this IServiceCollection collection)
        {
            collection.AddTransient<IMailSender, EmailSender>();
        }
    }
}