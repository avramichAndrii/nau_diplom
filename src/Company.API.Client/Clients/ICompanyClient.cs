﻿using Company.API.Contracts.Request;
using Company.API.Contracts.Response;
using System;
using System.Threading.Tasks;
using Infrastructure.HttpClientAbstractions.Interfaces;

namespace Company.API.Client
{
    public interface ICompanyClient : IClient
    {
        #region Company

        Task<GetCompaniesResponse> GetCompaniesAsync();
        Task<CompanyResponse> GetCompanyAsync(Guid id);
        Task<GetCompaniesResponse> GetCompaniesByTypeAsync(string type);
        Task<CompanyResponse> CreateCompanyAsync(CompanyRequest request);
        Task<CompanyResponse> UpdateCompanyAsync(Guid id, CompanyRequest request);
        Task<DeleteCompanyResponse> DeleteCompanyAsync(Guid id);

        #endregion

        #region Contract

        Task<GetContractsResponse> GetContractsAsync();
        Task<ContractResponse> GetContractByIdAsync(Guid contractId);
        Task<GetContractsResponse> GetContractsByCompanyTypeAndIdAsync(Guid companyId, string companyType);
        Task<GetContractsResponse> GetContractsByStatusAndCompanyTypeAndIdAsync
            (Guid companyId, string type, string status);
        Task<ContractResponse> CreateContractAsync(ContractRequest request);
        Task<ContractResponse> UpdateContractAsync(Guid contractId, ContractRequest request);

        #endregion
    }
}