﻿using Company.API.Contracts;
using Company.API.Contracts.Request;
using Company.API.Contracts.Response;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Infrastructure.HttpClientAbstractions;

namespace Company.API.Client
{
    public class CompanyClient : BaseHttpClient, ICompanyClient
    {
        public CompanyClient(HttpClient httpClient) : base(httpClient) { }

        #region Company

        public Task<GetCompaniesResponse> GetCompaniesAsync()
        {
            return GetAsync<GetCompaniesResponse>(new Uri(BaseUri + CompanyRoutes.BaseCompanyUri));
        }

        public Task<CompanyResponse> GetCompanyAsync(Guid id)
        {
            return GetAsync<CompanyResponse>(new Uri(BaseUri + CompanyRoutes.BaseCompanyUri + "/" + id.ToString()));
        }

        public Task<GetCompaniesResponse> GetCompaniesByTypeAsync(string type)
        {
            return GetAsync<GetCompaniesResponse>(new Uri(BaseUri + CompanyRoutes.BaseCompanyUri + "/" + CompanyRoutes.CompanyTypeSlash + type));
        }

        public Task<CompanyResponse> CreateCompanyAsync(CompanyRequest request)
        {
            return PostAsync<CompanyRequest, CompanyResponse>(new Uri(BaseUri + CompanyRoutes.BaseCompanyUri), request);
        }

        public Task<CompanyResponse> UpdateCompanyAsync(Guid id, CompanyRequest request)
        {
            return PutAsync<CompanyRequest, CompanyResponse>(new Uri(BaseUri + CompanyRoutes.BaseCompanyUri + "/" + id.ToString()), request);
        }

        public Task<DeleteCompanyResponse> DeleteCompanyAsync(Guid id)
        {
            return DeleteAsync<DeleteCompanyResponse>(new Uri(BaseUri + CompanyRoutes.BaseCompanyUri + "/" + id.ToString()));
        }

        #endregion

        #region Contract

        public Task<GetContractsResponse> GetContractsAsync()
        {
            return GetAsync<GetContractsResponse>(new Uri(BaseUri + CompanyRoutes.BaseContractUri));
        }

        public Task<ContractResponse> GetContractByIdAsync(Guid id)
        {
            return GetAsync<ContractResponse>(new Uri(BaseUri + CompanyRoutes.BaseContractUri + "/" + id.ToString()));
        }

        public Task<GetContractsResponse> GetContractsByCompanyTypeAndIdAsync(Guid companyId, string companyType)
        {
            return GetAsync<GetContractsResponse>(new Uri(BaseUri + CompanyRoutes.BaseContractUri + "/" + companyType + "/" + companyId.ToString()));
        }

        public Task<GetContractsResponse> GetContractsByStatusAndCompanyTypeAndIdAsync
            (Guid companyId, string type, string status)
        {
            return GetAsync<GetContractsResponse>(new Uri(BaseUri + CompanyRoutes.BaseContractUri + "/" + status + "/" + type + "/" + companyId.ToString()));
        }

        public Task<ContractResponse> CreateContractAsync(ContractRequest request)
        {
            return PostAsync<ContractRequest, ContractResponse>(new Uri(BaseUri + CompanyRoutes.BaseContractUri), request);
        }

        public Task<ContractResponse> UpdateContractAsync(Guid contractId, ContractRequest request)
        {
            return PutAsync<ContractRequest, ContractResponse>(new Uri(BaseUri + CompanyRoutes.BaseContractUri + "/" + contractId.ToString()), request);
        }
        #endregion
    }
}
