﻿// <auto-generated />
using System;
using GroupOrder.Persistence.Contexts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace GroupOrder.Persistence.Migrations
{
    [DbContext(typeof(GroupOrderContext))]
    [Migration("20200627142350_initCart")]
    partial class initCart
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.5")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("GroupOrder.Domain.Models.CartModel", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("char(36)");

                    b.Property<string>("AdditionalInfo")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("ChosenDishes")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<Guid>("ProviderId")
                        .HasColumnType("char(36)");

                    b.Property<Guid>("WorkerId")
                        .HasColumnType("char(36)");

                    b.HasKey("Id");

                    b.ToTable("Cart");
                });

            modelBuilder.Entity("GroupOrder.Domain.Models.GroupOrderModel", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("char(36)");

                    b.Property<DateTime>("Date")
                        .HasColumnType("datetime(6)");

                    b.Property<Guid>("LunchGroupId")
                        .HasColumnType("char(36)");

                    b.Property<Guid>("ProviderId")
                        .HasColumnType("char(36)");

                    b.HasKey("Id");

                    b.ToTable("GroupOrder");
                });

            modelBuilder.Entity("GroupOrder.Domain.Models.OrderModel", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("char(36)");

                    b.Property<string>("AdditionalInfo")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("ChosenDishes")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<DateTime>("Date")
                        .HasColumnType("datetime(6)");

                    b.Property<Guid>("GroupOrderId")
                        .HasColumnType("char(36)");

                    b.Property<Guid>("ProviderId")
                        .HasColumnType("char(36)");

                    b.Property<Guid>("WorkerId")
                        .HasColumnType("char(36)");

                    b.HasKey("Id");

                    b.HasIndex("GroupOrderId");

                    b.ToTable("Order");
                });

            modelBuilder.Entity("GroupOrder.Domain.Models.OrderModel", b =>
                {
                    b.HasOne("GroupOrder.Domain.Models.GroupOrderModel", "GroupOrder")
                        .WithMany("Orders")
                        .HasForeignKey("GroupOrderId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
