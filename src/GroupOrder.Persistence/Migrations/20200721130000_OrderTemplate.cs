﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GroupOrder.Persistence.Migrations
{
    public partial class OrderTemplate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MenuId",
                table: "OrderTemplateModels");

            migrationBuilder.AddColumn<Guid>(
                name: "ProviderId",
                table: "OrderTemplateModels",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProviderId",
                table: "OrderTemplateModels");

            migrationBuilder.AddColumn<Guid>(
                name: "MenuId",
                table: "OrderTemplateModels",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }
    }
}
