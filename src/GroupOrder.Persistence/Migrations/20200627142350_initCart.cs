﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace GroupOrder.Persistence.Migrations
{
    public partial class initCart : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
               name: "Cart",
               columns: table => new
               {
                   Id = table.Column<Guid>(nullable: false),
                   AdditionalInfo = table.Column<string>(nullable: true),
                   ProviderId = table.Column<Guid>(nullable: false),
                   WorkerId = table.Column<Guid>(nullable: false),
                   ChosenDishes = table.Column<Guid>(nullable: true)
               },
               constraints: table =>
               {
                   table.PrimaryKey("PK_Cart", x => x.Id);
               });
        }
        protected override void Down(MigrationBuilder migrationBuilder)
        {
          
        }
    }
}
