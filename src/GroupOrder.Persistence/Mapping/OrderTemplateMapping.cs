﻿using GroupOrder.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GroupOrder.Persistence.Mapping
{
    public class OrderTemplateMapping : IEntityTypeConfiguration<OrderTemplateModel>
    {
        private const string TableNameOrderTemplates = "OrderTemplateModels";

        public void Configure(EntityTypeBuilder<OrderTemplateModel> builder)
        {
            builder.ToTable(TableNameOrderTemplates);
            builder.HasKey(x => x.Id);
            builder.Property(prop => prop.Name);
            builder.Property(prop => prop.WorkerId);
            builder.Property(prop => prop.ProviderId);
            builder.Property(x => x.Dishes)
                   .HasConversion(el => MapDishToDB(el), el => MapDishFromDB(el));
        }

        private List<Guid> MapDishFromDB(string el)
        {
            List<string> dishes = el.Split(',', StringSplitOptions.RemoveEmptyEntries).ToList();
            List<Guid> dishesId = new List<Guid>();
            foreach (var item in dishes)
            {
                dishesId.Add(Guid.Parse(item));
            }
            return dishesId;
        }

        private string MapDishToDB(List<Guid> el)
        {
            string dishes = "";
            foreach (var item in el)
            {
                dishes += item.ToString() + ",";
            }
            return dishes;
        }
    }
}
