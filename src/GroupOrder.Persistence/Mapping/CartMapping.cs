﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using GroupOrder.Domain.Models;
using System.Collections.Generic;
using System;
using System.Linq;

namespace GroupOrder.Persistence.Mapping
{
    public class CartMapping : IEntityTypeConfiguration<CartModel>
    {
        private const string TableNameCarts = "Cart";
       
        public void Configure(EntityTypeBuilder<CartModel> builder)
        {
            builder.HasKey(x => x.Id);
            builder.ToTable(TableNameCarts);
            builder.Property(x => x.AdditionalInfo);
            builder.Property(x => x.ProviderId);
            builder.Property(x => x.WorkerId);
            builder.Property(x => x.ChosenDishes)
                   .HasConversion(el => MapDishToDB(el), el => MapDishFromDB(el));
        }

        private IReadOnlyCollection<Guid> MapDishFromDB(string el)
        {
            List<string> dishes = el.Split(',', StringSplitOptions.RemoveEmptyEntries).ToList();
            List<Guid> dishesId = new List<Guid>();
            foreach (var item in dishes)
            {
                dishesId.Add(Guid.Parse(item));
            }
            return dishesId;
        }

        private string MapDishToDB(IReadOnlyCollection<Guid> el)
        {
            string dishes = "";
            foreach (var item in el)
            {
                dishes += item.ToString() + ",";
            }
            return dishes;
        }
    }
}