﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using GroupOrder.Domain.Models;
using System.Collections.Generic;
using System;
using System.Linq;

namespace GroupOrder.Persistence.Mapping
{
    public class OrderMapping : IEntityTypeConfiguration<OrderModel>
    {
        private const string TableNameOrders = "Order";
        public void Configure(EntityTypeBuilder<OrderModel> builder)
        {
            builder.HasKey(x => x.Id);
            builder.ToTable(TableNameOrders);
            builder.Property(x => x.AdditionalInfo);
            builder.Property(x => x.Date);
            builder.Property(x => x.GroupOrderId);
            builder.Property(x => x.ProviderId);
            builder.Property(x => x.WorkerId);
            builder.Property(x => x.ChosenDishes)
                   .HasConversion(el => MapDishToDB(el), el => MapDishFromDB(el));
        }

        private List<Guid> MapDishFromDB(string el)
        {
            List<string> dishes = el.Split(',', StringSplitOptions.RemoveEmptyEntries).ToList();
            List<Guid> dishesId = new List<Guid>();
            foreach (var item in dishes)
            {
                dishesId.Add(Guid.Parse(item));
            }
            return dishesId;
        }

        private string MapDishToDB(IReadOnlyCollection<Guid> el)
        {
            string dishes="";
            foreach(var item in el)
            {
                dishes += item.ToString() + ",";
            }
            return dishes;           
        }
    }
}