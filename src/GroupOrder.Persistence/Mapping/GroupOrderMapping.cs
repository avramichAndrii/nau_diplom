﻿using GroupOrder.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GroupOrder.Persistence.Mapping
{
    public class GroupOrderMapping : IEntityTypeConfiguration<GroupOrderModel>
    {
        private const string TableName = "GroupOrder";

        public void Configure(EntityTypeBuilder<GroupOrderModel> builder)
        {
            builder.ToTable(TableName);
            builder.HasKey(x => x.Id);
            builder.Property(prop => prop.LunchGroupId);
            builder.Property(prop => prop.ProviderId);
            builder.Property(prop => prop.Date);
        }
    }
}
