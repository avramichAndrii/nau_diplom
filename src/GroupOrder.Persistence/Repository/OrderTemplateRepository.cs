﻿using GroupOrder.Domain.Models;
using GroupOrder.Domain.Repositories;
using GroupOrder.Domain.Exceptions;
using GroupOrder.Persistence.Contexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroupOrder.Persistence.Repository
{
    public class OrderTemplateRepository : IOrderTemplateRepository
    {
        private readonly GroupOrderContext _groupOrderContext;
        private const string OrderTemplateNotFoundMessage = "Order template corresponding given parameters not found";

        public OrderTemplateRepository(GroupOrderContext orderTemplateContext)
        {
            _groupOrderContext = orderTemplateContext;
        }

        public async Task<OrderTemplateModel> CreateAsync(OrderTemplateModel orderTemplateModel)
        {
            try
            {
                _groupOrderContext.OrderTemplateModels.Add(orderTemplateModel);
                await _groupOrderContext.SaveChangesAsync();
                return orderTemplateModel;
            }
            catch (InvalidOperationException ioex)
            {
                throw new OrderTemplateDomainException(ioex.Message);
            }
        }

        public async Task DeleteAsync(Guid orderTemplateId)
        {
            OrderTemplateModel modelToDelete = await _groupOrderContext.OrderTemplateModels
                                                                       .SingleOrDefaultAsync(model => model.Id == orderTemplateId);
            if (modelToDelete == null)
            {
                throw new OrderTemplateDomainException(OrderTemplateNotFoundMessage);
            }
            try
            {
                _groupOrderContext.OrderTemplateModels.Remove(modelToDelete);
                await _groupOrderContext.SaveChangesAsync();
            }
            catch (InvalidOperationException ioex)
            {
                throw new OrderTemplateDomainException(ioex.Message);
            }
        }

        public async Task<IReadOnlyCollection<OrderTemplateModel>> GetAllAsync()
        {
            return await _groupOrderContext.OrderTemplateModels.ToListAsync();
        }

        public async Task<IReadOnlyCollection<OrderTemplateModel>> GetAllByProviderIdAsync(Guid providerId)
        {
            var templates = await _groupOrderContext.OrderTemplateModels
                                                       .Where(orderTemplate => orderTemplate.ProviderId == providerId)
                                                       .ToListAsync();
            return templates;
        }

        public async Task<IReadOnlyCollection<OrderTemplateModel>> GetAllByWorkerIdAsync(Guid workerId)
        {
            return await _groupOrderContext.OrderTemplateModels
                                           .Where(model => model.WorkerId == workerId)
                                           .ToListAsync();   
        }

        public async Task<OrderTemplateModel> GetByIdAsync(Guid orderTemplateId)
        {
            var orderTemplateModel = await _groupOrderContext
                                           .OrderTemplateModels
                                           .SingleOrDefaultAsync(orderTemplateModel => orderTemplateModel.Id == orderTemplateId);
            if (orderTemplateModel == null)
            {
                throw new OrderTemplateDomainException(OrderTemplateNotFoundMessage);
            }

            return orderTemplateModel;
        }

        public async Task<OrderTemplateModel> UpdateDishesAsync(OrderTemplateModel orderTemplateModel)
        {
            var orderTemplateToUpdate = await _groupOrderContext
                                                 .OrderTemplateModels
                                                 .SingleOrDefaultAsync(modelToUpdate => modelToUpdate.Id == orderTemplateModel.Id);

            if (orderTemplateToUpdate == null)
            {
                throw new OrderTemplateDomainException(OrderTemplateNotFoundMessage);
            }

            orderTemplateToUpdate.ProviderId = orderTemplateModel.ProviderId;
            orderTemplateToUpdate.Dishes = orderTemplateModel.Dishes;

            try
            {
                await _groupOrderContext.SaveChangesAsync();
                return orderTemplateToUpdate;
            }
            catch (InvalidOperationException ioex)
            {
                throw new OrderTemplateDomainException(ioex.Message);
            }
        }

        public async Task<OrderTemplateModel> UpdateNameAsync(OrderTemplateModel orderTemplateModel)
        {
            var orderTemplateToUpdate = await _groupOrderContext
                                                 .OrderTemplateModels
                                                 .SingleOrDefaultAsync(modelToUpdate => modelToUpdate.Id == orderTemplateModel.Id);

            if (orderTemplateToUpdate == null)
            {
                throw new OrderTemplateDomainException(OrderTemplateNotFoundMessage);
            }

            orderTemplateToUpdate.Name = orderTemplateModel.Name;

            try
            {
                await _groupOrderContext.SaveChangesAsync();
                return orderTemplateToUpdate;
            }
            catch (InvalidOperationException ioex)
            {
                throw new OrderTemplateDomainException(ioex.Message);
            }
        }
    }
}
