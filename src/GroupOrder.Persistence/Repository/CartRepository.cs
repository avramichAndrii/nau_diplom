﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GroupOrder.Domain.Exceptions;
using GroupOrder.Domain.Models;
using GroupOrder.Domain.Repositories;
using GroupOrder.Persistence.Contexts;
using Microsoft.EntityFrameworkCore;

namespace GroupOrder.Persistence.Repository
{
    public class CartRepository : ICartRepository
    {
        private readonly GroupOrderContext _groupOrderContext;
        private const string CartNotFoundMessage = "Cart with id = {0} is not found";
        private const string DeleteMessage = "Couldn't be deleted.";
        private const string UpdateMessage = "Couldn't be updated. Please check sending data.";
        private const string CreateMessage = "Couldn't be created. Please check sending data.";

        public CartRepository(GroupOrderContext groupOrderContext)
        {
            _groupOrderContext = groupOrderContext;
        }

        public async Task<CartModel> UpdateAsync(CartModel cartModel)
        {
            var cartToUpdate = await _groupOrderContext.Carts
                                                       .SingleOrDefaultAsync(modelToUpdate => modelToUpdate.Id == cartModel.Id);

            if (cartToUpdate == null)
            {
                throw new NotFoundGroupOrderDomainException(string.Format(CartNotFoundMessage, cartModel.Id));
            }

            cartToUpdate.AdditionalInfo = cartModel.AdditionalInfo;
            cartToUpdate.ChosenDishes = cartModel.ChosenDishes;
            cartToUpdate.ProviderId = cartModel.ProviderId;
            
            try
            {
                await _groupOrderContext.SaveChangesAsync();
                return cartToUpdate;
            }
            catch (InvalidOperationException)
            {
                throw new InvalidDataGroupOrderDomainException(UpdateMessage);
            }
        }

        public async Task<CartModel> CreateAsync(CartModel cartModel)
        {
            try
            {
                _groupOrderContext.Carts.Add(cartModel);
                await _groupOrderContext.SaveChangesAsync();
                return cartModel;
            }
            catch (InvalidOperationException)
            {
                throw new InvalidDataGroupOrderDomainException(CreateMessage);
            }
        }

        public async Task DeleteAsync(Guid cartId)
        {
            CartModel modelToDelete = await _groupOrderContext.Carts
                                                              .SingleOrDefaultAsync(model => model.Id == cartId);
            if (modelToDelete == null)
            {
                throw new NotFoundGroupOrderDomainException(string.Format(DeleteMessage));
            }
            try
            {
                _groupOrderContext.Carts.Remove(modelToDelete);
                await _groupOrderContext.SaveChangesAsync();
            }
            catch (InvalidOperationException)
            {
                throw new InvalidDataGroupOrderDomainException(DeleteMessage);
            }
        }       

        public async Task<CartModel> GetCartByWorkerIdAsync(Guid workerId)
        {
            try
            {
                CartModel modelToGet = await _groupOrderContext.Carts
                                                               .SingleOrDefaultAsync(model => model.WorkerId == workerId);
                if (modelToGet == null)
                {
                    modelToGet = new CartModel(Guid.NewGuid(), null, new Guid(), workerId, new List<Guid>());
                    await CreateAsync(modelToGet);
                }
                return  modelToGet;
            }
            catch (InvalidOperationException)
            {
                CartModel newCartToGet = new CartModel(Guid.NewGuid(), null,  new Guid(), workerId, new List<Guid>());
                await CreateAsync(newCartToGet);            
                return newCartToGet;
            }
        }
    }
}
