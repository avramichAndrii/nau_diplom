﻿using GroupOrder.Domain.Exceptions;
using GroupOrder.Domain.Models;
using GroupOrder.Domain.Repositories;
using GroupOrder.Persistence.Contexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroupOrder.Persistence.Repository
{
    public class OrderRepository : IOrderRepository
    {
        private readonly GroupOrderContext _groupOrderContext;
        private const string OrderNotFoundMessage = "Order with id = {0} is not found";
        private const string DeleteMessage = "Couldn't be deleted.";
        private const string UpdateMessage = "Couldn't be updated. Please check sending data.";
        private const string CreateMessage = "Couldn't be created. Please check sending data.";
        private const string GetMessage = "Order not found. Please check sending data.";

        public OrderRepository(GroupOrderContext groupOrderContext)
        {
            _groupOrderContext = groupOrderContext;
        }

        public async Task<OrderModel> UpdateAsync(OrderModel orderModel)
        {
            var orderToUpdate = await _groupOrderContext.Orders
                                                        .SingleOrDefaultAsync(modelToUpdate => modelToUpdate.Id == orderModel.Id);

            if (orderToUpdate == null)
            {
                throw new NotFoundGroupOrderDomainException(string.Format(OrderNotFoundMessage, orderModel.Id));
            }

            orderToUpdate.AdditionalInfo = orderModel.AdditionalInfo;
            orderToUpdate.ChosenDishes = orderModel.ChosenDishes;
            orderToUpdate.Date = orderModel.Date;
            orderToUpdate.ProviderId = orderModel.ProviderId;

            try
            {
                await _groupOrderContext.SaveChangesAsync();
                return orderToUpdate;
            }
            catch (InvalidOperationException)
            {
                throw new InvalidDataGroupOrderDomainException(UpdateMessage);
            }
        }

        public async Task<OrderModel> CreateAsync(OrderModel orderModel)
        {
            try
            {
                _groupOrderContext.Orders.Add(orderModel);
                await _groupOrderContext.SaveChangesAsync();
                return orderModel;
            }
            catch (InvalidOperationException)
            {
                throw new InvalidDataGroupOrderDomainException(CreateMessage);
            }
        }

        public async Task DeleteAsync(Guid orderId)
        {
            OrderModel modelToDelete = await _groupOrderContext.Orders
                                                               .SingleOrDefaultAsync(model => model.Id == orderId);
            if (modelToDelete == null)
            {
                throw new NotFoundGroupOrderDomainException(string.Format(DeleteMessage));
            }
            try
            {
                _groupOrderContext.Orders.Remove(modelToDelete);
                await _groupOrderContext.SaveChangesAsync();
            }
            catch (InvalidOperationException)
            {
                throw new InvalidDataGroupOrderDomainException(DeleteMessage);
            }
        }

        public async Task<IReadOnlyCollection<OrderModel>> GetAllAsync()
        {
            return await _groupOrderContext.Orders
                                           .ToListAsync();
        }

        public async Task<OrderModel> GetByIdAsync(Guid orderId)
        {
            try
            {
                return await _groupOrderContext.Orders
                                               .SingleAsync(orderModel => orderModel.Id == orderId);
            }
            catch (InvalidOperationException)
            {
                throw new InvalidDataGroupOrderDomainException(GetMessage);
            }
        }

        public async Task<IReadOnlyCollection<OrderModel>> GetAllByWorkerIdAsync(Guid workerId)
        {
            try
            {
                return await _groupOrderContext.Orders
                                               .Where(order => order.WorkerId == workerId)
                                               .ToListAsync();
            }
            catch (NotFoundGroupOrderDomainException)
            {
                throw new NotFoundGroupOrderDomainException(OrderNotFoundMessage, workerId);
            }
        }

        public async Task<IReadOnlyCollection<OrderModel>> GetAllByProviderIdAsync(Guid providerId)
        {
            try
            {
                return await _groupOrderContext.Orders
                                           .Where(order => order.ProviderId == providerId)
                                           .ToListAsync();
            }
            catch (NotFoundGroupOrderDomainException)
            {
                throw new NotFoundGroupOrderDomainException(OrderNotFoundMessage, providerId);
            }
        }
    }
}
