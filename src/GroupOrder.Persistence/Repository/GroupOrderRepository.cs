﻿using GroupOrder.Domain.Exceptions;
using GroupOrder.Domain.Models;
using GroupOrder.Domain.Repositories;
using GroupOrder.Persistence.Contexts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroupOrder.Persistence.Repository
{
    public class GroupOrderRepository : IGroupOrderRepository
    {
        private readonly GroupOrderContext _groupOrderContext;
        private const string GroupOrderNotFoundMessage = "GroupOrder with id = {0} is not found";
        private const string DeleteMessage = "Couldn't be deleted.";


        public GroupOrderRepository(GroupOrderContext groupOrderContext)
        {
            _groupOrderContext = groupOrderContext;
        }

        private IIncludableQueryable<GroupOrderModel, IReadOnlyCollection<OrderModel>> IncludeOrderList()
        {
            return _groupOrderContext.GroupOrders.Include(model => model.Orders);
        }

        public async Task<GroupOrderModel> CreateAsync(GroupOrderModel groupOrder)
        {
            _groupOrderContext.GroupOrders.Add(groupOrder);
            await _groupOrderContext.SaveChangesAsync();
            return groupOrder;
        }

        public async Task DeleteAsync(Guid groupOrderId)
        {
            GroupOrderModel modelToDelete = await IncludeOrderList().SingleOrDefaultAsync(model => model.Id == groupOrderId);

            if (modelToDelete == null)
            {
                throw new NotFoundGroupOrderDomainException(string.Format(DeleteMessage));
            }
            try
            {
                _groupOrderContext.GroupOrders.Remove(modelToDelete);
                await _groupOrderContext.SaveChangesAsync();
            }
            catch (InvalidOperationException)
            {
                throw new InvalidDataGroupOrderDomainException(DeleteMessage);
            }
        }

        public async Task<IReadOnlyCollection<GroupOrderModel>> GetAllAsync()
        {
            return await IncludeOrderList().ToListAsync();
        }

        public async Task<IReadOnlyCollection<GroupOrderModel>> GetAllByProviderIdAsync(Guid providerId)
        {
            try
            {
                return await IncludeOrderList().Where(groupOrder => groupOrder.ProviderId == providerId)
                                               .ToListAsync();
            }
            catch (InvalidOperationException)
            {
                throw new NotFoundGroupOrderDomainException(GroupOrderNotFoundMessage, providerId);
            }
        }

        public async Task<IReadOnlyCollection<GroupOrderModel>> GetAllByLunchGroupIdAsync(Guid lunchGroupId)
        {
            try
            {
                return await IncludeOrderList().Where(groupOrder => groupOrder.LunchGroupId == lunchGroupId)
                                               .ToListAsync();
            }
            catch (InvalidOperationException)
            {
                throw new NotFoundGroupOrderDomainException(GroupOrderNotFoundMessage, lunchGroupId);
            }
        }

        public async Task<GroupOrderModel> GetByIdAsync(Guid groupOrderId)
        {
            try
            {
                return await IncludeOrderList().SingleAsync(groupOrder => groupOrder.Id == groupOrderId);
            }
            catch (InvalidOperationException)
            {
                throw new NotFoundGroupOrderDomainException(GroupOrderNotFoundMessage, groupOrderId);
            }
        }
    }
}
