﻿using GroupOrder.Domain.Repositories;
using GroupOrder.Persistence.Contexts;
using GroupOrder.Persistence.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;


namespace GroupOrder.Persistence.Extensions
{
    public static class GroupOrdertDIExtensions
    {
        private const string DBConnectionString = nameof(DBConnectionString);

        public static void RegisterGroupOrderPersistence(this IServiceCollection collection, IConfiguration configuration)
        {
            var connection = configuration.GetConnectionString(DBConnectionString);
            collection.AddDbContextPool<GroupOrderContext>(builder => builder.UseMySql(connection));
            collection.AddScoped<IOrderTemplateRepository, OrderTemplateRepository>();
	        collection.AddScoped<IOrderRepository, OrderRepository>();
            collection.AddScoped<IGroupOrderRepository, GroupOrderRepository>();
            collection.AddScoped<ICartRepository, CartRepository>();
        }
    }
}