﻿using GroupOrder.Domain.Models;
using GroupOrder.Persistence.Mapping;
using Microsoft.EntityFrameworkCore;

namespace GroupOrder.Persistence.Contexts
{
    public sealed class GroupOrderContext : DbContext
    {
        public GroupOrderContext(DbContextOptions options) : base(options) { }

        public DbSet<GroupOrderModel> GroupOrders { get; set; }

        public DbSet<OrderModel> Orders { get; set; }

        public DbSet<CartModel> Carts { get; set; }

        public DbSet<OrderTemplateModel> OrderTemplateModels { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new GroupOrderMapping());
            modelBuilder.ApplyConfiguration(new OrderMapping());
            modelBuilder.ApplyConfiguration(new CartMapping());
            modelBuilder.ApplyConfiguration(new OrderTemplateMapping());
            base.OnModelCreating(modelBuilder);
        }
    }
}