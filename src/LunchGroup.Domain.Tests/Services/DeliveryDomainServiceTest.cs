﻿using LunchGroup.Domain.Models;
using LunchGroup.Domain.Repositories;
using LunchGroup.Domain.Services;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace LunchGroup.Domain.Tests.Services
{
    public class DeliveryDomainServiceTest
    {
        private readonly DeliveryDomainService _deliveryDomainService;
        private readonly Mock<IDeliveryRepository> _deliveryRepoMock = new Mock<IDeliveryRepository>();

        public DeliveryDomainServiceTest()
        {
            _deliveryDomainService = new DeliveryDomainService(_deliveryRepoMock.Object);
        }



        [Fact]
        public async Task GetByIdAsync_ShouldReturnDelivery_WhenDeliveryExists()
        {
            //Arrange
            var deliveryId = Guid.NewGuid();

            DeliveryModel model = new DeliveryModel(deliveryId, Guid.NewGuid(), DateTime.Now, StatusModel.Done);
            _deliveryRepoMock.Setup(x => x.GetDeliveryByIdAsync(deliveryId)).ReturnsAsync(model);

            //Act     
            var delivery = await _deliveryDomainService.GetDeliveryByIdAsync(deliveryId);

            //Assert
            var result = Assert.IsType<DeliveryModel>(delivery);
            Assert.Equal(deliveryId, result.Id);
        }
        [Fact]
        public async Task GetByGroupOrderIdAsync_ShouldReturnDelivery_WhenDeliveryExists()
        {
            //Arrange
            var deliveryId = Guid.NewGuid();

            DeliveryModel model = new DeliveryModel(deliveryId, Guid.NewGuid(), DateTime.Now, StatusModel.Done);
            _deliveryRepoMock.Setup(x => x.GetDeliveryByGroupOrderIdAsync(deliveryId)).ReturnsAsync(model);

            //Act     
            var delivery = await _deliveryDomainService.GetDeliveryByGroupOrderIdAsync(deliveryId);

            //Assert
            var result = Assert.IsType<DeliveryModel>(delivery);
            Assert.Equal(deliveryId, result.Id);
        }
        [Fact]
        public async Task GetByIdAsync_ShouldReturnNothing_WhenDeliveryDoesNotExist()
        {
            // Arrange
            _deliveryRepoMock.Setup(x => x.GetDeliveryByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() => null);

            // Act
            var delivery = await _deliveryDomainService.GetDeliveryByIdAsync(Guid.NewGuid());

            // Assert
            Assert.Null(delivery);
        }
        [Fact]
        public async Task CreateAsync_ShouldReturnNewDeliveryModel()
        {
            //Arrange
            Guid groupOrderId = Guid.NewGuid();
            Guid testId = Guid.NewGuid();
            DeliveryModel model = new DeliveryModel(testId, Guid.NewGuid(), DateTime.Now, StatusModel.Done);
            _deliveryRepoMock.Setup(x => x.CreateAsync(groupOrderId))
                .ReturnsAsync(() => model);

            // Act
            var delivery = await _deliveryDomainService.CreateAsync(groupOrderId);

            //Assert
            var result = Assert.IsType<DeliveryModel>(delivery);
            Assert.Equal(testId, result.Id);
        }
        [Fact]
        public async void CreateAsync_ShouldThrowException_WhenServiceIsNullOrEmpty()
        {
            //Arrange
            var service = new DeliveryDomainService(null);
            Guid testId = Guid.NewGuid();

            //Assert
            await Assert.ThrowsAsync<NullReferenceException>(() => service.CreateAsync(testId));
        }
        [Fact]
        public async void UpdateAsync_ShouldReturnSameInfo_AfterUpdate()
        {
            //Arrange
            Guid testId = Guid.NewGuid();
            DeliveryModel model = new DeliveryModel(testId, Guid.NewGuid(), DateTime.Now, StatusModel.Done);
            _deliveryRepoMock.Setup(x => x.UpdateDeliveryAsync(testId, It.IsAny<DeliveryModel>()))
                .ReturnsAsync(() => model);

            // Act
            var check = await _deliveryDomainService.UpdateDeliveryAsync(testId, new DeliveryModel(testId, Guid.NewGuid(), DateTime.Now, StatusModel.Done));

            //Assert
            var result = Assert.IsType<DeliveryModel>(check);
            Assert.Equal(testId, result.Id);
            Assert.Equal(model.DeliveryTime, result.DeliveryTime);
            Assert.Equal(model.Status, result.Status);

        }
    }
}
