﻿using LunchGroup.Domain.Exceptions;
using LunchGroup.Domain.Models;
using LunchGroup.Domain.Repositories;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace LunchGroup.Domain.Services.Tests
{
    public class LunchGroupDomainServiceTests
    {
        [Fact]
        public async void GetAllGroupsByCompany_WhenCollectionIsEmpty()
        {
            //Arrange
            var mockRepository = new Mock<ILunchGroupRepository>();
            Guid companyId = Guid.NewGuid();
            IReadOnlyCollection<LunchGroupModel> modelsToReturn = new List<LunchGroupModel>();
            mockRepository.Setup(n => n.GetAllByCompanyAsync(companyId))
                .ReturnsAsync(modelsToReturn);
            var service = new LunchGroupDomainService(mockRepository.Object);

            //Act
            var result = await service.GetAllByCompanyAsync(companyId);

            //Assert
            var list = Assert.IsType<List<LunchGroupModel>>(result);
            Assert.Empty(result);
        }

        [Fact]
        public async void GetAllGroupsByCompany_WhenCollectionIsNotEmpty_ShouldReturn3()
        {
            //Arrange
            var mockRepository = new Mock<ILunchGroupRepository>();
            Guid companyId = Guid.NewGuid();
            IReadOnlyCollection<LunchGroupModel> modelsToReturn = new List<LunchGroupModel>()
            {
                new LunchGroupModel { Name = "Geeks" }
            };
            mockRepository.Setup(n => n.GetAllByCompanyAsync(companyId))
                .ReturnsAsync(modelsToReturn);
            var service = new LunchGroupDomainService(mockRepository.Object);

            //Act
            var result = await service.GetAllByCompanyAsync(companyId);

            //Assert
            Assert.Equal(modelsToReturn.Count, result.Count());
        }

        [Fact]
        public async void GetAllGroupsByCompany_WhenItThrowsException()
        {
            //Arrange
            var mockRepository = new Mock<ILunchGroupRepository>();
            Guid companyId = Guid.NewGuid();
            mockRepository.Setup(n => n.GetAllByCompanyAsync(companyId))
                .ThrowsAsync(new LunchGroupDomainException());
            var service = new LunchGroupDomainService(mockRepository.Object);

            //Act & Assert
            await Assert.ThrowsAsync<LunchGroupDomainException>(() => service.GetAllByCompanyAsync(companyId));
        }

        [Fact]
        public async void SetActiveFlag_WhenGroupNotExists_ThrowsNotFoundException()
        {
            //Arrange
            var mockRepository = new Mock<ILunchGroupRepository>();
            Guid groupId = Guid.NewGuid();
            bool activeFlag = false;
            mockRepository.Setup(n => n.SetActiveFlagAsync(groupId, activeFlag))
                .ThrowsAsync(new LunchGroupNotFoundException());
            var service = new LunchGroupDomainService(mockRepository.Object);

            //Act & Assert
            await Assert.ThrowsAsync<LunchGroupNotFoundException>(() => service.SetActiveFlagAsync(groupId, activeFlag));
        }

        [Fact]
        public async void UpdateTimetable_WhenGroupNotExists_ThrowsNotFoundException()
        {
            //Arrange
            var mockRepository = new Mock<ILunchGroupRepository>();
            Guid groupId = Guid.NewGuid();
            mockRepository.Setup(n => n.UpdateTimetableAsync(groupId))
                .ThrowsAsync(new LunchGroupNotFoundException());
            var service = new LunchGroupDomainService(mockRepository.Object);

            //Act & Assert
            await Assert.ThrowsAsync<LunchGroupNotFoundException>(() => service.UpdateTimetableAsync(groupId));
        }

        [Fact]
        public async void GetById_WhenGroupExists_ReturnsSpecificGroup()
        {
            //Arrange
            var mockRepository = new Mock<ILunchGroupRepository>();
            Guid groupId = Guid.NewGuid();
            var testModel = new LunchGroupModel(groupId)
            {
                Name = "TestGroup"
            };
            mockRepository.Setup(n => n.GetByIdAsync(groupId))
                .ReturnsAsync(testModel);
            var service = new LunchGroupDomainService(mockRepository.Object);

            //Act
            var result = await service.GetByIdAsync(groupId);

            //Act & Assert
            var actualModel = Assert.IsType<LunchGroupModel>(result);
            Assert.Equal(groupId, actualModel.Id);
            Assert.Equal(testModel.Name, actualModel.Name);
        }

        [Fact]
        public void GetById_WhenGroupNotExists_ThrowsNotFoundException()
        {
            //Arrange
            var mockRepository = new Mock<ILunchGroupRepository>();
            Guid groupId = Guid.NewGuid();
            mockRepository.Setup(n => n.GetByIdAsync(groupId))
                .ThrowsAsync(new LunchGroupNotFoundException());
            var service = new LunchGroupDomainService(mockRepository.Object);

            //Act
            var response = service.GetByIdAsync(groupId);
            //Assert
            Assert.Equal("Faulted", response.Status.ToString());
        }

        [Fact]
        public void GetByWorkerId_WhenLunchGroupSingle_ShouldReturnOkAsync()
        {
            //Arrange
            Guid testId = new Guid();
            var mockRep = new Mock<ILunchGroupRepository>();
            mockRep.Setup(obj => obj.GetAllByPersonIdAsync(testId))
                .ReturnsAsync(new LunchGroupModel[] { new LunchGroupModel() { Name="test"} });

            LunchGroupDomainService service = new LunchGroupDomainService(mockRep.Object);
            //Act
            var result = service.GetByWorkerIdAsync(testId);

            //Assert
            Assert.True(result.Status.ToString() == "RanToCompletion");
            Assert.IsType<Task<LunchGroupModel>>(result);
        }

        [Fact]
        public void GetByWorkerId_WhenLunchGroupMultiple_ShouldReturnError()
        {
            //Arrange
            Guid testId = new Guid();
            var mockRep = new Mock<ILunchGroupRepository>();
            mockRep.Setup(obj => obj.GetAllByPersonIdAsync(testId))
                .ReturnsAsync(new LunchGroupModel[] { new LunchGroupModel() { Name = "test" }, new LunchGroupModel() { Name = "Second" } });

            LunchGroupDomainService service = new LunchGroupDomainService(mockRep.Object);
            //Act
            var result = service.GetByWorkerIdAsync(testId);

            //Assert
            Assert.True(result.Status.ToString() == "Faulted");
        }
    }
}