using Menu.API.Contracts.Requests;
using Menu.API.Contracts.Responses;
using Menu.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Menu.API
{
    public static class MenuMappers
    {
        public static MenuModel MapFrom(this MenuRequest request)
        {
            return new MenuModel(Guid.NewGuid(), request.MenuName, request.MenuInfo, request.ProviderCompanyId, request.ImageData);
        }

        public static MenuResponse MapTo(this MenuModel menuModel)
        {
            TimeSpan preparationTime = menuModel.GetMenuPreparationTime();

            return new MenuResponse()
            {
                Id = menuModel.Id,
                MenuName = menuModel.MenuName,
                MenuInfo = menuModel.MenuInfo,
                PreparationTime = preparationTime.ToString(),
                ProviderCompanyId = menuModel.ProviderCompanyId,
                DishList = DishHelper(menuModel.DishList),
                IsInMarketplace = menuModel.IsInMarketplace,
                ImageData = menuModel.ImageData,
            };
        }
        public static List<DishResponse> DishHelper(List<DishModel> models)
        {
            List<DishResponse> dishList = new List<DishResponse>();
            if (models != null)
            {
                foreach (DishModel dish in models)
                {
                    DishResponse dishResponse = dish.MapTo();
                    dishList.Add(dishResponse);
                }
            }
            return dishList;
        }

        public static GetAllMenusResponse MapToCollection(this IReadOnlyCollection<MenuModel> menuModels)
        {
            return new GetAllMenusResponse(menuModels.Select(cm => cm.MapTo())
                                                     .ToList());
        }

        public static GetAllDishesResponse MapToCollection(this IReadOnlyCollection<DishModel> dishModels)
        {
            return new GetAllDishesResponse(dishModels.Select(cm => cm.MapTo())
                                                                  .ToList());
        }

        public static DishResponse MapTo(this DishModel dishModel)
        {

            return new DishResponse()
            {
                Id = dishModel.Id,
                DishName = dishModel.DishName,
                DishComposition = dishModel.DishComposition,
                PreparationTime = dishModel.PreparationTime.ToString(),
                Category = dishModel.Category,
                MenuID = dishModel.MenuID,
                Protein = dishModel.DishPFC.Proteins,
                Fats = dishModel.DishPFC.Fats,
                Carbohydrates = dishModel.DishPFC.Carbohydrates,
                CaloricContent = dishModel.DishPFC.CaloricContent,
                Currency = dishModel.Price.Currency,
                Amount = dishModel.Price.Amount,
                ImageData = dishModel.ImageData,
                IsActive = dishModel.IsActive

            };
        }

        public static DishModel MapFrom(this DishRequest request)
        {

            DishModel dishModel = new DishModel(Guid.NewGuid(), request.DishName, request.Category,
                                                TimeSpan.Parse(request.PreparationTime), request.DishComposition, request.MenuID, request.ImageData);
            dishModel.SetNutritionalValue(request.CaloricContent, request.Carbohydrates, request.Fats, request.Protein);
            dishModel.SetPrice(request.Amount, request.Currency);
            return dishModel;
        }

        public static DishModel MapFrom(this UpdateDishIsActiveRequest request, Guid id)
        {
            DishModel dishModel = new DishModel(id, String.Empty, String.Empty, new TimeSpan(), new List<string>(), new Guid(), null);
            dishModel.IsActive = request.IsActive;
            return dishModel;
        }
    }
}


