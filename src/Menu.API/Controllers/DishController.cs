﻿using Menu.API.Contracts.Requests;
using Menu.API.Contracts.Responses;
using Menu.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Menu.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DishController : ControllerBase
    {
        private readonly ILogger<DishController> _logger;
        private readonly IDishDomainService _dishDomainService;

        public DishController(ILogger<DishController> logger, IDishDomainService dishDomainService)
        {
            _logger = logger;
            _dishDomainService = dishDomainService;
        }

        /// <summary>
        /// Get all dishes 
        /// </summary>
        /// <returns>Collection of dishes with name of dish, category, information about price, PFC, preparation time, menu`s id </returns>
        [HttpGet]
        public async Task<ActionResult<GetAllDishesResponse>> GetAll()
        {
            try
            {
                IReadOnlyCollection<DishModel> dishModels = await _dishDomainService.GetAllAsync();
                return dishModels.MapToCollection();
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Get dish by dish id
        /// </summary>
        /// <param name="id"> The unique dish id </param>
        /// <returns>Dish with name, category, information about price, PFC, preparation time, menu`s id,</returns>
        [HttpGet("{dishId}")]
        public async Task<ActionResult<DishResponse>> GetAsync(Guid dishId)
        {
            try
            {
                DishModel res = await _dishDomainService.GetByIdAsync(dishId);
                return res.MapTo();
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Create new dish 
        /// </summary>
        /// <param name="request">Dish information for creation</param>
        /// <returns>Dish, which was created</returns>
        [HttpPost]
        public async Task<ActionResult<DishResponse>> CreateDishAsync([FromBody] DishRequest request)
        {
            try
            {
                var model = request.MapFrom();
                var res = await _dishDomainService.CreateAsync(model);
                return res.MapTo();
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Update exsting dish
        /// </summary>
        /// <param name="dishId">Unique dish id</param>
        /// <param name="request">Information of dish, which should update</param>
        /// <returns>Updated dish information</returns>
        [HttpPut("{dishId}")]
        public async Task<ActionResult<DishResponse>> UpdateDishAsync(Guid dishId, [FromBody] DishRequest request)
        {
            try
            {
                DishModel model = request.MapFrom();
                DishModel res = await _dishDomainService.UpdateAsync(dishId, model);
                return res.MapTo();
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpPatch("{dishId}")]
        public async Task<ActionResult<DishResponse>> UpdateDishIsActiveAsync(Guid dishId, [FromBody] UpdateDishIsActiveRequest request)
        {
            try
            {
                DishModel model = request.MapFrom(dishId);
                DishModel res = await _dishDomainService.UpdateDishIsActiveAsync(dishId, request.IsActive);
                return res.MapTo();
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Delete dish by id
        /// </summary>
        /// <param name="id">Unique dish id</param>
        /// <returns>Action status</returns>
        [HttpDelete("{dishId}")]
        public async Task<ActionResult> DeleteAsync(Guid dishId)
        {
            try
            {
                await _dishDomainService.DeleteAsync(dishId);
                return Ok();
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpPost("totalCost")]
        public async Task<ActionResult<CalculateDishListCostResponse>> GetDishListCostAsync([FromBody] CalculateDishListPriceRequest dishesIds)
        {
            try
            {
                decimal totalCost = await _dishDomainService.GetDishListCostAsync(dishesIds.DishIdsCollection);

                return new CalculateDishListCostResponse() { Cost = totalCost };
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}

