﻿using Microsoft.AspNetCore.Mvc;
using Menu.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System;
using Menu.API.Contracts.Responses;
using Menu.API.Contracts.Requests;

namespace Menu.API
{
    [ApiController]
    [Route("[controller]")]
    public class MarketplaceController : ControllerBase
    {
        private readonly IMarketplaceService _marketplaceService;

        public MarketplaceController(IMarketplaceService marketplaceService)
        {
            _marketplaceService = marketplaceService;
        }

        [HttpGet]
        public async Task<ActionResult<GetAllMenusResponse>> GetAllAsync()
        {
            try
            {
                IReadOnlyCollection<MenuModel> menuModels = await _marketplaceService.GetAllOffersAsync();
                return Ok(menuModels.MapToCollection());
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
        [HttpGet("{menuId}")]
        public async Task<ActionResult<MenuResponse>> GetByProviderIdAsync(Guid providerId)
        {
            try
            {
                IReadOnlyCollection<MenuModel> menuModels = await _marketplaceService.GetByProviderIdAsync(providerId);
                return Ok(menuModels.Select(d => d.MapTo()).ToList());
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
        [HttpPut("{menuId}")]
        public async Task<ActionResult<MenuResponse>> UpdateIsInMarketplaceAsync(Guid menuId, [FromBody] MarketplacePutRequest request)
        {
            try
            {
                MenuModel result = await _marketplaceService.UpdateIsInMarketplaceAsync(menuId, request.IsInMarketplace);
                return Ok(result.MapTo());
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}

