﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Menu.API.Contracts.Requests;
using Menu.API.Contracts.Responses;
using Menu.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Menu.API
{
    [ApiController]
    [Route("[controller]")]
    public class MenuController : ControllerBase
    {
        private readonly ILogger<MenuController> _logger;
        private readonly IMenuDomainService _menuDomainService;

        public MenuController(ILogger<MenuController> logger, IMenuDomainService menuDomainService)
        {
            _logger = logger;
            _menuDomainService = menuDomainService;
        }

        /// <summary>
        /// Get all menues
        /// </summary>
        /// <returns>List of menues with dishes and menues information</returns>
        [HttpGet]
        public async Task<ActionResult<GetAllMenusResponse>> GetAllAsync()
        {
            try
            {
                IReadOnlyCollection<MenuModel> menuModels = await _menuDomainService.GetAllAsync();
                return menuModels.MapToCollection();
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Get menu by menue id
        /// </summary>
        /// <param name="id">Unique menu id</param>
        /// <returns>Menu with information and dishes</returns>
        [HttpGet("{menuId}")]
        public async Task<ActionResult<MenuResponse>> GetAsync(Guid menuId)
        {
            try
            {
                MenuModel res = await _menuDomainService.GetByIdAsync(menuId);
                return res.MapTo();
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Get dishes by menu id
        /// </summary>
        /// <param name="menuId">Id of menu, which contain dishes</param>
        /// <returns>List of dishes with information</returns>
        [HttpGet("{menuId}/dishes")]
        public async Task<ActionResult<GetAllDishesResponse>> GetDishesByMenuAsync(Guid menuId)
        {
            try
            {
                IReadOnlyCollection<DishModel> dishModels = await _menuDomainService.GetDishesByMenuIdAsync(menuId);
                return dishModels.MapToCollection();
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Get menues by provider companies id
        /// </summary>
        /// <param name="providerCompanyId">Unique provider company id</param>
        /// <returns>Collection of menus with information and dishes</returns>
        [HttpGet("provider/{providerCompanyId}")]
        public async Task<ActionResult<GetAllMenusResponse>> GetByProviderAsync(Guid providerCompanyId)
        {
            try
            {
                IReadOnlyCollection<MenuModel> menuModels = await _menuDomainService.GetByProviderCompanyIdAsync(providerCompanyId);
                return menuModels.MapToCollection();
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Create new menu
        /// </summary>
        /// <param name="request">Menu information from UI</param>
        /// <returns>Menu, which was created</returns>
        [HttpPost]
        public async Task<ActionResult<MenuResponse>> CreateMenuAsync([FromBody] MenuRequest request)
        {
            try
            {
                var model = request.MapFrom();
                var res = await _menuDomainService.CreateAsync(model);
                return res.MapTo();
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }


        /// <summary>
        /// Update existing menu
        /// </summary>
        /// <param name="menuId"> Unique menu id</param>
        /// <param name="request"> Updating information from UI</param>
        /// <returns>Menu, which was updated</returns>
        [HttpPut("{menuId}")]
        public async Task<ActionResult<MenuResponse>> UpdateMenuAsync(Guid menuId, [FromBody] MenuRequest request)
        {
            try
            {
                MenuModel model = request.MapFrom();
                MenuModel res = await _menuDomainService.UpdateAsync(menuId, model);
                return res.MapTo();
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Delete menu with dishes by menu id
        /// </summary>
        /// <param name="menuId">Unique menu id</param>
        /// <returns>Action status</returns>
        [HttpDelete("{menuId}")]
        public async Task<ActionResult> DeleteAsync(Guid menuId)
        {
            try
            {
                await _menuDomainService.DeleteAsync(menuId);
                return Ok();
            }
            catch (MenuDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }


    }
}

