using Infrastructure.HttpClientAbstractions;
using LunchGroup.API.Contracts;
using LunchGroup.API.Contracts.Requests;
using LunchGroup.API.Contracts.Responses;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace LunchGroup.API.Client
{
    public class LunchGroupClient : BaseHttpClient, ILunchGroupClient
    {
        public LunchGroupClient(HttpClient client) : base(client) { }

        #region lunchgroup
        public Task<CreateLunchGroupResponse> CreateLunchGroupAsync(CreateLunchGroupRequest lunchGroupRequest)
        {
            return PostAsync<CreateLunchGroupRequest, CreateLunchGroupResponse>(new Uri(BaseUri + LunchGroupRoutes.BaseLunchGroupUri), lunchGroupRequest);
        }

        public Task<GetLunchGroupsCollectionResponse> GetAllLunchGroupsByCompanyAsync(Guid companyId)
        {
            return GetAsync<GetLunchGroupsCollectionResponse>(new Uri(BaseUri + LunchGroupRoutes.BaseLunchGroupUri + "/" + LunchGroupRoutes.CompanyUri + "/" + companyId));
        }

        public Task<GetLunchGroupResponse> GetLunchGroupByIdAsync(Guid groupId)
        {
            return GetAsync<GetLunchGroupResponse>(new Uri(BaseUri + LunchGroupRoutes.BaseLunchGroupUri + "/" + groupId.ToString()));
        }

        public Task<GetLunchGroupsCollectionResponse> GetLunchGroupsByManagerAsync(Guid managerId)
        {
            return GetAsync<GetLunchGroupsCollectionResponse>(new Uri(BaseUri + LunchGroupRoutes.BaseLunchGroupUri + "/" + LunchGroupRoutes.ManagerUri + "/" + managerId));
        }

        public Task<GetTimetableResponse> GetTimetableAsync(Guid groupId)
        {
            return GetAsync<GetTimetableResponse>(new Uri(BaseUri + LunchGroupRoutes.BaseLunchGroupUri + "/" + LunchGroupRoutes.TimetableUri + "/" + groupId));
        }

        public Task<GetLunchGroupResponse> GetWorkerLunchGroupAsync(Guid workerId)
        {
            return GetAsync<GetLunchGroupResponse>(new Uri(BaseUri + LunchGroupRoutes.BaseLunchGroupUri + "/" + LunchGroupRoutes.WorkerUri + "/" + workerId));
        }

        public Task SetActiveFlagAsync(Guid groupId, SetActiveFlagRequest request)
        {
            return PutAsync(new Uri(BaseUri + LunchGroupRoutes.BaseLunchGroupUri + "/" + LunchGroupRoutes.StatusUri + "/" + groupId), request);
        }

        public Task<Guid> UpdateLunchGroupAsync(UpdateLunchGroupRequest lunchGroupToUpdate, Guid id)
        {
            return PutAsync<UpdateLunchGroupRequest, Guid>(new Uri(BaseUri + LunchGroupRoutes.BaseLunchGroupUri + "/" + id), lunchGroupToUpdate);
        }

        public Task UpdateTimetableAsync(Guid groupId)
        {
            return PutAsync(new Uri(BaseUri + LunchGroupRoutes.BaseLunchGroupUri + "/" + LunchGroupRoutes.TimetableUri + "/" + groupId));
        }

        #endregion

        #region delivery
        public Task<GetAllDeliveriesResponse> GetAllDeliveriesAsync()
        {
            return GetAsync<GetAllDeliveriesResponse>(new Uri(BaseUri + DeliveryRouts.BaseDeliveryURL));
        }

        public Task<DeliveryResponse> GetDeliveryByIdAsync(Guid deliveryId)
        {
            return GetAsync<DeliveryResponse>(new Uri(BaseUri + DeliveryRouts.BaseDeliveryURLSlash + deliveryId.ToString()));
        }

        public Task<DeliveryResponse> GetDeliveryByGroupOrderIdAsync(Guid orderId)
        {
            return GetAsync<DeliveryResponse>(new Uri(BaseUri + DeliveryRouts.DeliveryOrderURLSlash + orderId.ToString()));
        }

        public Task<DeliveryResponse> CreateDeliveryAsync(CreateDeliveryRequest request)
        {
            return PostAsync<CreateDeliveryRequest, DeliveryResponse>(new Uri(BaseUri + DeliveryRouts.BaseDeliveryURLSlash + request.GroupOrderId.ToString()), request);
        }

        public Task<DeliveryResponse> UpdateDeliveryAsync(Guid deliveryId, UpdateDeliveryRequest request)
        {
            return PutAsync<UpdateDeliveryRequest, DeliveryResponse>(new Uri(BaseUri + DeliveryRouts.BaseDeliveryURLSlash + deliveryId.ToString()), request);
        }

        public Task<DeliveryResponse> DeleteDeliveryAsync(Guid deliveryId)
        {
            return DeleteAsync<DeliveryResponse>(new Uri(BaseUri + DeliveryRouts.BaseDeliveryURLSlash + deliveryId.ToString()));

        }
        #endregion
    }
}
