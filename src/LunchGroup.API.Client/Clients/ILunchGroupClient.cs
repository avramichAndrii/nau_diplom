﻿using LunchGroup.API.Contracts.Requests;
using LunchGroup.API.Contracts.Responses;
using System;
using System.Threading.Tasks;
using Infrastructure.HttpClientAbstractions.Interfaces;

namespace LunchGroup.API.Client
{
    public interface ILunchGroupClient : IClient
    {
        //LunchGroup

        Task<CreateLunchGroupResponse> CreateLunchGroupAsync(CreateLunchGroupRequest lunchGroupRequest);
        Task<GetLunchGroupsCollectionResponse> GetAllLunchGroupsByCompanyAsync(Guid companyId);
        Task<GetLunchGroupResponse> GetLunchGroupByIdAsync(Guid groupId);
        Task<GetLunchGroupsCollectionResponse> GetLunchGroupsByManagerAsync(Guid managerId);
        Task<GetTimetableResponse> GetTimetableAsync(Guid groupId);
        Task<GetLunchGroupResponse> GetWorkerLunchGroupAsync(Guid workerId);
        Task SetActiveFlagAsync(Guid groupId, SetActiveFlagRequest request);
        Task<Guid> UpdateLunchGroupAsync(UpdateLunchGroupRequest lunchGroupToUpdate, Guid id);
        Task UpdateTimetableAsync(Guid groupId);

        // Delivery
        public Task<GetAllDeliveriesResponse> GetAllDeliveriesAsync();
        public Task<DeliveryResponse> GetDeliveryByIdAsync(Guid deliveryId);
        public Task<DeliveryResponse> GetDeliveryByGroupOrderIdAsync(Guid orderId);
        public Task<DeliveryResponse> CreateDeliveryAsync(CreateDeliveryRequest request);
        public Task<DeliveryResponse> UpdateDeliveryAsync(Guid deliveryId, UpdateDeliveryRequest request);
        public Task<DeliveryResponse> DeleteDeliveryAsync(Guid deliveryId);
        
    }
}