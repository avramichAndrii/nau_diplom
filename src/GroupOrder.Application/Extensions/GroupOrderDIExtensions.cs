﻿using GroupOrder.Application.Services;
using GroupOrder.Domain.Services;
using Microsoft.Extensions.DependencyInjection;

namespace GroupOrder.Application.Extensions
{
    public static class GroupOrderDIExtensions
    {
        public static void RegisterGroupOrderAPI(this IServiceCollection collection)
        {
            collection.AddScoped<IGroupOrderService, GroupOrderService>();
            collection.AddScoped<IOrderService, OrderService>();
            collection.AddScoped<ICartService, CartService>();
            collection.AddScoped<IOrderTemplateService, OrderTemplateService>();
        }
    }
}
