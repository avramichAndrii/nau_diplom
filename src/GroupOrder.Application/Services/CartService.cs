﻿using GroupOrder.Domain.Models;
using GroupOrder.Domain.Repositories;
using GroupOrder.Domain.Services;
using System;
using System.Threading.Tasks;

namespace GroupOrder.Application.Services
{
    public class CartService : ICartService
    {
        private readonly ICartRepository _cartRepository;

        public CartService(ICartRepository cartRepository)
        {
            _cartRepository = cartRepository;
        }

        public Task<CartModel> Create(CartModel cartModel)
        {
            return _cartRepository.CreateAsync(cartModel);
        }

        public Task Delete(Guid cartId)
        {
            return _cartRepository.DeleteAsync(cartId);
        }

        public Task<CartModel> GetCartByWorkerId(Guid workerId)
        {
            return _cartRepository.GetCartByWorkerIdAsync(workerId);
        }

        public Task<CartModel> Update(CartModel cartModel)
        {
            return _cartRepository.UpdateAsync(cartModel);
        }
    }
}
