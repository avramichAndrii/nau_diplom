﻿using GroupOrder.Domain.Models;
using GroupOrder.Domain.Repositories;
using GroupOrder.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroupOrder.Application.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IGroupOrderService _groupOrderService;

        public OrderService(IOrderRepository orderRepository, IGroupOrderService groupOrderService)
        {
            _orderRepository = orderRepository;
            _groupOrderService = groupOrderService;
        }

        public async Task<OrderModel> Create(OrderModel orderModel, Guid lunchGroupId)
        {
            var groupOrders = _groupOrderService.GetAllByProviderId(orderModel.ProviderId).Result;
            GroupOrderModel groupOrder = groupOrders.SingleOrDefault
                (groupOrderResponse => groupOrderResponse.Date.Date == orderModel.Date.Date & groupOrderResponse.LunchGroupId==lunchGroupId);
            if (groupOrder == null)
            {
                orderModel.GroupOrderId = Guid.NewGuid();
                await _groupOrderService.Create(new GroupOrderModel(orderModel.GroupOrderId, orderModel.ProviderId, lunchGroupId, orderModel.Date));
            }
            else orderModel.GroupOrderId = groupOrder.Id;
            return await _orderRepository.CreateAsync(orderModel);
        }

        public Task Delete(Guid orderId)
        {
            return _orderRepository.DeleteAsync(orderId);
        }

        public Task<IReadOnlyCollection<OrderModel>> GetAll()
        {
            return _orderRepository.GetAllAsync();
        }

        public Task<IReadOnlyCollection<OrderModel>> GetAllByWorkerId(Guid workerId)
        {
            return _orderRepository.GetAllByWorkerIdAsync(workerId);
        }

        public Task<OrderModel> GetById(Guid orderId)
        {
            return _orderRepository.GetByIdAsync(orderId);
        }

        public Task<OrderModel> Update(OrderModel orderModel)
        {
            return _orderRepository.UpdateAsync(orderModel);
        }

        public Task<IReadOnlyCollection<OrderModel>> GetAllByProviderId(Guid providerId)
        {
            return _orderRepository.GetAllByProviderIdAsync(providerId);
        }
    }
}
