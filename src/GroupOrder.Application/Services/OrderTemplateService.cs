﻿using GroupOrder.Domain.Models;
using GroupOrder.Domain.Repositories;
using GroupOrder.Domain.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GroupOrder.Application.Services
{
    public class OrderTemplateService : IOrderTemplateService
    {
        private readonly IOrderTemplateRepository _orderTemplateRepository;

        public OrderTemplateService(IOrderTemplateRepository orderTemplateRepository)
        {
            _orderTemplateRepository = orderTemplateRepository;
        }

        public Task<OrderTemplateModel> Create(OrderTemplateModel orderTemplateModel)
        {
            return _orderTemplateRepository.CreateAsync(orderTemplateModel);
        }

        public Task Delete(Guid orderTemplateId)
        {
            return _orderTemplateRepository.DeleteAsync(orderTemplateId);
        }

        public Task<IReadOnlyCollection<OrderTemplateModel>> GetAll()
        {
            return _orderTemplateRepository.GetAllAsync();
        }

        public Task<IReadOnlyCollection<OrderTemplateModel>> GetAllByProviderId(Guid providerId)
        {
            return _orderTemplateRepository.GetAllByProviderIdAsync(providerId);
        }

        public Task<IReadOnlyCollection<OrderTemplateModel>> GetAllByWorkerId(Guid workerId)
        {
            return _orderTemplateRepository.GetAllByWorkerIdAsync(workerId);
        }

        public Task<OrderTemplateModel> GetById(Guid orderTemplateId)
        {
            return _orderTemplateRepository.GetByIdAsync(orderTemplateId);
        }

        public Task<OrderTemplateModel> UpdateDishes(OrderTemplateModel orderTemplateModel)
        {
            return _orderTemplateRepository.UpdateDishesAsync(orderTemplateModel);
        }

        public Task<OrderTemplateModel> UpdateName(OrderTemplateModel orderTemplateModel)
        {
            return _orderTemplateRepository.UpdateNameAsync(orderTemplateModel);
        }
    }
}
