﻿using GroupOrder.Domain.Models;
using GroupOrder.Domain.Repositories;
using GroupOrder.Domain.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GroupOrder.Application.Services
{
    public class GroupOrderService : IGroupOrderService
    {
        private readonly IGroupOrderRepository _groupOrderRepository;

        public GroupOrderService(IGroupOrderRepository groupOrderRepository)
        {
            _groupOrderRepository = groupOrderRepository;
        }

        public Task<GroupOrderModel> Create(GroupOrderModel groupOrder)
        {
            return _groupOrderRepository.CreateAsync(groupOrder);
        }

        public Task Delete(Guid groupOrderId)
        {
            return _groupOrderRepository.DeleteAsync(groupOrderId);
        }

        public Task<IReadOnlyCollection<GroupOrderModel>> GetAll()
        {
            return _groupOrderRepository.GetAllAsync();
        }

        public Task<IReadOnlyCollection<GroupOrderModel>> GetAllByProviderId(Guid providerId)
        {
            return _groupOrderRepository.GetAllByProviderIdAsync(providerId);
        }

        public Task<IReadOnlyCollection<GroupOrderModel>> GetAllByLunchGroupId(Guid lunchGroupId)
        {
            return _groupOrderRepository.GetAllByLunchGroupIdAsync(lunchGroupId);
        }

        public Task<GroupOrderModel> GetById(Guid groupOrderId)
        {
            return _groupOrderRepository.GetByIdAsync(groupOrderId);
        }
    }
}
