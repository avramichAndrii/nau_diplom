﻿using System;
using System.Threading.Tasks;

namespace EmailNotifier.Application.EmailNotificationService.Interfaces
{
    public interface ICompanyEmailNotificationService
    {
        Task<string> GetCompanyEmailAsync(Guid id);
    }
}
