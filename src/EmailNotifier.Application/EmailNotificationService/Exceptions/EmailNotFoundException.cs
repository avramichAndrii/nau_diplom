using System;

namespace EmailNotifier.Application.EmailNotificationService.Exceptions
{
    public class EmailNotFoundException : Exception
    {
        private const string EmailNotFoundMessage = "Email is not found";

        public EmailNotFoundException() : base(EmailNotFoundMessage) { }
    }
}