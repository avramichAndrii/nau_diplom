﻿using Company.API.Client;
using Company.API.Contracts.Response;
using EmailNotifier.Application.EmailNotificationService.Exceptions;
using EmailNotifier.Application.EmailNotificationService.Interfaces;
using System;
using System.Threading.Tasks;
using Infrastructure.HttpClientAbstractions.Interfaces;

namespace EmailNotifier.Application.EmailNotificationService.Services
{
    public class CompanyEmailNotificationService : ICompanyEmailNotificationService
    {
        private readonly ICompanyClient _companyClient;

        public CompanyEmailNotificationService(IClientFactory clientFactory)
        {
            _companyClient = clientFactory.GetClient<ICompanyClient>();
        }

        public async Task<string> GetCompanyEmailAsync(Guid id)
        {
            CompanyResponse company= await _companyClient.GetCompanyAsync(id);
            if (string.IsNullOrEmpty(company.CompanyEmail))
            {
                throw new EmailNotFoundException();
            }
            return company.CompanyEmail;
        }
    }
}
