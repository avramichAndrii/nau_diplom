﻿using EmailNotifier.Application.EmailNotificationService.Interfaces;
using EmailNotifier.Application.EmailNotificationService.Services;
using Microsoft.Extensions.DependencyInjection;

namespace EmailNotifier.Application.Extensions
{
    public static class EmailNotificationServiceDIExtension
    {
        public static void RegisterEmailNotificationService(this IServiceCollection collection)
        {
            collection.AddScoped<ICompanyEmailNotificationService, CompanyEmailNotificationService>();
        }
    }
}