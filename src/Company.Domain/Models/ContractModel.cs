﻿using Domain.Core;
using System;

namespace Company.Domain
{
    public class ContractModel: Entity<Guid>
    {
        public string ContractNumber { get; set; }
        public Guid ProviderCompanyId { get; set; }
        public CompanyModel ProviderCompany { get; set; }
        public Guid ConsumerCompanyId { get; set; }
        public CompanyModel ConsumerCompany { get; set; }
        public Guid MenuId { get; set; }
        public ContractStatus ContractStatus { get; set; }

        public ContractModel() : base(Guid.NewGuid()) { }

        public ContractModel(Guid id) : base(id) { }
    }
}
