using Domain.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Company.Domain
{
    public class CompanyModel : Entity<Guid>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public CompanyType TypeOfCompany { get; set; }
        public int OrderingPeriod { get; set; }
        public string CompanyEmail { get; set; }
        [InverseProperty("ProviderCompany")]
        public List<ContractModel> ProviderContracts { get; set; }
        [InverseProperty("ConsumerCompany")]
        public List<ContractModel> ConsumerContracts { get; set; }

        public CompanyModel() : base(Guid.NewGuid()) { }

        public CompanyModel(Guid id) : base(id) { }
    }
}
