using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Company.Domain
{
    public interface ICompanyRepository
    {
        Task<IReadOnlyCollection<CompanyModel>> GetAllAsync();
        Task<CompanyModel> GetByIdAsync(Guid id);
        Task<IReadOnlyCollection<CompanyModel>> GetByCompanyTypeAsync(string type);
        Task<CompanyModel> CreateAsync(CompanyModel companyModel);
        Task<CompanyModel> UpdateAsync(Guid id, CompanyModel companyModel);
        Task<CompanyModel> DeleteAsync(Guid id);
    }
}