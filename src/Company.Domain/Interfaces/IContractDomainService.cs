﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Company.Domain
{
    public interface IContractDomainService
    {
        Task<IReadOnlyCollection<ContractModel>> GetAllContractsAsync();
        Task<ContractModel> GetContractByIdAsync(Guid id);
        Task<IReadOnlyCollection<ContractModel>> GetByCompanyTypeAndIdAsync(Guid companyId, CompanyType companyType);
        Task<IReadOnlyCollection<ContractModel>> GetByStatusAndCompanyTypeAndIdAsync(Guid companyId, CompanyType companyType, ContractStatus contractStatus);
        Task<ContractModel> CreateContractAsync(ContractModel contractModel);
        Task<ContractModel> UpdateContractAsync(Guid id, ContractModel contractModel);
        bool IsContractStatusEqual(ContractModel previousModel, ContractModel newModel);
    }
}
