using System;

namespace Company.Domain.Exceptions
{
    
    public class ContractNotFoundDomainException : Exception
    {
        private const string ContractNotFoundMessage = "Contract with id = {0} is not found";

        public ContractNotFoundDomainException() : base() { }

        public ContractNotFoundDomainException(Guid id) : base(string.Format(ContractNotFoundMessage, id)) { }
    }
}