﻿using System;

namespace Company.Domain.Exceptions
{
    public class CompanyNotFoundDomainException : Exception
    {
        private const string CompanyNotFoundMessage = "Company with id = {0} is not found";

        public CompanyNotFoundDomainException() : base() { }

        public CompanyNotFoundDomainException(Guid id) : base(string.Format(CompanyNotFoundMessage, id)) { }
    }
}
