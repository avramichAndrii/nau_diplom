﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Company.Domain
{
    public class InvalidDataCompanyDomainException: Exception
    {
        public InvalidDataCompanyDomainException(string message) : base(message) { }

        public InvalidDataCompanyDomainException(string message, params object[] args)
            : base(String.Format(CultureInfo.CurrentCulture, message, args)) { }
    }
}
