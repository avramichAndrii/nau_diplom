﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Company.Domain
{
    public class ContractDomainService: IContractDomainService
    {
        private readonly IContractRepository _contractRepository;

        public ContractDomainService(IContractRepository contractRepository)
        {
            _contractRepository = contractRepository;
        }

        public Task<IReadOnlyCollection<ContractModel>> GetAllContractsAsync()
        {
            return _contractRepository.GetAllContractsAsync();
        }

        public Task<ContractModel> GetContractByIdAsync(Guid id)
        {
            return _contractRepository.GetContractByIdAsync(id);
        }

        public Task<IReadOnlyCollection<ContractModel>> GetByCompanyTypeAndIdAsync(Guid companyId, CompanyType companyType)
        {
            return _contractRepository.GetByCompanyTypeAndIdAsync(companyId, companyType);
        }

        public Task<IReadOnlyCollection<ContractModel>> GetByStatusAndCompanyTypeAndIdAsync(Guid companyId, CompanyType companyType, ContractStatus contractStatus)
        {
            return _contractRepository.GetByStatusAndCompanyTypeAndIdAsync(companyId, companyType, contractStatus);
        }

        public Task<ContractModel> CreateContractAsync(ContractModel contractModel)
        {
            return _contractRepository.CreateContractAsync(contractModel);
        }

        public Task<ContractModel> UpdateContractAsync(Guid id, ContractModel contractModel)
        {
            return _contractRepository.UpdateContractAsync(id, contractModel);
        }
        public bool IsContractStatusEqual(ContractModel previousModel, ContractModel newModel)
        {
            return (int) previousModel.ContractStatus == (int) newModel.ContractStatus;
        }
    }
}
