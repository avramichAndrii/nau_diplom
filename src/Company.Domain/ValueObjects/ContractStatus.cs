﻿namespace Company.Domain
{
    public enum ContractStatus
    {
        Proposed = 1,
        Active = 2,
        Changed = 3,
        Cancelled = 4
    }
}
