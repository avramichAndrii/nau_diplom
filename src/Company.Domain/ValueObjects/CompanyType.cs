namespace Company.Domain
{
    public enum CompanyType
    {
        Provider = 1,
        Consumer = 2
    }
}