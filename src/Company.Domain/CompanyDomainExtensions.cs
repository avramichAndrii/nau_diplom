﻿using Microsoft.Extensions.DependencyInjection;

namespace Company.Domain
{
    public static class CompanyDomainExtensions
    {
        public static void RegisterCompanyDomain(this IServiceCollection collection)
        {
            collection.AddScoped<ICompanyDomainServices, CompanyDomainService>();
            collection.AddScoped<IContractDomainService, ContractDomainService>();
        }
    }
}
