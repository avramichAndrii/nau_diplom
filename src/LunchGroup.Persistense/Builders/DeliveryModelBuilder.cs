﻿using LunchGroup.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace LunchGroup.Persistense.Mapping
{
    class DeliveryModelBuilder : IEntityTypeConfiguration<DeliveryModel>
    {
        private const string TableName = "Deliveries";
        public void Configure(EntityTypeBuilder<DeliveryModel> builder)
        {
            builder.ToTable(TableName);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Status).IsRequired().HasConversion(new EnumToStringConverter<StatusModel>());
            builder.Property(e => e.DeliveryTime);
            builder.Property(e => e.GroupOrderId).IsRequired();
        }
    }

}
