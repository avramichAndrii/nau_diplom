﻿using LunchGroup.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LunchGroup.Persistense.Mapping
{
    class LunchGroupMembersBuilder : IEntityTypeConfiguration<LunchGroupMember>
    {
        private const string TableName = "GroupMembers";
        public void Configure(EntityTypeBuilder<LunchGroupMember> builder)
        {
            builder.ToTable(TableName);
            builder.HasKey(gm => new { gm.LunchGroupModelId, gm.MemberId });
        }
    }
}
