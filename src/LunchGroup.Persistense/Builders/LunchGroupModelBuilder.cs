﻿using LunchGroup.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LunchGroup.Persistense.Mapping
{
    class LunchGroupBuilder : IEntityTypeConfiguration<LunchGroupModel>
    {
        private const string TableName = "LunchGroups";
        private const int NameMaxLength = 256;
        private const int AddressMaxLength = 1024;
        public void Configure(EntityTypeBuilder<LunchGroupModel> builder)
        {
            builder.ToTable(TableName);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name)
                .HasMaxLength(NameMaxLength)
                .IsRequired();

            builder.Property(e => e.Weekends)
                .HasConversion(
                    days => string.Join(",", days),
                    s => StringToDayOfWeekArray(s))
                .HasMaxLength(NameMaxLength)
                .IsRequired();

            builder.Property(e => e.Address)
                .HasConversion(
                    addr => JsonConvert.SerializeObject(addr),
                    s => JsonConvert.DeserializeObject<AddressModel>(s))
                .HasMaxLength(AddressMaxLength)
                .IsRequired();
        }

        private List<DayOfWeek> StringToDayOfWeekArray(string input)
        {
            return !string.IsNullOrEmpty(input)
                ? input.Split(",").Select((x) => Enum.Parse(typeof(DayOfWeek), x)).Cast<DayOfWeek>().ToList()
                : new List<DayOfWeek>();
        }
    }
}
