﻿using LunchGroup.Domain.Repositories;
using LunchGroup.Persistense.Contexts;
using LunchGroup.Persistense.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace LunchGroup.Persistense.ContextDIExtension
{
    public static class LunchGroupContextDIExtension
    {
        private const string DBConnectionString = nameof(DBConnectionString);
        public static void RegisterLunchGroupContext(this IServiceCollection services, IConfiguration configuration)
        {
            var connection = configuration.GetConnectionString(DBConnectionString);
            services.AddDbContext<LunchGroupContext>(options => options.UseMySql(connection));
            services.AddScoped<ILunchGroupRepository, LunchGroupRepository>();
            services.AddScoped<IDeliveryRepository, DeliveryRepository>();
        }
    }
}
