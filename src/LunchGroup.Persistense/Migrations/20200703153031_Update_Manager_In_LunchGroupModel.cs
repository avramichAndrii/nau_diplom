﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LunchGroup.Persistense.Migrations
{
    public partial class Update_Manager_In_LunchGroupModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GroupMembers_LunchGroups_LunchGroupId",
                table: "GroupMembers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GroupMembers",
                table: "GroupMembers");

            migrationBuilder.DropColumn(
                name: "LunchGroupId",
                table: "GroupMembers");

            migrationBuilder.AlterColumn<string>(
                name: "Address",
                table: "LunchGroups",
                maxLength: 1024,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(512) CHARACTER SET utf8mb4",
                oldMaxLength: 1024);

            migrationBuilder.AddColumn<Guid>(
                name: "ManagerId",
                table: "LunchGroups",
                nullable: false);

            migrationBuilder.AddColumn<Guid>(
                name: "LunchGroupModelId",
                table: "GroupMembers",
                nullable: false);

            migrationBuilder.AddPrimaryKey(
                name: "PK_GroupMembers",
                table: "GroupMembers",
                columns: new[] { "LunchGroupModelId", "MemberId" });

            migrationBuilder.AddForeignKey(
                name: "FK_GroupMembers_LunchGroups_LunchGroupModelId",
                table: "GroupMembers",
                column: "LunchGroupModelId",
                principalTable: "LunchGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GroupMembers_LunchGroups_LunchGroupModelId",
                table: "GroupMembers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GroupMembers",
                table: "GroupMembers");

            migrationBuilder.DropColumn(
                name: "ManagerId",
                table: "LunchGroups");

            migrationBuilder.DropColumn(
                name: "LunchGroupModelId",
                table: "GroupMembers");

            migrationBuilder.AlterColumn<string>(
                name: "Address",
                table: "LunchGroups",
                type: "varchar(512) CHARACTER SET utf8mb4",
                maxLength: 1024,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 1024);

            migrationBuilder.AddColumn<Guid>(
                name: "LunchGroupId",
                table: "GroupMembers",
                type: "char(36)",
                nullable: false);

            migrationBuilder.AddPrimaryKey(
                name: "PK_GroupMembers",
                table: "GroupMembers",
                columns: new[] { "LunchGroupId", "MemberId" });

            migrationBuilder.AddForeignKey(
                name: "FK_GroupMembers_LunchGroups_LunchGroupId",
                table: "GroupMembers",
                column: "LunchGroupId",
                principalTable: "LunchGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
