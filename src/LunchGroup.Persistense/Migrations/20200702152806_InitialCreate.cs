﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LunchGroup.Persistense.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LunchGroups",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    SubmitDeadline = table.Column<DateTime>(nullable: false),
                    PeriodStart = table.Column<DateTime>(nullable: false),
                    Periodicity = table.Column<int>(nullable: false),
                    Weekends = table.Column<string>(maxLength: 256, nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: false),
                    CompanyId = table.Column<Guid>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    Address = table.Column<string>(maxLength: 512, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LunchGroups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Deliveries",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    GroupOrderId = table.Column<Guid>(nullable: false),
                    DeliveryTime = table.Column<DateTime>(nullable: true),
                    Status = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Deliveries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GroupMembers",
                columns: table => new
                {
                    LunchGroupId = table.Column<Guid>(nullable: false),
                    MemberId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupMembers", x => new { x.LunchGroupId, x.MemberId });
                    table.ForeignKey(
                        name: "FK_GroupMembers_LunchGroups_LunchGroupId",
                        column: x => x.LunchGroupId,
                        principalTable: "LunchGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Deliveries");

            migrationBuilder.DropTable(
                name: "GroupMembers");

            migrationBuilder.DropTable(
                name: "LunchGroups");
        }
    }
}
