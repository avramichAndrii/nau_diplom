﻿using LunchGroup.Domain.Models;
using LunchGroup.Persistense.Mapping;
using Microsoft.EntityFrameworkCore;

namespace LunchGroup.Persistense.Contexts
{
    public class LunchGroupContext : DbContext
    {
        public DbSet<LunchGroupModel> LunchGroups { get; set; }
        public DbSet<DeliveryModel> Deliveries { get; set; }
        public DbSet<LunchGroupMember> LunchGroupMembers { get; set; }

        public LunchGroupContext(DbContextOptions<LunchGroupContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new LunchGroupMembersBuilder());
            modelBuilder.ApplyConfiguration(new LunchGroupBuilder());
            modelBuilder.ApplyConfiguration(new DeliveryModelBuilder());

            base.OnModelCreating(modelBuilder);
        }
    }
}
