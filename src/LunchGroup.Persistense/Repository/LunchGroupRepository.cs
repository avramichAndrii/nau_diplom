﻿using LunchGroup.Domain.Exceptions;
using LunchGroup.Domain.Models;
using LunchGroup.Domain.Repositories;
using LunchGroup.Persistense.Contexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LunchGroup.Persistense.Repository
{
    class LunchGroupRepository : ILunchGroupRepository
    {
        private readonly LunchGroupContext _lunchGroupContext;
        private const string MembersDuplicatesExceptionMessage = "You have member duplicates";

        public LunchGroupRepository(LunchGroupContext lunchGroupContext)
        {
            _lunchGroupContext = lunchGroupContext;
        }

        public async Task<IReadOnlyCollection<LunchGroupModel>> GetAllByCompanyAsync(Guid companyId)
        {
            try
            {
                var models = await _lunchGroupContext.LunchGroups
                    .Include(lg => lg.GroupMembers)
                    .Where(lg => lg.CompanyId == companyId && lg.IsActive).ToListAsync();
                return models;
            }
            catch (InvalidOperationException ex)
            {
                throw new LunchGroupDomainException(ex.Message);
            }
        }

        public async Task SetActiveFlagAsync(Guid groupId, bool isActive)
        {
            var lunchGroupModel = await _lunchGroupContext.LunchGroups
                .Include(lg => lg.GroupMembers)
                .SingleOrDefaultAsync(lg => lg.Id == groupId);
            if (lunchGroupModel == null)
            {
                throw new LunchGroupNotFoundException(groupId);
            }
            if (lunchGroupModel.IsActive != isActive)
            {
                lunchGroupModel.IsActive = isActive;
                if (!isActive)
                {
                    _lunchGroupContext.LunchGroupMembers.RemoveRange(lunchGroupModel.GroupMembers);
                }
                await _lunchGroupContext.SaveChangesAsync();
            }
        }

        public async Task<LunchGroupModel> GetByIdAsync(Guid groupId)
        {
            var lunchGroupModel = await _lunchGroupContext.LunchGroups
                .Include(lg => lg.GroupMembers)
                .SingleOrDefaultAsync(lg => lg.Id == groupId);
            if (lunchGroupModel == null)
            {
                throw new LunchGroupNotFoundException(groupId);
            }
            return lunchGroupModel;
        }

        public async Task UpdateTimetableAsync(Guid groupId)
        {
            var lunchGroupModel = await _lunchGroupContext.LunchGroups.SingleOrDefaultAsync(lg => lg.Id == groupId);
            if (lunchGroupModel == null)
            {
                throw new LunchGroupNotFoundException(groupId);
            }
            lunchGroupModel.UpdateTimetable();
            await _lunchGroupContext.SaveChangesAsync();
        }

        public async Task<IReadOnlyCollection<Guid>> GetAllMembersByGroupIdAsync(Guid groupId)
        {
            var pairs = await _lunchGroupContext.LunchGroupMembers.Where(pair => pair.LunchGroupModelId == groupId).ToListAsync();
            
            return pairs.Select(pair => pair.MemberId).ToList();
        }

        /// <summary>
        /// Create a database record for given model
        /// It`s have checks for
        ///  - members duplicates
        ///  - members that already have group
        /// </summary>
        /// <param name="modelToCreate"></param>
        /// <returns>Created model</returns>
        public async Task<LunchGroupModel> CreateAsync(LunchGroupModel modelToCreate)
        {
            try
            {
                List<Guid> members = modelToCreate.GroupMembers.Select(member => member.MemberId).ToList();
                HashSet<Guid> membersSet = members.ToHashSet();
                if(members.Count != membersSet.Count)
                {
                    throw new LunchGroupDomainException(MembersDuplicatesExceptionMessage);
                }

                foreach(Guid member in members)
                {
                    var groups = await GetAllByPersonIdAsync(member);
                    //this will not cause an error that the user is being used because if at least one is found, there will be an domain error
                    if (groups.Count > 0)
                    {
                        throw new UserHaveGroupException(groups.ElementAt(0).Name);
                    }
                }

                await _lunchGroupContext.LunchGroups.AddAsync(modelToCreate);
                await _lunchGroupContext.SaveChangesAsync();

                return modelToCreate;
            }
            catch (InvalidOperationException ex)
            {
                throw new LunchGroupDomainException(ex.Message);
            }
        }

        public async Task<IReadOnlyCollection<LunchGroupModel>> GetAllByPersonIdAsync(Guid personId)
        {
            try
            {
                List<LunchGroupMember> pairs = await _lunchGroupContext.LunchGroupMembers.Where(pairs => pairs.MemberId == personId).ToListAsync();
                HashSet<Guid> groups = pairs.Select(pair => pair.LunchGroupModelId).ToHashSet();
                List<LunchGroupModel> groupsForReturn = groups.Select(group => GetByIdAsync(group).Result).ToList();
                return groupsForReturn;
            }
            catch (InvalidOperationException ex)
            {
                throw new LunchGroupDomainException(ex.Message);
            }
        }

        public async Task<IReadOnlyCollection<LunchGroupModel>> GetAllByManagerIdAsync(Guid personId)
        {
            try
            {
                return await _lunchGroupContext.LunchGroups
                    .Include(group => group.GroupMembers)
                    .Where(group => group.ManagerId == personId && group.IsActive).ToListAsync();
            }
            catch (InvalidOperationException ex)
            {
                throw new LunchGroupDomainException(ex.Message);
            }
        }

        public async Task<LunchGroupModel> UpdateAsync(Guid groupId, LunchGroupModel newlunchGroup)
        {

            try
            {
                foreach(LunchGroupMember member in newlunchGroup.GroupMembers)
                {
                    member.LunchGroupModelId = groupId;
                }
                LunchGroupModel original = _lunchGroupContext
                                                .LunchGroups
                                                .Find(groupId);


                List<Guid> newPersons = newlunchGroup.GroupMembers.Select(person => person.MemberId).ToList();

                foreach (Guid member in newPersons)
                {
                    var groups = await GetAllByPersonIdAsync(member);
                    //this will not cause an error that the user is being used because if at least one is found, there will be an domain error
                    if (groups.Count > 0 && groups.ElementAt(0).Id != groupId)
                    {
                        throw new UserHaveGroupException(groups.ElementAt(0).Name);
                    }
                }

                _lunchGroupContext.LunchGroupMembers
                    .RemoveRange(_lunchGroupContext
                                    .LunchGroupMembers
                                    .Where(
                                        person =>
                                        person.LunchGroupModelId == groupId 
                                        ));

                if (original != null)
                {
                    original.Name = newlunchGroup.Name;
                    original.PeriodStart = newlunchGroup.PeriodStart;
                    original.SubmitDeadline = newlunchGroup.SubmitDeadline;
                    original.Weekends = newlunchGroup.Weekends;
                    original.Address = newlunchGroup.Address;
                    original.GroupMembers = newlunchGroup.GroupMembers;

                    await _lunchGroupContext.SaveChangesAsync();
                    return original;
                }
                throw new LunchGroupNotFoundException(groupId);
            }
            catch (InvalidOperationException ex)
            {
                throw new LunchGroupDomainException(ex.Message);
            }
        }
    }
}
