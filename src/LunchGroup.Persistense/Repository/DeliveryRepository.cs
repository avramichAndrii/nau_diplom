﻿using LunchGroup.Domain.Exceptions;
using LunchGroup.Domain.Models;
using LunchGroup.Domain.Repositories;
using LunchGroup.Persistense.Contexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LunchGroup.Persistense.Repository
{
    public class DeliveryRepository : IDeliveryRepository
    {
        private readonly LunchGroupContext _lunchGroupContext;
        public DeliveryRepository(LunchGroupContext lunchGroupContext)
        {
            _lunchGroupContext = lunchGroupContext;
        }

        public async Task<IReadOnlyCollection<DeliveryModel>> GetAllDeliveriesAsync()
        {
            try
            {
                return await _lunchGroupContext.Deliveries.ToListAsync();
            }
            catch (InvalidOperationException ex)
            {
                throw new LunchGroupDomainException(ex.Message);
            }

        }

        public async Task<DeliveryModel> GetDeliveryByIdAsync(Guid deliveryId)
        {
            var deliveryModel = await _lunchGroupContext.Deliveries.SingleOrDefaultAsync(d => d.Id == deliveryId);
            if (deliveryModel == null)
            {
                throw new DeliveryNotFoundException(deliveryId);
            }
            return deliveryModel;
        }

        public async Task<DeliveryModel> GetDeliveryByGroupOrderIdAsync(Guid groupOrderId)
        {
            var deliveryModel = await _lunchGroupContext.Deliveries.SingleOrDefaultAsync(d => d.GroupOrderId == groupOrderId);
            if (deliveryModel == null)
            {
                throw new DeliveryNotFoundException(groupOrderId);
            }
            return deliveryModel;
        }


        public async Task<DeliveryModel> CreateAsync(Guid groupOrderId)
        {
            try
            {
                DeliveryModel modelToCreate = new DeliveryModel(Guid.NewGuid(), groupOrderId, null, StatusModel.Created);
                await _lunchGroupContext.Deliveries.AddAsync(modelToCreate);
                await _lunchGroupContext.SaveChangesAsync();
                return modelToCreate;
            }
            catch (InvalidOperationException ex)
            {
                throw new LunchGroupDomainException(ex.Message);
            }
        }
        public async Task<DeliveryModel> UpdateDeliveryAsync(Guid deliveryId, DeliveryModel deliveryModel)
        {
            DeliveryModel modelToUpdate = await _lunchGroupContext.Deliveries.SingleOrDefaultAsync(d => d.Id == deliveryId);
            if (modelToUpdate == null)
            {
                throw new DeliveryNotFoundException(deliveryId);
            }
            try
            {
                modelToUpdate.DeliveryTime = deliveryModel.DeliveryTime;
                modelToUpdate.Status = deliveryModel.Status;
                _lunchGroupContext.Deliveries.Update(modelToUpdate);
                _lunchGroupContext.SaveChanges();
                return modelToUpdate;
            }
            catch (InvalidOperationException ex)
            {
                throw new LunchGroupDomainException(ex.Message);
            }
        }

        public async Task<DeliveryModel> DeleteAsync(Guid deliveryId)
        {
            DeliveryModel modelToRemove = await _lunchGroupContext.Deliveries.SingleOrDefaultAsync(d => d.Id == deliveryId);
            if (modelToRemove == null)
            {
                throw new DeliveryNotFoundException(deliveryId);
            }
            try
            {
                _lunchGroupContext.Deliveries.Remove(modelToRemove);
                await _lunchGroupContext.SaveChangesAsync();
                return modelToRemove;
            }
            catch (InvalidOperationException ex)
            {
                throw new LunchGroupDomainException(ex.Message);
            }

        }

    }
}
