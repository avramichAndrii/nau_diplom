using System;
using System.Threading.Tasks;
using UserManagement.API.Contracts.Requests;
using UserManagement.API.Contracts.Responses;
using Infrastructure.HttpClientAbstractions.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace UserManagement.API.Client.ClientInterface
{
    
    public interface IUserManagementClient : IClient
    {
        public Task<AuthenticateResponse> AuthenticateAsync(AuthenticateRequest model);
        public Task<UsersCollectionResponse> GetAllAsync();
        public Task<UsersCollectionResponse> GetAllByCompanyAndRoleAsync(Guid companyId, string role);
        public Task<UserResponse> RegisterAsync(RegisterRequest model);
        public Task<UserResponse> GetByIdAsync(Guid id);
        public Task<UserResponse> UpdateByIdAsync(Guid id, UpdateRequest model);
        public Task<Guid> DeleteByIdAsync(Guid id);
        public Task SetCompanyByUserIdAsync(Guid userId, Guid companyId);
        public Task<UserResponse> UpdatePasswordAsync(Guid id, UpdatePasswordRequest model);
        public Task<AuthenticateResponse> RefreshTokenAsync(Guid token);
        public Task<HandleForgottenPasswordResponse> HandleForgottenPassword(ForgotPasswordRequest forgotPasswordRequest);
        public Task<UserResponse> ResetPasswordAsync(ResetPasswordRequest request);
    }
}
