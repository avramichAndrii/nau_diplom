using System;
using System.Net.Http;
using System.Threading.Tasks;
using Infrastructure.HttpClientAbstractions;
using Microsoft.AspNetCore.Mvc;
using UserManagement.API.Client.ClientInterface;
using UserManagement.API.Contracts.Requests;
using UserManagement.API.Contracts.Responses;
using UserManagement.API.Contracts.Routs;

namespace UserManagement.API.Client.Client
{
    
    public class UserManagementClient : BaseHttpClient, IUserManagementClient
    {
        public UserManagementClient(HttpClient client): base(client) { }

        public Task<AuthenticateResponse> AuthenticateAsync(AuthenticateRequest model)
        {
            return PostAsync<AuthenticateRequest, AuthenticateResponse>(new Uri(BaseUri + UserManagementRouting.AuthenticateUri), model);
        }

        public Task<UsersCollectionResponse> GetAllAsync()
        {
            return GetAsync<UsersCollectionResponse>(new Uri(BaseUri + UserManagementRouting.UsersUri));
        }

        public async Task<UsersCollectionResponse> GetAllByCompanyAndRoleAsync(Guid companyId, string role)
        {
            return await GetAsync<UsersCollectionResponse>(
                new Uri(BaseUri + UserManagementRouting.UsersUri + "/" + 
                        UserManagementRouting.CompanyUri + "/" + companyId + "/" + UserManagementRouting.RoleUri + "/" + role));
        }

        public Task<UserResponse> RegisterAsync(RegisterRequest model)
        {
            return PostAsync<RegisterRequest, UserResponse>(new Uri(BaseUri + UserManagementRouting.RegisterUri), model);
        }

        public Task<UserResponse> GetByIdAsync(Guid id)
        {
            return GetAsync<UserResponse>(new Uri(BaseUri + UserManagementRouting.BaseControllerUri + id.ToString())); ;
        }

        public Task<UserResponse> UpdateByIdAsync(Guid id,UpdateRequest model)
        { 
            return PutAsync<UpdateRequest, UserResponse>(new Uri(BaseUri + UserManagementRouting.BaseControllerUri + id.ToString()), model); ;
        }

        public Task<Guid> DeleteByIdAsync(Guid id)
        {
            return DeleteAsync<Guid>(new Uri(BaseUri + UserManagementRouting.BaseControllerUri + id.ToString()));
        }

        public Task SetCompanyByUserIdAsync(Guid userId, Guid companyId)
        {
            var request = new SetCompanyRequest { CompanyId = companyId };
            return PatchAsync(new Uri(BaseUri + UserManagementRouting.BaseControllerUri + userId.ToString()), request);
        }

        public Task<UserResponse> UpdatePasswordAsync(Guid id, UpdatePasswordRequest model)
        {
            return PutAsync<UpdatePasswordRequest, UserResponse>(new Uri(BaseUri + UserManagementRouting.BaseControllerUri + UserManagementRouting.ChangePasswordUri + id.ToString()), model);
        }

        public Task<AuthenticateResponse> RefreshTokenAsync(Guid token)
        {
            return PostAsync<Guid, AuthenticateResponse>(new Uri(BaseUri + UserManagementRouting.RefreshUri + token.ToString()), token);
        }

        public async Task<HandleForgottenPasswordResponse> HandleForgottenPassword(ForgotPasswordRequest forgotPasswordRequest)
        {
            return await PostAsync<ForgotPasswordRequest, HandleForgottenPasswordResponse>(new Uri(BaseUri + UserManagementRouting.BaseControllerUri + UserManagementRouting.HandleForgottenPasswordUri),
                forgotPasswordRequest);
        }

        public Task<UserResponse> ResetPasswordAsync(ResetPasswordRequest request)
        {
            return PostAsync<ResetPasswordRequest, UserResponse>(new Uri(BaseUri + UserManagementRouting.BaseControllerUri + UserManagementRouting.ResetPasswordUri), request);
        }
    }
}
