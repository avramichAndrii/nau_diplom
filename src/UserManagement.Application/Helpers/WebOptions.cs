﻿namespace UserManagement.Application.Helpers
{
    public class WebOptions
    {
        public string FrontendAppUrl { get; set; }
    }
}
