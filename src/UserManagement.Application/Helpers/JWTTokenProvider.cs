﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using UserManagement.Domain.Models;

namespace UserManagement.Application.Helpers
{
    public static class JWTTokenProvider
    {
        
        public static string GenerateAccessToken(AuthenticationSettings authSettings, User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(authSettings.Secret);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, user.Role.ToString())
                }),
                Issuer = authSettings.Issuer,
                Audience = authSettings.Audience,
                Expires = DateTime.UtcNow.AddMinutes(Int32.Parse(authSettings.JwtLifetime)),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public static string ValidateResetToken(PasswordResetSettings resetSettings, string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(resetSettings.Secret);
                var s = tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    RequireExpirationTime = true,
                    RequireSignedTokens = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuer = true,
                    ValidIssuer = resetSettings.Issuer,
                    ValidAudience = resetSettings.Audience
                }, out SecurityToken validatedToken);

                //returning id of user
                return s.Identity.Name;
            }
            catch(Exception)
            {
                throw new UserDomainException("Wrong JWT token");
            }
        }
        public static string GenerateResetToken(PasswordResetSettings resetSettings, User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(resetSettings.Secret);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Issuer = resetSettings.Issuer,
                Audience = resetSettings.Audience,
                Expires = DateTime.UtcNow.AddMinutes(Int32.Parse(resetSettings.JwtLifetime)),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public static RefreshToken GenerateRefreshToken(Guid userId, RefreshTokenSettings refreshTokenSettings)
        {
            return new RefreshToken
            {
                Token = Guid.NewGuid(),
                ExpiresAt = DateTime.UtcNow.AddDays(Int32.Parse(refreshTokenSettings.Lifetime)),
                UserId = userId
            };
        }
    }
}
