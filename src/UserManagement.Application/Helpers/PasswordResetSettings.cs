﻿namespace UserManagement.Application.Helpers
{
    public class PasswordResetSettings
    {
        public string Secret { get; set; }
        public string JwtLifetime { get; set; }
        public string Audience { get; set; }
        public string Issuer { get; set; }
    }
}
