﻿namespace UserManagement.Application.Helpers
{
    public class RefreshTokenSettings
    {
        public string Lifetime { get; set; }
    }
}