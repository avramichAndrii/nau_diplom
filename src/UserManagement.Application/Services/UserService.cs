﻿using EmailNotifier.EmailNotification;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UserManagement.API.Contracts.Requests;
using UserManagement.Application.Helpers;
using UserManagement.Domain.Models;
using UserManagement.Domain.Repositories;
using UserManagement.Domain.Services;

namespace UserManagement.Application.Services
{
    public class UserService : IUserService
    {
        private readonly AuthenticationSettings _authSettings;
        private readonly PasswordResetSettings _passwordResetSettings;
        private readonly RefreshTokenSettings _refreshTokenSettings;

        private readonly WebOptions _webOptions;

        private readonly IUserRepository _userRepository;
        private readonly IMailSender _mailSender;
        private const string _mailSubject = "Reset password at LunchMe application";
        public UserService(
            IUserRepository userRepository, 
            IMailSender mailSender, 
            IOptions<AuthenticationSettings> authSettings, 
            IOptions<PasswordResetSettings> passwordResetSettings, 
            IOptions<WebOptions> webOptions,
            IOptions<RefreshTokenSettings> refreshTokenSettings)
        {
            _authSettings = authSettings.Value;
            _passwordResetSettings = passwordResetSettings.Value;
            _mailSender = mailSender;
            _refreshTokenSettings = refreshTokenSettings.Value;
            _userRepository = userRepository;
            _webOptions = webOptions.Value;
        }

        public async Task<AuthenticateModel> AuthenticateAsync(string login, string password)
        {            
            var user = await _userRepository.GetByLoginAsync(login);

            if (user == null)
            {
                return null;
            }
            try
            {
                if (!Cryptography.VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
                {
                    return null;
                }

                var refreshToken = JWTTokenProvider.GenerateRefreshToken(user.Id, _refreshTokenSettings);
                await _userRepository.CreateRefreshTokenAsync(refreshToken);
                return new AuthenticateModel 
                { 
                    AccessToken = JWTTokenProvider.GenerateAccessToken(_authSettings, user), 
                    RefreshToken = refreshToken.Token,
                    ExpiresAt = refreshToken.ExpiresAt
                };
            }
            catch (ArgumentNullException anex)
            {
                throw new UserDomainException(anex.Message);
            }
            catch (ArgumentException aex)
            {
                throw new UserDomainException(aex.Message);
            }
        }

        public Task<IReadOnlyCollection<User>> GetAllAsync()
        {
            return _userRepository.GetAllAsync();
        }

        public Task<IReadOnlyCollection<User>> GetAllByCompanyAndRoleAsync(Guid companyId, string role)
        {
            return _userRepository.GetAllByCompanyAndRoleAsync(companyId, role);
        }

        public Task<User> GetByIdAsync(Guid id)
        {
            return _userRepository.GetByIdAsync(id);
        }

        public Task<User> CreateAsync(User user, string password)
        {
            Task<User> userByLogin = _userRepository.GetByLoginAsync(user.Login);
            if (userByLogin.GetAwaiter().GetResult() != null)
            {
                throw new UserDomainException("Login " + user.Login + " is already taken");
            }
                   
            byte[] passwordHash, passwordSalt;

            try
            {
                Cryptography.CreatePasswordHash(password, out passwordHash, out passwordSalt);
                user.SetPasswordItems(passwordHash, passwordSalt);
            }
            catch(ArgumentNullException anEx)
            {
                throw new UserDomainException(anEx.Message);
            }
            return  _userRepository.CreateAsync(user);
        }

        public Task<User> UpdateAsync(User user)
        {
            try
            {
                return _userRepository.UpdateAsync(user);
            }
            catch (ArgumentNullException anEx)
            {
                throw new UserDomainException(anEx.Message);
            }
        }

        public Task<User> DeleteAsync(Guid id)
        {
            return _userRepository.DeleteAsync(id);
        }

        public Task SetCompanyAsync(Guid userId, Guid companyId)
        {
            return _userRepository.SetCompanyAsync(userId, companyId);
        }
        public async Task<string> HandleForgottenPassword(string email)
        {
            var account = await _userRepository.GetByEmailAsync(email);

            if (account == null)
            {
                throw new UserDomainException();
            }

            var resetToken = JWTTokenProvider.GenerateResetToken(_passwordResetSettings, account);
            var resetUrl = $"{_webOptions.FrontendAppUrl}reset-password/{resetToken}";
            // send email
            await _mailSender.SendEmailAsync(email, _mailSubject, resetUrl);
            return email;
        }

        public async Task<User> ResetPassword(string token, string password)
        {
            var id = JWTTokenProvider.ValidateResetToken(_passwordResetSettings, token);
            try
            {
                var account = await _userRepository.GetByIdAsync(Guid.Parse(id));
                if (account == null)
                {
                    throw new UserDomainException("No user with such id");
                }
                return await SetPasswordAsync(account, password);
            }
            catch (Exception ex)
            {
                throw new UserDomainException(ex.Message);
            }
        }

        public async Task<User> UpdatePasswordAsync(string oldPassword, string newPassword, Guid id)
        {
            try
            {
                var account = await _userRepository.GetByIdAsync(id);
                if (account == null)
                {
                    throw new UserDomainException();
                }
                if (Cryptography.VerifyPasswordHash(oldPassword, account.PasswordHash, account.PasswordSalt))
                {

                    return await SetPasswordAsync(account, newPassword);
                }
                throw new Exception ();
            }
            catch (Exception ex)
            {
                throw new UserDomainException(ex.Message);
            }
        }

        public Task<User> SetPasswordAsync(User user, string password)
        {
            try
            {
                Cryptography.CreatePasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt);
                user.SetPasswordItems(passwordHash, passwordSalt);

                return _userRepository.UpdatePasswordAsync(user);
            }
            catch (ArgumentNullException anEx)
            {
                throw new UserDomainException(anEx.Message);
            }
        }

        public async Task<AuthenticateModel> RefreshTokenAsync(Guid token)
        {
            var refreshToken = await _userRepository.GetRefreshTokenAsync(token);

            if (refreshToken == null)
            {
                throw new UserDomainException("Wrong refresh token");
            }
            // replace old refresh token with a new one
            var newRefreshToken = JWTTokenProvider.GenerateRefreshToken(refreshToken.UserId, _refreshTokenSettings);

            await _userRepository.CreateRefreshTokenAsync(newRefreshToken);

            var user = await _userRepository.GetByIdAsync(refreshToken.UserId);

            return new AuthenticateModel
            {
                AccessToken = JWTTokenProvider.GenerateAccessToken(_authSettings, user),
                RefreshToken = newRefreshToken.Token,
                ExpiresAt = newRefreshToken.ExpiresAt
            };
        }
    }
}
