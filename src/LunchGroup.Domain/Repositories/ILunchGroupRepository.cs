﻿using LunchGroup.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LunchGroup.Domain.Repositories
{
    public interface ILunchGroupRepository
    {
        Task<IReadOnlyCollection<LunchGroupModel>> GetAllByCompanyAsync(Guid companyId);
        Task SetActiveFlagAsync(Guid groupId, bool isActive);
        Task<LunchGroupModel> GetByIdAsync(Guid groupId);
        Task UpdateTimetableAsync(Guid groupId);
        Task<IReadOnlyCollection<LunchGroupModel>> GetAllByPersonIdAsync(Guid personId);
        Task<IReadOnlyCollection<LunchGroupModel>> GetAllByManagerIdAsync(Guid personId);
        Task<LunchGroupModel> CreateAsync(LunchGroupModel modelToCreate);
        Task<LunchGroupModel> UpdateAsync(Guid groupId, LunchGroupModel lunchGroup);
        Task<IReadOnlyCollection<Guid>> GetAllMembersByGroupIdAsync(Guid groupId);
    }
}
