﻿namespace LunchGroup.Domain.Models
{
    public enum StatusModel
    {
        Created,
        Declined,
        Delivering,
        Submitted,
        Done
    }

}
