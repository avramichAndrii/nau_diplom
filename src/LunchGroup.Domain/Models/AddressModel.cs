﻿namespace LunchGroup.Domain.Models
{
    public class AddressModel
    {
        public string City { get; set; }
        public string Street { get; set; }
        public string BuildingNumber { get; set; }
        public string Office { get; set; }
    }
}
