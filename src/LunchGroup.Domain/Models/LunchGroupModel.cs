﻿using Domain.Core;
using System;
using System.Collections.Generic;

namespace LunchGroup.Domain.Models
{
    public class LunchGroupModel : Entity<Guid>
    {
        public LunchGroupModel() : base() { }
        public LunchGroupModel(Guid id) : base(id) { }

        public DateTime SubmitDeadline { get; set; }
        public DateTime PeriodStart { get; set; }
        public int Periodicity { get; set; }
        public List<DayOfWeek> Weekends { get; set; }
        public string Name { get; set; }
        public Guid CompanyId { get; set; }
        public bool IsActive { get; set; }
        public Guid ManagerId { get; set; }
        public List<LunchGroupMember> GroupMembers { get; set; }
        public AddressModel Address { get; set; }
        
        /// <summary>
        /// Finds all workdays from PeriodStart and further Periodicity days period.
        /// </summary>
        /// <returns>Collection of workdays.</returns>
        public IReadOnlyCollection<DateTime> GetAvailableDates()
        {
            DateTime currentDate = PeriodStart;
            DateTime periodEnd = PeriodStart.AddDays(Periodicity);
            var availableDates = new List<DateTime>();
            do
            {
                if (!Weekends.Contains(currentDate.DayOfWeek))
                {
                    availableDates.Add(currentDate);
                }
                currentDate = currentDate.AddDays(1);
            }
            while (currentDate < periodEnd || availableDates.Count == 0);
            return availableDates;
        }

        /// <summary>
        /// Sets new deadline and start-of-next-period days (weekends are excluded!).
        /// </summary>
        public void UpdateTimetable()
        {
            DateTime newSubmitDeadline = SubmitDeadline.AddDays(Periodicity);
            DateTime newPeriodStart = PeriodStart.AddDays(Periodicity);

            while (Weekends.Contains(newSubmitDeadline.DayOfWeek))
            {
                newSubmitDeadline = newSubmitDeadline.AddDays(1);
            }
            while (Weekends.Contains(newPeriodStart.DayOfWeek))
            {
                newPeriodStart = newPeriodStart.AddDays(1);
            }
            SubmitDeadline = newSubmitDeadline;
            PeriodStart = newPeriodStart;
        }
    }
}
