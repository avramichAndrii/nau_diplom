﻿using Domain.Core;
using System;

namespace LunchGroup.Domain.Models
{
    public class DeliveryModel : Entity<Guid>
    {
        public Guid GroupOrderId { get; set; }
        public DateTime? DeliveryTime { get; set; }
        public StatusModel Status { get; set; }

        public DeliveryModel() : base() { }
        public DeliveryModel(Guid id, Guid groupOrderId, DateTime? deliveryTime, StatusModel status) : base()
        {
            Id = id;
            GroupOrderId = groupOrderId;
            DeliveryTime = deliveryTime;
            Status = status;
        }
    }
}
