﻿using System;

namespace LunchGroup.Domain.Models
{
    public class LunchGroupMember
    {
        public Guid LunchGroupModelId { get; set; }
        public Guid MemberId { get; set; }
    }
}
