﻿using LunchGroup.Domain.Services;
using Microsoft.Extensions.DependencyInjection;

namespace LunchGroup.Domain.Extensions
{
    public static class LunchGroupDomainExtensions
    {
        public static void RegisterLunchGroupDomain(this IServiceCollection collection)
        {
            collection.AddScoped<ILunchGroupDomainService, LunchGroupDomainService>();
            collection.AddScoped<IDeliveryDomainService, DeliveryDomainService>();
        }
    }
}
