﻿using System;

namespace LunchGroup.Domain.Exceptions
{
    public class LunchGroupNotFoundException : LunchGroupDomainException
    {
        public const string NotFoundMessage = "LunchGroup with id = {0} is not found.";

        public LunchGroupNotFoundException() : base() { }

        public LunchGroupNotFoundException(Guid id) : base(string.Format(NotFoundMessage, id)) { }
    }
}
