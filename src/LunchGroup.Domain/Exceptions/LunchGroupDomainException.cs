﻿using System;
using System.Globalization;

namespace LunchGroup.Domain.Exceptions
{
    public class LunchGroupDomainException : Exception
    {
        public LunchGroupDomainException() : base() { }

        public LunchGroupDomainException(string message) : base(message) { }

        public LunchGroupDomainException(string message, params object[] args)
            : base(string.Format(CultureInfo.CurrentCulture, message, args)) { }
    }
}
