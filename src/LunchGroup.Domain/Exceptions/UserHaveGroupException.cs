﻿using System;

namespace LunchGroup.Domain.Exceptions
{
    public class UserHaveGroupException : LunchGroupDomainException
    {
        public const string UserInGroupMessage = "Some worker is already in '{0}' group!";

        public UserHaveGroupException() : base() { }

        public UserHaveGroupException(string groupName) : base(string.Format(UserInGroupMessage, groupName)) { }
    }
}
