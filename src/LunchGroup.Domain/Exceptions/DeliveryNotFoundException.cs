﻿using System;

namespace LunchGroup.Domain.Exceptions
{
    public class DeliveryNotFoundException : LunchGroupDomainException
    {
        private const string NotFoundMessage = "Delivery info  with id = {0} is not found.";

        public DeliveryNotFoundException() : base() { }

        public DeliveryNotFoundException(Guid id) : base(string.Format(NotFoundMessage, id)) { }
    }
}
