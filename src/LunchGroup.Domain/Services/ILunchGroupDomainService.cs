﻿using LunchGroup.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LunchGroup.Domain.Services
{
    public interface ILunchGroupDomainService
    {
        Task<IReadOnlyCollection<LunchGroupModel>> GetAllByCompanyAsync(Guid companyId);
        Task SetActiveFlagAsync(Guid groupId, bool isActive);
        Task UpdateTimetableAsync(Guid groupId);
        Task<LunchGroupModel> GetByIdAsync(Guid groupId);
        Task<LunchGroupModel> CreateAsync(LunchGroupModel lunchGroup);
        Task<LunchGroupModel> UpdateAsync(Guid groupId, LunchGroupModel lunchGroup);
        Task<IReadOnlyCollection<LunchGroupModel>> GetAllByManagerIdAsync(Guid personId);
        Task<LunchGroupModel> GetByWorkerIdAsync(Guid personId);
    }

}
