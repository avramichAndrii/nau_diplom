﻿using LunchGroup.Domain.Exceptions;
using LunchGroup.Domain.Models;
using LunchGroup.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LunchGroup.Domain.Services
{
    public class LunchGroupDomainService : ILunchGroupDomainService
    {
        private readonly ILunchGroupRepository _repository;

        public LunchGroupDomainService(ILunchGroupRepository lunchGroupRepository)
        {
            _repository = lunchGroupRepository;
        }

        public Task<IReadOnlyCollection<LunchGroupModel>> GetAllByCompanyAsync(Guid companyId)
        {
            return _repository.GetAllByCompanyAsync(companyId);
        }

        public Task SetActiveFlagAsync(Guid groupId, bool isActive)
        {
            return _repository.SetActiveFlagAsync(groupId, isActive);
        }

        public Task UpdateTimetableAsync(Guid groupId)
        {
            return _repository.UpdateTimetableAsync(groupId);
        }

        public Task<LunchGroupModel> GetByIdAsync(Guid groupId)
        {
            return _repository.GetByIdAsync(groupId);
        }

        public async Task<LunchGroupModel> CreateAsync(LunchGroupModel lunchGroup)
        {
            return await _repository.CreateAsync(lunchGroup);
        }

        //every group that has person inside as a member
        public async Task<IReadOnlyCollection<LunchGroupModel>> GetAllByManagerIdAsync(Guid personId)
        {
            return await _repository.GetAllByManagerIdAsync(personId);
        }

        public async Task<LunchGroupModel> GetByWorkerIdAsync(Guid personId)
        {
            IEnumerable<LunchGroupModel> groups = await _repository.GetAllByPersonIdAsync(personId);
            if (groups.Count() == 1)
            {
                return groups.First();
            }
            throw new LunchGroupDomainException("Somehow you have more than one group or even any");
        }

        public async Task<LunchGroupModel> UpdateAsync(Guid groupId, LunchGroupModel lunchGroup)
        {
            return await _repository.UpdateAsync(groupId, lunchGroup);
        }
    }
}
