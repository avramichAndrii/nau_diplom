﻿using LunchGroup.Domain.Models;
using LunchGroup.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LunchGroup.Domain.Services
{
    public class DeliveryDomainService : IDeliveryDomainService
    {
        private readonly IDeliveryRepository _deliveryRepository;

        public DeliveryDomainService(IDeliveryRepository deliveryRepository)
        {
            _deliveryRepository = deliveryRepository;
        }

        public Task<DeliveryModel> CreateAsync(Guid groupOrderId)
        {
            return _deliveryRepository.CreateAsync(groupOrderId);
        }

        public Task<IReadOnlyCollection<DeliveryModel>> GetAllDeliveriesAsync()
        {
            return _deliveryRepository.GetAllDeliveriesAsync();
        }

            public Task<DeliveryModel> GetDeliveryByIdAsync(Guid deliveryId)
            {
                return _deliveryRepository.GetDeliveryByIdAsync(deliveryId);
            }

        public Task<DeliveryModel> GetDeliveryByGroupOrderIdAsync(Guid groupOrderId)
        {
            return _deliveryRepository.GetDeliveryByGroupOrderIdAsync(groupOrderId);
        }

        public Task<DeliveryModel> UpdateDeliveryAsync(Guid deliveryId, DeliveryModel deliveryModel)
        {
            return _deliveryRepository.UpdateDeliveryAsync(deliveryId, deliveryModel);
        }

        public Task<DeliveryModel> DeleteDeliveryAsync(Guid deliveryId)
        {
            return _deliveryRepository.DeleteAsync(deliveryId);
        }

    }
}
