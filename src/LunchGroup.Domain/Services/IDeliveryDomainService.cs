﻿using LunchGroup.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LunchGroup.Domain.Services
{
    public interface IDeliveryDomainService
    {
        public Task<DeliveryModel> CreateAsync(Guid groupOrderId);
        public Task<IReadOnlyCollection<DeliveryModel>> GetAllDeliveriesAsync();
        public Task<DeliveryModel> GetDeliveryByIdAsync(Guid deliveryId);
        public Task<DeliveryModel> GetDeliveryByGroupOrderIdAsync(Guid groupOrderId);
        public Task<DeliveryModel> UpdateDeliveryAsync(Guid deliveryId, DeliveryModel deliveryModel);
        public Task<DeliveryModel> DeleteDeliveryAsync(Guid deliveryId);
    }
}
