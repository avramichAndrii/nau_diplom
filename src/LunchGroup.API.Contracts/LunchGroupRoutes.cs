﻿namespace LunchGroup.API.Contracts
{
    public static class LunchGroupRoutes
    {
        public const string BaseLunchGroupUri = "lunchgroup";
        public const string CompanyUri = "company";
        public const string MembersUri = "members";
        public const string TimetableUri = "timetable";
        public const string StatusUri = "status";
        public const string ManagerUri = "manager";
        public const string WorkerUri = "worker";
    }
}
