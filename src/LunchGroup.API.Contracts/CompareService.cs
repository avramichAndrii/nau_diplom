﻿using System.Collections.Generic;
using System.Linq;

namespace LunchGroup.API.Contracts
{
    public static class CompareService
    {
        public static bool IsCollectionsEqual<CT>(IReadOnlyCollection<CT> firstCollection, IReadOnlyCollection<CT> otherCollection)
        {
            if (firstCollection.Count != otherCollection.Count)
            {
                return false;
            }
            foreach (var element in otherCollection)
            {
                if (!firstCollection.Contains(element))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
