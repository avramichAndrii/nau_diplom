﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LunchGroup.API.Contracts
{
    public static class DeliveryRouts
    {
        public const string BaseDeliveryURL = "delivery";
        public const string BaseDeliveryURLSlash = "delivery/";
        public const string DeliveryOrderURL = "delivery/order";
        public const string DeliveryOrderURLSlash = "delivery/order/";
        public const string DeliveryLunchGroupURL = "delivery/lunchgroup";
    }
}
