﻿using System;
using System.Collections.Generic;

namespace LunchGroup.API.Contracts.Responses
{
    public class CreateLunchGroupResponse
    {
        public Guid Id { get; set; }
        public string SubmitDeadline { get; set; }
        public string PeriodStart { get; set; }
        public int Periodicity { get; set; }
        public IReadOnlyCollection<string> Weekends { get; set; }
        public string Name { get; set; }
        public IReadOnlyCollection<Guid> Members { get; set; }
        public Guid CompanyId { get; set; }
        public AddressResponse Address { get; set; }
        public Guid ManagerId { get; set; }


        public override bool Equals(object obj)
        {
            return Equals(obj as CreateLunchGroupResponse);
        }

        public bool Equals(CreateLunchGroupResponse other)
        {
            return other != null &&
                   Id.Equals(other.Id) &&
                   SubmitDeadline == other.SubmitDeadline &&
                   PeriodStart == other.PeriodStart &&
                   Periodicity == other.Periodicity &&
                   CompareService.IsCollectionsEqual(Weekends, other.Weekends) &&
                   Name == other.Name &&
                   CompareService.IsCollectionsEqual(Members, other.Members) &&
                   CompanyId.Equals(other.CompanyId) &&
                   EqualityComparer<AddressResponse>.Default.Equals(Address, other.Address) &&
                   ManagerId.Equals(other.ManagerId);
        }

        public override int GetHashCode()
        {
            HashCode hash = new HashCode();
            hash.Add(Id);
            hash.Add(SubmitDeadline);
            hash.Add(PeriodStart);
            hash.Add(Periodicity);
            hash.Add(Weekends);
            hash.Add(Name);
            hash.Add(Members);
            hash.Add(CompanyId);
            hash.Add(Address);
            hash.Add(ManagerId);
            return hash.ToHashCode();
        }

        public static bool operator ==(CreateLunchGroupResponse left, CreateLunchGroupResponse right)
        {
            return EqualityComparer<CreateLunchGroupResponse>.Default.Equals(left, right);
        }

        public static bool operator !=(CreateLunchGroupResponse left, CreateLunchGroupResponse right)
        {
            return !(left == right);
        }
    }
}
