﻿using System;
using System.Collections.Generic;

namespace LunchGroup.API.Contracts.Responses
{
    public class GetTimetableResponse
    {
        public DateTime SubmitDeadline { get; }
        public IReadOnlyCollection<DateTime> AvailableDates { get; }

        public GetTimetableResponse(DateTime submitDeadline, IReadOnlyCollection<DateTime> availableDates)
        {
            SubmitDeadline = submitDeadline;
            AvailableDates = availableDates;
        }



        public override bool Equals(object obj)
        {
            return Equals(obj as GetTimetableResponse);
        }

        public bool Equals(GetTimetableResponse other)
        {
            return other != null &&
                   SubmitDeadline == other.SubmitDeadline &&
                   CompareService.IsCollectionsEqual(AvailableDates, other.AvailableDates);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(SubmitDeadline, AvailableDates);
        }

        public static bool operator ==(GetTimetableResponse left, GetTimetableResponse right)
        {
            return EqualityComparer<GetTimetableResponse>.Default.Equals(left, right);
        }

        public static bool operator !=(GetTimetableResponse left, GetTimetableResponse right)
        {
            return !(left == right);
        }
    }
}
