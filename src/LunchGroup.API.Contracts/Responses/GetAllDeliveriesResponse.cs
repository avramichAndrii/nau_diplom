﻿using System.Collections.Generic;

namespace LunchGroup.API.Contracts.Responses
{
    public class GetAllDeliveriesResponse
    {
        public IReadOnlyCollection<DeliveryResponse> DeliveryCollection { get; private set; }

        public GetAllDeliveriesResponse(IReadOnlyCollection<DeliveryResponse> deliveryCollection)
        {
            DeliveryCollection = deliveryCollection;
        }
    }
}
