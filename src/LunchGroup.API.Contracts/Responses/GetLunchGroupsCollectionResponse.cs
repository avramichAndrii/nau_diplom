﻿using System.Collections.Generic;

namespace LunchGroup.API.Contracts.Responses
{
    public class GetLunchGroupsCollectionResponse
    {
        public IReadOnlyCollection<GetLunchGroupResponse> LunchGroupsCollection { get; }
        
        public GetLunchGroupsCollectionResponse(IReadOnlyCollection<GetLunchGroupResponse> lunchGroupsCollection)
        {
            LunchGroupsCollection = lunchGroupsCollection;
        }
    }
}
