﻿using System;
using System.Collections.Generic;

namespace LunchGroup.API.Contracts.Responses
{
    public class AddressResponse 
    {
        public string City { get; set; }
        public string Street { get; set; }
        public string BuildingNumber { get; set; }
        public string Office { get; set; }

        public override bool Equals(object obj)
        {
            return Equals(obj as AddressResponse);
        }

        public bool Equals(AddressResponse other)
        {
            return other != null &&
                   City == other.City &&
                   Street == other.Street &&
                   BuildingNumber == other.BuildingNumber &&
                   Office == other.Office;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(City, Street, BuildingNumber, Office);
        }

        public static bool operator ==(AddressResponse left, AddressResponse right)
        {
            return EqualityComparer<AddressResponse>.Default.Equals(left, right);
        }

        public static bool operator !=(AddressResponse left, AddressResponse right)
        {
            return !(left == right);
        }
    }
}
