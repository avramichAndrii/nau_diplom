﻿using System;

namespace LunchGroup.API.Contracts.Responses
{
    public class DeliveryResponse
    {
        public Guid DeliveryId { get; set; }
        public Guid GroupOrderId { get; set; }
        public DateTime? DeliveryTime { get; set; }
        public string Status { get; set; }
    }
}
