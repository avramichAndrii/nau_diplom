﻿using System;
using System.Collections.Generic;

namespace LunchGroup.API.Contracts.Responses
{
    public class LunchGroupResponse
    {
        public DateTime SubmitDeadline { get; set; }
        public DateTime PeriodStart { get; set; }
        public int Periodicity { get; set; }
        public IReadOnlyCollection<string> Weekends { get; set; }
        public string Name { get; set; }
        public IReadOnlyCollection<Guid> Members { get; set; }
        public Guid CompanyId { get; set; }
        public bool IsActive { get; set; }
        public AddressResponse Address { get; set; }
        public Guid Id { get; set; }
    }
}
