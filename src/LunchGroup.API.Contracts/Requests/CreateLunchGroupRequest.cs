﻿using System;

namespace LunchGroup.API.Contracts.Requests
{
    public class CreateLunchGroupRequest
    {
        public DateTime SubmitDeadline { get; set; }
        public DateTime PeriodStart { get; set; }
        public int Periodicity { get; set; }
        public string[] Weekends { get; set; }
        public string Name { get; set; }
        public Guid[] Members { get; set; }
        public AddressRequest Address { get; set; }
        public Guid CompanyId { get; set; }
        public bool IsActive { get; } = true;
        public Guid ManagerId { get; set; }
    }
}
