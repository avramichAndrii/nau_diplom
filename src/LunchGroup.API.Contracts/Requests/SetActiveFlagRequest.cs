﻿namespace LunchGroup.API.Contracts.Requests
{
    public class SetActiveFlagRequest
    {
        public bool IsActive { get; set; }
    }
}
