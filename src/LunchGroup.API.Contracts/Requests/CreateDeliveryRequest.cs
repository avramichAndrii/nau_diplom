﻿using System;

namespace LunchGroup.API.Contracts.Requests
{
    public class CreateDeliveryRequest
    {
        public Guid GroupOrderId { get; set; }
    }
}
