﻿using System;

namespace LunchGroup.API.Contracts.Requests
{
    public class UpdateDeliveryRequest
    {
        public Guid DeliveryId { get; set; }
        public Guid GroupOrderId { get; set; }
        public DateTime? DeliveryTime { get; set; }
        public string Status { get; set; }
    }
}
