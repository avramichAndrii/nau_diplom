using System;

namespace Company.API.Contracts.Request
{
    public class ContractRequest
    {
        public string ContractNumber { get; set; }
        public Guid ProviderCompanyId { get; set; }
        public Guid ConsumerCompanyId { get; set; }
        public Guid MenuId { get; set; }
        public string ContractStatus { get; set; }
    }
}