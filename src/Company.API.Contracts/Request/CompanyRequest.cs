﻿namespace Company.API.Contracts.Request
{
    public class CompanyRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string CompanyEmail { get; set; }
        public string TypeOfCompany { get; set; }
        public int OrderingPeriod { get; set; }
    }
}
