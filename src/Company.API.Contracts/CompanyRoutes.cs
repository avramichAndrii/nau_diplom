﻿namespace Company.API.Contracts
{
    public static class CompanyRoutes
    {
        public const string BaseCompanyUri = "company";
        public const string BaseContractUri = "contract";
        public const string CompanyTypeSlash = "type/";
    }
}
