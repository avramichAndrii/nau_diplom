﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Company.API.Contracts.Response
{
    public class GetContractsResponse
    {
        public IReadOnlyCollection<ContractResponse> ContractResponses { get; private set; }

        public GetContractsResponse(IList<ContractResponse> contractResponses)
        {
            ContractResponses = new ReadOnlyCollection<ContractResponse>(contractResponses);
        }
    }
}