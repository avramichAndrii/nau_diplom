﻿using System;

namespace Company.API.Contracts.Response
{
    public class CompanyResponse
    {
        public Guid CompanyId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public  string CompanyEmail { get; set; }
        public string TypeOfCompany { get; set; }
        public int OrderingPeriod { get; set; }
    }
}
