using System;

namespace Company.API.Contracts.Response
{
    public class ContractResponse
    {
        public Guid Id { get; set; }
        public string ContractNumber { get; set; }
        public Guid ProviderCompanyId { get; set; }
        public Guid ConsumerCompanyId { get; set; }
        public Guid MenuId { get; set; }
        public string ContractStatus { get; set; }
    }
}