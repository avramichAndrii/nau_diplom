﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Company.API.Contracts.Response
{
    public class GetCompaniesResponse
    {
        public IReadOnlyCollection<CompanyResponse> CompaniesCollection { get; private set; }
        
        public GetCompaniesResponse(IList<CompanyResponse> companiesCollection)
        {
            CompaniesCollection = new ReadOnlyCollection<CompanyResponse>(companiesCollection);
        }
    }
}
