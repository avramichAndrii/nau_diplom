﻿using System;

namespace Company.API.Contracts.Response
{
    public class DeleteCompanyResponse
    {
        public Guid CompanyId { get; set; }
    }
}
