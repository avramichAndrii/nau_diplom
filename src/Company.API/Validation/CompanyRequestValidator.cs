﻿using Company.API.Contracts.Request;
using FluentValidation;

namespace Company.API.Validation
{
    public class CompanyRequestValidator : AbstractValidator<CompanyRequest>
    {
        private const int StringMinSize = 1;
        private const int StringMaxSize = 1500;
        public CompanyRequestValidator()
        {
            RuleFor(c => c.Name).NotNull().NotEmpty();
            RuleFor(c => c.Name).Length(StringMinSize, StringMaxSize);
            RuleFor(c => c.Description).MaximumLength(StringMaxSize);
        }
    }
}
