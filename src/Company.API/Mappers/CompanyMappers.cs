using Company.API.Contracts.Request;
using Company.API.Contracts.Response;
using Company.Domain;
using System;

namespace Company.API.Mappers
{
    public static class CompanyMappers
    {
        public static CompanyModel MapFromRequest(this CompanyRequest request) 
            => new CompanyModel()
            {
                Name = request.Name,
                Description = request.Description,
                CompanyEmail = request.CompanyEmail,
                TypeOfCompany = Enum.Parse<CompanyType>(request.TypeOfCompany),
                OrderingPeriod = request.OrderingPeriod
            };

        public static CompanyResponse MapToResponse(this CompanyModel model)
            => new CompanyResponse
            {
                CompanyId = model.Id,
                Name = model.Name,
                Description = model.Description,
                CompanyEmail = model.CompanyEmail,
                TypeOfCompany = model.TypeOfCompany.ToString(),
                OrderingPeriod = model.OrderingPeriod
            };

        public static DeleteCompanyResponse MapToDeleteResponse(this CompanyModel model)
            => new DeleteCompanyResponse
            {
                CompanyId = model.Id
            };
    }
}
