using Company.API.Contracts.Request;
using Company.API.Contracts.Response;
using Company.Domain;
using System;

namespace Company.API.Mappers
{
    public static class ContractMapper
    {
        public static ContractModel MapFromRequest(this ContractRequest request)
            => new ContractModel
            {
                ContractNumber = request.ContractNumber,
                ProviderCompanyId = request.ProviderCompanyId,
                ConsumerCompanyId = request.ConsumerCompanyId,
                MenuId = request.MenuId,
                ContractStatus = Enum.Parse<ContractStatus>(request.ContractStatus)
            };

        public static ContractResponse MapToResponse(this ContractModel model)
            => new ContractResponse
            {
                Id = model.Id,
                ContractNumber = model.ContractNumber,
                ProviderCompanyId = model.ProviderCompanyId,
                ConsumerCompanyId = model.ConsumerCompanyId,
                MenuId = model.MenuId,
                ContractStatus = model.ContractStatus.ToString()
            };
    }
}
