using Company.API.Contracts.Request;
using Company.API.Contracts.Response;
using Company.API.Mappers;
using Company.Domain;
using Company.Domain.Exceptions;
using EmailNotifier.Application.EmailNotificationService.Exceptions;
using EmailNotifier.Application.EmailNotificationService.Interfaces;
using EmailNotifier.EmailNotification;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Company.API.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class ContractController : ControllerBase
    {
        private readonly IContractDomainService _contractDomainService;
        private readonly IMailSender _mailSender;
        private readonly ICompanyEmailNotificationService _companyEmailNotificationService;

        private const string ValidationFailedMessage = "Contract validation failed: name is empty or strings is too long";
        
        private const string СontractCreationMessage =
            "<h2>New contract №{0} has been successfully created!</h2><p>Go to your personal account to view details.</p>";

        private const string СontractStatusChangeMessage =
            "<h2>Status of contract №{0}  has been successfully updated from {1} to {2}!</h2><p>Go to your personal account to view details.</p>";

        private const string CreationEmailSubjectMessage = "New contract creation";
        private const string ChangingEmailSubjectMessage = "Contract status updating";


        public ContractController(IContractDomainService contractDomainService, IMailSender mailSender, ICompanyEmailNotificationService companyEmailNotificationService)
        {
            _contractDomainService = contractDomainService;
            _mailSender = mailSender;
            _companyEmailNotificationService = companyEmailNotificationService;
        }

        private async Task SendEmailNotification(ContractModel contractModel, string messageContent, string subject)
        {
            string providerEmail = await _companyEmailNotificationService.GetCompanyEmailAsync(contractModel.ProviderCompanyId);
            string consumerEmail = await _companyEmailNotificationService.GetCompanyEmailAsync(contractModel.ConsumerCompanyId);
            await _mailSender.SendEmailAsync(providerEmail, subject, messageContent);
            await _mailSender.SendEmailAsync(consumerEmail, subject, messageContent);
        }

        [HttpGet]
        public async Task<GetContractsResponse> GetAllContracts()
        {
            IReadOnlyCollection<ContractModel> contractModels =
                await _contractDomainService.GetAllContractsAsync();

            return new GetContractsResponse(contractModels
                .Select(cm => cm.MapToResponse()).ToList());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ContractResponse>> GetContractById(Guid id)
        {
            try
            {
                ContractModel result = await _contractDomainService.GetContractByIdAsync(id);
                return result.MapToResponse();
            }
            catch (ContractNotFoundDomainException e)
            {
                return BadRequest(e.Message);
            }
        }
        
        [HttpGet("{type}/{id}")]
        public async Task<GetContractsResponse> GetAllByCompanyTypeAndId(Guid id, string type)
        {
            IReadOnlyCollection<ContractModel> contractModels =
                await _contractDomainService.GetByCompanyTypeAndIdAsync(id,
                    Enum.Parse<CompanyType>(type));

            return new GetContractsResponse(contractModels
                .Select(cm => cm.MapToResponse()).ToList());
        }


        [HttpGet("{status}/{type}/{id}")]
        public async Task<GetContractsResponse> GetAllByStatusAndCompanyTypeAndId
                                        (Guid id, string type, string status)
        {
            IReadOnlyCollection<ContractModel> contractModels =
                await _contractDomainService.GetByStatusAndCompanyTypeAndIdAsync
                (id, Enum.Parse<CompanyType>(type), Enum.Parse<ContractStatus>(status));

            return new GetContractsResponse(contractModels
                .Select(cm => cm.MapToResponse()).ToList());
        }


        [HttpPost]
        public async Task<ActionResult<ContractResponse>> CreateContract([FromBody] ContractRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ValidationFailedMessage);
            }

            ContractModel requestModel = request.MapFromRequest();
            ContractModel responseModel = await _contractDomainService.CreateContractAsync(requestModel);
            string messageContent = String.Format(СontractCreationMessage, requestModel.ContractNumber);
            try
            {
                await SendEmailNotification(responseModel, messageContent, CreationEmailSubjectMessage);

            }
            catch (EmailNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            return responseModel.MapToResponse();
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<ContractResponse>> UpdateContract(Guid id, [FromBody] ContractRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ValidationFailedMessage);
            }

            ContractModel model = request.MapFromRequest();
            try
            {
                ContractModel previousContract = await _contractDomainService.GetContractByIdAsync(id);
                string messageContent = String.Format(СontractStatusChangeMessage, model.ContractNumber,
                    previousContract.ContractStatus.ToString(), model.ContractStatus.ToString());
                bool flag = _contractDomainService.IsContractStatusEqual(previousContract, model);

                ContractModel result = await _contractDomainService.UpdateContractAsync(id, model);
                
                if (!flag)
                {
                    try
                    {
                        await SendEmailNotification(result, messageContent, ChangingEmailSubjectMessage);
                    }
                    catch (EmailNotFoundException e)
                    {
                        return BadRequest(e.Message);
                    }
                }
                
                return result.MapToResponse();
            }
            catch (ContractNotFoundDomainException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}