using Company.API.Contracts.Request;
using Company.API.Contracts.Response;
using Company.API.Mappers;
using Company.Domain;
using Company.Domain.Exceptions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Company.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CompanyController : ControllerBase
    {
        private readonly ICompanyDomainServices _companyDomainService;
        private const string ValidationFailedMessage = "Company validation failed: name is empty or strings is too long";

        public CompanyController(ICompanyDomainServices companyDomainService)
        {
            _companyDomainService = companyDomainService;
        }

        /// <summary>
        /// Gets all elements from Company table in database.
        /// </summary>
        /// <returns>
        /// A task that represents the asynchronous operation.
        /// The task result contains collection of response objects.
        /// </returns>
        [HttpGet]
        public async Task<GetCompaniesResponse> GetAllCompanies()
        {
            IReadOnlyCollection<CompanyModel> companyModels = await _companyDomainService.GetAllAsync();

            return new GetCompaniesResponse(companyModels
                .Select(cm => cm.MapToResponse())
                .ToList());
        }
        
        /// <summary>
        /// Gets single element by specified id from Company table in database.
        /// </summary>
        /// <returns>
        /// A task that represents the asynchronous operation.
        /// The task result contains response object for entity satisfies a condition (id),
        /// or BadRequest if entity was not found in database.
        /// </returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CompanyResponse>> GetCompany(Guid id)
        {
            try
            {
                CompanyModel res = await _companyDomainService.GetByIdAsync(id);
                return res.MapToResponse();
            }
            catch (CompanyNotFoundDomainException ex)
            {
                return BadRequest(ex.Message);
            }
        }
        /// <summary>
        /// Gets all elements from Company table in database by specified type of company.
        /// </summary>
        /// <returns>
        /// A task that represents the asynchronous operation.
        /// The task result contains collection of response objects.
        /// </returns>
        [HttpGet("type/{type}")]
        public async Task<GetCompaniesResponse> GetCompaniesByType(string type)
        {
            IReadOnlyCollection<CompanyModel> res = await _companyDomainService.GetByCompanyTypeAsync(type);
            return new GetCompaniesResponse(res
                .Select(cm => cm.MapToResponse()).ToList());
        }

        /// <summary>
        /// Creates entity in Company table using object from request body.
        /// </summary>
        /// <returns>
        /// A task that represents the asynchronous operation.
        /// The task result contains response object for created entity,
        /// or BadRequest if request object was not valid.
        /// </returns>
        [HttpPost]
        public async Task<ActionResult<CompanyResponse>> CreateCompany([FromBody] CompanyRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ValidationFailedMessage);
            }
            CompanyModel requestModel = request.MapFromRequest();
            CompanyModel responseModel = await _companyDomainService.CreateAsync(requestModel);
            return responseModel.MapToResponse();
        }

        /// <summary>
        /// Updates single element in Company table by specified id using object from request body.
        /// </summary>
        /// <returns>
        /// A task that represents the asynchronous operation.
        /// The task result contains response object for updated entity that satisfies a condition (id),
        /// or BadRequest if request object was not valid or entity was not found in database.
        /// </returns>
        [HttpPut("{id}")]
        public async Task<ActionResult<CompanyResponse>> UpdateCompany(Guid id, [FromBody] CompanyRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ValidationFailedMessage);
            }
            CompanyModel requestModel = request.MapFromRequest();
            try
            {
                CompanyModel responseModel = await _companyDomainService.UpdateAsync(id, requestModel);
                return responseModel.MapToResponse();
            }
            catch (CompanyNotFoundDomainException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Removes single element by specified id from Company table in database.
        /// </summary>
        /// <returns>
        /// A task that represents the asynchronous operation.
        /// The task result contains response object for removed entity that satisfies a condition (id),
        /// or BadRequest if entity was not found in database.
        /// </returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<DeleteCompanyResponse>> RemoveCompany(Guid id)
        {
            try
            {
                CompanyModel model = await _companyDomainService.DeleteAsync(id);
                return model.MapToDeleteResponse();
            }
            catch (CompanyNotFoundDomainException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}

