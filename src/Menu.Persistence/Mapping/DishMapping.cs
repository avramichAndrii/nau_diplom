﻿using Menu.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;

namespace Menu.Persistence
{
    public class DishMapping : IEntityTypeConfiguration<DishModel>
    {
        private const string TableName = "Dish";
        private const int MaxSize = 1000;
        public void Configure(EntityTypeBuilder<DishModel> builder)
        {
            builder.ToTable(TableName);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.DishName);
            builder.Property(x => x.Category);
            builder.Property(x => x.PreparationTime);
            builder.Property(x => x.DishComposition)
                .HasConversion(
                el => string.Join(',', el),
                el => new List<string>(el.Split(',', StringSplitOptions.RemoveEmptyEntries)));
            builder.OwnsOne(x => x.DishPFC);
            builder.OwnsOne(x => x.Price); 

        }
    }
}
