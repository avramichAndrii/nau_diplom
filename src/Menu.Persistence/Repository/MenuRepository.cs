﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Menu.Domain;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Microsoft.EntityFrameworkCore.Query;

namespace Menu.Persistence
{
    public class MenuRepository : IMenuRepository
    {
        private readonly MenuContext _menuContext;
        const string MenuNotFoundMessage = "Menu with id = {0} is not found";
        const string UpdateMessage = "Couldn't be updated. Please check sending data.";
        const string DeleteMessage = "Couldn't be deleted.";

        public MenuRepository(MenuContext applicationContext)
        {
            _menuContext = applicationContext;
        }
        public IIncludableQueryable<MenuModel, List<DishModel>> IncludeDishList()
        {
            return _menuContext.MenuModels.Include(model => model.DishList);
        }
        public async Task<MenuModel> CreateAsync(MenuModel menuModel)
        {
            _menuContext.MenuModels.Add(menuModel);
            await _menuContext.SaveChangesAsync();
            return menuModel;
        }

        public async Task<IReadOnlyCollection<MenuModel>> GetAllAsync()
        {
            return await IncludeDishList().ToListAsync();
        }

        public async Task<MenuModel> GetByIdAsync(Guid menuId)
        {
            MenuModel menuModel = await IncludeDishList().FirstOrDefaultAsync(model => model.Id == menuId);
            if (menuModel == null)
            {
                throw new NotFoundMenuDomainException(String.Format(MenuNotFoundMessage, menuId));
            }
            return menuModel;
        }

        public async Task<IReadOnlyCollection<MenuModel>> GetByProviderCompanyIdAsync(Guid providerCompanyId)
        {
            return await IncludeDishList().Where(model => model.ProviderCompanyId == providerCompanyId)
                                          .ToListAsync();
        }

        public async Task<MenuModel> UpdateAsync(Guid menuId, MenuModel menuModel)
        {
            MenuModel modelToUpdate = await IncludeDishList().FirstOrDefaultAsync(model => model.Id == menuId);
            if (modelToUpdate == null)
            {
                throw new NotFoundMenuDomainException(String.Format(MenuNotFoundMessage, menuId));
            }
            modelToUpdate.MenuInfo = menuModel.MenuInfo;
            modelToUpdate.MenuName = menuModel.MenuName;
            modelToUpdate.ProviderCompanyId = menuModel.ProviderCompanyId;
            modelToUpdate.ImageData = menuModel.ImageData;
            try
            {
                _menuContext.MenuModels.Update(modelToUpdate);
                await _menuContext.SaveChangesAsync();
                return modelToUpdate;
            }
            catch (Exception)
            {
                throw new InvalidDataMenuDomainException(UpdateMessage);
            }
        }
        public async Task<IReadOnlyCollection<DishModel>> GetDishesByMenuIdAsync(Guid menuId)
        {
            MenuModel menuModel = await IncludeDishList().FirstOrDefaultAsync(model => model.Id == menuId);
            if (menuModel == null)
            {
                throw new NotFoundMenuDomainException(String.Format(MenuNotFoundMessage, menuId));
            }
            return menuModel.DishList;
        }
        public async Task DeleteAsync(Guid menuId)
        {
            MenuModel modelToDelete = await IncludeDishList().FirstOrDefaultAsync(model => model.Id == menuId);
            if (modelToDelete == null)
            {
                throw new NotFoundMenuDomainException(String.Format(MenuNotFoundMessage, menuId));
            }
            try
            {
                _menuContext.MenuModels.Remove(modelToDelete);
                await _menuContext.SaveChangesAsync();
            }
            catch (InvalidOperationException)
            {
                throw new InvalidDataMenuDomainException(DeleteMessage);
            }
        }

        public async Task<IReadOnlyCollection<MenuModel>> GetAllOffersAsync()
        {
            return await IncludeDishList().Where(menu => menu.IsInMarketplace)
                                          .ToListAsync();
        }

        public async Task<IReadOnlyCollection<MenuModel>> GetByProviderIdFromMarketplaceAsync(Guid providerId)
        {
            return await IncludeDishList().Where(menu => menu.IsInMarketplace &&
                                                         menu.ProviderCompanyId == providerId)
                                          .ToListAsync();
        }

        public async Task<MenuModel> UpdateIsInMarketplaceAsync(Guid menuId, bool isInMarketplace)
        {
            MenuModel modelToUpdate = await IncludeDishList().FirstOrDefaultAsync(model => model.Id == menuId);
            if (modelToUpdate == null)
            {
                throw new NotFoundMenuDomainException(String.Format(MenuNotFoundMessage, menuId));
            }
            modelToUpdate.IsInMarketplace = isInMarketplace;
            try
            {
                await _menuContext.SaveChangesAsync();
            }
            catch (InvalidOperationException)
            {
                throw new InvalidDataMenuDomainException(UpdateMessage);
            }
            return modelToUpdate;
        }
    }
}

