﻿using Menu.Domain;
using Microsoft.EntityFrameworkCore;

namespace Menu.Persistence
{
     public sealed class MenuContext : DbContext
    {
        public MenuContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<MenuModel> MenuModels { get; set; }
        public DbSet<DishModel> DishModels { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration<MenuModel>(new MenuMapping());
            modelBuilder.ApplyConfiguration<DishModel>(new DishMapping());
            base.OnModelCreating(modelBuilder);
        }
    }
}

