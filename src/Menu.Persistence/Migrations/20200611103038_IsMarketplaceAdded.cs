﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Menu.Persistence.Migrations
{
    public partial class IsMarketplaceAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Menu",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    MenuName = table.Column<string>(maxLength: 1500, nullable: true),
                    MenuInfo = table.Column<string>(maxLength: 1500, nullable: true),
                    ProviderCompanyId = table.Column<Guid>(nullable: false),
                    IsInMarketplace = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Menu", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Dish",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DishName = table.Column<string>(nullable: true),
                    Category = table.Column<string>(nullable: true),
                    DishComposition = table.Column<string>(nullable: true),
                    PreparationTime = table.Column<TimeSpan>(nullable: false),
                    DishPFC_Proteins = table.Column<double>(nullable: true),
                    DishPFC_Fats = table.Column<double>(nullable: true),
                    DishPFC_Carbohydrates = table.Column<double>(nullable: true),
                    DishPFC_CaloricContent = table.Column<double>(nullable: true),
                    Price_Amount = table.Column<decimal>(nullable: true),
                    Price_Currency = table.Column<string>(nullable: true),
                    MenuID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dish", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Dish_Menu_MenuID",
                        column: x => x.MenuID,
                        principalTable: "Menu",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Dish_MenuID",
                table: "Dish",
                column: "MenuID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Dish");

            migrationBuilder.DropTable(
                name: "Menu");
        }
    }
}
