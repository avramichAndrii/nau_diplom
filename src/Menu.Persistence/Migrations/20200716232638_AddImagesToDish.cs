﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Menu.Persistence.Migrations
{
    public partial class AddImagesToDish : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "ImageData",
                table: "Dish",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImageData",
                table: "Dish");
        }
    }
}
