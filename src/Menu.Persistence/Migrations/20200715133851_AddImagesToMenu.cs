﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Menu.Persistence.Migrations
{
    public partial class AddImagesToMenu : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "ImageData",
                table: "Menu",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ImageFileName",
                table: "Menu",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImageData",
                table: "Menu");

            migrationBuilder.DropColumn(
                name: "ImageFileName",
                table: "Menu");
        }
    }
}
