using Company.API.Client;
using Company.API.Contracts.Response;
using EmailNotifier.EmailNotification;
using Gateway.API.Contracts;
using Gateway.Application.Helpers.AssignToken;
using Infrastructure.HttpClientAbstractions;
using Infrastructure.HttpClientAbstractions.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Threading.Tasks;
using UserManagement.API.Client.ClientInterface;
using UserManagement.API.Contracts.Requests;
using UserManagement.API.Contracts.Responses;
using UserManagement.Domain.Models;
using Xunit;

namespace Gateway.Application.Services.Tests
{
    public class UserAssignerServiceTests
    {
        private readonly Mock<IConfiguration> _configurationMock;

        private readonly AssignmentSettings _initialSettings = new AssignmentSettings
        {
            Audience = "http://localhost:4200/",
            Issuer = "http://localhost:5001/",
            JwtLifetime = "1",
            Secret = "extra secure secret"
        };
        private const string TokenMail = "johndoe@gmail.com";
        private const string ManagerRole = nameof(Role.Manager);
        private readonly Guid _inviterId = Guid.NewGuid();
        private readonly Guid _companyId = Guid.NewGuid();

        public UserAssignerServiceTests()
        {
            _configurationMock = new Mock<IConfiguration>();
            _configurationMock.Setup(c => c.GetSection("WebOptions")).Returns(new Mock<IConfigurationSection>().Object);
            
            var context = new DefaultHttpContext();
            context.Request.Scheme = "http";
            context.Request.Host = new HostString("localhost");
        }

        [Fact]
        public async Task SendInvitation_IfInviterIsWorker_ShouldThrowException()
        {
            // Arrange
            var settingsMock = new Mock<IOptions<AssignmentSettings>>();
            settingsMock.Setup(m => m.Value).Returns(_initialSettings);

            var userManagementClientMock = new Mock<IUserManagementClient>();
            userManagementClientMock.Setup(cl => cl.GetByIdAsync(_inviterId))
                .ReturnsAsync(new UserResponse
                {
                    Role = nameof(Role.Worker)
                });

            var clientFactoryMock = new Mock<IClientFactory>();
            clientFactoryMock.Setup(cf => cf.GetClient<IUserManagementClient>())
                .Returns(() => userManagementClientMock.Object);

            var userAssignerService = new UserAssignerService(clientFactoryMock.Object,
                null, _configurationMock.Object, settingsMock.Object);

            // Act & Assert
            await Assert.ThrowsAsync<ClientErrorHttpResponseException>(
                () => userAssignerService.SendInvitation(_inviterId, string.Empty, string.Empty));
        }

        [Fact]
        public async Task SendInvitation_IfInviterIsManager_ShouldSendMailOnce()
        {
            // Arrange
            Guid inviterId = Guid.NewGuid();
            var settingsMock = new Mock<IOptions<AssignmentSettings>>();
            settingsMock.Setup(m => m.Value).Returns(_initialSettings);

            var userManagementClientMock = new Mock<IUserManagementClient>();
            userManagementClientMock.Setup(cl => cl.GetByIdAsync(inviterId))
                .ReturnsAsync(new UserResponse
                {
                    Role = nameof(ManagerRole),
                    CompanyId = _companyId
                });

            var companyClientMock = new Mock<ICompanyClient>();
            companyClientMock.Setup(cl => cl.GetCompanyAsync(_companyId))
                .ReturnsAsync(new CompanyResponse());

            var clientFactoryMock = new Mock<IClientFactory>();
            clientFactoryMock.Setup(cf => cf.GetClient<IUserManagementClient>())
                .Returns(() => userManagementClientMock.Object);
            clientFactoryMock.Setup(cf => cf.GetClient<ICompanyClient>())
                .Returns(() => companyClientMock.Object);

            const string TargetMailAddress = nameof(TargetMailAddress);
            var mailSenderMock = new Mock<IMailSender>();
            mailSenderMock.Setup(ms => ms.SendEmailAsync(TargetMailAddress, It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(default(object)));

            var userAssignerService = new UserAssignerService(clientFactoryMock.Object,
                mailSenderMock.Object, _configurationMock.Object, settingsMock.Object);

            // Act
            await userAssignerService.SendInvitation(inviterId, string.Empty, TargetMailAddress);

            // Assert
            mailSenderMock.Verify(ms => ms.SendEmailAsync(TargetMailAddress, It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public async Task RegisterWorker_IfTokenIsNotValid_ShouldThrowException()
        {
            // Arrange
            string token = JWTTokenProvider.GenerateAssignToken(_initialSettings, TokenMail, ManagerRole, _companyId);

            var settingsMock = new Mock<IOptions<AssignmentSettings>>();
            _initialSettings.Issuer = "LunchMe Inc";
            settingsMock.Setup(m => m.Value).Returns(_initialSettings);

            var clientFactoryMock = new Mock<IClientFactory>(); 
            
            var userAssignerService = new UserAssignerService(
                clientFactoryMock.Object, null, _configurationMock.Object, settingsMock.Object);

            // Act & Assert
            await Assert.ThrowsAsync<ClientErrorHttpResponseException>(() => userAssignerService.RegisterWorker(token, null));
        }

        [Fact]
        public async Task RegisterWorker_IfEmailsNotMatch_ShouldThrowException()
        {
            // Arrange
            _initialSettings.Issuer = "http://localhost:5001/";
            string token = JWTTokenProvider.GenerateAssignToken(_initialSettings, TokenMail, ManagerRole, _companyId);

            var settingsMock = new Mock<IOptions<AssignmentSettings>>();
            settingsMock.Setup(m => m.Value).Returns(_initialSettings);

            var clientFactoryMock = new Mock<IClientFactory>();
            clientFactoryMock.Setup(cf => cf.GetClient<ICompanyClient>())
                .Returns(() => new Mock<ICompanyClient>().Object);

            var userAssignerService = new UserAssignerService(
                clientFactoryMock.Object, null, _configurationMock.Object, settingsMock.Object);
            var request = new RegisterRequest
            {
                Email = "janedoe@gmail.com"
            };

            // Act & Assert
            await Assert.ThrowsAsync<ClientErrorHttpResponseException>(() => userAssignerService.RegisterWorker(token, request));
        }

        [Fact]
        public async Task RegisterWorkerSuccessTest()
        {
            // Arrange
            var settingsMock = new Mock<IOptions<AssignmentSettings>>();
            settingsMock.Setup(m => m.Value).Returns(_initialSettings);

            var companyClientMock = new Mock<ICompanyClient>();
            companyClientMock.Setup(cl => cl.GetCompanyAsync(_companyId))
                .ReturnsAsync(new CompanyResponse()
                {
                    CompanyId = _companyId,
                    CompanyEmail = TokenMail,
                    Name = "TestCompany"
                });
            
            var userManagementClientMock = new Mock<IUserManagementClient>();
            userManagementClientMock.Setup(cl => cl.RegisterAsync(It.IsAny<RegisterRequest>()))
                .ReturnsAsync(new UserResponse
                {
                    Email = TokenMail,
                    Role = ManagerRole
                });

            var clientFactoryMock = new Mock<IClientFactory>();
            clientFactoryMock.Setup(cf => cf.GetClient<ICompanyClient>())
                .Returns(() => companyClientMock.Object);
            clientFactoryMock.Setup(cf => cf.GetClient<IUserManagementClient>())
                .Returns(() => userManagementClientMock.Object);

            var registerRequest = new MailRegisterRequest
            {
                Token = JWTTokenProvider.GenerateAssignToken(_initialSettings, TokenMail, ManagerRole, _companyId),
                UserRegisterRequest = new RegisterRequest
                {
                    Email = TokenMail,
                    Role = ManagerRole
                }
            };

            var userAssignerService = new UserAssignerService(
                clientFactoryMock.Object,null, _configurationMock.Object, settingsMock.Object);
        
            // Act
            var user = await userAssignerService.RegisterWorker(registerRequest.Token, registerRequest.UserRegisterRequest);
        
            // Assert
            Assert.Equal(TokenMail, user.Email);
            Assert.Equal(_companyId, user.CompanyId);
        }
    }
}