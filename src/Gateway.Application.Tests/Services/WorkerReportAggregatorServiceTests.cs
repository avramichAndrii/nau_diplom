﻿using Company.API.Client;
using Company.API.Contracts.Response;
using Gateway.Application.Services.Reports;
using GroupOrder.API.Client.Client;
using GroupOrder.API.Contracts.Responses;
using GroupOrder.API.Mappers;
using GroupOrder.Domain.Models;
using Infrastructure.HttpClientAbstractions.Interfaces;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UserManagement.API.Client.ClientInterface;
using UserManagement.API.Contracts.Responses;
using Xunit;

namespace Gateway.Application.Tests.Services
{
    public class WorkerReportAggregatorServiceTests
    {
        [Fact]
        public async Task GetCompanyNameById_ShouldReturnName()
        {
            // Arrange
            Guid companyId = Guid.NewGuid();
            string companyName = "Test company name";

            var companyClientMock = new Mock<ICompanyClient>();
            companyClientMock.Setup(cl => cl.GetCompanyAsync(companyId))
                             .ReturnsAsync(new CompanyResponse() { 
                                 CompanyId = companyId, 
                                 Name = companyName 
                             });

            var groupOrderClientMock = new Mock<IGroupOrderClient>();
            var userManagementClientMock = new Mock<IUserManagementClient>();

            var clientFactoryMock = new Mock<IClientFactory>();
            clientFactoryMock.Setup(cf => cf.GetClient<ICompanyClient>())
                             .Returns(() => companyClientMock.Object);
            clientFactoryMock.Setup(cf => cf.GetClient<IGroupOrderClient>())
                             .Returns(() => groupOrderClientMock.Object);
            clientFactoryMock.Setup(cf => cf.GetClient<IUserManagementClient>())
                             .Returns(() => userManagementClientMock.Object);


            var workerReportAgregatorService = new WorkerReportAggregatorService(clientFactoryMock.Object);

            // Act
            var actualResponse = await workerReportAgregatorService.getProviderCompanyAsync(companyId);

            // Assert
            Assert.NotNull(actualResponse);
            Assert.Equal(actualResponse, companyName);
        }

        [Fact]
        public async Task GetUserInformationById_ShouldReturnNameAndSurname()
        {
            // Arrange
            Guid userId = Guid.NewGuid();
            string firstName = "Tom";
            string lastName = "Orlando";
            string expectedResult = firstName + "" + lastName;
            var userManagementClientMock = new Mock<IUserManagementClient>();
            userManagementClientMock.Setup(cl => cl.GetByIdAsync(userId))
                          .ReturnsAsync(new UserResponse() { 
                              Id = userId, 
                              FirstName = firstName, 
                              LastName = lastName 
                          });

            var groupOrderClientMock = new Mock<IGroupOrderClient>();
            var companyClientMock = new Mock<ICompanyClient>();

            var clientFactoryMock = new Mock<IClientFactory>();
            clientFactoryMock.Setup(cf => cf.GetClient<ICompanyClient>())
                             .Returns(() => companyClientMock.Object);
            clientFactoryMock.Setup(cf => cf.GetClient<IGroupOrderClient>())
                             .Returns(() => groupOrderClientMock.Object);
            clientFactoryMock.Setup(cf => cf.GetClient<IUserManagementClient>())
                             .Returns(() => userManagementClientMock.Object);


            var workerReportAggregatorService = new WorkerReportAggregatorService(clientFactoryMock.Object);

            // Act
            var actualResponse = await workerReportAggregatorService.GetUserInformationAsync(userId);

            // Assert
            Assert.NotNull(actualResponse);
            Assert.Equal(actualResponse, expectedResult);
        }

        [Fact]
        public async Task GetOrdersByWorkerId_ShouldReturnList()
        {
            // Arrange
            Guid userId = Guid.NewGuid();
            Guid workerId = Guid.NewGuid();
            List<Guid> dishes = new List<Guid>()
            {
                Guid.NewGuid()
            };
            List<OrderModel> orderList = new List<OrderModel>()
            {
                new OrderModel(Guid.NewGuid(), DateTime.Now, "Information1", Guid.NewGuid(), userId, Guid.NewGuid(), new List<Guid>()),
                new OrderModel(Guid.NewGuid(), DateTime.Now, "Information2", Guid.NewGuid(), userId, Guid.NewGuid(), new List<Guid>()),
                new OrderModel(Guid.NewGuid(), DateTime.Now, "Information3", Guid.NewGuid(), userId, Guid.NewGuid(), new List<Guid>()),

            };
            GetAllOrdersResponse ordersResponse = orderList.MapToCollection();

            var groupOrderClientMock = new Mock<IGroupOrderClient>();
            groupOrderClientMock.Setup(cl => cl.GetOrdersByWorkerIdAsync(userId))
                                .ReturnsAsync(ordersResponse);
            var companyClientMock = new Mock<ICompanyClient>();
            var userManagementClientMock = new Mock<IUserManagementClient>();

            var clientFactoryMock = new Mock<IClientFactory>();
            clientFactoryMock.Setup(cf => cf.GetClient<ICompanyClient>())
                .Returns(() => companyClientMock.Object);
            clientFactoryMock.Setup(cf => cf.GetClient<IGroupOrderClient>())
               .Returns(() => groupOrderClientMock.Object);
            clientFactoryMock.Setup(cf => cf.GetClient<IUserManagementClient>())
               .Returns(() => userManagementClientMock.Object);

            var workerReportAgregatorService = new WorkerReportAggregatorService(clientFactoryMock.Object);

            // Act
            var actualResponse = await workerReportAgregatorService.GetOrdersAsync(userId, new DateTime(2012, 11, 11), new DateTime(2050, 12, 12));


            // Assert
            Assert.NotEmpty(actualResponse);
        }

    }
}
