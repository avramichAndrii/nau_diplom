﻿using Company.API.Client;
using Company.API.Contracts.Response;
using Gateway.Application.Abstractions.Reports;
using Gateway.Application.Services.Reports;
using GroupOrder.API.Client.Client;
using GroupOrder.API.Contracts.Responses;
using GroupOrder.API.Mappers;
using GroupOrder.Domain.Models;
using Infrastructure.HttpClientAbstractions.Interfaces;
using Menu.API.Client.Clients;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml.Serialization;
using UserManagement.API.Client.ClientInterface;
using UserManagement.API.Contracts.Responses;
using UserManagement.Domain.Models;
using Xunit;

namespace Gateway.Application.Tests.Services
{
    public class WorkerReportGeneratorServiceTests
    {
        [Fact]
        public async Task GenerateReport_ShouldReturnAnotherArray()
        {
            // Arrange
            Guid userId = Guid.NewGuid();
            DateTime start = new DateTime(2012, 12, 12);
            DateTime end = new DateTime(2022, 12, 14);
            byte[] report = new byte[3];
            List<OrderResponse> orderList = new List<OrderResponse>()
            {
                new OrderResponse()
                {
                    Id = Guid.NewGuid(),
                    AdditionalInfo="Information1",
                    GroupOrderId = Guid.NewGuid(),
                    WorkerId = userId,
                    ChosenDishes = new List<Guid>()
                },
                new OrderResponse()
                {
                    Id = Guid.NewGuid(),
                    AdditionalInfo="Information2",
                    GroupOrderId = Guid.NewGuid(),
                    WorkerId = userId,
                    ChosenDishes = new List<Guid>()
                }
            };

            var menuClientMock = new Mock<IMenuClient>();
            var reportAggregatorWorkerService = new Mock<IReportAggregatorWorkerService>();

            reportAggregatorWorkerService.Setup(cl => cl.GetUserInformationAsync(userId))
                             .ReturnsAsync("NameSurname");
            reportAggregatorWorkerService.Setup(cl => cl.GetOrdersAsync(userId, start, end))
                              .ReturnsAsync(orderList);

            var clientFactoryMock = new Mock<IClientFactory>();
            clientFactoryMock.Setup(cf => cf.GetClient<IMenuClient>())
                             .Returns(() => menuClientMock.Object);


            var workerReportGeneratorrService = new WorkerReportGeneratorService(clientFactoryMock.Object, reportAggregatorWorkerService.Object);

            // Act
            var actualResponse = await workerReportGeneratorrService.GenerateReportAsync(userId,start,end);

            // Assert
            Assert.NotEqual(actualResponse,report);
        }

        
    }
}
