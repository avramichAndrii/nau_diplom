﻿using Company.API.Client;
using Company.API.Contracts.Response;
using GroupOrder.API.Client.Client;
using GroupOrder.API.Contracts.Responses;
using Infrastructure.HttpClientAbstractions;
using Infrastructure.HttpClientAbstractions.Interfaces;
using LunchGroup.API.Client;
using LunchGroup.API.Contracts.Responses;
using Menu.API.Client.Clients;
using Menu.API.Contracts.Requests;
using Menu.API.Contracts.Responses;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Gateway.Application.Services.Reports.Tests
{
    public class ManagerReportAggregatorServiceTests
    {
        [Fact]
        public async Task GetGroupOrderSummaryCost_ReturnsRightCost()
        {
            // Arrange
            decimal singleOrderCost = 1000M;
            var menuClientMock = new Mock<IMenuClient>();
            menuClientMock.Setup(cl => cl.PostDishListCostAsync(It.IsAny<CalculateDishListPriceRequest>()))
                .ReturnsAsync(new CalculateDishListCostResponse { Cost = singleOrderCost });

            var testOrder = new OrderResponse();
            GroupOrderResponse testGroupOrder = new GroupOrderResponse(default, default, default,
                new List<OrderResponse> { testOrder, testOrder }, default);

            var clientFactoryMock = new Mock<IClientFactory>();
            clientFactoryMock.Setup(cf => cf.GetClient<IMenuClient>())
                .Returns(() => menuClientMock.Object);

            var managerReportAggregatorService = new ManagerReportAggregatorService(clientFactoryMock.Object);

            // Act
            decimal actualCost = await managerReportAggregatorService.GetGroupOrderSummaryCostAsync(testGroupOrder);

            // Assert
            Assert.Equal(singleOrderCost * testGroupOrder.Orders.Count, actualCost);
        }

        [Fact]
        public async Task GetGroupOrdersData_ForTwoLunchGroups_ReturnsGroupOrdersFromBoth()
        {
            // Arrange
            Guid lunchGroupId1 = Guid.NewGuid();
            Guid lunchGroupId2 = Guid.NewGuid();
            IReadOnlyCollection<Guid> lunchGroupIds = new List<Guid> { lunchGroupId1, lunchGroupId2 };
            DateTime testDate = DateTime.Now;
            Guid groupOrderId1 = Guid.NewGuid();
            var response1 = new GetAllGroupOrdersResponse(new List<GroupOrderResponse>
            {
                new GroupOrderResponse(groupOrderId1, default, lunchGroupId1, null, testDate)
            });
            var groupOrderClientMock = new Mock<IGroupOrderClient>();
            groupOrderClientMock.Setup(cl => cl.GetGroupOrdersByLunchGroupsIdAsync(lunchGroupId1))
                .ReturnsAsync(response1);
            Guid groupOrderId2 = Guid.NewGuid();
            Guid groupOrderId3 = Guid.NewGuid();
            var response2 = new GetAllGroupOrdersResponse(new List<GroupOrderResponse>
            {
                new GroupOrderResponse(groupOrderId2, default, lunchGroupId2, null, testDate),
                new GroupOrderResponse(groupOrderId3, default, lunchGroupId2, null, testDate)
            });
            groupOrderClientMock.Setup(cl => cl.GetGroupOrdersByLunchGroupsIdAsync(lunchGroupId2))
                .ReturnsAsync(response2);

            const string Done = nameof(Done);
            var deliveries = new GetAllDeliveriesResponse(new List<DeliveryResponse>
            {
                new DeliveryResponse { GroupOrderId = groupOrderId1, Status = Done },
                new DeliveryResponse { GroupOrderId = groupOrderId2, Status = Done },
                new DeliveryResponse { GroupOrderId = groupOrderId3, Status = Done }
            });
            var lunchGroupClientMock = new Mock<ILunchGroupClient>();
            lunchGroupClientMock.Setup(cl => cl.GetAllDeliveriesAsync()).ReturnsAsync(deliveries);

            var clientFactoryMock = new Mock<IClientFactory>();
            clientFactoryMock.Setup(cf => cf.GetClient<ILunchGroupClient>())
                .Returns(() => lunchGroupClientMock.Object);
            clientFactoryMock.Setup(cf => cf.GetClient<IGroupOrderClient>())
                .Returns(() => groupOrderClientMock.Object);

            var managerReportAggregatorService = new ManagerReportAggregatorService(clientFactoryMock.Object);

            // Act
            IReadOnlyCollection<GroupOrderResponse> actualResponse = await managerReportAggregatorService.GetGroupOrdersDataAsync(
                lunchGroupIds, testDate, testDate.AddDays(1));

            // Assert
            Assert.NotNull(actualResponse);
            Assert.Equal(response1.GroupOrderCollection.Count + response2.GroupOrderCollection.Count, actualResponse.Count);
        }

        [Fact]
        public async Task GetGroupOrdersData_ForTwoLunchGroupsAndSingleRightDelivery_ReturnsSingleGroupOrder()
        {
            // Arrange
            Guid lunchGroupId1 = Guid.NewGuid();
            Guid lunchGroupId2 = Guid.NewGuid();
            IReadOnlyCollection<Guid> lunchGroupIds = new List<Guid> { lunchGroupId1, lunchGroupId2 };
            DateTime testDate = DateTime.Now;
            Guid groupOrderId1 = Guid.NewGuid();
            var response1 = new GetAllGroupOrdersResponse(new List<GroupOrderResponse>
            {
                new GroupOrderResponse(groupOrderId1, default, lunchGroupId1, null, testDate)
            });
            var groupOrderClientMock = new Mock<IGroupOrderClient>();
            groupOrderClientMock.Setup(cl => cl.GetGroupOrdersByLunchGroupsIdAsync(lunchGroupId1))
                .ReturnsAsync(response1);
            Guid groupOrderId2 = Guid.NewGuid();
            Guid groupOrderId3 = Guid.NewGuid();
            var response2 = new GetAllGroupOrdersResponse(new List<GroupOrderResponse>
            {
                new GroupOrderResponse(groupOrderId2, default, lunchGroupId2, null, testDate),
                new GroupOrderResponse(groupOrderId3, default, lunchGroupId2, null, testDate)
            });
            groupOrderClientMock.Setup(cl => cl.GetGroupOrdersByLunchGroupsIdAsync(lunchGroupId2))
                .ReturnsAsync(response2);

            const string Done = nameof(Done);
            const string Cancelled = nameof(Cancelled);
            var deliveries = new GetAllDeliveriesResponse(new List<DeliveryResponse>
            {
                new DeliveryResponse { GroupOrderId = Guid.NewGuid(), Status = Done },
                new DeliveryResponse { GroupOrderId = groupOrderId2, Status = Done },
                new DeliveryResponse { GroupOrderId = groupOrderId3, Status = Cancelled }
            });
            var lunchGroupClientMock = new Mock<ILunchGroupClient>();
            lunchGroupClientMock.Setup(cl => cl.GetAllDeliveriesAsync()).ReturnsAsync(deliveries);

            var clientFactoryMock = new Mock<IClientFactory>();
            clientFactoryMock.Setup(cf => cf.GetClient<ILunchGroupClient>())
                .Returns(() => lunchGroupClientMock.Object);
            clientFactoryMock.Setup(cf => cf.GetClient<IGroupOrderClient>())
                .Returns(() => groupOrderClientMock.Object);

            var managerReportAggregatorService = new ManagerReportAggregatorService(clientFactoryMock.Object);

            // Act
            IReadOnlyCollection<GroupOrderResponse> actualResponse = await managerReportAggregatorService.GetGroupOrdersDataAsync(
                lunchGroupIds, testDate, testDate.AddDays(1));

            // Assert
            Assert.NotNull(actualResponse);
            Assert.Equal(1, actualResponse.Count);
        }

        [Fact]
        public async Task GetGroupOrdersData_WhenNoDataAfterFiltering_ShouldThrowException()
        {
            // Arrange
            IReadOnlyCollection<Guid> lunchGroupIds = new List<Guid>();
            DateTime testDate = DateTime.Now;
            var deliveries = new GetAllDeliveriesResponse(new List<DeliveryResponse>());
            var lunchGroupClientMock = new Mock<ILunchGroupClient>();
            lunchGroupClientMock.Setup(cl => cl.GetAllDeliveriesAsync()).ReturnsAsync(deliveries);

            var clientFactoryMock = new Mock<IClientFactory>();
            clientFactoryMock.Setup(cf => cf.GetClient<ILunchGroupClient>())
                .Returns(() => lunchGroupClientMock.Object);

            var managerReportAggregatorService = new ManagerReportAggregatorService(clientFactoryMock.Object);

            // Act & Assert
            await Assert.ThrowsAsync<ClientErrorHttpResponseException>(() => managerReportAggregatorService.GetGroupOrdersDataAsync(lunchGroupIds, testDate, testDate.AddDays(1)));
            lunchGroupClientMock.Verify(m => m.GetAllDeliveriesAsync(), Times.Once);
        }

        [Fact]
        public void GetOrdersData_ReturnsAggregatedOrders_SortedByDate()
        {
            // Arrange
            DateTime earliestDate = DateTime.Now;
            DateTime mediumDate = DateTime.Now.AddMonths(1);
            DateTime latestDate = DateTime.Now.AddYears(1);
            IReadOnlyCollection<GroupOrderResponse> groupOrders = new List<GroupOrderResponse>
            {
                new GroupOrderResponse(default, default, default, new List<OrderResponse>{ new OrderResponse { Date = latestDate } }, default),
                new GroupOrderResponse(default, default, default, new List<OrderResponse>{ new OrderResponse { Date = earliestDate } }, default),
                new GroupOrderResponse(default, default, default, new List<OrderResponse>{ new OrderResponse { Date = mediumDate } }, default)
            };
            var managerReportAggregatorService = new ManagerReportAggregatorService(null);

            // Act
            IReadOnlyCollection<OrderResponse> orders = managerReportAggregatorService.GetOrdersData(groupOrders);

            // Assert
            Assert.NotNull(orders);
            Assert.Equal(groupOrders.Count, orders.Count);
            Assert.Equal(earliestDate, orders.First().Date);
            Assert.Equal(latestDate, orders.Last().Date);
        }

        [Fact]
        public async Task GetProviderCompaniesData_IfProvidersAreDifferent_ShouldReturnSameCount()
        {
            // Arrange
            IReadOnlyCollection<GroupOrderResponse> groupOrders = new List<GroupOrderResponse>
            {
                new GroupOrderResponse(default, Guid.NewGuid(), default, new List<OrderResponse>(), default),
                new GroupOrderResponse(default, Guid.NewGuid(), default, new List<OrderResponse>(), default),
                new GroupOrderResponse(default, Guid.NewGuid(), default, new List<OrderResponse>(), default)
            };
            var companyClientMock = new Mock<ICompanyClient>();
            companyClientMock.Setup(cl => cl.GetCompanyAsync(It.IsAny<Guid>())).ReturnsAsync(new CompanyResponse());

            var clientFactoryMock = new Mock<IClientFactory>();
            clientFactoryMock.Setup(cf => cf.GetClient<ICompanyClient>())
                .Returns(() => companyClientMock.Object);

            var managerReportAggregatorService = new ManagerReportAggregatorService(clientFactoryMock.Object);

            // Act
            IReadOnlyCollection<CompanyResponse> companies = await managerReportAggregatorService.GetProviderCompaniesDataAsync(groupOrders);

            // Assert
            Assert.NotNull(companies);
            Assert.Equal(groupOrders.Count, companies.Count);
        }

        [Fact]
        public async Task GetProviderCompaniesData_IfProviderRepeats_ShouldReturnLess()
        {
            // Arrange
            Guid popularProviderId = Guid.NewGuid();
            IReadOnlyCollection<GroupOrderResponse> groupOrders = new List<GroupOrderResponse>
            {
                new GroupOrderResponse(default, popularProviderId, default, new List<OrderResponse>(), default),
                new GroupOrderResponse(default, popularProviderId, default, new List<OrderResponse>(), default),
                new GroupOrderResponse(default, Guid.NewGuid(), default, new List<OrderResponse>(), default)
            };
            var companyClientMock = new Mock<ICompanyClient>();
            companyClientMock.Setup(cl => cl.GetCompanyAsync(It.IsAny<Guid>())).ReturnsAsync(new CompanyResponse());

            var clientFactoryMock = new Mock<IClientFactory>();
            clientFactoryMock.Setup(cf => cf.GetClient<ICompanyClient>())
                .Returns(() => companyClientMock.Object);

            var managerReportAggregatorService = new ManagerReportAggregatorService(clientFactoryMock.Object);

            // Act
            IReadOnlyCollection<CompanyResponse> companies = await managerReportAggregatorService.GetProviderCompaniesDataAsync(groupOrders);

            // Assert
            Assert.NotNull(companies);
            Assert.Equal(2, companies.Count);
        }

        [Fact]
        public async Task GetMenuesData_IfProvidersAreDifferent_ShouldReturnSumOfCount()
        {
            // Arrange
            IReadOnlyCollection<GroupOrderResponse> groupOrders = new List<GroupOrderResponse>
            {
                new GroupOrderResponse(default, Guid.NewGuid(), default, new List<OrderResponse>(), default),
                new GroupOrderResponse(default, Guid.NewGuid(), default, new List<OrderResponse>(), default)
            };
            var menuesForOneProvider = new GetAllMenusResponse(new List<MenuResponse>
            { 
                new MenuResponse(), new MenuResponse()
            });
            var menuClientMock = new Mock<IMenuClient>();
            menuClientMock.Setup(cl => cl.GetByProviderAsync(It.IsAny<Guid>()))
                .ReturnsAsync(menuesForOneProvider);

            var clientFactoryMock = new Mock<IClientFactory>();
            clientFactoryMock.Setup(cf => cf.GetClient<IMenuClient>())
                .Returns(() => menuClientMock.Object);

            var managerReportAggregatorService = new ManagerReportAggregatorService(clientFactoryMock.Object);

            // Act
            IReadOnlyCollection<MenuResponse> menues = await managerReportAggregatorService.GetMenuesDataAsync(groupOrders);

            // Assert
            Assert.NotNull(menues);
            Assert.Equal(groupOrders.Count * menuesForOneProvider.MenusCollection.Count, menues.Count);
        }

        [Fact]
        public async Task GetMenuesData_IfProviderRepeats_ShouldReturn2()
        {
            // Arrange
            Guid popularProviderId = Guid.NewGuid();
            IReadOnlyCollection<GroupOrderResponse> groupOrders = new List<GroupOrderResponse>
            {
                new GroupOrderResponse(default, popularProviderId, default, new List<OrderResponse>(), default),
                new GroupOrderResponse(default, popularProviderId, default, new List<OrderResponse>(), default)
            };
            var menuesForOneProvider = new GetAllMenusResponse(new List<MenuResponse>
            {
                new MenuResponse(), new MenuResponse()
            });
            var menuClientMock = new Mock<IMenuClient>();
            menuClientMock.Setup(cl => cl.GetByProviderAsync(It.IsAny<Guid>()))
                .ReturnsAsync(menuesForOneProvider);

            var clientFactoryMock = new Mock<IClientFactory>();
            clientFactoryMock.Setup(cf => cf.GetClient<IMenuClient>())
                .Returns(() => menuClientMock.Object);

            var managerReportAggregatorService = new ManagerReportAggregatorService(clientFactoryMock.Object);

            // Act
            IReadOnlyCollection<MenuResponse> menues = await managerReportAggregatorService.GetMenuesDataAsync(groupOrders);

            // Assert
            Assert.NotNull(menues);
            Assert.Equal(2, menues.Count);
        }

        [Fact]
        public void GetMenuNamesByGroupOrder_ShouldReturnSingleMenuName()
        {
            // Arrange
            Guid dishId1 = Guid.NewGuid();
            Guid dishId2 = Guid.NewGuid();
            Guid dishId3 = Guid.NewGuid();
            var groupOrder = new GroupOrderResponse(default, default, default, new List<OrderResponse>
            {
                new OrderResponse { ChosenDishes = new List<Guid> { dishId1, dishId2 } },
                new OrderResponse { ChosenDishes = new List<Guid> { dishId2, dishId3 } }
            }, default);
            const string TestName = "TestMenu";
            var menues = new List<MenuResponse>
            {
                new MenuResponse {
                    MenuName = TestName,
                    DishList = new List<DishResponse>
                    {
                        new DishResponse { Id = dishId1 },
                        new DishResponse { Id = dishId2 },
                        new DishResponse { Id = dishId3 }
                    }
                }
            };
            var managerReportAggregatorService = new ManagerReportAggregatorService(null);

            // Act
            HashSet<string> menuNames = managerReportAggregatorService.GetMenuNamesByGroupOrder(groupOrder, menues);

            // Assert
            Assert.NotNull(menuNames);
            Assert.Single(menuNames);
            Assert.Equal(TestName, menuNames.Single());
        }

        [Fact]
        public void GetMenuNamesByOrder_ShouldReturn2MenuNames()
        {
            // Arrange
            Guid dishId1 = Guid.NewGuid();
            Guid dishId2 = Guid.NewGuid();
            Guid dishId3 = Guid.NewGuid();
            var order = new OrderResponse
            {
                ChosenDishes = new List<Guid> { dishId1, dishId2, dishId3 }
            };
            const string TestName = "TestMenu";
            var menues = new List<MenuResponse>
            {
                new MenuResponse {
                    MenuName = TestName,
                    DishList = new List<DishResponse>
                    {
                        new DishResponse { Id = dishId1 },
                        new DishResponse { Id = dishId2 }
                    }
                },
                new MenuResponse {
                    MenuName = TestName + TestName,
                    DishList = new List<DishResponse>
                    {
                        new DishResponse { Id = dishId3 }
                    }
                }
            };

            var managerReportAggregatorService = new ManagerReportAggregatorService(null);

            // Act
            HashSet<string> menuNames = managerReportAggregatorService.GetMenuNamesByOrder(order, menues);

            // Assert
            Assert.NotNull(menuNames);
            Assert.Equal(2, menuNames.Count);
        }
    }
}