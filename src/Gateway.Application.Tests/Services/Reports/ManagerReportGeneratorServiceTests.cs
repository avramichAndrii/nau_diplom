﻿using Company.API.Contracts.Response;
using Gateway.Application.Abstractions.Reports;
using GroupOrder.API.Contracts.Responses;
using LunchGroup.API.Contracts.Responses;
using Menu.API.Contracts.Responses;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UserManagement.API.Contracts.Responses;
using Xunit;

namespace Gateway.Application.Services.Reports.Tests
{
    public class ManagerReportGeneratorServiceTests
    {
        [Fact]
        public async Task GenerateReportAsync_ShouldCallAggregationMethods()
        {
            // Arrange
            Guid managerId = Guid.NewGuid();
            DateTime startDate = DateTime.Now.AddMonths(-1);
            DateTime endDate = DateTime.Now;

            Guid companyId = Guid.NewGuid();
            var aggregatorServiceMock = new Mock<IReportAggregatorService>();
            aggregatorServiceMock.Setup(a => a.GetUserDataAsync(managerId))
                .ReturnsAsync(new UserResponse { CompanyId = companyId });
            aggregatorServiceMock.Setup(a => a.GetCompanyNameAsync(companyId))
                .ReturnsAsync(string.Empty);

            Guid lunchGroupId1 = Guid.NewGuid();
            Guid lunchGroupId2 = Guid.NewGuid();
            AddressResponse testAddress = new AddressResponse
            {
                City = "Kyiv",
                Street = "Zoolohichna",
                BuildingNumber = "5",
                Office = "7"
            };
            IReadOnlyCollection<GetLunchGroupResponse> lunchGroups = new List<GetLunchGroupResponse>
            {
                new GetLunchGroupResponse { Id = lunchGroupId1, Address = testAddress },
                new GetLunchGroupResponse { Id = lunchGroupId2, Address = testAddress }
            };
            aggregatorServiceMock.Setup(a => a.GetLunchGroupsDataAsync(managerId))
                .ReturnsAsync(lunchGroups);

            Guid providerId1 = Guid.NewGuid();
            Guid providerId2 = Guid.NewGuid();
            IReadOnlyCollection<GroupOrderResponse> groupOrders = new List<GroupOrderResponse>
            {
                new GroupOrderResponse(default, providerId1, lunchGroupId1, new List<OrderResponse>(), default),
                new GroupOrderResponse(default, providerId2, lunchGroupId2, new List<OrderResponse>(), default)
            };
            aggregatorServiceMock
                .Setup(a => a.GetGroupOrdersDataAsync(It.IsAny<List<Guid>>(), startDate, endDate))
                .ReturnsAsync(groupOrders);

            IReadOnlyCollection<CompanyResponse> providers = new List<CompanyResponse>
            {
                new CompanyResponse { CompanyId = providerId1 },
                new CompanyResponse { CompanyId = providerId2 }
            };
            aggregatorServiceMock
                .Setup(a => a.GetProviderCompaniesDataAsync(groupOrders))
                .ReturnsAsync(providers);

            IReadOnlyCollection<MenuResponse> menues = new List<MenuResponse>
            {
                new MenuResponse { ProviderCompanyId = providerId1 },
                new MenuResponse { ProviderCompanyId = providerId2 }
            };
            aggregatorServiceMock
                .Setup(a => a.GetMenuesDataAsync(groupOrders))
                .ReturnsAsync(menues);

            aggregatorServiceMock
                .Setup(a => a.GetMenuNamesByGroupOrder(It.IsAny<GroupOrderResponse>(), It.IsAny<List<MenuResponse>>()))
                .Returns(new HashSet<string>());
            aggregatorServiceMock
                .Setup(a => a.GetGroupOrderSummaryCostAsync(It.IsAny<GroupOrderResponse>()))
                .ReturnsAsync(new decimal());

            var generatorService = new ManagerReportGeneratorService(aggregatorServiceMock.Object);

            // Act
            byte[] contents = await generatorService.GenerateReportAsync(managerId, startDate, endDate);

            // Assert
            aggregatorServiceMock.Verify(a => a.GetUserDataAsync(managerId), Times.Once);
            aggregatorServiceMock.Verify(a => a.GetCompanyNameAsync(companyId), Times.Once);
            aggregatorServiceMock.Verify(a => a.GetLunchGroupsDataAsync(managerId), Times.Once);
            aggregatorServiceMock.Verify(
                a => a.GetGroupOrdersDataAsync(It.IsAny<List<Guid>>(), startDate, endDate), Times.Once);
            aggregatorServiceMock.Verify(a => a.GetProviderCompaniesDataAsync(groupOrders), Times.Once);
            aggregatorServiceMock.Verify(a => a.GetMenuesDataAsync(groupOrders), Times.Once);
            aggregatorServiceMock.Verify(
                a => a.GetMenuNamesByGroupOrder(It.IsAny<GroupOrderResponse>(), It.IsAny<List<MenuResponse>>()), Times.Exactly(groupOrders.Count));
            aggregatorServiceMock.Verify(
                a => a.GetGroupOrderSummaryCostAsync(It.IsAny<GroupOrderResponse>()), Times.Exactly(groupOrders.Count));
        }
    }
}