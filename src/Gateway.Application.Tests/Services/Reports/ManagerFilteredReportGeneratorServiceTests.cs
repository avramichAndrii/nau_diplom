﻿using Company.API.Contracts.Response;
using Gateway.Application.Abstractions.Reports;
using GroupOrder.API.Contracts.Responses;
using Menu.API.Contracts.Responses;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UserManagement.API.Contracts.Responses;
using Xunit;

namespace Gateway.Application.Services.Reports.Tests
{
    public class ManagerFilteredReportGeneratorServiceTests
    {
        [Fact]
        public async Task GenerateReportAsync_ShouldCallAggregationMethods()
        {
            // Arrange
            Guid managerId = Guid.NewGuid();
            DateTime startDate = DateTime.Now.AddMonths(-1);
            DateTime endDate = DateTime.Now;
            Guid lunchGroupId = Guid.NewGuid();

            Guid companyId = Guid.NewGuid();
            var aggregatorServiceMock = new Mock<IReportAggregatorService>();
            aggregatorServiceMock.Setup(a => a.GetUserDataAsync(It.IsAny<Guid>()))
                .ReturnsAsync(new UserResponse { CompanyId = companyId });
            aggregatorServiceMock.Setup(a => a.GetCompanyNameAsync(companyId))
                .ReturnsAsync(string.Empty);
            aggregatorServiceMock.Setup(a => a.GetLunchGroupNameAsync(lunchGroupId))
                .ReturnsAsync(string.Empty);

            Guid providerId1 = Guid.NewGuid();
            Guid providerId2 = Guid.NewGuid();
            IReadOnlyCollection<GroupOrderResponse> groupOrders = new List<GroupOrderResponse>
            {
                new GroupOrderResponse(default, providerId1, default, new List<OrderResponse>(), default),
                new GroupOrderResponse(default, providerId2, default, new List<OrderResponse>(), default)
            };
            aggregatorServiceMock
                .Setup(a => a.GetGroupOrdersDataAsync(It.IsAny<List<Guid>>(), startDate, endDate))
                .ReturnsAsync(groupOrders);

            IReadOnlyCollection<OrderResponse> orders = new List<OrderResponse>
            {
                new OrderResponse { ProviderId = providerId1 },
                new OrderResponse { ProviderId = providerId2 }
            };
            aggregatorServiceMock.Setup(a => a.GetOrdersData(groupOrders)).Returns(orders);

            IReadOnlyCollection<CompanyResponse> providers = new List<CompanyResponse>
            {
                new CompanyResponse { CompanyId = providerId1 },
                new CompanyResponse { CompanyId = providerId2 }
            };
            aggregatorServiceMock
                .Setup(a => a.GetProviderCompaniesDataAsync(groupOrders))
                .ReturnsAsync(providers);

            IReadOnlyCollection<MenuResponse> menues = new List<MenuResponse>
            {
                new MenuResponse { ProviderCompanyId = providerId1 },
                new MenuResponse { ProviderCompanyId = providerId2 }
            };
            aggregatorServiceMock
                .Setup(a => a.GetMenuesDataAsync(groupOrders))
                .ReturnsAsync(menues);

            aggregatorServiceMock
                .Setup(a => a.GetMenuNamesByOrder(It.IsAny<OrderResponse>(), It.IsAny<List<MenuResponse>>()))
                .Returns(new HashSet<string>());
            aggregatorServiceMock
                .Setup(a => a.GetDishesCostAsync(It.IsAny<List<Guid>>()))
                .ReturnsAsync(new decimal());

            var generatorService = new ManagerFilteredReportGeneratorService(aggregatorServiceMock.Object);

            // Act
            byte[] contents = await generatorService.GenerateReportAsync(managerId, startDate, endDate, lunchGroupId);

            // Assert
            aggregatorServiceMock.Verify(a => a.GetUserDataAsync(managerId), Times.Once);
            aggregatorServiceMock.Verify(a => a.GetCompanyNameAsync(companyId), Times.Once);
            aggregatorServiceMock.Verify(a => a.GetLunchGroupNameAsync(lunchGroupId), Times.Once);
            aggregatorServiceMock.Verify(
                a => a.GetGroupOrdersDataAsync(It.IsAny<List<Guid>>(), startDate, endDate), Times.Once);
            aggregatorServiceMock.Verify(a => a.GetProviderCompaniesDataAsync(groupOrders), Times.Once);
            aggregatorServiceMock.Verify(a => a.GetMenuesDataAsync(groupOrders), Times.Once);
            aggregatorServiceMock.Verify(
                a => a.GetMenuNamesByOrder(It.IsAny<OrderResponse>(), It.IsAny<List<MenuResponse>>()), Times.Exactly(orders.Count));
            aggregatorServiceMock.Verify(
                a => a.GetDishesCostAsync(It.IsAny<List<Guid>>()), Times.Exactly(orders.Count));
        }
    }
}