﻿using Company.API.Client;
using Gateway.Application.Services.Reports;
using GroupOrder.API.Client.Client;
using GroupOrder.API.Contracts.Responses;
using GroupOrder.API.Mappers;
using GroupOrder.Domain.Models;
using Infrastructure.HttpClientAbstractions.Interfaces;
using LunchGroup.API.Client;
using Menu.API.Client.Clients;
using Menu.API.Contracts.Responses;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagement.API.Client.ClientInterface;
using Xunit;

namespace Gateway.Application.Tests.Services.Reports
{
    public class ProviderReportAggregatorServiceTests
    {
        //[Fact]
        //public async Task GetCompanyNameById_ShouldReturnName()
        //{
        //    // Arrange
        //    Guid providerId = Guid.NewGuid();
        //    string firstMenuName = "tst";
        //    var tst = new Mock<GetAllMenusResponse>();
        //    var offersTest = new List<MenuResponse>()
        //    {
        //      new MenuResponse(){ Id = Guid.NewGuid(),IsInMarketplace = true,MenuName="tst",MenuInfo="something",PreparationTime = DateTime.Now.ToString(),
        //      ProviderCompanyId = Guid.NewGuid(),DishList = new List<DishResponse>(),ImageData = new byte[2] }
        //    };
        //    var menuClientMock = new Mock<IMenuClient>();
        //    menuClientMock.Setup(cl => cl.GetByProviderAsync(providerId))
        //                     .ReturnsAsync(tst);
        //    var groupOrderClientMock = new Mock<IGroupOrderClient>();
        //    var userManagementClientMock = new Mock<IUserManagementClient>();
        //    var companyMock = new Mock<ICompanyClient>();
        //    var lgMock = new Mock<ILunchGroupClient>();
        //    var clientFactoryMock = new Mock<IClientFactory>();
        //    clientFactoryMock.Setup(cf => cf.GetClient<IMenuClient>())
        //                     .Returns(() => menuClientMock.Object);
        //    clientFactoryMock.Setup(cf => cf.GetClient<IGroupOrderClient>())
        //                     .Returns(() => groupOrderClientMock.Object);
        //    clientFactoryMock.Setup(cf => cf.GetClient<IUserManagementClient>())
        //                     .Returns(() => userManagementClientMock.Object);
        //    clientFactoryMock.Setup(cf => cf.GetClient<ICompanyClient>())
        //                     .Returns(() => companyMock.Object);
        //    clientFactoryMock.Setup(cf => cf.GetClient<ILunchGroupClient>())
        //                     .Returns(() => lgMock.Object);


        //    var providerReportAgregatorService = new ProviderReportAggregatorService(clientFactoryMock.Object);

        //    // Act
        //    var actualResponse = await providerReportAgregatorService.GetMenuNames(providerId);

        //    // Assert
        //    Assert.NotNull(actualResponse);
        //    Assert.Equal(actualResponse.MenusCollection.First().MenuName, firstMenuName);
        //}
        //[Fact]
        //public async Task GetOrders_ReturnList()
        //{
        //    // Arrange
        //    Guid providerId = Guid.NewGuid();
        //    var groupOrders = new List<OrderResponse>()
        //    {
        //        new OrderResponse(){Id = Guid.NewGuid(),AdditionalInfo="tst",ProviderId=Guid.NewGuid(),WorkerId= Guid.NewGuid(),GroupOrderId=Guid.NewGuid(),Date= DateTime.Now,ChosenDishes = new List<Guid>()}
        //    };
        //    var groupClientMock = new Mock<IGroupOrderClient>();
        //    groupClientMock.Setup(cl => cl.GetAllByProviderIdAsync(providerId))
        //                     .Returns(GetAllGroupOrdersResponse());

        //    var providerReportAgregatorService = new ProviderReportAggregatorService(clientFactoryMock.Object);

        //    // Act
        //    var actualResponse = await providerReportAgregatorService.GetMenuNames(providerId);

        //    // Assert
        //    Assert.NotNull(actualResponse);
        //    Assert.Equal(actualResponse.MenusCollection.First().MenuName, firstMenuName);
        //}

        [Fact]
        public async Task GetOrdersByProvidrId_ShouldReturnList()
        {
            // Arrange
            Guid userId = Guid.NewGuid();
            Guid providerId = Guid.NewGuid();
         
            List<OrderModel> orderList = new List<OrderModel>()
            {
                new OrderModel(Guid.NewGuid(), DateTime.Now, "Information1", Guid.NewGuid(), userId, Guid.NewGuid(), new List<Guid>()),
                new OrderModel(Guid.NewGuid(), DateTime.Now, "Information2", Guid.NewGuid(), userId, Guid.NewGuid(), new List<Guid>()),
                new OrderModel(Guid.NewGuid(), DateTime.Now, "Information3", Guid.NewGuid(), userId, Guid.NewGuid(), new List<Guid>()),

            };
            GetAllOrdersResponse ordersResponse = orderList.MapToCollection();

            var groupOrderClientMock = new Mock<IGroupOrderClient>();
            groupOrderClientMock.Setup(cl => cl.GetOrdersByProviderIdAsync(providerId))
                                .ReturnsAsync(ordersResponse);
            var companyClientMock = new Mock<ICompanyClient>();
            var userManagementClientMock = new Mock<IUserManagementClient>();
            var menuClientMock = new Mock<IMenuClient>();

            var clientFactoryMock = new Mock<IClientFactory>();
            clientFactoryMock.Setup(cf => cf.GetClient<ICompanyClient>())
                .Returns(() => companyClientMock.Object);
            clientFactoryMock.Setup(cf => cf.GetClient<IGroupOrderClient>())
               .Returns(() => groupOrderClientMock.Object);
            clientFactoryMock.Setup(cf => cf.GetClient<IUserManagementClient>())
               .Returns(() => userManagementClientMock.Object);
            clientFactoryMock.Setup(cf => cf.GetClient<IMenuClient>())
              .Returns(() => menuClientMock.Object);

            var workerReportAgregatorService = new ProviderReportAggregatorService(clientFactoryMock.Object);

            // Act
            var actualResponse = workerReportAgregatorService.GetOrdersAsync(providerId);


            // Assert
            Assert.NotEmpty(actualResponse.Result);
        }
    }
}
