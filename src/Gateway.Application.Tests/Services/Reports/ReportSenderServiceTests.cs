﻿using EmailNotifier.EmailNotification;
using Infrastructure.HttpClientAbstractions.Interfaces;
using Moq;
using System;
using System.Threading.Tasks;
using UserManagement.API.Client.ClientInterface;
using UserManagement.API.Contracts.Responses;
using Xunit;

namespace Gateway.Application.Services.Reports.Tests
{
    public class ReportSenderServiceTests
    {
        [Fact]
        public async Task SendReportFileAsyncTest()
        {
            // Arrange
            Guid managerId = Guid.NewGuid();
            byte[] contents = new byte[2048];
            const string TestEmail = "abc@gmail.com";
            var testUser = new UserResponse
            {
                Email = TestEmail
            };
            var userManagementClientMock = new Mock<IUserManagementClient>();
            userManagementClientMock.Setup(cl => cl.GetByIdAsync(managerId)).ReturnsAsync(testUser);

            var clientFactoryMock = new Mock<IClientFactory>();
            clientFactoryMock.Setup(cf => cf.GetClient<IUserManagementClient>())
                .Returns(() => userManagementClientMock.Object);

            var mailSenderMock = new Mock<IMailSender>();
            mailSenderMock.Setup(ms => ms.SendEmailWithAttachmentAsync(
                TestEmail, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), contents, It.IsAny<string>()))
                .Returns(Task.FromResult(default(object)));

            var reportSenderService = new ReportSenderService(clientFactoryMock.Object, mailSenderMock.Object);

            // Act
            string actualEmail = await reportSenderService.SendReportFileAsync(managerId, contents);

            // Assert
            Assert.Equal(TestEmail, actualEmail);
            mailSenderMock.Verify(ms => ms.SendEmailWithAttachmentAsync(
                TestEmail, It.IsAny<string>(), It.IsAny<string>(), 
                It.IsAny<string>(), contents, It.IsAny<string>()), Times.Once);
        }
    }
}