using GroupOrder.API.Client.Client;
using GroupOrder.API.Contracts.Requests;
using GroupOrder.API.Contracts.Responses;
using Infrastructure.HttpClientAbstractions;
using Infrastructure.HttpClientAbstractions.Interfaces;
using LunchGroup.API.Client;
using LunchGroup.API.Contracts.Requests;
using LunchGroup.API.Contracts.Responses;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Gateway.Application.Services.Tests
{
    public class OrderProcessingServiceTests
    {
        private readonly Guid _workerId = Guid.NewGuid();
        private readonly Guid _lunchGroupId = Guid.NewGuid();
        private readonly DateTime _testDate = DateTime.Now.Date;
        private readonly DateTime _greaterThanTestDate = DateTime.Now.AddDays(1).Date;
        private readonly DateTime _lessThanTestDate = DateTime.Now.AddDays(-1).Date;

        [Fact]
        public async Task CreateOrderByWorker_IfDateIsWrong_ShouldThrowException()
        {
            // Arrange
            var lunchGroupClientMock = new Mock<ILunchGroupClient>();
            lunchGroupClientMock.Setup(cl => cl.GetWorkerLunchGroupAsync(_workerId))
                .ReturnsAsync(new GetLunchGroupResponse() { Id = _lunchGroupId });
            lunchGroupClientMock.Setup(cl => cl.GetTimetableAsync(_lunchGroupId))
                .ReturnsAsync(new GetTimetableResponse(DateTime.Now, new List<DateTime> { _testDate }));

            var clientFactoryMock = new Mock<IClientFactory>();
            clientFactoryMock.Setup(cf => cf.GetClient<ILunchGroupClient>())
                .Returns(() => lunchGroupClientMock.Object);

            DateTime desiredDate = _greaterThanTestDate;
            var createOrderRequest = new OrderRequest
            {
                WorkerId = _workerId,
                Date = desiredDate
            };
            var orderProcessingService = new OrderProcessingService(clientFactoryMock.Object);

            // Act & Assert
            await Assert.ThrowsAsync<ClientErrorHttpResponseException>(
                () => orderProcessingService.CreateOrderByWorker(createOrderRequest));
        }

        [Fact]
        public async Task CreateOrderByWorker_IfDateIsAvailable_ShouldWorkFine()
        {
            // Arrange
            var lunchGroupClientMock = new Mock<ILunchGroupClient>();
            lunchGroupClientMock.Setup(cl => cl.GetWorkerLunchGroupAsync(_workerId))
                .ReturnsAsync(new GetLunchGroupResponse() { Id = _lunchGroupId });
            lunchGroupClientMock.Setup(cl => cl.GetTimetableAsync(_lunchGroupId))
                .ReturnsAsync(new GetTimetableResponse(DateTime.Now, new List<DateTime> { _testDate }));

            Guid providerId = Guid.NewGuid();
            var createOrderRequest = new OrderRequest
            {
                WorkerId = _workerId,
                Date = _testDate,
                ProviderId = providerId
            };
            var orderResponse = new OrderResponse
            {
                WorkerId = _workerId,
                Date = _testDate,
                ProviderId = providerId
            };
            var groupOrderClientMock = new Mock<IGroupOrderClient>();
            groupOrderClientMock.Setup(cl => cl.CreateOrderAsync(createOrderRequest))
                .ReturnsAsync(orderResponse);

            var clientFactoryMock = new Mock<IClientFactory>();
            clientFactoryMock.Setup(cf => cf.GetClient<ILunchGroupClient>())
                .Returns(() => lunchGroupClientMock.Object);
            clientFactoryMock.Setup(cf => cf.GetClient<IGroupOrderClient>())
                .Returns(() => groupOrderClientMock.Object);

            var orderProcessingService = new OrderProcessingService(clientFactoryMock.Object);

            // Act
            var actualResponse = await orderProcessingService.CreateOrderByWorker(createOrderRequest);

            // Assert
            Assert.NotNull(actualResponse);
            Assert.Equal(createOrderRequest.Date, actualResponse.Date);
            Assert.Equal(createOrderRequest.WorkerId, actualResponse.WorkerId);
            Assert.Equal(createOrderRequest.ProviderId, actualResponse.ProviderId);
        }

        [Fact]
        public async Task CreateDeliveriesByManager_IfDeadlineIsNotReached_ShouldThrowException()
        {
            // Arrange
            var lunchGroupClientMock = new Mock<ILunchGroupClient>();
            lunchGroupClientMock.Setup(cl => cl.GetLunchGroupByIdAsync(_lunchGroupId))
                .ReturnsAsync(new GetLunchGroupResponse() { Id = _lunchGroupId });
            lunchGroupClientMock.Setup(cl => cl.GetTimetableAsync(_lunchGroupId))
                .ReturnsAsync(new GetTimetableResponse(_greaterThanTestDate, new List<DateTime>()));

            var clientFactoryMock = new Mock<IClientFactory>();
            clientFactoryMock.Setup(cf => cf.GetClient<ILunchGroupClient>())
                .Returns(() => lunchGroupClientMock.Object);

            var orderProcessingService = new OrderProcessingService(clientFactoryMock.Object);

            // Act & Assert
            await Assert.ThrowsAsync<ClientErrorHttpResponseException>(
                () => orderProcessingService.CreateDeliveriesByManager(_lunchGroupId));
        }

        [Fact]
        public async Task CreateDeliveriesByManager_IfDeadlineIsReached_ShouldReturnTwoDeliveries()
        {
            // Arrange
            DateTime availableDate = _greaterThanTestDate;
            var lunchGroupClientMock = new Mock<ILunchGroupClient>();
            lunchGroupClientMock.Setup(cl => cl.GetLunchGroupByIdAsync(_lunchGroupId))
                .ReturnsAsync(new GetLunchGroupResponse() { Id = _lunchGroupId });
            lunchGroupClientMock.Setup(cl => cl.GetTimetableAsync(_lunchGroupId))
                .ReturnsAsync(new GetTimetableResponse(_lessThanTestDate, new List<DateTime>() { availableDate }));
            lunchGroupClientMock.Setup(cl => cl.CreateDeliveryAsync(It.IsAny<CreateDeliveryRequest>()))
                .ReturnsAsync(new DeliveryResponse());

            var groupOrdersCollection = new List<GroupOrderResponse>
            {
                new GroupOrderResponse(default, Guid.NewGuid(), Guid.NewGuid(), null, availableDate),
                new GroupOrderResponse(default, Guid.NewGuid(), Guid.NewGuid(), null, availableDate)
            };
            var groupOrderClientMock = new Mock<IGroupOrderClient>();
            groupOrderClientMock.Setup(cl => cl.GetGroupOrdersByLunchGroupsIdAsync(_lunchGroupId))
                .ReturnsAsync(new GetAllGroupOrdersResponse(groupOrdersCollection));

            var clientFactoryMock = new Mock<IClientFactory>();
            clientFactoryMock.Setup(cf => cf.GetClient<ILunchGroupClient>())
                .Returns(() => lunchGroupClientMock.Object);
            clientFactoryMock.Setup(cf => cf.GetClient<IGroupOrderClient>())
                .Returns(() => groupOrderClientMock.Object);

            var orderProcessingService = new OrderProcessingService(clientFactoryMock.Object);

            // Act
            var actualList = await orderProcessingService.CreateDeliveriesByManager(_lunchGroupId);

            // Assert
            Assert.NotNull(actualList);
            Assert.Equal(groupOrdersCollection.Count, actualList.Count);
        }
    }
}