﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Xunit;

namespace Gateway.Application.Helpers.AssignToken.Tests
{
    public class JWTTokenProviderTests
    {
        [Theory, MemberData(nameof(TestCases))]
        public void GenerateValidateAndParseAssignTokenTest(
            AssignmentSettings initialSettings,
            string testMail,
            string testRole,
            Guid testCompanyId,
            AssignmentSettings validationSettings,
            bool expectedValidationResult)
        {
            // Arrange
            string jwtToken = JWTTokenProvider.GenerateAssignToken(initialSettings, testMail, testRole, testCompanyId);
            // Act
            bool actualValidationResult = JWTTokenProvider.ValidateAssignToken(validationSettings, jwtToken);
            // Assert
            Assert.Equal(expectedValidationResult, actualValidationResult);
            if (actualValidationResult)
            {
                JWTTokenProvider.GetClaims(jwtToken, out string parsedMail, out string parsedRole, out Guid parsedCompanyId);
                Assert.Equal(testMail, parsedMail);
                Assert.Equal(testRole, parsedRole);
                Assert.Equal(testCompanyId, parsedCompanyId);
            }
        }

        [Fact]
        public void ValidateToken_IfItIsNotValid_ShouldReturnFalse()
        {
            // Arrange
            var testSettings = new AssignmentSettings
            {
                Audience = InitialSettings.Audience,
                Issuer = InitialSettings.Issuer,
                JwtLifetime = InitialSettings.JwtLifetime,
                Secret = InitialSettings.Secret
            };
            string jwtToken = JWTTokenProvider.GenerateAssignToken(testSettings, TestMail, TestRole, TestCompanyId);
            // Act
            jwtToken = string.Concat(jwtToken.Reverse());
            bool validationResult = JWTTokenProvider.ValidateAssignToken(testSettings, jwtToken);
            // Assert
            Assert.False(validationResult);
        }

        public static AssignmentSettings InitialSettings = new AssignmentSettings
        {
            Audience = "http://localhost:4200/",
            Issuer = "http://localhost:5001/",
            JwtLifetime = "1",
            Secret = "extra secure secret"
        };
        public const string TestMail = "johndoe@gmail.com";
        public const string TestRole = "Worker";
        public static readonly Guid TestCompanyId = Guid.NewGuid();
        public static IEnumerable<object[]> TestCases
        {
            get
            {
                //all right
                yield return new object[]
                {
                    InitialSettings,
                    TestMail,
                    TestRole,
                    TestCompanyId,
                    InitialSettings,
                    true
                };
                //wrong issuer
                yield return new object[]
                {
                    InitialSettings,
                    TestMail,
                    TestRole,
                    TestCompanyId,
                    new AssignmentSettings
                    {
                        Audience = InitialSettings.Audience,
                        Issuer = "LunchMe Inc",
                        JwtLifetime = InitialSettings.JwtLifetime,
                        Secret = InitialSettings.Secret
                    },
                    false
                };
            }
        }
    }
}
 