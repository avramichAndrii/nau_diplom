using Infrastructure.HttpClientAbstractions.Interfaces;

namespace Infrastructure.HttpClientAbstractions.Interfaces
{
    public interface IClientFactory
    {
        public TResult GetClient<TResult>() where TResult : IClient;
    }
}