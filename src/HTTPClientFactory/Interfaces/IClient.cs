using System;

namespace Infrastructure.HttpClientAbstractions.Interfaces
{
    public interface IClient
    {
        Uri BaseUri { set; }
    }
}