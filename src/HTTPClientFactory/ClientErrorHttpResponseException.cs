using System;
using System.Net;
using Newtonsoft.Json;

namespace Infrastructure.HttpClientAbstractions
{
    public class ClientErrorHttpResponseException : Exception
    {
        public HttpStatusCode StatusCode { get; set; }
        
        public ClientErrorHttpResponseException(string message) : base(message){ }
        public ClientErrorHttpResponseException(HttpStatusCode statusCode, string message) : base(message)
        {
            StatusCode = statusCode;
        }
        public ClientErrorHttpResponseException(HttpStatusCode statusCode, object messageContainer) 
            : base(JsonConvert.SerializeObject(messageContainer))
        {
            StatusCode = statusCode;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}