﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Infrastructure.HttpClientAbstractions
{
    public abstract class BaseHttpClient 
    {
        private readonly HttpClient _httpClient;

        protected BaseHttpClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public Uri BaseUri { get; set; }

        protected virtual async Task<TR> DeleteAsync<TR>(Uri uri)
        {
            HttpResponseMessage httpResponse = await _httpClient.DeleteAsync(uri);

            await EnsureSuccessStatusCode(httpResponse);

            return await ConvertToObject<TR>(httpResponse);
        }

        protected virtual async Task DeleteAsync(Uri uri)
        {
            HttpResponseMessage httpResponse = await _httpClient.DeleteAsync(uri);

            await EnsureSuccessStatusCode(httpResponse);
        }

        protected virtual async Task<TR> PostAsync<T, TR>(Uri uri, T request)
        {
            StringContent content = ConvertToStringContent(request);

            HttpResponseMessage httpResponse = await _httpClient.PostAsync(uri, content);

            await EnsureSuccessStatusCode(httpResponse);
            return await ConvertToObject<TR>(httpResponse);
        }

        protected virtual async Task<TR> GetAsync<TR>(Uri uri)
        {
            HttpResponseMessage httpResponse = await _httpClient.GetAsync(uri);

            await EnsureSuccessStatusCode(httpResponse);
            return await ConvertToObject<TR>(httpResponse);
        }

        protected virtual async Task PostAsync<T>(Uri uri, T request)
        {
            StringContent content = ConvertToStringContent(request);

            HttpResponseMessage httpResponse = await _httpClient.PostAsync(uri, content);

            await EnsureSuccessStatusCode(httpResponse);
        }

        protected virtual async Task<TR> PutAsync<T, TR>(Uri uri, T request)
        {
            StringContent content = ConvertToStringContent(request);

            HttpResponseMessage httpResponse = await _httpClient.PutAsync(uri, content);

            await EnsureSuccessStatusCode(httpResponse);

            return await ConvertToObject<TR>(httpResponse);
        }

        protected virtual async Task PutAsync(Uri uri)
        {
            HttpResponseMessage httpResponse = await _httpClient.PutAsync(uri, new StringContent(string.Empty));

            await EnsureSuccessStatusCode(httpResponse);
        }

        protected virtual async Task PutAsync<T>(Uri uri, T request)
        {
            StringContent content = ConvertToStringContent(request);

            HttpResponseMessage httpResponse = await _httpClient.PutAsync(uri, content);

            await EnsureSuccessStatusCode(httpResponse);
        }

        protected virtual async Task<TR> PatchAsync<T,TR>(Uri uri, T request)
        {
            StringContent content = ConvertToStringContent(request);

            HttpResponseMessage httpResponse = await _httpClient.PatchAsync(uri, content);

            await EnsureSuccessStatusCode(httpResponse);

            return await ConvertToObject<TR>(httpResponse);
        }

        protected virtual async Task PatchAsync<T>(Uri uri, T request)
        {
            StringContent content = ConvertToStringContent(request);

            HttpResponseMessage httpResponse = await _httpClient.PatchAsync(uri, content);

            await EnsureSuccessStatusCode(httpResponse);
        }

        private async Task<TR> ConvertToObject<TR>(HttpResponseMessage httpResponseMessage)
        {
            string responseBody = await httpResponseMessage.Content.ReadAsStringAsync();
            
            var test = JsonConvert.DeserializeObject<TR>(responseBody); 
            return test;
        }

        private StringContent ConvertToStringContent<T>(T request)
        {
            var stringContent = new StringContent(
                JsonConvert.SerializeObject(request),
                Encoding.UTF8,
                "application/json");
            return stringContent;
        }

        private async Task EnsureSuccessStatusCode(HttpResponseMessage httpResponseMessage)
        {
            if (httpResponseMessage.IsSuccessStatusCode)
            {
                return;
            }
            string errorMessage = await httpResponseMessage.Content.ReadAsStringAsync();
            throw new ClientErrorHttpResponseException(httpResponseMessage.StatusCode, errorMessage);
        }
    }
}