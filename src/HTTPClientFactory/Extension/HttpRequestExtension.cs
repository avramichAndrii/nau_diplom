using System;
using Microsoft.AspNetCore.Http;

namespace Infrastructure.HttpClientAbstractions.Extension
{
    public static class HttpRequestExtension
    {
        private const int DefaultPort = -1;
        
        public static Uri GetBaseUri(this HttpRequest httpRequest)
        {
            string scheme = httpRequest.Scheme;
            string host = httpRequest.Host.Host;
            int port = httpRequest.Host.Port ?? DefaultPort;
            var uriBuilder = new UriBuilder(scheme, host, port);
            return uriBuilder.Uri;
        }
    }
}