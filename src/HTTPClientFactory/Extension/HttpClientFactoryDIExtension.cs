using Infrastructure.HttpClientAbstractions.Factory;
using Infrastructure.HttpClientAbstractions.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.HttpClientAbstractions.Extension
{
    public static class HttpClientFactoryDIExtension
    {
        public static void RegisterHttpClientFactory(this IServiceCollection collection)
        {
            collection.AddScoped<IClientFactory, ClientFactory>();
        }
    }
}