using System;

namespace Infrastructure.HttpClientAbstractions
{
    public class ClientNotFoundException : Exception
    {
        public ClientNotFoundException(): base() { }

        public ClientNotFoundException(string message): base(message) { }
    }
}