using Infrastructure.HttpClientAbstractions.Extension;
using Infrastructure.HttpClientAbstractions.Interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.HttpClientAbstractions.Factory
{
    public class ClientFactory : IClientFactory
    {
        private readonly IReadOnlyCollection<IClient> _clients;
        private readonly Uri _baseUri;

        public ClientFactory(IHttpContextAccessor httpContextAccessor, IEnumerable<IClient> clients)
        {
            _clients = clients.ToList();
            _baseUri = httpContextAccessor.HttpContext.Request.GetBaseUri();
        }

        public TResult GetClient<TResult>()
            where TResult : IClient
        {
            IClient client = _clients.FirstOrDefault(x => x is TResult);
            if (client == null)
            {
                throw new ClientNotFoundException($"{typeof(TResult).FullName} not register as IClient");
            }

            client.BaseUri = _baseUri;

            return (TResult)client;
        }
    }
}