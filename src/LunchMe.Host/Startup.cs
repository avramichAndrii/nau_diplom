using AutoMapper;
using Company.API.Client;
using Company.API.Validation;
using Company.Domain;
using Company.Persistence;
using EmailNotifier;
using EmailNotifier.Application.Extensions;
using ErrorHandling;
using FluentValidation.AspNetCore;
using Gateway.Application.Extensions;
using Gateway.Application.Helpers.AssignToken;
using GroupOrder.API.Client.Client;
using GroupOrder.API.Validators;
using GroupOrder.Application.Extensions;
using GroupOrder.Persistence.Extensions;
using LunchGroup.API.Client;
using LunchGroup.API.Validation;
using LunchGroup.Domain.Extensions;
using LunchGroup.Persistense.ContextDIExtension;
using Menu.API.Client.Clients;
using Menu.Domain;
using Menu.Persistence;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using System;
using Infrastructure.HttpClientAbstractions.Extension;
using Infrastructure.HttpClientAbstractions.Interfaces;
using UserManagement.API.Client.Client;
using UserManagement.API.Extentions;
using UserManagement.API.Validators;
using UserManagement.Application.Helpers;
using UserManagement.Persistence.Extentions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;

namespace LunchMe.Host
{
    
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {       
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //here we will add db context

            // Adding cors-services for requests from different domains
            services.AddCors();
            services.AddControllers()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                })
                .AddFluentValidation(fv =>
                {
                    fv.RegisterValidatorsFromAssemblyContaining<CompanyRequestValidator>();

                    fv.RegisterValidatorsFromAssemblyContaining<RegisterRequestValidator>();
                    fv.RegisterValidatorsFromAssemblyContaining<ResetPasswordValidator>();
                    fv.RegisterValidatorsFromAssemblyContaining<UpdatePasswordValidator>();
                    fv.RegisterValidatorsFromAssemblyContaining<UpdateRequestValidator>();

                    fv.RegisterValidatorsFromAssemblyContaining<CreateLunchGroupRequestValidator>();
                    fv.RegisterValidatorsFromAssemblyContaining<UpdateLunchGroupRequestValidator>();

                    fv.RegisterValidatorsFromAssemblyContaining<OrderTemplateRequestValidator>();
                    fv.RegisterValidatorsFromAssemblyContaining<OrderRequestValidator>();
                    fv.RegisterValidatorsFromAssemblyContaining<GroupOrderRequestValidator>();
                });
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });
            services.RegisterMenuDomain();
            services.RegisterMenuPersistence(_configuration);
            services.RegisterCompanyDomain();
            services.RegisterCompanyPersistence(_configuration);
            services.RegisterEmailNotifier();
            services.RegisterEmailNotificationService();
            services.RegisterHttpClientFactory();

            services.RegisterGroupOrderPersistence(_configuration);
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.RegisterPersistence(_configuration);
            services.RegisterAPI();
            services.RegisterGroupOrderAPI();
            services.Configure<AuthenticationSettings>(_configuration.GetSection("AuthenticationSettings"));
            services.Configure<PasswordResetSettings>(_configuration.GetSection("ResetPasswordSettings"));
            services.Configure<RefreshTokenSettings>(_configuration.GetSection("RefreshTokenSettings"));
            services.Configure<WebOptions>(_configuration.GetSection("WebOptions"));
            services.Configure<SendGridEmailOptions>(_configuration.GetSection("SendGridEmailOptions"));
            services.Configure<AssignmentSettings>(_configuration.GetSection("AssignmentSettings"));

            services.AddHttpClient();
            services.AddHttpClient<IClient, UserManagementClient>();
            services.AddHttpClient<IClient, CompanyClient>();
            services.AddHttpClient<IClient, MenuClient>();
            services.AddHttpClient<IClient, GroupOrderClient>();
            services.AddHttpClient<IClient, LunchGroupClient>();
            services.RegisterGateway();

            services.RegisterLunchGroupContext(_configuration);
            services.RegisterLunchGroupDomain();

            var appSettingsSection = _configuration.GetSection("AuthenticationSettings");
            var appSettings = appSettingsSection.Get<AuthenticationSettings>();
            var key = System.Text.Encoding.ASCII.GetBytes(appSettings.Secret);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateLifetime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.FromSeconds(5)
                };
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder => builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            //app.UseHttpsRedirection();

            app.UseMiddleware<ExceptionMiddleware>();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}