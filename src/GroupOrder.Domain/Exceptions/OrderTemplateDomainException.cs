﻿using System;
using System.Globalization;

namespace GroupOrder.Domain.Exceptions
{
    public class OrderTemplateDomainException : Exception
    {
        public OrderTemplateDomainException() : base() { }

        public OrderTemplateDomainException(string message) : base(message) { }

        public OrderTemplateDomainException(string message, params object[] args)
            : base(String.Format(CultureInfo.CurrentCulture, message, args)) { }
    }
}
