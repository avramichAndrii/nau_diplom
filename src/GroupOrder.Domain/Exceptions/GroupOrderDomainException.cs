﻿using System;
using System.Globalization;

namespace GroupOrder.Domain.Exceptions
{
    public class GroupOrderDomainException : Exception
    {
        public GroupOrderDomainException() : base() { }

        public GroupOrderDomainException(string message) : base(message) { }

        public GroupOrderDomainException(string message, params object[] args)
            : base(String.Format(CultureInfo.CurrentCulture, message, args)) { }
    }

    public class NotFoundGroupOrderDomainException : GroupOrderDomainException
    {
        public NotFoundGroupOrderDomainException() : base() { }

        public NotFoundGroupOrderDomainException(string message) : base(message) { }

        public NotFoundGroupOrderDomainException(string message, params object[] args)
            : base(String.Format(CultureInfo.CurrentCulture, message, args)) { }
    }

    public class InvalidDataGroupOrderDomainException : GroupOrderDomainException
    {
        public InvalidDataGroupOrderDomainException() : base() { }

        public InvalidDataGroupOrderDomainException(string message) : base(message) { }

        public InvalidDataGroupOrderDomainException(string message, params object[] args)
            : base(String.Format(CultureInfo.CurrentCulture, message, args)) { }
    }
}
