﻿using GroupOrder.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GroupOrder.Domain.Services
{
    public interface IOrderService
    {
        Task<OrderModel> Update(OrderModel orderModel);
        Task<OrderModel> Create(OrderModel orderModel, Guid lunchGroupId);
        Task Delete(Guid orderId);
        Task<IReadOnlyCollection<OrderModel>> GetAll();
        Task<OrderModel> GetById(Guid orderId);
        Task<IReadOnlyCollection<OrderModel>> GetAllByWorkerId(Guid workerId);
        Task<IReadOnlyCollection<OrderModel>> GetAllByProviderId(Guid providerId);
    }
}
