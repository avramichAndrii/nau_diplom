﻿using GroupOrder.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GroupOrder.Domain.Services
{
    public interface IGroupOrderService
    {
        Task<GroupOrderModel> Create(GroupOrderModel groupOrder);
        Task Delete(Guid groupOrderId);
        Task<IReadOnlyCollection<GroupOrderModel>> GetAll();
        Task<IReadOnlyCollection<GroupOrderModel>> GetAllByProviderId(Guid providerId);
        Task<IReadOnlyCollection<GroupOrderModel>> GetAllByLunchGroupId(Guid lunchGroupId);
        Task<GroupOrderModel> GetById(Guid groupOrderId);
    }
}
