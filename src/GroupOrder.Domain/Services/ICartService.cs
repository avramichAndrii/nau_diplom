﻿using GroupOrder.Domain.Models;
using System;
using System.Threading.Tasks;

namespace GroupOrder.Domain.Services
{
    public interface ICartService
    {
        Task<CartModel> Update(CartModel cartModel);
        Task<CartModel> Create(CartModel orderModel);
        Task Delete(Guid orderId);
        Task<CartModel> GetCartByWorkerId(Guid workerId);
    }
}
