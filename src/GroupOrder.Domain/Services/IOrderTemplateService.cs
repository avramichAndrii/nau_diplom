﻿using GroupOrder.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GroupOrder.Domain.Services
{
    public interface IOrderTemplateService
    {
        Task<OrderTemplateModel> UpdateName(OrderTemplateModel orderTemplateModel);
        Task<OrderTemplateModel> UpdateDishes(OrderTemplateModel orderTemplateModel);
        Task<OrderTemplateModel> Create(OrderTemplateModel orderTemplateModel);
        Task Delete(Guid orderTemplateId);
        Task<IReadOnlyCollection<OrderTemplateModel>> GetAll();
        Task<IReadOnlyCollection<OrderTemplateModel>> GetAllByWorkerId(Guid workerId);
        Task<IReadOnlyCollection<OrderTemplateModel>> GetAllByProviderId(Guid providerId);
        Task<OrderTemplateModel> GetById(Guid orderTemplateId);
    }
}
