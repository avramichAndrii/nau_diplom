﻿using Domain.Core;
using System;
using System.Collections.Generic;

namespace GroupOrder.Domain.Models
{
    public class OrderTemplateModel : Entity<Guid>
    {
        public string Name { get; set; }

        public Guid ProviderId { get; set; }

        public Guid WorkerId { get; }

        public List<Guid> Dishes { get; set; }

        public OrderTemplateModel(Guid id, string name, Guid workerId, Guid providerId, List<Guid> dishes) : base(id)
        {
            Name = name;
            WorkerId = workerId;
            ProviderId = providerId;
            Dishes = dishes;
        }
    }
}
