﻿using Domain.Core;
using System;
using System.Collections.Generic;

namespace GroupOrder.Domain.Models
{
    public class CartModel : Entity<Guid>
    {
        public IReadOnlyCollection<Guid> ChosenDishes { get; set; }

        public string AdditionalInfo { get; set; }

        public Guid ProviderId { get; set; }

        public Guid WorkerId { get; }

        public CartModel() : base() { }

        public CartModel(Guid id, string additionalInfo, Guid providerId, Guid workerId, IReadOnlyCollection<Guid> chosenDishes) : base(id)
        {
            AdditionalInfo = additionalInfo;
            ProviderId = providerId;
            WorkerId = workerId;
            ChosenDishes = chosenDishes;
        }
    }
}
