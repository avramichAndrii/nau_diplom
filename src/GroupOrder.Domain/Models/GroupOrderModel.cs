﻿using Domain.Core;
using System;
using System.Collections.Generic;

namespace GroupOrder.Domain.Models
{
    public class GroupOrderModel : Entity<Guid>
    {
        public IReadOnlyCollection<OrderModel> Orders { get; }

        public Guid ProviderId { get; }

        public Guid LunchGroupId { get; }

        public DateTime Date { get; }

        public GroupOrderModel(Guid id, Guid providerId, Guid lunchGroupId, DateTime date) : base(id)
        {
            ProviderId = providerId;
            LunchGroupId = lunchGroupId;
            Date = date;
            Orders = new List<OrderModel>();
        }
    }
}
