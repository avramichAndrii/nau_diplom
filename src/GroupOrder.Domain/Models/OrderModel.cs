﻿using Domain.Core;
using System;
using System.Collections.Generic;

namespace GroupOrder.Domain.Models
{
    public class OrderModel : Entity<Guid>
    {
        public IReadOnlyCollection<Guid> ChosenDishes { get; set; }

        public DateTime Date { get; set; }

        public string AdditionalInfo { get; set; }

        public Guid ProviderId { get; set; }

        public Guid WorkerId { get; set; }

        public Guid GroupOrderId { get; set; }

        public GroupOrderModel GroupOrder { get; set; }

        public OrderModel(Guid id, DateTime date, string additionalInfo, Guid providerId, Guid workerId, Guid groupOrderId, IReadOnlyCollection<Guid> chosenDishes) : base(id)
        {
            Date = date;
            AdditionalInfo = additionalInfo;
            ProviderId = providerId;
            WorkerId = workerId;
            ChosenDishes = new List<Guid>();
            GroupOrderId = groupOrderId;
            ChosenDishes = chosenDishes;
        }
    }
}
