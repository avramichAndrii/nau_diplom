﻿using GroupOrder.Domain.Models;
using System;
using System.Threading.Tasks;

namespace GroupOrder.Domain.Repositories
{
    public interface ICartRepository
    {
        Task<CartModel> UpdateAsync(CartModel cartModel);
        Task<CartModel> CreateAsync(CartModel orderModel);
        Task DeleteAsync(Guid orderId);
        Task<CartModel> GetCartByWorkerIdAsync(Guid workerId);
    }
}