﻿using GroupOrder.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GroupOrder.Domain.Repositories
{
    public interface IOrderTemplateRepository
    {
        Task<OrderTemplateModel> UpdateNameAsync(OrderTemplateModel orderTemplateModel);
        Task<OrderTemplateModel> UpdateDishesAsync(OrderTemplateModel orderTemplateModel);
        Task<OrderTemplateModel> CreateAsync(OrderTemplateModel orderTemplateModel);
        Task DeleteAsync(Guid orderTemplateId);
        Task<IReadOnlyCollection<OrderTemplateModel>> GetAllAsync();
        Task<IReadOnlyCollection<OrderTemplateModel>> GetAllByWorkerIdAsync(Guid workerId);
        Task<IReadOnlyCollection<OrderTemplateModel>> GetAllByProviderIdAsync(Guid providerId);
        Task<OrderTemplateModel> GetByIdAsync(Guid orderTemplateId);
    }
}
