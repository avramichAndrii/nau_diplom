﻿using GroupOrder.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GroupOrder.Domain.Repositories
{
    public interface IOrderRepository
    {
        Task<OrderModel> UpdateAsync(OrderModel orderModel);
        Task<OrderModel> CreateAsync(OrderModel orderModel);
        Task DeleteAsync(Guid orderId);
        Task<IReadOnlyCollection<OrderModel>> GetAllAsync();
        Task<OrderModel> GetByIdAsync(Guid orderId);
        Task<IReadOnlyCollection<OrderModel>> GetAllByWorkerIdAsync(Guid workerId);
        Task<IReadOnlyCollection<OrderModel>> GetAllByProviderIdAsync(Guid providerId);
    }
}
