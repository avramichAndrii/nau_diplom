﻿using GroupOrder.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GroupOrder.Domain.Repositories
{
    public interface IGroupOrderRepository
    {
        Task<GroupOrderModel> CreateAsync(GroupOrderModel groupOrder);
        Task DeleteAsync(Guid groupOrderId);
        Task<IReadOnlyCollection<GroupOrderModel>> GetAllAsync();
        Task<IReadOnlyCollection<GroupOrderModel>> GetAllByProviderIdAsync(Guid providerId);
        Task<IReadOnlyCollection<GroupOrderModel>> GetAllByLunchGroupIdAsync(Guid lunchGroupId);
        Task<GroupOrderModel> GetByIdAsync(Guid groupOrderId);
    }
}
