﻿using System;
using System.Collections.Generic;

namespace GroupOrder.API.Contracts.Requests
{
    public class OrderRequest
    {
        public OrderRequest(Guid id, IReadOnlyCollection<Guid> chosenDishes, DateTime date, string additionalInfo, Guid providerId, Guid workerId, Guid groupOrderId, Guid lunchGroupId)
        {
            Id = id;
            ChosenDishes = chosenDishes;
            Date = date;
            AdditionalInfo = additionalInfo;
            ProviderId = providerId;
            WorkerId = workerId;
            GroupOrderId = groupOrderId;
            LunchGroupId = lunchGroupId;
        }

        public OrderRequest() { }

        public Guid Id { get; set; }

        public IReadOnlyCollection<Guid> ChosenDishes { get; set; }

        public DateTime Date { get; set; }

        public string AdditionalInfo { get; set; }

        public Guid ProviderId { get; set; }

        public Guid WorkerId { get; set; }

        public Guid GroupOrderId { get; set; }

        public Guid LunchGroupId { get; set; }

    }
}
