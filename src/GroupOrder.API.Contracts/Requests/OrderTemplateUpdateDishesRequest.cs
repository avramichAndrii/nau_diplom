﻿using System;
using System.Collections.Generic;

namespace GroupOrder.API.Contracts.Requests
{
    public class OrderTemplateUpdateDishesRequest
    {
        public Guid ProviderId { get; set; }

        public IReadOnlyCollection<Guid> Dishes { get; set; }
    }
}
