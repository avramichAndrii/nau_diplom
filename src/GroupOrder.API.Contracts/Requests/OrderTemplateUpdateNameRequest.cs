﻿namespace GroupOrder.API.Contracts.Requests
{
    public class OrderTemplateUpdateNameRequest
    {
        public string Name { get; set; }
    }
}
