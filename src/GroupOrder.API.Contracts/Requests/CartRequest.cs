﻿using System;
using System.Collections.Generic;

namespace GroupOrder.API.Contracts.Requests
{
    public class CartRequest
    {
        public CartRequest(Guid id, IReadOnlyCollection<Guid> chosenDishes, string additionalInfo, Guid providerId, Guid workerId)
        {
            Id = id;
            ChosenDishes = chosenDishes;
            AdditionalInfo = additionalInfo;
            ProviderId = providerId;
            WorkerId = workerId;
        }

        public CartRequest() { }

        public Guid Id { get; set; }

        public IReadOnlyCollection<Guid> ChosenDishes { get; set; }

        public string AdditionalInfo { get; set; }

        public Guid ProviderId { get; set; }

        public Guid WorkerId { get; set; }
    }
}
