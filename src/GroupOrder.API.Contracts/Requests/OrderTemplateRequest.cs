﻿using System;

namespace GroupOrder.API.Contracts.Requests
{
    public class OrderTemplateRequest
    {
        public string Name { get; set; }

        public Guid WorkerId { get; set; }
    }
}
