﻿using System;

namespace GroupOrder.API.Contracts.Requests
{
    public class GroupOrderRequest
    {
        public GroupOrderRequest(Guid providerId, Guid lunchGroupId, DateTime date)
        {
            ProviderId = providerId;
            LunchGroupId = lunchGroupId;
            Date = date;
        }

        public GroupOrderRequest(){ }

        public Guid ProviderId { get; set; }

        public Guid LunchGroupId { get; set; }

        public DateTime Date { get; set; }
    }
}
