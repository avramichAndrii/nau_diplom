﻿using System;
using System.Collections.Generic;

namespace GroupOrder.API.Contracts.Responses
{
    public class CartResponse
    {
        public Guid Id { get; set; }

        public IReadOnlyCollection<Guid> ChosenDishes { get; set; }

        public string AdditionalInfo { get; set; }

        public Guid ProviderId { get; set; }

        public Guid WorkerId { get; set; }

    }
}
