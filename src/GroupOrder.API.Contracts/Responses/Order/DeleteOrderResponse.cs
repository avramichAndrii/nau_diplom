using System;

namespace GroupOrder.API.Contracts.Responses
{
    public class DeleteOrderResponse
    {
        public Guid OrderId { get; set; }
    }
}