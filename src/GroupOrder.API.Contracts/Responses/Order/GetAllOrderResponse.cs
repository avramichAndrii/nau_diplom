﻿using System.Collections.Generic;

namespace GroupOrder.API.Contracts.Responses
{
    public class GetAllOrdersResponse
    {
        public IReadOnlyCollection<OrderResponse> OrdersCollection { get; set; }
    }
}

