﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace GroupOrder.API.Contracts.Responses
{
    public class GetAllGroupOrdersResponse
    {
        public IReadOnlyCollection<GroupOrderResponse> GroupOrderCollection { get; }

        public GetAllGroupOrdersResponse(IList<GroupOrderResponse> groupOrderCollection)
        {
            GroupOrderCollection = new ReadOnlyCollection<GroupOrderResponse>(groupOrderCollection);
        }
    }
}
