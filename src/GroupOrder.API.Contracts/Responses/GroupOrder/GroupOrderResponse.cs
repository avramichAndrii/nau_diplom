﻿using System;
using System.Collections.Generic;

namespace GroupOrder.API.Contracts.Responses
{
    public class GroupOrderResponse
    {
        public GroupOrderResponse(Guid id, Guid providerId, Guid lunchGroupId, List<OrderResponse> orders, DateTime date)
        {
            Id = id;
            ProviderId = providerId;
            LunchGroupId = lunchGroupId;
            Date = date;
            Orders = orders;
        }

        public Guid Id { get; }

        public IReadOnlyCollection<OrderResponse> Orders { get; }

        public Guid ProviderId { get; }

        public Guid LunchGroupId { get; }

        public DateTime Date { get; }
    }
}
