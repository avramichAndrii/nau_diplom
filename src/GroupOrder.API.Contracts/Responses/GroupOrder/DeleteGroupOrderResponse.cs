using System;

namespace GroupOrder.API.Contracts.Responses
{
    public class DeleteGroupOrderResponse
    {
        public Guid GroupOrderId { get; set; }
    }
}