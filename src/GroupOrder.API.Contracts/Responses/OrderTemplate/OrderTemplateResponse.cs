﻿using System;
using System.Collections.Generic;

namespace GroupOrder.API.Contracts.Responses
{
    public class OrderTemplateResponse
    {
        public Guid Id { get; }

        public string Name { get; }

        public Guid ProviderId { get; }

        public Guid WorkerId { get; }

        public IReadOnlyCollection<Guid> Dishes { get; }

        public OrderTemplateResponse(Guid id, string name, Guid workerId, Guid providerId, IReadOnlyCollection<Guid> dishes)
        {
            Id = id;
            Name = name;
            WorkerId = workerId;
            ProviderId = providerId;
            Dishes = dishes;
        }
    }
}
