﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace GroupOrder.API.Contracts.Responses
{
    public class GetAllOrderTemplatesResponse
    {
        public IReadOnlyCollection<OrderTemplateResponse> OrderTemplatesCollection { get; }

        public GetAllOrderTemplatesResponse(IList<OrderTemplateResponse> orderTemplatesCollection)
        {
            OrderTemplatesCollection = new ReadOnlyCollection<OrderTemplateResponse>(orderTemplatesCollection);
        }
    }
}
