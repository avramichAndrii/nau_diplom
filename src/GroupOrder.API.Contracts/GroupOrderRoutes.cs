﻿namespace GroupOrder.API.Contracts
{
    public static class GroupOrderRoutes
    {
        public const string GroupOrderUri = "groupOrder";
        public const string OrderUri = "order";
        public const string OrderTemplateUri = "orderTemplate";
        public const string LunchGroupUri = "lunchGroup";
        public const string WorkerUri = "worker";
        public const string ProviderUri = "provider";
        public const string MenuUri = "menu";
        public const string CartUri = "cart";
    }
}
