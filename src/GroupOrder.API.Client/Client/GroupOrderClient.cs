﻿using GroupOrder.API.Contracts;
using GroupOrder.API.Contracts.Requests;
using GroupOrder.API.Contracts.Responses;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Infrastructure.HttpClientAbstractions;

namespace GroupOrder.API.Client.Client
{
    public class GroupOrderClient : BaseHttpClient, IGroupOrderClient
    {
        
        private Uri _baseUri => new Uri(BaseUri.ToString());
        private Uri _baseGroupOrderUri => new Uri(_baseUri + GroupOrderRoutes.GroupOrderUri + "/");
        private Uri _baseOrderUri => new Uri(BaseUri + GroupOrderRoutes.OrderUri + "/");
        private Uri _baseOrderTemplateUri => new Uri(_baseUri + GroupOrderRoutes.OrderTemplateUri + "/");
        private Uri _baseCartUri => new Uri(_baseUri + GroupOrderRoutes.CartUri + "/");
       

        public GroupOrderClient(HttpClient httpClient) : base(httpClient) { }

        #region GroupOrder
        public Task<GetAllGroupOrdersResponse> GetGroupOrdersAsync()
        {
            return GetAsync<GetAllGroupOrdersResponse>(_baseGroupOrderUri);
        }

        public Task<GroupOrderResponse> GetGroupOrderByIdAsync(Guid groupOrderId)
        {
            return GetAsync<GroupOrderResponse>(new Uri(_baseGroupOrderUri + groupOrderId.ToString()));
        }

        public Task<GetAllGroupOrdersResponse> GetAllByProviderIdAsync(Guid providerId)
        {
            return GetAsync<GetAllGroupOrdersResponse>(new Uri(_baseGroupOrderUri + GroupOrderRoutes.ProviderUri + "/" + providerId.ToString()));
        }

        public Task<GetAllGroupOrdersResponse> GetGroupOrdersByLunchGroupsIdAsync(Guid lunchGroupId)
        {
            return GetAsync<GetAllGroupOrdersResponse>(new Uri(_baseGroupOrderUri + GroupOrderRoutes.LunchGroupUri + "/" + lunchGroupId.ToString()));
        }

        public Task<GroupOrderResponse> CreateGroupOrderAsync(GroupOrderRequest request)
        {
            return PostAsync<GroupOrderRequest, GroupOrderResponse>(_baseGroupOrderUri, request);

        }

        public Task<DeleteGroupOrderResponse> DeleteGroupOrderAsync(Guid groupOrderId)
        {
            return DeleteAsync<DeleteGroupOrderResponse>(new Uri(_baseGroupOrderUri + groupOrderId.ToString()));
        }
        #endregion

        #region Order
        public Task<GetAllOrdersResponse> GetOrdersAsync()
        {
            return GetAsync<GetAllOrdersResponse>(_baseOrderUri);
        }

        public Task<OrderResponse> GetOrderByIdAsync(Guid orderId)
        {
            return GetAsync<OrderResponse>(new Uri(_baseOrderUri + orderId.ToString()));
        }

        public Task<GetAllOrdersResponse> GetOrdersByProviderIdAsync(Guid providerId)
        {
            return GetAsync<GetAllOrdersResponse>(new Uri(_baseOrderUri + GroupOrderRoutes.ProviderUri + "/" + providerId.ToString()));
        }
        public Task<GetAllOrdersResponse> GetOrdersByWorkerIdAsync(Guid workerId)
        {
            return GetAsync<GetAllOrdersResponse>(new Uri(_baseOrderUri + GroupOrderRoutes.WorkerUri + "/" + workerId.ToString()));
        }

        public Task<OrderResponse> CreateOrderAsync(OrderRequest request)
        {
            return PostAsync<OrderRequest, OrderResponse>(_baseOrderUri, request);
        }

        public Task<DeleteOrderResponse> DeleteOrderAsync(Guid orderId)
        {
            return DeleteAsync<DeleteOrderResponse>(new Uri(_baseOrderUri + orderId.ToString()));
        }

        public Task<OrderResponse> UpdateOrderAsync(OrderRequest request)
        {
            return PutAsync<OrderRequest, OrderResponse>(_baseOrderUri, request);
        }
        #endregion

        #region OrderTemplate

        public Task<GetAllOrderTemplatesResponse> GetOrderTemplatesAsync()
        {
            return GetAsync<GetAllOrderTemplatesResponse>(_baseOrderTemplateUri);
        }

        public Task<OrderTemplateResponse> GetOrderTemplateByIdAsync(Guid orderTemplateId)
        {
            return GetAsync<OrderTemplateResponse>(new Uri(_baseOrderTemplateUri + orderTemplateId.ToString()));
        }

        public Task<GetAllOrderTemplatesResponse> GetOrderTemplatesByWorkerIdAsync(Guid workerId)
        {
            return GetAsync<GetAllOrderTemplatesResponse>(new Uri(_baseOrderTemplateUri + GroupOrderRoutes.WorkerUri + "/" + workerId.ToString()));
        }

        public Task<GetAllOrderTemplatesResponse> GetOrderTemplatesByProviderIdAsync(Guid providerId)
        {
            return GetAsync<GetAllOrderTemplatesResponse>(new Uri(_baseOrderTemplateUri + GroupOrderRoutes.ProviderUri + "/" + providerId.ToString()));
        }

        public Task<OrderTemplateResponse> CreateOrderTemplateAsync(OrderTemplateRequest request)
        {
            return PostAsync<OrderTemplateRequest, OrderTemplateResponse>(_baseOrderTemplateUri, request);
        }

        public Task DeleteOrderTemplateAsync(Guid orderTemplateId)
        {
            return DeleteAsync<DeleteOrderResponse>(new Uri(_baseOrderTemplateUri + orderTemplateId.ToString()));
        }

        public Task UpdateOrderTemplateNameAsync(Guid id, OrderTemplateUpdateNameRequest request)
        {
            return PatchAsync<OrderTemplateUpdateNameRequest>(new Uri(_baseOrderTemplateUri.ToString() + id + "/name"), request);
        }
        public Task UpdateOrderTemplateDishesAsync(Guid id, OrderTemplateUpdateDishesRequest request)
        {
            return PatchAsync<OrderTemplateUpdateDishesRequest>(new Uri(_baseOrderTemplateUri.ToString() + id + "/dishes"), request);
        }
        #endregion

        #region Cart
        public Task<CartResponse> GetCartByIdAsync(Guid cartId)
        {
            return GetAsync<CartResponse>(new Uri(_baseCartUri + cartId.ToString()));
        }

        public Task<CartResponse> GetCartByWorkerIdAsync(Guid workerId)
        {
            return GetAsync<CartResponse>(new Uri(_baseCartUri + GroupOrderRoutes.WorkerUri + "/" + workerId.ToString()));
        }

        public Task<CartResponse> CreateOrderTemplateAsync(CartRequest request)
        {
            return PostAsync<CartRequest, CartResponse>(_baseCartUri, request);
        }

        public Task DeleteCartAsync(Guid cartId)
        {
            return DeleteAsync(new Uri(_baseCartUri + cartId.ToString()));
        }

        public Task<CartResponse> UpdateCartAsync(CartRequest request)
        {
            return PutAsync<CartRequest, CartResponse>(_baseCartUri, request);

        }

        #endregion


    }
}
