﻿using System;
using System.Threading.Tasks;
using GroupOrder.API.Contracts.Requests;
using GroupOrder.API.Contracts.Responses;
using Infrastructure.HttpClientAbstractions.Interfaces;


namespace GroupOrder.API.Client.Client
{
    public interface IGroupOrderClient : IClient
    {

        #region GroupOrder
        Task<GetAllGroupOrdersResponse> GetGroupOrdersAsync();
        Task<GroupOrderResponse> GetGroupOrderByIdAsync(Guid groupOrderId);
        Task<GetAllGroupOrdersResponse> GetAllByProviderIdAsync(Guid providerId);
        Task<GetAllGroupOrdersResponse> GetGroupOrdersByLunchGroupsIdAsync(Guid lunchGroupId);
        Task<GroupOrderResponse> CreateGroupOrderAsync(GroupOrderRequest request);
        Task<DeleteGroupOrderResponse> DeleteGroupOrderAsync(Guid groupOrderId);

        #endregion

        #region Order
        Task<GetAllOrdersResponse> GetOrdersAsync();
        Task<OrderResponse> GetOrderByIdAsync(Guid orderId);
        Task<GetAllOrdersResponse> GetOrdersByProviderIdAsync(Guid providerId);
        Task<GetAllOrdersResponse> GetOrdersByWorkerIdAsync(Guid workerId);
        Task<OrderResponse> CreateOrderAsync(OrderRequest request);
        Task<DeleteOrderResponse> DeleteOrderAsync(Guid orderId);
        Task<OrderResponse> UpdateOrderAsync(OrderRequest request);

        #endregion

        #region OrderTemplate

        Task<GetAllOrderTemplatesResponse> GetOrderTemplatesAsync();
        Task<OrderTemplateResponse> GetOrderTemplateByIdAsync(Guid orderTemplateId);
        Task<GetAllOrderTemplatesResponse> GetOrderTemplatesByWorkerIdAsync(Guid workerId);
        Task<GetAllOrderTemplatesResponse> GetOrderTemplatesByProviderIdAsync(Guid menuId);
        Task<OrderTemplateResponse> CreateOrderTemplateAsync(OrderTemplateRequest request);
        Task DeleteOrderTemplateAsync(Guid orderTemplateId);
        Task UpdateOrderTemplateNameAsync(Guid id, OrderTemplateUpdateNameRequest request);
        Task UpdateOrderTemplateDishesAsync(Guid id, OrderTemplateUpdateDishesRequest request);

        #endregion

        #region Cart

        Task<CartResponse> GetCartByIdAsync(Guid cartId);
        Task<CartResponse> GetCartByWorkerIdAsync(Guid workerId);
        Task<CartResponse> CreateOrderTemplateAsync(CartRequest request);
        Task DeleteCartAsync(Guid cartId);
        Task<CartResponse> UpdateCartAsync(CartRequest request);

        #endregion

    }
}