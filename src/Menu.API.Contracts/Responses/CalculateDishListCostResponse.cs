﻿namespace Menu.API.Contracts.Responses
{
    public class CalculateDishListCostResponse
    {
        public decimal Cost { get; set; }
    }
}
