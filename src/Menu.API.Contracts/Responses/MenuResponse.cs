﻿using System;
using System.Collections.Generic;

namespace Menu.API.Contracts.Responses
{
    public class MenuResponse
    {
        public Guid Id { get; set; }
        public string MenuName { get; set; }
        public string MenuInfo { get; set; }
        public Guid ProviderCompanyId { get; set; }
        public List<DishResponse> DishList { get; set; }
        public string PreparationTime { get; set; }
        public bool IsInMarketplace { get; set; }
        public byte[] ImageData { get; set; }
    }
}
