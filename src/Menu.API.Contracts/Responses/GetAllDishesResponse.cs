﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Menu.API.Contracts.Responses
{
    public class GetAllDishesResponse
    {
        public IReadOnlyCollection<DishResponse> DishesCollection { get; private set; }
        public GetAllDishesResponse(IList<DishResponse> dishesCollection)
        {
            DishesCollection = new ReadOnlyCollection<DishResponse>(dishesCollection);
        }
    }
}
