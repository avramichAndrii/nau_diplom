﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Menu.API.Contracts.Responses
{
    public class GetAllMenusResponse
    {
        public IReadOnlyCollection<MenuResponse> MenusCollection { get; private set; }
        public GetAllMenusResponse(IList<MenuResponse> menusCollection)
        {
            MenusCollection = new ReadOnlyCollection<MenuResponse>(menusCollection);
        }
    }
}
