﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Menu.API.Contracts.Routs
{
    public static class MenuRouting
    {
        public const string EmptyURL = "";

        public const string BaseMenuURL = "menu";
        public const string BaseMenuURLSlash = "menu/";
        public const string GetByProviderURL = "menu/provider/";
        public const string BaseMenuCompanyURL = "menus/workerCompany/";

        public const string BaseDishURL = "dish";
        public const string BaseDishURLSlash = "dish/";

        public const string BaseMarketplaceURL = "marketplace";
        public const string BaseMarketplaceURLSlash = "marketplace/";
    }
}
