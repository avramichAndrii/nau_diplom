﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Menu.API.Contracts.Requests
{
    public class UpdateDishIsActiveRequest
    {
        public bool IsActive { get; set; }
    }
}

