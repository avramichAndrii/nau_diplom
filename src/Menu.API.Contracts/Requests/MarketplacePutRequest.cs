﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Menu.API.Contracts.Requests
{
    public class MarketplacePutRequest
    {
        public bool IsInMarketplace { get; set; }
    }
}
