﻿using System;
using System.Collections.Generic;

namespace Menu.API.Contracts.Requests
{
    public class CalculateDishListPriceRequest
    {
        public IReadOnlyCollection<Guid> DishIdsCollection { get; set; }

        public CalculateDishListPriceRequest() { }
        public CalculateDishListPriceRequest(IReadOnlyCollection<Guid> dishesIds)
        {
            DishIdsCollection = dishesIds;
        }
    }

}
