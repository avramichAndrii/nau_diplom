﻿using System;

namespace Menu.API.Contracts.Requests
{
    public class MenuRequest
    {
        public string MenuName { get; set; }
        public string MenuInfo { get; set; }
        public Guid ProviderCompanyId { get; set; }
        public byte[] ImageData { get; set; }
    }
}

