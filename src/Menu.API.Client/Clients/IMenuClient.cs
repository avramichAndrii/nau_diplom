﻿using Menu.API.Contracts.Requests;
using Menu.API.Contracts.Responses;
using System;
using System.Threading.Tasks;
using Infrastructure.HttpClientAbstractions.Interfaces;

namespace Menu.API.Client.Clients
{
    public interface IMenuClient : IClient
    {

        #region Menu

        public Task<GetAllMenusResponse> GetMenusAsync();
        public Task<MenuResponse> GetMenuByIdAsync(Guid menuId);
        public Task<GetAllDishesResponse> GetDishesByMenuAsync(Guid menuId);
        public Task<GetAllMenusResponse> GetByProviderAsync(Guid providerId);
        public Task<MenuResponse> CreateMenuAsync(MenuRequest request);
        public Task<MenuResponse> UpdateMenuAsync(Guid menuId, MenuRequest request);
        public Task DeleteMenuAsync(Guid menuId);
        

        #endregion

        #region Dish
        
        public Task<GetAllDishesResponse> GetDishesAsync();
        public Task<DishResponse> GetDishByIdAsync(Guid dishId);
        public Task<DishResponse> CreateDishAsync(DishRequest request);
        public Task<DishResponse> UpdateDishAsync(Guid dishId, DishRequest request);
        public Task<DishResponse> UpdateDishIsActiveAsync(Guid dishId, UpdateDishIsActiveRequest request);
        public Task DeleteDishAsync(Guid dishId);
        public Task<CalculateDishListCostResponse> PostDishListCostAsync(CalculateDishListPriceRequest dishesIds);

        #endregion

        #region Marketplace

        public Task<GetAllMenusResponse> GetOffersAsync();
        public Task<MenuResponse> GetByProviderOffersIdAsync(Guid providerId);
        public Task<MenuResponse> UpdateIsInMarketplaceAsync (Guid offerId, MarketplacePutRequest req);

        #endregion
    }
}
