﻿using Menu.API.Contracts.Requests;
using Menu.API.Contracts.Responses;
using Menu.API.Contracts.Routs;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Infrastructure.HttpClientAbstractions;

namespace Menu.API.Client.Clients
{
    public class MenuClient : BaseHttpClient, IMenuClient
    {
        public MenuClient(HttpClient client): base(client) { }

        #region MenuController
        public Task<GetAllMenusResponse> GetMenusAsync()
        {
            return GetAsync<GetAllMenusResponse>(new Uri(BaseUri + MenuRouting.BaseMenuURL));
        }

        public Task<MenuResponse> GetMenuByIdAsync(Guid menuId)
        {
            return GetAsync<MenuResponse>(new Uri(BaseUri + MenuRouting.BaseMenuURLSlash + menuId));
        }

        public Task<GetAllDishesResponse> GetDishesByMenuAsync(Guid menuId)
        {
            return GetAsync<GetAllDishesResponse>(new Uri(BaseUri + MenuRouting.BaseMenuURLSlash + menuId.ToString() + "/dishes")); ;
        }

        public Task<GetAllMenusResponse> GetByProviderAsync(Guid providerId)
        {
            return GetAsync<GetAllMenusResponse>(new Uri(BaseUri + MenuRouting.GetByProviderURL + providerId.ToString())); ; ;
        }

        public Task<MenuResponse> CreateMenuAsync(MenuRequest request)
        {
            return PostAsync<MenuRequest, MenuResponse>(new Uri(BaseUri + MenuRouting.BaseMenuURL), request);
        }

        public Task<MenuResponse> UpdateMenuAsync(Guid menuId, MenuRequest request)
        {
            return PutAsync<MenuRequest, MenuResponse>(new Uri(BaseUri + MenuRouting.BaseMenuURLSlash + menuId.ToString()), request);
        }

        public Task DeleteMenuAsync(Guid menuId)
        {
            return DeleteAsync(new Uri(BaseUri + MenuRouting.BaseMenuURLSlash + menuId.ToString()));
        }


        #endregion

        #region DishController
        public Task<GetAllDishesResponse> GetDishesAsync()
        {
            return GetAsync<GetAllDishesResponse>(new Uri(BaseUri + MenuRouting.BaseDishURL));
        }

        public Task<DishResponse> GetDishByIdAsync(Guid dishId)
        {
            return GetAsync<DishResponse>(new Uri(BaseUri + MenuRouting.BaseDishURLSlash + dishId.ToString()));
        }

        public Task<DishResponse> CreateDishAsync(DishRequest request)
        {
            return PostAsync<DishRequest, DishResponse>(new Uri(BaseUri + MenuRouting.BaseDishURL), request);
        }

        public Task<DishResponse> UpdateDishAsync(Guid dishId, DishRequest request)
        {
            return PutAsync<DishRequest, DishResponse>(new Uri(BaseUri + MenuRouting.BaseDishURLSlash + dishId.ToString()), request);
        }

        public Task<DishResponse> UpdateDishIsActiveAsync(Guid dishId, UpdateDishIsActiveRequest request)
        {
            return PatchAsync<UpdateDishIsActiveRequest, DishResponse>(new Uri(BaseUri + MenuRouting.BaseDishURLSlash + dishId.ToString()), request);
        }

        public Task DeleteDishAsync(Guid dishId)
        {
            return DeleteAsync(new Uri(BaseUri + MenuRouting.BaseDishURLSlash + dishId.ToString()));

        }

        public Task<CalculateDishListCostResponse> PostDishListCostAsync(CalculateDishListPriceRequest dishesIds)
        {
            return PostAsync<CalculateDishListPriceRequest, CalculateDishListCostResponse>(new Uri(BaseUri + MenuRouting.BaseDishURLSlash + "totalCost"), dishesIds);
        }

        #endregion

        #region marketplace
        public Task<GetAllMenusResponse> GetOffersAsync()
        {
            return GetAsync<GetAllMenusResponse>(new Uri(BaseUri + MenuRouting.BaseMarketplaceURL));
        }
        public Task<MenuResponse> GetByProviderOffersIdAsync(Guid providerId)
        {
            return GetAsync<MenuResponse>(new Uri(BaseUri + MenuRouting.BaseMarketplaceURLSlash + providerId.ToString()));
        }

        public Task<MenuResponse> UpdateIsInMarketplaceAsync(Guid menuId, MarketplacePutRequest request)
        {
            return PutAsync<MarketplacePutRequest, MenuResponse>(new Uri(BaseUri + MenuRouting.BaseMarketplaceURLSlash + menuId.ToString()), request);
        }

        #endregion
    }
}
