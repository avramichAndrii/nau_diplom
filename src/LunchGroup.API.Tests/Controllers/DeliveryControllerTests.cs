﻿using LunchGroup.API.Contracts.Requests;
using LunchGroup.API.Contracts.Responses;
using LunchGroup.API.Controllers;
using LunchGroup.Domain.Models;
using LunchGroup.Domain.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xunit;

namespace LunchGroup.API.Tests.Controllers
{
    public class DeliveryControllerTests
    {
        public static IEnumerable<object[]> TestDelivery
        {
            get
            {
                yield return new object[] { Guid.NewGuid(), Guid.NewGuid(), DateTime.Now, StatusModel.Done };
            }
        }

        private readonly DeliveryController _deliveryController;
        private readonly Mock<IDeliveryDomainService> _deliveryServiceMock = new Mock<IDeliveryDomainService>();

        public DeliveryControllerTests()
        {
            _deliveryController = new DeliveryController(_deliveryServiceMock.Object);
        }

        [Theory, MemberData(nameof(TestDelivery))]
        public async void GetById_ExistingEntity_Test(Guid id, Guid orderId, DateTime time, StatusModel status)
        {
            // Arrange

            string targetStatus = StatusModel.Done.ToString();
            var modelToCreate = new DeliveryModel(id, orderId, time, status);
            _deliveryServiceMock.Setup(x => x.GetDeliveryByIdAsync(id)).ReturnsAsync(modelToCreate);

            // Act
            var result = await _deliveryController.GetDeliveryByIdAsync(id);

            // Assert
            var response = Assert.IsType<DeliveryResponse>(result.Value);
            Assert.Equal(targetStatus, response.Status);
        }

        [Theory, MemberData(nameof(TestDelivery))]
        public async void GetByGroupOrderId_ExistingEntity_Test(Guid id, Guid orderId, DateTime time, StatusModel status)
        {
            // Arrange
            string targetStatus = StatusModel.Done.ToString();
            var modelToCreate = new DeliveryModel(id, orderId, time, status);
            _deliveryServiceMock.Setup(x => x.GetDeliveryByGroupOrderIdAsync(id)).ReturnsAsync(modelToCreate);

            // Act
            var result = await _deliveryController.GetDeliveryByGroupOrderIdAsync(id);

            // Assert
            Assert.NotNull(result);
            var response = Assert.IsType<DeliveryResponse>(result.Value);
            Assert.Equal(targetStatus, response.Status);
        }

        [Fact]
        public async void GetAllDeliveries_ShouldReturn_CountOfTestModels_Items()
        {
            // Arrange
            _deliveryServiceMock.Setup(ms => ms.GetAllDeliveriesAsync()).ReturnsAsync(GetTestModels());

            // Act
            var result = await _deliveryController.GetAllDeliveriesAsync();

            // Assert
            var list = result.Value;
            Assert.NotNull(list);
            Assert.Equal(GetTestModels().Count, list.DeliveryCollection.Count);
        }

        private IReadOnlyCollection<DeliveryModel> GetTestModels()
        {
            List<DeliveryModel> sample = new List<DeliveryModel>
            {
            new DeliveryModel(Guid.NewGuid(), Guid.NewGuid(), DateTime.Now, StatusModel.Done),
            new DeliveryModel(Guid.NewGuid(), Guid.NewGuid(), DateTime.Now, StatusModel.Submited)
        };
            return new ReadOnlyCollection<DeliveryModel>(sample);
        }

        [Fact]
        public async void Get_All_Deliveries_ReturnsNull_WhenEmpty()
        {
            // Arrange
            _deliveryServiceMock.Setup(ms => ms.GetAllDeliveriesAsync())
                .ReturnsAsync(new List<DeliveryModel>());
            // Act
            var result = await _deliveryController.GetAllDeliveriesAsync();
            // Assert
            var list = result.Value;

            Assert.Empty(list.DeliveryCollection);
        }

        [Theory, MemberData(nameof(TestDelivery))]
        public async void Create_ShouldPass_WhenResponseNameAndTestNameEqual(Guid id, Guid orderId, DateTime time, StatusModel status)
        {
            // Arrange
            Guid targetOrderId = Guid.NewGuid();
            var modelToCreate = new DeliveryModel(id, targetOrderId, time, status);
            _deliveryServiceMock.Setup(x => x.CreateAsync(orderId))
                .ReturnsAsync(() =>
                {
                    return modelToCreate;
                });
            // Act
            var result = await _deliveryController.CreateDeliveryAsync(orderId);
            // Assert
            var response = result.Value;
            Assert.NotNull(response);
            Assert.Equal(targetOrderId, response.GroupOrderId);
        }

        [Theory, MemberData(nameof(TestDelivery))]
        public async void Update_RequestNameShouldBeDifferent_From_BaseName(Guid id, Guid orderId, DateTime time, StatusModel status)
        {
            // Arrange
            Guid requestedId = Guid.NewGuid();
            var modelToCreate = new DeliveryModel(id, orderId, time, status);
            _deliveryServiceMock.Setup(x => x.UpdateDeliveryAsync(requestedId, It.IsAny<DeliveryModel>()))
                .ReturnsAsync(() =>
                {
                    return modelToCreate;
                });
            // Act
            var request = new UpdateDeliveryRequest()
            {
                DeliveryId = Guid.NewGuid(),
                GroupOrderId = Guid.NewGuid(),
                DeliveryTime = DateTime.Now,
                Status = StatusModel.Done.ToString()
            };
            var result = await _deliveryController.UpdateDeliveryAsync(requestedId, request);

            // Assert
            var response = result.Value;
            Assert.NotNull(response);
            Assert.NotEqual(request.DeliveryId, response.DeliveryId);
            Assert.Equal(request.Status, response.Status);
        }
    }
}
