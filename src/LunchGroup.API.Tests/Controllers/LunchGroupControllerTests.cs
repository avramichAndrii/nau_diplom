﻿using AutoMapper;
using LunchGroup.API.Contracts.Requests;
using LunchGroup.API.Contracts.Responses;
using LunchGroup.API.Tests.TestModels;
using LunchGroup.Domain.Exceptions;
using LunchGroup.Domain.Models;
using LunchGroup.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace LunchGroup.API.Controllers.Tests
{
    public class LunchGroupControllerTests
    {
        [Fact]
        public async void GetLunchGroupsCount_WhenCollectionIsNotEmpty_ShouldReturn3()
        {
            // Arrange
            var mockService = new Mock<ILunchGroupDomainService>();
            Guid companyId = Guid.NewGuid();
            IReadOnlyCollection<LunchGroupModel> testModelsCollection = new List<LunchGroupModel>
            {
                new LunchGroupModel { Name = "LG" },
                new LunchGroupModel { Name = "LG" },
                new LunchGroupModel { Name = "LG" }
            };

            mockService.Setup(ms => ms.GetAllByCompanyAsync(companyId))
                .Returns(Task.FromResult(testModelsCollection));

            var mockMapper = new Mock<IMapper>();
            mockMapper.Setup(m => m.Map<GetLunchGroupResponse>(It.IsAny<LunchGroupModel>()))
                .Returns(new GetLunchGroupResponse { Name = "LG" });

            var controller = new LunchGroupController(mockService.Object, mockMapper.Object);

            // Act
            var result = await controller.GetAllLunchGroupsByCompanyAsync(companyId);

            // Assert
            var response = Assert.IsType<GetLunchGroupsCollectionResponse>(result.Value);
            Assert.Equal(testModelsCollection.Count, response.LunchGroupsCollection.Count);
            Assert.Equal(testModelsCollection.ElementAt(0).Name, response.LunchGroupsCollection.ElementAt(0).Name);
        }

        [Fact]
        public async void GetLunchGroupsByCompany_WhenItThrowsException_CausesBadRequest()
        {
            // Arrange
            var mockService = new Mock<ILunchGroupDomainService>();
            Guid companyId = Guid.NewGuid();
            mockService.Setup(ms => ms.GetAllByCompanyAsync(companyId))
                .Throws<LunchGroupDomainException>();

            var controller = new LunchGroupController(mockService.Object, null);

            // Act
            var result = await controller.GetAllLunchGroupsByCompanyAsync(companyId);

            // Assert
            var objectResult = Assert.IsType<BadRequestObjectResult>(result.Result);
        }

        [Fact]
        public async void GetTimetable_WhenLunchGroupExists()
        {
            // Arrange
            var mockService = new Mock<ILunchGroupDomainService>();
            Guid groupId = Guid.NewGuid();
            var testModel = new LunchGroupModel
            {
                SubmitDeadline = new DateTime(2020, 6, 26)
            };
            
            mockService.Setup(ms => ms.GetByIdAsync(groupId))
                .Returns(Task.FromResult(testModel));

            var mockMapper = new Mock<IMapper>();
            var expectedResponse = new GetTimetableResponse(
                testModel.SubmitDeadline, 
                new List<DateTime>() { DateTime.Now });
            mockMapper.Setup(m => m.Map<GetTimetableResponse>(testModel))
                .Returns(expectedResponse);

            var controller = new LunchGroupController(mockService.Object, mockMapper.Object);

            // Act
            var result = await controller.GetTimetableAsync(groupId);

            // Assert
            var actualResponse = Assert.IsType<GetTimetableResponse>(result.Value);
            Assert.Equal(testModel.SubmitDeadline, actualResponse.SubmitDeadline);
            Assert.Equal(expectedResponse.AvailableDates.Count, actualResponse.AvailableDates.Count);
        }

        [Fact]
        public async void GetTimetable_WhenLunchGroupNotExists_CausesBadRequest()
        {
            // Arrange
            var mockService = new Mock<ILunchGroupDomainService>();
            Guid groupId = Guid.NewGuid();
            mockService.Setup(ms => ms.GetByIdAsync(groupId))
                .Throws(new LunchGroupNotFoundException(groupId));

            var controller = new LunchGroupController(mockService.Object, null);

            // Act
            var result = await controller.GetTimetableAsync(groupId);

            // Assert
            var objectResult = Assert.IsType<BadRequestObjectResult>(result.Result);
            Assert.Contains(groupId.ToString(), objectResult.Value.ToString());
        }

        [Fact]
        public async void UpdateTimetable_WhenLunchGroupExists_ShouldReturnOk()
        {
            // Arrange
            var mockService = new Mock<ILunchGroupDomainService>();
            Guid groupId = Guid.NewGuid();
            mockService.Setup(ms => ms.UpdateTimetableAsync(groupId))
                .Returns(Task.FromResult(default(object)));

            var controller = new LunchGroupController(mockService.Object, null);

            // Act
            var result = await controller.UpdateTimetableAsync(groupId);

            // Assert
            var actualResponse = Assert.IsType<OkResult>(result);
        }

        [Fact]
        public async void UpdateTimetable_WhenLunchGroupNotExists_CausesBadRequest()
        {
            // Arrange
            var mockService = new Mock<ILunchGroupDomainService>();
            Guid groupId = Guid.NewGuid();
            mockService.Setup(ms => ms.UpdateTimetableAsync(groupId))
                .Throws(new LunchGroupNotFoundException(groupId));

            var controller = new LunchGroupController(mockService.Object, null);

            // Act
            var result = await controller.UpdateTimetableAsync(groupId);

            // Assert
            var objectResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Contains(groupId.ToString(), objectResult.Value.ToString());
        }

        [Fact]
        public async void SetActiveFlag_WhenLunchGroupExists_ShouldReturnOk()
        {
            // Arrange
            var mockService = new Mock<ILunchGroupDomainService>();
            Guid groupId = Guid.NewGuid();
            var request = new SetActiveFlagRequest { IsActive = false };
            mockService.Setup(ms => ms.SetActiveFlagAsync(groupId, request.IsActive))
                .Returns(Task.FromResult(default(object)));

            var controller = new LunchGroupController(mockService.Object, null);

            // Act
            var result = await controller.SetActiveFlagAsync(groupId, request);

            // Assert
            var actualResponse = Assert.IsType<OkResult>(result);
        }

        [Fact]
        public async void SetActiveFlag_WhenLunchGroupNotExists_CausesBadRequest()
        {
            // Arrange
            var mockService = new Mock<ILunchGroupDomainService>();
            Guid groupId = Guid.NewGuid();
            var request = new SetActiveFlagRequest { IsActive = false };
            mockService.Setup(ms => ms.SetActiveFlagAsync(groupId, request.IsActive))
                .Throws(new LunchGroupNotFoundException(groupId));

            var controller = new LunchGroupController(mockService.Object, null);

            // Act
            var result = await controller.SetActiveFlagAsync(groupId, request);

            // Assert
            var objectResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Contains(groupId.ToString(), objectResult.Value.ToString());
        }


        [Fact]
        public async Task GetLunchGroupsById_WhenLunchGroupExists_ShouldReturnOkAsync()
        {
            // Arrange
            var mockService = new Mock<ILunchGroupDomainService>();
            var mockMapper = new Mock<IMapper>();
            Guid groupId = Guid.NewGuid();
            var request = groupId;
            
            mockService.Setup(ms => ms.GetByIdAsync(groupId))
                .Returns(Task.FromResult(ModelsForLunchGroupTests.lunchGroupModelServiceResponse));
            mockMapper.Setup(mm => mm.Map<LunchGroupResponse>(ModelsForLunchGroupTests.lunchGroupModelServiceResponse))
                .Returns(ModelsForLunchGroupTests.lunchGroupResponse);

            var controller = new LunchGroupController(mockService.Object, mockMapper.Object);

            // Act
            var result = await controller.GetLunchGroupByIdAsync(groupId);

            // Assert
            var actualResponse = Assert.IsType<OkObjectResult>(result.Result);
        }

        [Fact]
        public async Task GetLunchGroupsByManager_WhenLunchGroupExists_ShouldReturnOkAsync()
        {
            // Arrange
            var mockService = new Mock<ILunchGroupDomainService>();
            var mockMapper = new Mock<IMapper>();
            Guid groupId = Guid.NewGuid();
            IReadOnlyCollection<LunchGroupModel> request = new List<LunchGroupModel>
            {
                ModelsForLunchGroupTests.lunchGroupModelServiceResponse
            };
            LunchGroupModel[] seviceResponse = ModelsForLunchGroupTests.multipleGroupsSeviceResponse;

            mockService.Setup(ms => ms.GetAllByManagerIdAsync(ModelsForLunchGroupTests.testGuid))
                .ReturnsAsync(request);
            mockMapper.Setup(mm => mm.Map<LunchGroupResponse>(seviceResponse[0]))
                .Returns(ModelsForLunchGroupTests.lunchGroupResponse);

            var controller = new LunchGroupController(mockService.Object, mockMapper.Object);

            // Act
            var result = await controller.GetLunchGroupsByManagerAsync(ModelsForLunchGroupTests.testGuid);

            // Assert
            var actualResponse = Assert.IsType<OkObjectResult>(result.Result);
        }

        [Fact]
        public async Task GetManagerLunchGroupTest_WhenLunchGroupExists_ShouldReturnOkAsync()
        {
            // Arrange
            var mockService = new Mock<ILunchGroupDomainService>();
            var mockMapper = new Mock<IMapper>();
            Guid groupId = Guid.NewGuid();
            var request = groupId;

            mockService.Setup(ms => ms.GetAllByManagerIdAsync(ModelsForLunchGroupTests.testGuid))
                .ReturnsAsync(ModelsForLunchGroupTests.multipleGroupsSeviceResponse.ToList());

            mockMapper.Setup(mm => mm.Map<GetLunchGroupResponse>(ModelsForLunchGroupTests.lunchGroupModelServiceResponse))
                .Returns(ModelsForLunchGroupTests.getLunchGroupResponse);

            var controller = new LunchGroupController(mockService.Object, mockMapper.Object);

            // Act
            var result = await controller.GetLunchGroupsByManagerAsync(ModelsForLunchGroupTests.testGuid);

            // Assert
            var actualResponse = Assert.IsType<OkObjectResult>(result.Result);
        }

        [Fact]
        public async void CreateLunchGroup_ShouldReturnOkAsync()
        {
            // Arrange
            var mockService = new Mock<ILunchGroupDomainService>();
            var mockMapper = new Mock<IMapper>();
            Guid groupId = Guid.NewGuid();
            var request = new CreateLunchGroupRequest() { Members = new Guid[] { ModelsForLunchGroupTests.testGuid } };


            mockService.Setup(ms => ms.CreateAsync(ModelsForLunchGroupTests.lunchGroupModelServiceResponse))
                .Returns(Task.FromResult(ModelsForLunchGroupTests.lunchGroupModelServiceResponse));
            mockMapper.Setup(mm => mm.Map<LunchGroupModel>(request))
                .Returns(ModelsForLunchGroupTests.lunchGroupModelServiceResponse);

            var controller = new LunchGroupController(mockService.Object, mockMapper.Object);

            // Act
            var result = await controller.CreateLunchGroupAsync(request);

            // Assert
            var actualResponse = Assert.IsType<OkObjectResult>(result.Result);
        }

        [Fact]
        public async void UpdateLunchGroup_ShouldReturnOkAsync()
        {
            // Arrange
            var mockService = new Mock<ILunchGroupDomainService>();
            var mockMapper = new Mock<IMapper>();
            Guid groupId = Guid.NewGuid();
            var request = new UpdateLunchGroupRequest() { Members = new Guid[] { ModelsForLunchGroupTests.testGuid } };


            mockService.Setup(ms => ms.UpdateAsync(groupId, ModelsForLunchGroupTests.lunchGroupModelServiceResponse))
                .Returns(Task.FromResult(ModelsForLunchGroupTests.lunchGroupModelServiceResponse));
            mockMapper.Setup(mm => mm.Map<LunchGroupModel>(request))
                .Returns(ModelsForLunchGroupTests.lunchGroupModelServiceResponse);

            var controller = new LunchGroupController(mockService.Object, mockMapper.Object);

            // Act
            var result = await controller.UpdateLunchGroupAsync(request, groupId);

            // Assert
            var actualResponse = Assert.IsType<OkObjectResult>(result.Result);
        }
    }
}