﻿using LunchGroup.API.Contracts.Requests;
using System;
using System.Collections.Generic;
using Xunit;

namespace LunchGroup.API.Validation.Tests
{
    public class LunchGroupRequestValidatorTests
    {
        private static readonly IHaveEditableLunchGroupDataRequest validLunchGroupRequest =
            new CreateLunchGroupRequest
            {
                SubmitDeadline = new DateTime(2020, 6, 27, 16, 0, 0),
                PeriodStart = new DateTime(2020, 6, 29),
                Weekends = new string[] { "Sunday" },
                Periodicity = 1,
                Name = "TestName",
                Members = new Guid[] { Guid.NewGuid() }
            };
        private static readonly AddressRequest validAddressObject =
            new AddressRequest
            {
                City = "Kyiv",
                Street = "Vatslava Gavela",
                BuildingNumber = "8",
                Office = "3"
            };

        [Theory, MemberData(nameof(TestCases))]
        public void TestLunchGroupRequestValidation(IHaveEditableLunchGroupDataRequest request, bool expectedResult)
        {
            // Arrange
            var crv = new LunchGroupRequestValidator();
            // Act
            bool actualResult = crv.Validate(request).IsValid;
            // Assert
            Assert.Equal(expectedResult, actualResult);
        }

        public static IEnumerable<object[]> TestCases
        {
            get
            {
                #region WeeklyPeriod
                #region Deadline
                yield return new object[]
                {
                    new CreateLunchGroupRequest
                    {
                        SubmitDeadline = new DateTime(2020, 6, 24),
                        PeriodStart = new DateTime(2020, 6, 24),
                        Periodicity = 7,
                        Weekends = new string[]{ },
                        Name = "TestName",
                        Members = new Guid[]{ Guid.NewGuid() },
                        Address = validAddressObject
                    }, false
                };
                yield return new object[]
                {
                    new CreateLunchGroupRequest
                    {
                        SubmitDeadline = new DateTime(2020, 6, 29),
                        PeriodStart = new DateTime(2020, 6, 29),
                        Periodicity = 7,
                        Weekends = new string[]{ },
                        Name = "TestName",
                        Members = new Guid[]{ Guid.NewGuid() },
                        Address = validAddressObject
                    }, false
                };
                yield return new object[]
                {
                    new CreateLunchGroupRequest
                    {
                        SubmitDeadline = new DateTime(2020, 6, 24),
                        PeriodStart = new DateTime(2020, 6, 23),
                        Periodicity = 7,
                        Weekends = new string[]{ },
                        Name = "TestName",
                        Members = new Guid[]{ Guid.NewGuid() },
                        Address = validAddressObject
                    }, false
                };
                yield return new object[]
                {
                    new CreateLunchGroupRequest
                    {
                        SubmitDeadline = new DateTime(2020, 6, 21),
                        PeriodStart = new DateTime(2020, 6, 29),
                        Periodicity = 7,
                        Weekends = new string[]{ },
                        Name = "TestName",
                        Members = new Guid[]{ Guid.NewGuid() },
                        Address = validAddressObject
                    }, false
                };
                yield return new object[]
                {
                    new CreateLunchGroupRequest
                    {
                        SubmitDeadline = new DateTime(2020, 6, 24),
                        PeriodStart = new DateTime(2020, 6, 29),
                        Periodicity = 7,
                        Weekends = new string[]{ },
                        Name = "TestName",
                        Members = new Guid[]{ Guid.NewGuid() },
                        Address = validAddressObject
                    }, true
                };
                #endregion
                #region NotWeekendTest
                yield return new object[]
                {
                    new CreateLunchGroupRequest
                    {
                        SubmitDeadline = new DateTime(2020, 6, 24),
                        PeriodStart = new DateTime(2020, 6, 27),
                        Weekends = new string[]{ "Saturday", "Sunday" },
                        Periodicity = 7,
                        Name = "TestName",
                        Members = new Guid[]{ Guid.NewGuid() },
                        Address = validAddressObject
                    }, false
                };
                yield return new object[]
                {
                    new CreateLunchGroupRequest
                    {
                        SubmitDeadline = new DateTime(2020, 6, 27),
                        PeriodStart = new DateTime(2020, 6, 29),
                        Weekends = new string[]{ "Saturday", "Sunday" },
                        Periodicity = 7,
                        Name = "TestName",
                        Members = new Guid[]{ Guid.NewGuid() },
                        Address = validAddressObject
                    }, false
                };
                yield return new object[]
                {
                    new CreateLunchGroupRequest
                    {
                        SubmitDeadline = new DateTime(2020, 6, 25),
                        PeriodStart = new DateTime(2020, 6, 29),
                        Weekends = new string[]{ "Saturday", "Sunday" },
                        Periodicity = 7,
                        Name = "TestName",
                        Members = new Guid[]{ Guid.NewGuid() },
                        Address = validAddressObject
                    }, true
                };
                #endregion
                #endregion

                #region DailyPeriod
                #region Deadline
                yield return new object[]
                {
                    new CreateLunchGroupRequest
                    {
                        SubmitDeadline = new DateTime(2020, 6, 24),
                        PeriodStart = new DateTime(2020, 6, 24),
                        Periodicity = 1,
                        Weekends = new string[]{ },
                        Name = "TestName",
                        Members = new Guid[]{ Guid.NewGuid() },
                        Address = validAddressObject
                    }, false
                };
                yield return new object[]
                {
                    new CreateLunchGroupRequest
                    {
                        SubmitDeadline = new DateTime(2020, 6, 24),
                        PeriodStart = new DateTime(2020, 6, 25),
                        Periodicity = 1,
                        Weekends = new string[]{ },
                        Name = "TestName",
                        Members = new Guid[]{ Guid.NewGuid() },
                        Address = validAddressObject
                    }, false
                };
                yield return new object[]
                {
                    new CreateLunchGroupRequest
                    {
                        SubmitDeadline = new DateTime(2020, 6, 25),
                        PeriodStart = new DateTime(2020, 6, 24),
                        Periodicity = 1,
                        Weekends = new string[]{ },
                        Name = "TestName",
                        Members = new Guid[]{ Guid.NewGuid() },
                        Address = validAddressObject
                    }, false
                };
                yield return new object[]
                {
                    new CreateLunchGroupRequest
                    {
                        SubmitDeadline = new DateTime(2020, 6, 23),
                        PeriodStart = new DateTime(2020, 6, 25),
                        Periodicity = 1,
                        Weekends = new string[]{ },
                        Name = "TestName",
                        Members = new Guid[]{ Guid.NewGuid() },
                        Address = validAddressObject
                    }, false
                };
                yield return new object[]
                {
                    new CreateLunchGroupRequest
                    {
                        SubmitDeadline = new DateTime(2020, 6, 24, 18, 0, 0),
                        PeriodStart = new DateTime(2020, 6, 25),
                        Periodicity = 1,
                        Weekends = new string[]{ },
                        Name = "TestName",
                        Members = new Guid[]{ Guid.NewGuid() },
                        Address = validAddressObject
                    }, true
                };
                #endregion
                #region NotWeekendTest
                yield return new object[]
                {
                    new CreateLunchGroupRequest
                    {
                        SubmitDeadline = new DateTime(2020, 6, 24),
                        PeriodStart = new DateTime(2020, 6, 28),
                        Weekends = new string[]{ "Sunday" },
                        Periodicity = 1,
                        Name = "TestName",
                        Members = new Guid[]{ Guid.NewGuid() },
                        Address = validAddressObject
                    }, false
                };
                yield return new object[]
                {
                    new CreateLunchGroupRequest
                    {
                        SubmitDeadline = new DateTime(2020, 6, 28),
                        PeriodStart = new DateTime(2020, 6, 29),
                        Weekends = new string[]{ "Sunday" },
                        Periodicity = 1,
                        Name = "TestName",
                        Members = new Guid[]{ Guid.NewGuid() },
                        Address = validAddressObject
                    }, false
                };
                yield return new object[]
                {
                    new CreateLunchGroupRequest
                    {
                        SubmitDeadline = new DateTime(2020, 6, 27, 16, 0, 0),
                        PeriodStart = new DateTime(2020, 6, 29),
                        Weekends = new string[]{ "Sunday" },
                        Periodicity = 1,
                        Name = "TestName",
                        Members = new Guid[]{ Guid.NewGuid() },
                        Address = validAddressObject
                    }, true
                };
                #endregion
                #endregion

                #region Weekends
                yield return new object[]
                {
                    new CreateLunchGroupRequest
                    {
                        SubmitDeadline = new DateTime(2020, 6, 24),
                        PeriodStart = new DateTime(2020, 6, 25),
                        Periodicity = 7,
                        Weekends = new string[]{ "Sapturday" },
                        Name = "TestName",
                        Members = new Guid[]{ Guid.NewGuid() },
                        Address = validAddressObject
                    }, false
                };
                yield return new object[]
                {
                    new CreateLunchGroupRequest
                    {
                        SubmitDeadline = new DateTime(2020, 6, 24),
                        PeriodStart = new DateTime(2020, 6, 25),
                        Periodicity = 7,
                        Weekends = new string[]{ "Sunday", "Monday", "Tuesday", "", "Thursday", "Friday", "Saturday" },
                        Name = "TestName",
                        Members = new Guid[]{ Guid.NewGuid() },
                        Address = validAddressObject
                    }, false
                };
                #endregion

                #region Name'n'Members
                yield return new object[]
                {
                    new CreateLunchGroupRequest
                    {
                        SubmitDeadline = new DateTime(2020, 6, 27, 16, 0, 0),
                        PeriodStart = new DateTime(2020, 6, 29),
                        Weekends = new string[]{ "Sunday" },
                        Periodicity = 1,
                        Name = null,
                        Members = new Guid[]{ Guid.NewGuid() },
                        Address = validAddressObject
                    }, false
                };
                yield return new object[]
                {
                    new CreateLunchGroupRequest
                    {
                        SubmitDeadline = new DateTime(2020, 6, 27, 16, 0, 0),
                        PeriodStart = new DateTime(2020, 6, 29),
                        Weekends = new string[]{ "Sunday" },
                        Periodicity = 1,
                        Name = "",
                        Members = new Guid[]{ Guid.NewGuid() },
                        Address = validAddressObject
                    }, false
                };
                yield return new object[]
                {
                    new CreateLunchGroupRequest
                    {
                        SubmitDeadline = new DateTime(2020, 6, 27, 16, 0, 0),
                        PeriodStart = new DateTime(2020, 6, 29),
                        Weekends = new string[]{ "Sunday" },
                        Periodicity = 1,
                        Name = "TestName",
                        Members = new Guid[]{ },
                        Address = validAddressObject
                    }, false
                };
                yield return new object[]
                {
                    new CreateLunchGroupRequest
                    {
                        SubmitDeadline = new DateTime(2020, 6, 27, 16, 0, 0),
                        PeriodStart = new DateTime(2020, 6, 29),
                        Weekends = new string[]{ "Sunday" },
                        Periodicity = 1,
                        Name = "TestName",
                        Members = new Guid[]{ Guid.NewGuid() },
                        Address = validAddressObject
                    }, true
                };
                #endregion

                #region Address
                var invalidAddressRequest =
                    new AddressRequest
                    {
                        City = null,
                        Street = "Vatslava Gavela",
                        BuildingNumber = "8",
                        Office = "3"
                    };
                validLunchGroupRequest.Address = invalidAddressRequest;
                yield return new object[]
                {
                    validLunchGroupRequest, false
                };
                invalidAddressRequest =
                    new AddressRequest
                    {
                        City = "Kyiv",
                        Street = "",
                        BuildingNumber = "8",
                        Office = "3"
                    };
                validLunchGroupRequest.Address = invalidAddressRequest;
                yield return new object[]
                {
                    validLunchGroupRequest, false
                };
                invalidAddressRequest =
                    new AddressRequest
                    {
                        City = "Kyiv",
                        Street = "Vatslava Gavela",
                        BuildingNumber = "8",
                        Office = "   "
                    };
                validLunchGroupRequest.Address = invalidAddressRequest;
                yield return new object[]
                {
                    validLunchGroupRequest, false
                };
                invalidAddressRequest =
                    new AddressRequest
                    {
                        City = "Kyiv",
                        Street = "Vatslava Gavela",
                        BuildingNumber = "123456789",
                        Office = "3"
                    };
                validLunchGroupRequest.Address = invalidAddressRequest;
                yield return new object[]
                {
                    validLunchGroupRequest, false
                };
                #endregion
            }
        }
    }
}