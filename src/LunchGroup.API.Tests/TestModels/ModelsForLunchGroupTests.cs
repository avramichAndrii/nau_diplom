﻿using LunchGroup.API.Contracts.Responses;
using LunchGroup.Domain.Models;
using System;
using System.Collections.Generic;

namespace LunchGroup.API.Tests.TestModels
{
    public static class ModelsForLunchGroupTests
    {
        public static Guid testGuid = Guid.Parse("08d81c00-9886-40fe-80b9-59b2f1ee0779");
        public static Guid[] testGuids = new Guid[] { Guid.Parse("08d81c00-9886-40fe-80b9-59b2f1ee0779") };
        public static LunchGroupModel lunchGroupModelServiceResponse = new LunchGroupModel
        {
            Address = new AddressModel
            {
                City = "test",
                Office = "test",
                Street = "1",
                BuildingNumber = "23"
            },
            CompanyId = Guid.Parse("08d81c00-9886-40fe-80b9-59b2f1ee0779"),
            IsActive = false,
            Name = "test",
            SubmitDeadline = DateTime.Parse("2018 - 08 - 09"),
            PeriodStart = DateTime.Parse("2018-08-10"),
            Periodicity = 7,
            Weekends = new List<DayOfWeek>() { DayOfWeek.Monday },
        };

        public static GetLunchGroupResponse getLunchGroupResponse = new GetLunchGroupResponse
        {
            CompanyId = Guid.Parse("08d81c00-9886-40fe-80b9-59b2f1ee0779"),
            Name = "test",
            SubmitDeadline = DateTime.Parse("2018 - 08 - 09"),
            PeriodStart = DateTime.Parse("2018-08-10"),
            Periodicity = 7,
            Weekends = new List<string> { "Monday" }
        };

        public static LunchGroupModel[] multipleGroupsSeviceResponse = new LunchGroupModel[] { new LunchGroupModel
            {
                Address = new AddressModel
                {
                    City = "test",
                    Office = "test",
                    Street = "1",
                    BuildingNumber = "23"
                },
                CompanyId = Guid.Parse("08d81c00-9886-40fe-80b9-59b2f1ee0779"),
                IsActive = false,
                Name = "test",
                SubmitDeadline = DateTime.Parse("2018 - 08 - 09"),
                PeriodStart = DateTime.Parse("2018-08-10"),
                Periodicity = 7,
                Weekends = new List<DayOfWeek>() { DayOfWeek.Monday },
            },
            new LunchGroupModel
            {
                Address = new AddressModel
                {
                    City = "test2",
                    Office = "test2",
                    Street = "12",
                    BuildingNumber = "232"
                },
                CompanyId = Guid.Parse("08d81c00-9886-40fe-80b9-59b2f1ee0779"),
                IsActive = false,
                Name = "test2",
                SubmitDeadline = DateTime.Parse("2018 - 08 - 09"),
                PeriodStart = DateTime.Parse("2018-08-10"),
                Periodicity = 7,
                Weekends = new List<DayOfWeek>() { DayOfWeek.Monday },
            }
        };

        public static LunchGroupResponse lunchGroupResponse = new LunchGroupResponse
        {
            Address = new AddressResponse
            {
                City = "test",
                Office = "test",
                Street = "1",
                BuildingNumber = "23"
            },
            CompanyId = Guid.Parse("08d81c00-9886-40fe-80b9-59b2f1ee0779"),
            IsActive = false,
            Name = "test",
            SubmitDeadline = DateTime.Parse("2018 - 08 - 09"),
            PeriodStart = DateTime.Parse("2018-08-10"),
            Periodicity = 7,
            Weekends = new List<string>() { "Monday" },
            Members = new List<Guid> { Guid.Parse("08d81c00-9886-40fe-80b9-59b2f1ee0779") }
        };
    }
}
