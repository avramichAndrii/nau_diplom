﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using UserManagement.API.Contracts.Requests;
using UserManagement.API.Contracts.Responses;
using UserManagement.Application.Helpers;
using UserManagement.Domain.Models;
using UserManagement.Domain.Services;

namespace UserManagement.API.Controllers
{
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public UserController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> AuthenticateAsync([FromBody]AuthenticateRequest model)
        {
            var authenticateModel = await _userService.AuthenticateAsync(model.Login, model.Password);
            if (authenticateModel == null)
            {
                return Unauthorized(new { message = "Incorrect login or password" });
            }
            return Ok(_mapper.Map<AuthenticateResponse>(authenticateModel));
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> RegisterAsync([FromBody]RegisterRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new { message = ModelState.Select(x => x.Value.Errors)
                           .Where(y => y.Count > 0)
                           .ToList()
                });
            }

            var user = _mapper.Map<User>(request);

            try
            {
                var createdUser = await _userService.CreateAsync(user, request.Password);
                var response = _mapper.Map<UserResponse>(createdUser);
                return Ok(response);
            }
            catch (UserDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpGet("users")]
        public async Task<ActionResult<UsersCollectionResponse>> GetAllAsync()
        {
            try
            {
                var users = await _userService.GetAllAsync();
                var responseList = users.Select(user => _mapper.Map<UserResponse>(user)).ToList();
                return Ok(new UsersCollectionResponse(responseList));
            }
            catch (UserDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpGet("users/company/{companyId}/role/{role}")]
        public async Task<ActionResult<UsersCollectionResponse>> GetAllByCompanyAndRoleAsync(Guid companyId, string role)
        {
            try
            {
                var users = await _userService.GetAllByCompanyAndRoleAsync(companyId, role);
                var responseList = users.Select(user => _mapper.Map<UserResponse>(user)).ToList();
                return Ok(new UsersCollectionResponse(responseList));
            }
            catch (UserDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpGet("user/{id}")]
        public async Task<ActionResult<UserResponse>> GetByIdAsync(Guid id)
        {
            try
            {
                var user = await _userService.GetByIdAsync(id);
                var model = _mapper.Map<UserResponse>(user);
                return Ok(model);
            }
            catch (UserDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }         
        }

        [HttpPut("user/{id}")]
        public async Task<ActionResult<UserResponse>> UpdateAsync(Guid id, [FromBody]UpdateRequest model)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(new
                {
                    message = ModelState.Select(x => x.Value.Errors)
                           .Where(y => y.Count > 0)
                           .ToList()
                });
            }

            var user = _mapper.Map<User>(model);
            user.SetId(id);

            try
            {
                await _userService.UpdateAsync(user);
                return Ok(model);
            }
            catch (UserDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpDelete("user/{id}")]
        public async Task<ActionResult<Guid>> DeleteAsync(Guid id)
        {
            try
            {
                var user = await _userService.DeleteAsync(id);
                return Ok(user.Id);
            }
            catch (UserDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpPatch("user/{id}")]
        public async Task<ActionResult<UserResponse>> SetCompanyForUserAsync(Guid id, [FromBody] SetCompanyRequest request)
        {
            try
            {
                await _userService.SetCompanyAsync(id, request.CompanyId);
                return Ok();
            }
            catch (UserDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpPost("user/reset-password")]
        public async Task<ActionResult<UserResponse>> ResetPasswordAsync([FromBody] ResetPasswordRequest model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        message = ModelState.Select(x => x.Value.Errors)
                               .Where(y => y.Count > 0)
                               .ToList()
                    });
                };
                var user = await _userService.ResetPassword(model.Token, model.Password);
                return Ok(_mapper.Map<UserResponse>(user));
            }
            catch (UserDomainException)
            {
                return BadRequest(new { message = "Unable to change password" });
            }

        }

        [HttpPost("user/forgot-password")]
        public async Task<ActionResult<HandleForgottenPasswordResponse>> HandleForgottenPasswordAsync([FromBody]ForgotPasswordRequest model)
        {
            try
            {
                var email = await _userService.HandleForgottenPassword(model.Email);
                return Ok( new HandleForgottenPasswordResponse(){ Email = email });
            }
            catch (UserDomainException)
            {
                return BadRequest(new { message = "Wrong email" });
            }
        }

        [HttpPut("user/change-password/{id}")]
        public async Task<IActionResult> UpdatePasswordAsync(Guid id, [FromBody] UpdatePasswordRequest model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        message = ModelState.Select(x => x.Value.Errors)
                               .Where(y => y.Count > 0)
                               .ToList()
                    });
                };
                await _userService.UpdatePasswordAsync(model.OldPassword, model.NewPassword, id);
                return Ok("Password was successfully changed");
            }
            catch (UserDomainException)
            {
                return BadRequest("Unable to change password");
            }
        }

        [HttpPost("refresh-token/{oldToken}")]
        public async  Task<IActionResult> RefreshTokenAsync(Guid oldToken)
        {
            try
            {
                var authenticateModel = await _userService.RefreshTokenAsync(oldToken);
                return Ok(_mapper.Map<AuthenticateResponse>(source: authenticateModel));
            }
            catch
            {
                return BadRequest("Unable to refresh token");
            }
        }
    }
}