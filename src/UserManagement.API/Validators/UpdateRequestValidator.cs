﻿using FluentValidation;
using UserManagement.API.Contracts.Requests;
namespace UserManagement.API.Validators
{
    public class UpdateRequestValidator : AbstractValidator<UpdateRequest>
    {
        private readonly int minPassLength = 8;
        private readonly int minLoginLength = 4;

        private const string notNull = "must not be empty";
        private const string notEmpty = "must not be empty or consist of whitespaces only";

        public UpdateRequestValidator()
        {
            RuleFor(user => user.Login).NotNull().WithMessage("Login " + notNull);
            RuleFor(user => user.Login).NotEmpty().WithMessage("Login " + notEmpty);
            RuleFor(user => user.Login.Length).GreaterThanOrEqualTo(minLoginLength).WithMessage("Your Login must be at least " + minLoginLength + " characters long");
            RuleFor(user => user.FirstName).NotNull();
            RuleFor(user => user.FirstName).NotEmpty();
            RuleFor(user => user.LastName).NotNull();
            RuleFor(user => user.LastName).NotEmpty();
        }
    }
}
