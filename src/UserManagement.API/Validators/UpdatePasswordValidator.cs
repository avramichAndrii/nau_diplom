﻿using FluentValidation;
using UserManagement.API.Contracts.Requests;

namespace UserManagement.API.Validators
{
    public class UpdatePasswordValidator : AbstractValidator<UpdatePasswordRequest>
    {
        private const string _empty = "Please enter ";
        private readonly int minPassLength = 8;

        public UpdatePasswordValidator()
        {
            RuleFor(request => request.NewPassword).NotEmpty().WithMessage(_empty + "new password");
            RuleFor(request => request.OldPassword).NotEmpty().WithMessage(_empty + "old password");
            RuleFor(request => request.NewPassword.Length).GreaterThanOrEqualTo(minPassLength).WithMessage("Your password must be at least " + minPassLength + " characters long");
            RuleFor(request => request.OldPassword.Length).GreaterThanOrEqualTo(minPassLength).WithMessage("Your password must be at least " + minPassLength + " characters long");
        }

    }
}
