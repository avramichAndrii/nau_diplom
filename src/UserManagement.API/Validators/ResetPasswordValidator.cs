﻿using FluentValidation;
using UserManagement.API.Contracts.Requests;

namespace UserManagement.API.Validators
{
    public class ResetPasswordValidator : AbstractValidator<ResetPasswordRequest>
    {
        private const string _empty = "Please enter ";
        private readonly int minPassLength = 8;

        public ResetPasswordValidator()
        {
            RuleFor(request => request.Password).NotEmpty().WithMessage(_empty + "password");
            RuleFor(request => request.ConfirmPassword).NotEmpty().WithMessage(_empty + "confirm password");
            RuleFor(request => request.Token).NotEmpty().WithMessage(_empty + "token");
            RuleFor(request => request.Password.Length).GreaterThanOrEqualTo(minPassLength).WithMessage("Your password must be at least " + minPassLength + " characters long");
            RuleFor(x => x).Custom((x, context) =>
            {
                if (x.Password != x.ConfirmPassword)
                {
                    context.AddFailure(nameof(x.Password), "Passwords should match");
                }
            });
        }
  
    }
}
