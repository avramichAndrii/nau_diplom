﻿using AutoMapper;
using UserManagement.API.Contracts.Requests;
using UserManagement.API.Contracts.Responses;
using UserManagement.Domain.Models;

namespace UserManagement.API.Mappers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserResponse>();
            CreateMap<RegisterRequest, User>();
            CreateMap<UpdateRequest, User>();
            CreateMap<ValidateResetTokenRequest, RefreshToken>();
            CreateMap<AuthenticateModel, AuthenticateResponse>();
        }
    }
}
