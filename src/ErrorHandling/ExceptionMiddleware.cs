using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Infrastructure.HttpClientAbstractions;

namespace ErrorHandling
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        
        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (ClientErrorHttpResponseException ex)
            {
                await HandleExceptionAsync(httpContext, ex);
            }
        }

        private Task HandleExceptionAsync(HttpContext context, ClientErrorHttpResponseException exception)
        {
            string errorMessage = exception.Message;
            if (string.IsNullOrEmpty(errorMessage))
            {
                errorMessage = "Internal Server Error from custom middleware";
            }

            context.Response.StatusCode = (int)exception.StatusCode;
            return context.Response.WriteAsync(errorMessage);
        }
    }
}