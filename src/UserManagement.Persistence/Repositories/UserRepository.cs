﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserManagement.Application.Helpers;
using UserManagement.Domain.Models;
using UserManagement.Domain.Repositories;
using UserManagement.Persistence.Contexts;

namespace UserManagement.Persistence.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly UserManagementContext _userContext;
        private const string IdNotFoundMessage = "User with requested id not found";

        public UserRepository(UserManagementContext userContext)
        {
            _userContext = userContext;
            _userContext.Database.EnsureCreated();
        }

        public async Task<User> CreateAsync(User userModel)
        {
            try
            {
                _userContext.Users.Add(userModel);
                await _userContext.SaveChangesAsync();
                return userModel;
            }
            catch (InvalidOperationException ioex)
            {
                throw new UserDomainException(ioex.Message);

            }
        }

        public async Task<User> DeleteAsync(Guid userId)
        {
            try
            {
                var userToRemove = await _userContext.Users.SingleOrDefaultAsync(user => user.Id == userId);
                _userContext.Users.Remove(userToRemove);
                await _userContext.SaveChangesAsync();
                return userToRemove;
            }
            catch (InvalidOperationException)
            {
                throw new UserDomainException(IdNotFoundMessage);
            }
        }

        public async Task<IReadOnlyCollection<User>> GetAllAsync()
        {
            try
            {
                return await _userContext.Users.ToListAsync();
            }
            catch (InvalidOperationException ioex)
            {
                throw new UserDomainException(ioex.Message);
            }
        }

        public async Task<IReadOnlyCollection<User>> GetAllByCompanyAndRoleAsync(Guid companyId, string role)
        {
            try
            {
                return await _userContext.Users.Where(user => user.CompanyId == companyId && user.Role == role).ToListAsync();
            }
            catch (InvalidOperationException ioex)
            {
                throw new UserDomainException(ioex.Message);
            }
        }

        public async Task<User> GetByIdAsync(Guid userId)
        {
            try
            {
                User user = await _userContext.Users.SingleOrDefaultAsync(user => user.Id == userId);
                if (user == null)
                {
                    throw new UserDomainException(IdNotFoundMessage);
                }
                return user;
            }
            catch (InvalidOperationException)
            {
                throw new UserDomainException(IdNotFoundMessage);
            }
        }

        public async Task<User> UpdateAsync(User newUser)
        {
            try
            {
                User oldUser = await _userContext.Users.SingleOrDefaultAsync(user => user.Id == newUser.Id);

                if (oldUser == null)
                {
                    throw new UserDomainException("User not found");
                }

                if (newUser.Login != oldUser.Login)
                {
                    if (GetByLoginAsync(newUser.Login).GetAwaiter().GetResult() != null)
                        throw new UserDomainException("Login " + newUser.Login + " is already taken");

                    oldUser.Login = newUser.Login;
                }

                oldUser.Email = newUser.Email;
                oldUser.FirstName = newUser.FirstName;
                oldUser.LastName = newUser.LastName;

                await _userContext.SaveChangesAsync();
                return newUser;
            }
            catch (InvalidOperationException ioex)
            {
                throw new UserDomainException(ioex.Message);
            }
        }

        public async Task<User> GetByLoginAsync(string userLogin)
        {
            try
            {
                return await _userContext.Users.SingleOrDefaultAsync(user => user.Login == userLogin);
            }
            catch (ArgumentNullException anex)
            {
                throw new UserDomainException(anex.Message);
            }
        }

        public async Task<User> GetByEmailAsync(string userEmail)
        {
            try
            {
                return await _userContext.Users.SingleOrDefaultAsync(user => user.Email == userEmail);
            }
            catch (ArgumentNullException anex)
            {
                throw new UserDomainException(anex.Message);
            }
        }

        public async Task SetCompanyAsync(Guid userId, Guid companyId)
        {
            try
            {
                User user = await _userContext.Users.SingleOrDefaultAsync(user => user.Id == userId);
                if (user == null)
                {
                    throw new UserDomainException(IdNotFoundMessage);
                }
                user.CompanyId = companyId;
                await _userContext.SaveChangesAsync();
            }
            catch (InvalidOperationException ioex)
            {
                throw new UserDomainException(ioex.Message);
            }
        }
        public async Task<User> UpdatePasswordAsync(User newUser)
        {
            try
            {
                User oldUser = await _userContext.Users.SingleOrDefaultAsync(user => user.Id == newUser.Id);

                if (oldUser == null)
                {
                    throw new UserDomainException("User not found");
                }
                oldUser.SetPasswordItems(newUser.PasswordHash, newUser.PasswordSalt);

                await _userContext.SaveChangesAsync();
                return newUser;
            }
            catch (InvalidOperationException ioex)
            {
                throw new UserDomainException(ioex.Message);
            }
        }

        public async Task<RefreshToken> GetRefreshTokenAsync(Guid token)
        {
            try
            {
                return await _userContext.RefreshTokens.SingleOrDefaultAsync(x =>
                    x.Token == token &&
                    x.ExpiresAt > DateTime.UtcNow);
            }
            catch(ArgumentNullException)
            {
                throw new UserDomainException("Invalid refresh token");
            }
        }

        public async Task<RefreshToken> CreateRefreshTokenAsync(RefreshToken token)
        {
            try
            {
                await DeleteRefreshTokenAsync(token);
                _userContext.RefreshTokens.Add(token);
                await _userContext.SaveChangesAsync();
                return token;
            }
            catch (InvalidOperationException ioex)
            {
                throw new UserDomainException(ioex.Message);

            }
        }

        public async Task DeleteRefreshTokenAsync(RefreshToken token)
        {
            try
            {
                var oldToken = await _userContext.RefreshTokens.SingleOrDefaultAsync(rt => rt.UserId == token.UserId);
                if (oldToken != null)
                {
                    _userContext.RefreshTokens.Remove(oldToken);
                    await _userContext.SaveChangesAsync();
                }
            }
            catch (InvalidOperationException ioex)
            {
                throw new UserDomainException(ioex.Message);
            }
        }
    }
}
