﻿using LunchGroup.API.Contracts.Requests;
using LunchGroup.API.Contracts.Responses;
using LunchGroup.API.Mapping;
using LunchGroup.Domain.Exceptions;
using LunchGroup.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace LunchGroup.API.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class DeliveryController : ControllerBase
    {
        private readonly IDeliveryDomainService _deliveryService;

        public DeliveryController(IDeliveryDomainService deliveryService)
        {
            _deliveryService = deliveryService;
        }

        [HttpGet]
        public async Task<ActionResult<GetAllDeliveriesResponse>> GetAllDeliveriesAsync()
        {
            try
            {
                var response = await _deliveryService.GetAllDeliveriesAsync();
                return response.MapToCollection();
            }
            catch (LunchGroupDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpGet("{deliveryId}")]
        public async Task<ActionResult<DeliveryResponse>> GetDeliveryByIdAsync(Guid deliveryId)
        {
            try
            {
                var response = await _deliveryService.GetDeliveryByIdAsync(deliveryId);
                return response.MapToResponse();
            }
            catch (LunchGroupDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpGet("order/" + "{groupOrderId}")]
        public async Task<ActionResult<DeliveryResponse>> GetDeliveryByGroupOrderIdAsync(Guid groupOrderId)
        {
            try
            {
                var response = await _deliveryService.GetDeliveryByGroupOrderIdAsync(groupOrderId);
                return response.MapToResponse();
            }
            catch (LunchGroupDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpPost("{groupOrderId}")]
        public async Task<ActionResult<DeliveryResponse>> CreateDeliveryAsync(Guid groupOrderId)
        {
            try
            {
                var response = await _deliveryService.CreateAsync(groupOrderId);
                return response.MapToResponse();
            }
            catch (LunchGroupDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpPut("{deliveryId}")]
        public async Task<ActionResult<DeliveryResponse>> UpdateDeliveryAsync(Guid deliveryId, [FromBody] UpdateDeliveryRequest request)
        {
            try
            {
                var model = request.MapFromRequest();
                var response = await _deliveryService.UpdateDeliveryAsync(deliveryId, model);
                return response.MapToResponse();
            }
            catch (LunchGroupDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpDelete("{deliveryId}")]
        public async Task<ActionResult<DeliveryResponse>> DeleteAsync(Guid deliveryId)
        {
            try
            {
                var response = await _deliveryService.DeleteDeliveryAsync(deliveryId);
                return response.MapToResponse();
            }
            catch (LunchGroupDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

    }
}
