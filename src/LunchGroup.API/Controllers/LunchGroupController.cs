﻿using AutoMapper;
using LunchGroup.API.Contracts;
using LunchGroup.API.Contracts.Requests;
using LunchGroup.API.Contracts.Responses;
using LunchGroup.Domain.Exceptions;
using LunchGroup.Domain.Models;
using LunchGroup.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace LunchGroup.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LunchGroupController : ControllerBase
    {
        private readonly ILunchGroupDomainService _lunchGroupDomainService;
        private readonly IMapper _mapper;

        public LunchGroupController(ILunchGroupDomainService lunchGroupDomainService, IMapper mapper)
        {
            _lunchGroupDomainService = lunchGroupDomainService;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all groups that company has
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns>Collection of groups</returns>
        [HttpGet(LunchGroupRoutes.CompanyUri + "/{companyId}")]
        public async Task<ActionResult<GetLunchGroupsCollectionResponse>> GetAllLunchGroupsByCompanyAsync(Guid companyId)
        {
            try
            {
                var models = await _lunchGroupDomainService.GetAllByCompanyAsync(companyId);
                return new GetLunchGroupsCollectionResponse(
                    models.Select(model => _mapper.Map<GetLunchGroupResponse>(model)).ToList()
                );
                
            }
            catch (LunchGroupDomainException lgEx)
            {
                return BadRequest(new { message = lgEx.Message });
            }
        }

        /// <summary>
        /// Get information about time periods of group
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns>Deadline, avaliable dates</returns>
        [HttpGet(LunchGroupRoutes.TimetableUri + "/{groupId}")]
        public async Task<ActionResult<GetTimetableResponse>> GetTimetableAsync(Guid groupId)
        {
            try
            {
                LunchGroupModel model = await _lunchGroupDomainService.GetByIdAsync(groupId);

                return _mapper.Map<GetTimetableResponse>(model);
            }
            catch (LunchGroupNotFoundException lgEx)
            {
                return BadRequest(new { message = lgEx.Message });
            }
        }

        /// <summary>
        /// Update information about time information
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns>Success of fault result </returns>
        [HttpPut(LunchGroupRoutes.TimetableUri + "/{groupId}")]
        public async Task<IActionResult> UpdateTimetableAsync(Guid groupId)
        {
            try
            {
                await _lunchGroupDomainService.UpdateTimetableAsync(groupId);
                return Ok();
            }
            catch (LunchGroupNotFoundException lgEx)
            {
                return BadRequest(new { message = lgEx.Message });
            }
        }

        /// <summary>
        /// Change flag status
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="request"></param>
        /// <returns>Success of fault result</returns>
        [HttpPut(LunchGroupRoutes.StatusUri + "/{groupId}")]
        public async Task<IActionResult> SetActiveFlagAsync(Guid groupId, [FromBody] SetActiveFlagRequest request)
        {
            try
            {
                await _lunchGroupDomainService.SetActiveFlagAsync(groupId, request.IsActive);
                return Ok();
            }
            catch (LunchGroupNotFoundException lgEx)
            {
                return BadRequest(new { message = lgEx.Message });
            }
        }

        /// <summary>
        /// Get group by id
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns>Lunch group</returns>
        [HttpGet("{groupId}")]
        public async Task<ActionResult<GetLunchGroupResponse>> GetLunchGroupByIdAsync(Guid groupId)
        {
            try
            {
                GetLunchGroupResponse response = _mapper.Map<GetLunchGroupResponse>(await _lunchGroupDomainService.GetByIdAsync(groupId));
                return Ok(response);
            }
            catch (LunchGroupDomainException exception)
            {
                return BadRequest(new { message = exception.Message });
            }
        }

        /// <summary>
        /// Get all groups that has a current manager
        /// </summary>
        /// <param name="managerId"></param>
        /// <returns>Lunch groups</returns>
        [HttpGet(LunchGroupRoutes.ManagerUri + "/{managerId}")]
        public async Task<ActionResult<GetLunchGroupsCollectionResponse>> GetLunchGroupsByManagerAsync(Guid managerId)
        {
            try
            {
                GetLunchGroupsCollectionResponse groups = new GetLunchGroupsCollectionResponse(
                    (await _lunchGroupDomainService.GetAllByManagerIdAsync(managerId))
                        .Select(group => _mapper.Map<GetLunchGroupResponse>(group))
                        .ToList()
                    );
                return Ok(groups);
            }
            catch (LunchGroupDomainException exception)
            {
                return BadRequest(new { message = exception.Message });
            }
        }

        /// <summary>
        /// Returns lunchGroup that has worker as a member
        /// </summary>
        /// <param name="workerId"></param>
        /// <returns>workers lunchgroup </returns>
        [HttpGet(LunchGroupRoutes.WorkerUri + "/{workerId}")]
        public async Task<ActionResult<GetLunchGroupResponse>> GetWorkerLunchGroupAsync(Guid workerId)
        {
            try
            {
                GetLunchGroupResponse response = _mapper.Map<GetLunchGroupResponse>(await _lunchGroupDomainService.GetByWorkerIdAsync(workerId));
                return Ok(response);
            }
            catch (LunchGroupDomainException exception)
            {
                return BadRequest(new { message = exception.Message });
            }
        }

        /// <summary>
        /// Create new group
        /// </summary>
        /// <param name="lunchGroupRequest"></param>
        /// <returns>Guid of new group</returns>
        [HttpPost]
        public async Task<ActionResult<CreateLunchGroupResponse>> CreateLunchGroupAsync([FromBody] CreateLunchGroupRequest lunchGroupRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new
                {
                    message = ModelState
                        .Select(x => x.Value.Errors)
                        .Where(y => y.Count > 0)
                        .First()
                        .ElementAt(0)
                        .ErrorMessage
                });
            }

            LunchGroupModel group = _mapper.Map<LunchGroupModel>(lunchGroupRequest);
            try
            {
                return Ok(_mapper.Map<CreateLunchGroupResponse>(await _lunchGroupDomainService.CreateAsync(group)));
            }
            catch (LunchGroupDomainException exception)
            {
                return BadRequest(new { message = exception.Message });
            }
        }

        /// <summary>
        /// Update group to new model
        /// </summary>
        /// <param name="lunchGroupToUpdate"></param>
        /// <param name="id"></param>
        /// <returns>Group ID</returns>
        [HttpPut("{id}")]
        public async Task<ActionResult<Guid>> UpdateLunchGroupAsync([FromBody] UpdateLunchGroupRequest lunchGroupToUpdate, Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new
                {
                    message = ModelState
                        .Select(x => x.Value.Errors)
                        .Where(y => y.Count > 0)
                        .First()
                        .ElementAt(0)
                        .ErrorMessage
                });
            }

            LunchGroupModel group = _mapper.Map<LunchGroupModel>(lunchGroupToUpdate);
            try
            {
                return Ok((await _lunchGroupDomainService.UpdateAsync(id, group)).Id);
            }
            catch (LunchGroupDomainException exception)
            {
                return BadRequest(new { message = exception.Message });
            }
        }
    }
}
