﻿using LunchGroup.API.Contracts.Requests;
using LunchGroup.API.Contracts.Responses;
using LunchGroup.Domain.Models;
using System.Collections.Generic;
using System.Linq;
using System;

namespace LunchGroup.API.Mapping
{
    
    public static class DeliveryMapper
    {
        public static DeliveryResponse MapToResponse(this DeliveryModel deliveryModel)
        {
            return new DeliveryResponse()
            {
                DeliveryId = deliveryModel.Id,
                DeliveryTime = deliveryModel.DeliveryTime,
                GroupOrderId = deliveryModel.GroupOrderId,
                Status = deliveryModel.Status.ToString()

            };
        }
        public static GetAllDeliveriesResponse MapToCollection(this IReadOnlyCollection<DeliveryModel> deliveryModels)
        {
            return new GetAllDeliveriesResponse(deliveryModels.Select(d => d.MapToResponse()).ToList());
        }

        public static DeliveryModel MapFromRequest(this UpdateDeliveryRequest request)
        {
            return new DeliveryModel()
            {
               DeliveryTime = request.DeliveryTime, 
               Status = Enum.Parse<StatusModel>(request.Status), 
               GroupOrderId = request.GroupOrderId
            };
        }
    }
}
