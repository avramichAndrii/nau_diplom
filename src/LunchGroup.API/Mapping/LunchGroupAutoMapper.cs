﻿using AutoMapper;
using LunchGroup.API.Contracts.Requests;
using LunchGroup.API.Contracts.Responses;
using LunchGroup.Domain.Models;
using System.Collections.Generic;
using System.Linq;

namespace LunchGroup.API.Mapping
{
    public class LunchGroupAutoMapper : Profile
    {
        public LunchGroupAutoMapper()
        {
            CreateMap<UpdateLunchGroupRequest, LunchGroupModel>()
                .ForMember(dest => dest.GroupMembers, opt => opt.MapFrom(src => src.Members.Select(gm => new LunchGroupMember() { MemberId = gm })));
            CreateMap<CreateLunchGroupRequest, LunchGroupModel>()
                .ForMember(dest => dest.GroupMembers, opt => opt.MapFrom(src => src.Members.Select(gm => new LunchGroupMember() {MemberId=gm})));

            CreateMap<GetLunchGroupsCollectionResponse, IReadOnlyCollection<LunchGroupMember>>();

            CreateMap<LunchGroupModel, GetLunchGroupResponse>()
                .ForMember(dest => dest.Members, opt => opt.MapFrom(src => src.GroupMembers.Select(gm => gm.MemberId)));
            CreateMap<LunchGroupModel, LunchGroupResponse>()
                .ForMember(dest => dest.Members, opt => opt.MapFrom(src => src.GroupMembers.Select(gm => gm.MemberId)));
            CreateMap<LunchGroupModel, CreateLunchGroupResponse>()
                .ForMember(dest => dest.Members, opt => opt.MapFrom(src => src.GroupMembers.Select(gm => gm.MemberId)));
            CreateMap<LunchGroupModel, GetTimetableResponse>()
                .ForMember(dest => dest.AvailableDates, opt => opt.MapFrom(src => src.GetAvailableDates()));

            CreateMap<AddressRequest, AddressModel>();
            CreateMap<AddressModel, AddressResponse>();
        }
    }
}
