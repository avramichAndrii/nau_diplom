﻿using FluentValidation;
using LunchGroup.API.Contracts.Requests;
using System;
using System.Linq;

namespace LunchGroup.API.Validation
{
    public class UpdateLunchGroupRequestValidator : AbstractValidator<UpdateLunchGroupRequest>
    {
        private const int StringMaxSize = 256;
        private const int CityStringMaxSize = 64;
        private const int BuildingNumberStringMaxSize = 8;

        private const string PeriodicityMessage = "Available periodicity: day, week";
        private const string WeekendOverflowMessage = "Too much weekends!";
        private const string DeadlineBelongsToMessage = "Deadline should belong to current period";
        private const string WorkingDeadlineMessage = "Deadline shouln't be a weekend";
        private const string WorkingPeriodStartMessage = "Period start date shouln't be a weekend";
        private const string NotEmptyMembersMessage = "Specify at least one member of lunch group";

        public UpdateLunchGroupRequestValidator()
        {
            RuleFor(r => r.Periodicity)
                .Must(p => p == 1 || p == 7)
                .WithMessage(PeriodicityMessage);
            RuleFor(r => r.Weekends)
                .ForEach(s => s.Must(s => Enum.TryParse(typeof(DayOfWeek), s, out object dow)));
            RuleFor(r => r.Weekends.Length)
                .LessThan(5)
                .WithMessage(WeekendOverflowMessage)
                .DependentRules(() => 
                {
                    RuleFor(r => r.SubmitDeadline)
                    .GreaterThan(r => DetermineBaseDate(r))
                    .WithMessage(DeadlineBelongsToMessage)
                    .LessThan(r => r.PeriodStart)
                    .WithMessage(DeadlineBelongsToMessage);
                });
            RuleFor(r => r.SubmitDeadline.DayOfWeek.ToString())
                .Must((r, d) => !r.Weekends.Contains(d))
                .WithMessage(WorkingDeadlineMessage);
            RuleFor(r => r.PeriodStart.DayOfWeek.ToString())
                .Must((r, d) => !r.Weekends.Contains(d))
                .WithMessage(WorkingPeriodStartMessage);

            RuleFor(r => r.Name).MaximumLength(StringMaxSize).NotEmpty();
            RuleFor(r => r.Members.Length).GreaterThan(0).WithMessage(NotEmptyMembersMessage);

            RuleFor(r => r.Address).NotNull();
            RuleFor(r => r.Address.City).MaximumLength(CityStringMaxSize).NotEmpty();
            RuleFor(r => r.Address.Street).MaximumLength(StringMaxSize).NotEmpty();
            RuleFor(r => r.Address.BuildingNumber).MaximumLength(BuildingNumberStringMaxSize).NotEmpty();
            RuleFor(r => r.Address.Office).MaximumLength(CityStringMaxSize).NotEmpty();
        }

        /// <summary>
        /// Determines the base date which is start of the previous period and shouldn't be a weekend.
        /// </summary>
        /// <returns>Date which is first of the previous period.</returns>
        private DateTime DetermineBaseDate(UpdateLunchGroupRequest requestToValidate)
        {
            DateTime baseDate = requestToValidate.PeriodStart.AddDays(-requestToValidate.Periodicity);
            while (requestToValidate.Weekends.Contains(baseDate.DayOfWeek.ToString()))
            {
                baseDate = baseDate.AddDays(-1);
            }
            return baseDate;
        }
    }
}
