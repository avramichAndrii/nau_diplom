﻿using Company.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Company.Persistence
{
    public static class CompanyContextDIExtensions
    {
        private const string DBConnectionString = nameof(DBConnectionString);
        public static void RegisterCompanyPersistence(this IServiceCollection collection, IConfiguration configuration)
        {
            var connection = configuration.GetConnectionString(DBConnectionString);
            collection.AddDbContextPool<CompanyContext>(builder => builder.UseMySql(connection));
            collection.AddScoped<ICompanyRepository, CompanyRepository>();
            collection.AddScoped<IContractRepository, ContractRepository>();
        }
    }
}
