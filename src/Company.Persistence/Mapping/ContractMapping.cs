﻿using Company.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Company.Persistence
{
    class ContractMapping : IEntityTypeConfiguration<ContractModel>
    {
        private const string TableName = "Contract";
        private const int MaxSize = 150;

        public void Configure(EntityTypeBuilder<ContractModel> builder)
        {
            builder.ToTable(TableName);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.ContractNumber).HasMaxLength(MaxSize).IsRequired();
            builder.Property(x => x.ContractStatus).HasConversion<string>();
        }
    }
}
