using Company.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Company.Persistence
{
    public class CompanyMapping : IEntityTypeConfiguration<CompanyModel>
    {
        private const string TableName = "Company";
        private const int MaxSize = 1500;
        
        public void Configure(EntityTypeBuilder<CompanyModel> builder)
        {
            builder.ToTable(TableName);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name).HasMaxLength(MaxSize).IsRequired();
            builder.Property(x => x.Description).HasMaxLength(MaxSize);
            builder.Property(x => x.TypeOfCompany).HasConversion<string>();
            builder.Property(x => x.CompanyEmail).IsRequired();
        }
    }
}