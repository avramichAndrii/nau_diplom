﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Company.Persistence.Migrations
{
    public partial class CompanyType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "TypeOfCompany",
                table: "Company",
                nullable: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TypeOfCompany",
                table: "Company");
        }
    }
}
