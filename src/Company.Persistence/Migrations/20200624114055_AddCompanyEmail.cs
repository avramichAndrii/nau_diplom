﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Company.Persistence.Migrations
{
    public partial class AddCompanyEmail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CompanyEmail",
                table: "Company",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CompanyEmail",
                table: "Company");
        }
    }
}
