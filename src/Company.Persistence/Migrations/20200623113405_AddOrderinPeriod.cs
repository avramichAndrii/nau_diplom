﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Company.Persistence.Migrations
{
    public partial class AddOrderinPeriod : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "OrderingPeriod",
                table: "Company",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OrderingPeriod",
                table: "Company");
        }
    }
}
