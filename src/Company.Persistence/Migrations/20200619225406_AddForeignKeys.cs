﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Company.Persistence.Migrations
{
    public partial class AddForeignKeys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Contract_ConsumerCompanyId",
                table: "Contract",
                column: "ConsumerCompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Contract_ProviderCompanyId",
                table: "Contract",
                column: "ProviderCompanyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Contract_Company_ConsumerCompanyId",
                table: "Contract",
                column: "ConsumerCompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Contract_Company_ProviderCompanyId",
                table: "Contract",
                column: "ProviderCompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contract_Company_ConsumerCompanyId",
                table: "Contract");

            migrationBuilder.DropForeignKey(
                name: "FK_Contract_Company_ProviderCompanyId",
                table: "Contract");

            migrationBuilder.DropIndex(
                name: "IX_Contract_ConsumerCompanyId",
                table: "Contract");

            migrationBuilder.DropIndex(
                name: "IX_Contract_ProviderCompanyId",
                table: "Contract");
        }
    }
}
