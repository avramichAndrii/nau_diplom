using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Company.Domain;
using Microsoft.EntityFrameworkCore;
using Company.Domain.Exceptions;
using System.Linq;

namespace Company.Persistence
{
    public class CompanyRepository : ICompanyRepository
    {
        private readonly CompanyContext _applicationContext;

        const string CreateMessage = "Couldn't be created. Please check sending data.";
        const string UpdateMessage = "Couldn't be updated. Please check sending data.";
        const string DeleteMessage = "Couldn't be deleted.";

        public CompanyRepository(CompanyContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public async Task<IReadOnlyCollection<CompanyModel>> GetAllAsync()
        {
            return await _applicationContext.CompanyModels.ToListAsync();
        }

        public async Task<CompanyModel> GetByIdAsync(Guid id)
        {
            var companyModel = await _applicationContext.CompanyModels.SingleOrDefaultAsync(cm => cm.Id == id);
            if (companyModel == null)
            {
                throw new CompanyNotFoundDomainException(id);
            }
            return companyModel;
        }

        public async Task<IReadOnlyCollection<CompanyModel>> GetByCompanyTypeAsync(string type)
        {
            var companyModel =
                await _applicationContext.CompanyModels
                    .Where(cm => cm.TypeOfCompany == Enum.Parse<CompanyType>(type)).ToListAsync();
            return companyModel;
        }

        public async Task<CompanyModel> CreateAsync(CompanyModel companyModel)
        {
            _applicationContext.CompanyModels.Add(companyModel);
            try
            {
                await _applicationContext.SaveChangesAsync();
                return companyModel;
            }
            catch (InvalidOperationException)
            {
                throw new InvalidDataCompanyDomainException(CreateMessage);
            }
        }

        public async Task<CompanyModel> UpdateAsync(Guid id, CompanyModel companyModel)
        {
            CompanyModel modelToUpdate = await _applicationContext.CompanyModels.SingleOrDefaultAsync(cm => cm.Id == id);
            if (modelToUpdate == null)
            {
                throw new CompanyNotFoundDomainException(id);
            }
            modelToUpdate.Name = companyModel.Name;
            modelToUpdate.Description = companyModel.Description;
            modelToUpdate.CompanyEmail = companyModel.CompanyEmail;
            modelToUpdate.TypeOfCompany = companyModel.TypeOfCompany;
            modelToUpdate.OrderingPeriod = companyModel.OrderingPeriod;
            try
            {
                await _applicationContext.SaveChangesAsync();
                return modelToUpdate;
            }
            catch (InvalidOperationException)
            {
                throw new InvalidDataCompanyDomainException(UpdateMessage);
            }
        }

        public async Task<CompanyModel> DeleteAsync(Guid id)
        {
            CompanyModel modelToRemove = await _applicationContext.CompanyModels.SingleOrDefaultAsync(cm => cm.Id == id);
            if (modelToRemove == null)
            {
                throw new CompanyNotFoundDomainException(id);
            }
            _applicationContext.CompanyModels.Remove(modelToRemove);
            try
            {
                await _applicationContext.SaveChangesAsync();
                return modelToRemove;
            }
            catch (InvalidOperationException)
            {
                throw new InvalidDataCompanyDomainException(DeleteMessage);
            }
        }
    }
}