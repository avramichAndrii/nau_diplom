﻿using Company.Domain;
using Company.Domain.Exceptions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Company.Persistence
{
    public class ContractRepository : IContractRepository
    {
        private readonly CompanyContext _applicationContext;

        const string CreateMessage = "Couldn't be created. Please check sending data.";
        const string UpdateMessage = "Couldn't be updated. Please check sending data.";

        public ContractRepository(CompanyContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public async Task<IReadOnlyCollection<ContractModel>> GetAllContractsAsync()
        {
            return await _applicationContext.ContractModels.ToListAsync();
        }

        public async Task<ContractModel> GetContractByIdAsync(Guid id)
        {
            var contractModel = await _applicationContext.ContractModels.SingleOrDefaultAsync(cm => cm.Id == id);
            if (contractModel == null)
            {
                throw new ContractNotFoundDomainException(id);
            }
            return contractModel;
        }

        public async Task<IReadOnlyCollection<ContractModel>> GetByCompanyTypeAndIdAsync(Guid id, CompanyType companyType)
        {
            var contractModels =
                await _applicationContext.ContractModels
                    .Where(cm => (companyType == CompanyType.Provider) ? cm.ProviderCompanyId == id : cm.ConsumerCompanyId == id)
                    .ToListAsync();
            return contractModels;
        }

        public async Task<IReadOnlyCollection<ContractModel>> GetByStatusAndCompanyTypeAndIdAsync(Guid id, CompanyType companyType, ContractStatus contractStatus)
        {
            var contractModels =
                await _applicationContext.ContractModels
                    .Where(cm => (companyType == CompanyType.Provider) ? cm.ProviderCompanyId == id : cm.ConsumerCompanyId == id)
                    .Where(cm => cm.ContractStatus == contractStatus)
                    .ToListAsync();
            return contractModels;
        }

        public async Task<ContractModel> CreateContractAsync(ContractModel contractModel)
        {
            contractModel.ProviderCompany = await _applicationContext.CompanyModels.SingleOrDefaultAsync(cm => cm.Id == contractModel.ProviderCompanyId);
            contractModel.ConsumerCompany = await _applicationContext.CompanyModels.SingleOrDefaultAsync(cm => cm.Id == contractModel.ConsumerCompanyId);
            contractModel.ContractNumber = contractModel.ProviderCompany.Name.Substring(0, 2) + contractModel.ProviderCompanyId.ToString().Substring(0, 2) +
                                           contractModel.ConsumerCompany.Name.Substring(0, 2) + contractModel.ConsumerCompanyId.ToString().Substring(0, 2) + "_" +
                                           contractModel.MenuId.ToString().Substring(0, 2) + "_" + DateTime.Today.Day.ToString() +
                                           DateTime.Today.Month.ToString() + DateTime.Today.Year.ToString();
            _applicationContext.ContractModels.Add(contractModel);
            try
            {
                await _applicationContext.SaveChangesAsync();
                return contractModel;
            }
            catch (InvalidOperationException)
            {
                throw new InvalidDataCompanyDomainException(CreateMessage);
            }
        }

        public async Task<ContractModel> UpdateContractAsync(Guid id, ContractModel contractModel)
        {
            ContractModel modelToUpdate = await _applicationContext.ContractModels.SingleOrDefaultAsync(cm => cm.Id == id);
            if (modelToUpdate == null)
            {
                throw new ContractNotFoundDomainException(id);
            }
            modelToUpdate.ContractNumber = contractModel.ContractNumber;
            modelToUpdate.ConsumerCompanyId = contractModel.ConsumerCompanyId;
            modelToUpdate.ProviderCompanyId = contractModel.ProviderCompanyId;
            modelToUpdate.ContractStatus = contractModel.ContractStatus;
            modelToUpdate.MenuId = contractModel.MenuId;
            try
            {
                await _applicationContext.SaveChangesAsync();
                return modelToUpdate;
            }
            catch (InvalidOperationException)
            {
                throw new InvalidDataCompanyDomainException(UpdateMessage);
            }
        }
    }
}
