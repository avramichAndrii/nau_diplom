using Company.Domain;
using Microsoft.EntityFrameworkCore;

namespace Company.Persistence
{
    public class CompanyContext : DbContext
    {
        public DbSet<CompanyModel> CompanyModels { get; set; }
        public DbSet<ContractModel> ContractModels { get; set; }

        public CompanyContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration<CompanyModel>(new CompanyMapping());
            modelBuilder.ApplyConfiguration<ContractModel>(new ContractMapping());
            base.OnModelCreating(modelBuilder);
        }
    }
}