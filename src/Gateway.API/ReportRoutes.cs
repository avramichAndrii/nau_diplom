﻿namespace Gateway.API
{
    static class ReportRoutes
    {
        public const string WorkerUri = "worker/";
        public const string ManagerUri = "manager/";
        public const string ProviderUri = "provider/";
        public const string StartDateUri = "/start/";
        public const string EndDateUri = "/end/";
        public const string LunchGroupUri = "/lunchgroup/";
        public const string DownloadUri = "/download";
        public const string SendMailUri = "/mail";
    }
}
