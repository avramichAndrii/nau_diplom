﻿using Gateway.API.Contracts;
using Gateway.Application.Abstractions;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using UserManagement.API.Contracts.Responses;

namespace Gateway.API.Controllers
{
    [ApiController]
    [Route("gateway/invitation")]
    public class UserAssignerController : ControllerBase
    {
        private readonly IUserAssignerService _userAssignerService;

        public UserAssignerController(IUserAssignerService userAssignerService)
        {
            _userAssignerService = userAssignerService;
        }

        [HttpPost]
        public async Task<IActionResult> SendInvitation([FromBody] SendInvitationRequest request)
        {
            await _userAssignerService.SendInvitation(request.InviterId, request.InvitedUserRole, request.InvitedUserMail);
            return Ok();
        }

        [HttpPut]
        public async Task<ActionResult<UserResponse>> RegisterWorker([FromBody] MailRegisterRequest request)
        {
            var response = await _userAssignerService.RegisterWorker(request.Token, request.UserRegisterRequest);
            return Ok(response);
        }
    }
}
