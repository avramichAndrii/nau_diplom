﻿using EmailNotifier.EmailNotification;
using Gateway.API.Contracts;
using Gateway.Application;
using Gateway.Application.Abstractions;
using Gateway.Application.Services;
using GroupOrder.API.Contracts.Responses;
using LunchGroup.API.Contracts;
using LunchGroup.API.Contracts.Requests;
using LunchGroup.API.Contracts.Responses;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gateway.API.Controllers
{

    [Controller]
    [Route("gateway")]
    public class LunchGroupGatewayController:ControllerBase
    {
        private readonly ILunchGroupRedirectorService _lunchGroupRedirectorService;
        private readonly IGroupOrderRedirectorService _groupOrderRedirectorService;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IMailSender _mailSender;
        private readonly ICompanyRedirectorService _companyRedirectorService;
        private readonly IUserManagementRedirectorService _userManagementRedirectorService;

        public LunchGroupGatewayController(
            ILunchGroupRedirectorService lunchGroupRedirectorService,
            IGroupOrderRedirectorService groupOrderRedirectorService,
            IOrderProcessingService orderProcessingService,
            IMailSender mailSender,
            ICompanyRedirectorService companyRedirectorService,
            IUserManagementRedirectorService userManagementRedirectorService)
        {
            _lunchGroupRedirectorService = lunchGroupRedirectorService;
            _groupOrderRedirectorService = groupOrderRedirectorService;
            _orderProcessingService = orderProcessingService;
            _mailSender = mailSender;
            _companyRedirectorService = companyRedirectorService;
            _userManagementRedirectorService = userManagementRedirectorService;
        }

        [HttpGet(LunchGroupRoutes.BaseLunchGroupUri + "/provider/" + "{providerCompanyId}/" + DeliveryRouts.BaseDeliveryURL)]
        public async Task<ActionResult<GetAllDeliveriesResponse>> GetDeliveriesByProviderId(Guid providerCompanyId)
        {
            var allGroupOrdersOfProvider = _groupOrderRedirectorService.GetGroupOrdersByProviderIdAsync(providerCompanyId).Result;
            List<DeliveryResponse> deliveriesByProvider = new List<DeliveryResponse>();
            for (int i = 0; i < allGroupOrdersOfProvider.GroupOrderCollection.Count; i++)
            {
                deliveriesByProvider.Add(
                    await _lunchGroupRedirectorService.GetDeliveryByGroupOrderIdAsync(
                        allGroupOrdersOfProvider.GroupOrderCollection.ElementAt(i).Id));
            }
            
            return new GetAllDeliveriesResponse(
                deliveriesByProvider.ToList().AsReadOnly());
        }

        [HttpGet(LunchGroupRoutes.BaseLunchGroupUri + "/manager/" + "{managerId}/" + DeliveryRouts.BaseDeliveryURL)]
        public async Task<ActionResult<GetAllDeliveriesResponse>> GetDeliveriesByManagerId(Guid managerId)
        {
            var allLunchGroupsOfManager = await _lunchGroupRedirectorService.GetLunchGroupsByManagerAsync(managerId);
            List<DeliveryResponse> deliveriesByManager = new List<DeliveryResponse>();
            List<GroupOrderResponse> groupOrdersByLunchGroup = new List<GroupOrderResponse>();
            
            for (int i = 0; i < allLunchGroupsOfManager.LunchGroupsCollection.Count; i++)
            {
                var item = await _groupOrderRedirectorService
                    .GetAllByLunchGroupsIdAsync(allLunchGroupsOfManager.LunchGroupsCollection.ElementAt(i).Id);
                for (int j = 0; j < item.GroupOrderCollection.Count; j++)
                {
                    groupOrdersByLunchGroup.Add(item.GroupOrderCollection.ElementAt(j));
                }
            }

            for (int i = 0; i < groupOrdersByLunchGroup.Count; i++)
            {
                deliveriesByManager.Add(
                    await _lunchGroupRedirectorService.GetDeliveryByGroupOrderIdAsync(groupOrdersByLunchGroup[i].Id));
            }

            return new GetAllDeliveriesResponse(
                deliveriesByManager.ToList().AsReadOnly());
        }

        [HttpGet(LunchGroupRoutes.BaseLunchGroupUri + "/worker/{workerId}/" + DeliveryRouts.BaseDeliveryURL)]
        public async Task<ActionResult<GetAllDeliveriesResponse>> GetDeliveriesByWorkerId(Guid workerId)
        {
            var ordersOfWorker = await _groupOrderRedirectorService.GetOrdersByWorkerIdAsync(workerId);
            List<DeliveryResponse> deliveriesOfWorker = new List<DeliveryResponse>();

            for (int i = 0; i < ordersOfWorker.OrdersCollection.Count; i++)
            {
                var item = await _lunchGroupRedirectorService.GetDeliveryByGroupOrderIdAsync(ordersOfWorker.OrdersCollection.ElementAt(i)
                               .GroupOrderId);
                deliveriesOfWorker.Add(item);
            }
            
            GetAllDeliveriesResponse deliveriesCollection = new GetAllDeliveriesResponse(
                deliveriesOfWorker.ToList().AsReadOnly());

            return deliveriesCollection;
        }

        [HttpGet(LunchGroupRoutes.BaseLunchGroupUri + "/" + LunchGroupRoutes.CompanyUri + "/{companyId}")]
        public async Task<ActionResult<GetLunchGroupsCollectionResponse>> GetAllLunchGroupsByCompanyAsync(Guid companyId)
        {
            return await _lunchGroupRedirectorService.GetAllLunchGroupsByCompanyAsync(companyId) ;
        }

        [HttpGet(LunchGroupRoutes.BaseLunchGroupUri + "/" + LunchGroupRoutes.TimetableUri + "/{groupId}")]
        public async Task<ActionResult<GetTimetableResponse>> GetTimetableAsync(Guid groupId)
        {
            return await _lunchGroupRedirectorService.GetTimetableAsync(groupId);
        }

        [HttpPut(LunchGroupRoutes.BaseLunchGroupUri + "/" + LunchGroupRoutes.TimetableUri + "/{groupId}")]
        public async Task<IActionResult> UpdateTimetableAsync(Guid groupId)
        {
            await _lunchGroupRedirectorService.UpdateTimetableAsync(groupId);
            return Ok();
        }

        [HttpPut(LunchGroupRoutes.BaseLunchGroupUri + "/" + LunchGroupRoutes.StatusUri + "/{groupId}")]
        public async Task<IActionResult> SetActiveFlagAsync(Guid groupId, [FromBody] SetActiveFlagRequest request)
        {
            await _lunchGroupRedirectorService.SetActiveFlagAsync(groupId, request);
            return Ok();
        }

        [HttpGet(LunchGroupRoutes.BaseLunchGroupUri + "/" + "{groupId}")]
        public async Task<ActionResult<GetLunchGroupResponse>> GetLunchGroupByIdAsync(Guid groupId)
        {
            return await _lunchGroupRedirectorService.GetLunchGroupByIdAsync(groupId);
        }

        [HttpGet(LunchGroupRoutes.BaseLunchGroupUri + "/" + LunchGroupRoutes.ManagerUri + "/{managerId}")]
        public async Task<ActionResult<GetLunchGroupsCollectionResponse>> GetLunchGroupsByManagerAsync(Guid managerId)
        {
            return await _lunchGroupRedirectorService.GetLunchGroupsByManagerAsync(managerId);
        }

        [HttpGet(LunchGroupRoutes.BaseLunchGroupUri + "/" + LunchGroupRoutes.WorkerUri + "/{workerId}")]
        public async Task<ActionResult<GetLunchGroupResponse>> GetWorkerLunchGroupAsync(Guid workerId)
        {
            return await _lunchGroupRedirectorService.GetWorkerLunchGroupAsync(workerId);
        }

        [HttpPost(LunchGroupRoutes.BaseLunchGroupUri)]
        public async Task<ActionResult<CreateLunchGroupResponse>> CreateLunchGroupAsync([FromBody] CreateLunchGroupRequest lunchGroupRequest)
        {
            return await _lunchGroupRedirectorService.CreateLunchGroupAsync(lunchGroupRequest);
        }

        [HttpPut(LunchGroupRoutes.BaseLunchGroupUri + "/" + "{id}")]
        public async Task<ActionResult<Guid>> UpdateLunchGroupAsync([FromBody] UpdateLunchGroupRequest lunchGroupToUpdate, Guid id)
        {
            return await _lunchGroupRedirectorService.UpdateLunchGroupAsync(lunchGroupToUpdate, id);
        }

        #region delivery
        [HttpGet(DeliveryRouts.BaseDeliveryURL)]
        public async Task<ActionResult<GetAllDeliveriesResponse>> GetAllDeliveriesAsync()
        {
            return await _lunchGroupRedirectorService.GetAllDeliveriesAsync();
        }

        [HttpGet(DeliveryRouts.BaseDeliveryURL + "/{deliveryId}")]
        public async Task<ActionResult<DeliveryResponse>> GetDeliveryByIdAsync(Guid deliveryId)
        {
            return await _lunchGroupRedirectorService.GetDeliveryByIdAsync(deliveryId);
        }

        [HttpGet(DeliveryRouts.DeliveryOrderURL + "/{groupOrderId}")]
        public async Task<ActionResult<DeliveryResponse>> GetDeliveryByGroupOrderIdAsync(Guid groupOrderId)
        {
            return await _lunchGroupRedirectorService.GetDeliveryByGroupOrderIdAsync(groupOrderId);
        }

        [HttpPost(DeliveryRouts.BaseDeliveryURL + "/{groupOrderId}")]
        public async Task<ActionResult<DeliveryResponse>> CreateDeliveryAsync(Guid groupOrderId, CreateDeliveryRequest request)
        {
            return await _lunchGroupRedirectorService.CreateDeliveryAsync(groupOrderId, request);
        }

        [HttpPost(DeliveryRouts.DeliveryLunchGroupURL)]
        public async Task<IReadOnlyCollection<DeliveryResponse>> CreateDeliveriesByLunchGroupAsync([FromBody] CreateDeliveriesByLunchGroupRequest request)
        {
            return await _orderProcessingService.CreateDeliveriesByManager(request.LunchGroupId);
        }

        [HttpPut(DeliveryRouts.BaseDeliveryURL + "/{deliveryId}")]
        public async Task<ActionResult<DeliveryResponse>> UpdateDeliveryAsync(Guid deliveryId, [FromBody] UpdateDeliveryRequest request)
        {
            var deliveryBeforeUpdate = _lunchGroupRedirectorService.GetDeliveryByIdAsync(request.DeliveryId).GetAwaiter().GetResult();
            var deliveryResponse = await _lunchGroupRedirectorService.UpdateDeliveryAsync(deliveryId, request);
            if ((deliveryResponse!=null) && (request.Status == "Declined" || request.Status == "Submitted" || request.Status == "Done" || (request.DeliveryTime.Value.Date != deliveryBeforeUpdate.DeliveryTime.Value.Date)))
            {   
                var groupOrder = await _groupOrderRedirectorService.GetGroupOrderByIdAsync(request.GroupOrderId);
                var companyInfo = await _companyRedirectorService.GetCompanyAsync(groupOrder.ProviderId);
                var lunchGroup = await GetLunchGroupByIdAsync(groupOrder.LunchGroupId);
                var managerEmail = _userManagementRedirectorService.GetByIdAsync(lunchGroup.Value.ManagerId).GetAwaiter().GetResult().Email;                
                string subject = "Delivery information was updated";
                string message ="Hi! Status of your delivery: " + request.Status+" (Date: " + request.DeliveryTime.Value.Date + ") for lunch group: "
                + lunchGroup.Value.Name +  ". Provider company: " + companyInfo.Name + ". Email: " + companyInfo.CompanyEmail + ".";
                await _mailSender.SendEmailAsync(managerEmail, subject, message);
            }

            return deliveryResponse;
        }

        [HttpDelete(DeliveryRouts.BaseDeliveryURL + "/{deliveryId}")]
        public async Task<ActionResult<DeliveryResponse>> DeleteAsync(Guid deliveryId)
        {
            return await _lunchGroupRedirectorService.DeleteDeliveryAsync(deliveryId);
        }
        #endregion

    }
}
