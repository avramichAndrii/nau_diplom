﻿using Gateway.Application.Abstractions;
using Gateway.Application.Abstractions.Reports;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Gateway.API.Controllers
{
    [ApiController]
    [Route("gateway/report")]
    public class ReportController : ControllerBase
    {
        private readonly IReportGeneratorService _reportGeneratorService;
        private readonly IFilteredReportGeneratorService _filteredReportGeneratorService;
        private readonly IReportSenderService _reportSenderService;
        private readonly IDetailedProviderReport _detailedProviderReport;
        private const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        private const string WrongPeriodMessage = "End date must be not less than start";
        private const string SuccessMailMessage = "Report was sent to {0}";
        private const string ContentType = "Application / vnd.ms-excel";

        public ReportController(IReportGeneratorService reportGeneratorService,
            IFilteredReportGeneratorService filteredReportGeneratorService,
            IReportSenderService reportSenderService,IDetailedProviderReport detailedProviderReport)
        {
            _reportGeneratorService = reportGeneratorService;
            _filteredReportGeneratorService = filteredReportGeneratorService;
            _reportSenderService = reportSenderService;
            _detailedProviderReport = detailedProviderReport;
        }
     

        [HttpGet(ReportRoutes.ManagerUri + "{managerId}" + ReportRoutes.StartDateUri + "{startDate}" + ReportRoutes.EndDateUri + "{endDate}" + ReportRoutes.DownloadUri)]
        public async Task<ActionResult> GetManagerReport(Guid managerId, DateTime startDate, DateTime endDate)
        {
            if (startDate > endDate)
            {
                return BadRequest(new { message = WrongPeriodMessage });
            }
            byte[] content = await _reportGeneratorService.GenerateReportAsync(managerId, startDate, endDate);
            return new FileContentResult(content, XlsxContentType);
        }

        [HttpGet(ReportRoutes.ManagerUri + "{managerId}" + ReportRoutes.StartDateUri + "{startDate}" + ReportRoutes.EndDateUri + "{endDate}" + ReportRoutes.SendMailUri)]
        public async Task<ActionResult> SendManagerReport(Guid managerId, DateTime startDate, DateTime endDate)
        {
            if (startDate > endDate)
            {
                return BadRequest(new { message = WrongPeriodMessage });
            }
            byte[] content = await _reportGeneratorService.GenerateReportAsync(managerId, startDate, endDate);
            string email = await _reportSenderService.SendReportFileAsync(managerId, content);
            return Ok(string.Format(SuccessMailMessage, email));
        }

        [HttpGet(ReportRoutes.ManagerUri + "{managerId}" + ReportRoutes.StartDateUri + "{startDate}" + ReportRoutes.EndDateUri + "{endDate}" + ReportRoutes.LunchGroupUri + "{lunchGroupId}" + ReportRoutes.DownloadUri)]
        public async Task<ActionResult> GetWorkersLunchGroupReport(Guid managerId, DateTime startDate, DateTime endDate, Guid lunchGroupId)
        {
            if (startDate > endDate)
            {
                return BadRequest(new { message = WrongPeriodMessage });
            }
            byte[] content = await _filteredReportGeneratorService.GenerateReportAsync(managerId, startDate, endDate, lunchGroupId);
            return new FileContentResult(content, XlsxContentType);
        }

        [HttpGet(ReportRoutes.ManagerUri + "{managerId}" + ReportRoutes.StartDateUri + "{startDate}" + ReportRoutes.EndDateUri + "{endDate}" + ReportRoutes.LunchGroupUri + "{lunchGroupId}" + ReportRoutes.SendMailUri)]
        public async Task<ActionResult> SendWorkersLunchGroupReport(Guid managerId, DateTime startDate, DateTime endDate, Guid lunchGroupId)
        {
            if (startDate > endDate)
            {
                return BadRequest(new { message = WrongPeriodMessage });
            }
            byte[] content = await _filteredReportGeneratorService.GenerateReportAsync(managerId, startDate, endDate, lunchGroupId);
            string email = await _reportSenderService.SendReportFileAsync(managerId, content);
            return Ok(string.Format(SuccessMailMessage, email));
        }

        [HttpGet(ReportRoutes.WorkerUri + "{workerId}" + ReportRoutes.StartDateUri + "{startDate}" + ReportRoutes.EndDateUri + "{endDate}" + ReportRoutes.DownloadUri)]
        public async Task<ActionResult> GetWorkerReport(Guid workerId, DateTime startDate, DateTime endDate)
        
        {
            const string ContentType = "Application / vnd.ms-excel";
            byte[] content = await _reportGeneratorService.GenerateReportAsync(workerId, startDate, endDate);
            return new FileContentResult(content, ContentType);
        }

        [HttpGet(ReportRoutes.WorkerUri + "{workerId}" + ReportRoutes.StartDateUri + "{startDate}" + ReportRoutes.EndDateUri + "{endDate}" + ReportRoutes.SendMailUri)]
        public async Task<ActionResult> SendWorkerReport(Guid workerId, DateTime startDate, DateTime endDate)
        {
            byte[] content = await _reportGeneratorService.GenerateReportAsync(workerId, startDate, endDate);
            string email = await _reportSenderService.SendReportFileAsync(workerId, content);
            return Ok(string.Format(SuccessMailMessage, email));
        }




        [HttpGet(ReportRoutes.ProviderUri + "{providerId}" + ReportRoutes.DownloadUri)]
        public async Task<ActionResult> GetProviderReport(Guid providerId, DateTime startDate, DateTime endDate)

        {
            byte[] content = await _reportGeneratorService.GenerateReportAsync(providerId, startDate, endDate);
            return new FileContentResult(content, ContentType);
        }

        [HttpGet(ReportRoutes.ProviderUri + "{providerId}" + ReportRoutes.SendMailUri)]
        public async Task<ActionResult> SendProviderReport(Guid providerId, DateTime startDate, DateTime endDate)
        {
            byte[] content = await _reportGeneratorService.GenerateReportAsync(providerId, startDate, endDate);
            string email = await _reportSenderService.SendReportFileAsync(providerId, content);
            return Ok(string.Format(SuccessMailMessage, email));
        }

        
        [HttpGet(ReportRoutes.ProviderUri + "{providerId}/" + "consumerName/" + "{consumerCompanyName}" + ReportRoutes.StartDateUri + "{startDate}" + ReportRoutes.EndDateUri + "{endDate}" + ReportRoutes.DownloadUri)]
        public async Task<ActionResult> GetDetailedProviderReport(Guid providerId, string consumerCompanyName, DateTime startDate, DateTime endDate)
        {
            byte[] content = await _detailedProviderReport.GenerateProviderDetailedReportAsync(providerId, consumerCompanyName, startDate, endDate);
            return new FileContentResult(content, ContentType);
        }

        [HttpGet(ReportRoutes.ProviderUri + "{providerId}/" + "consumerName/" + "{consumerCompanyName}" + ReportRoutes.StartDateUri + "{startDate}" + ReportRoutes.EndDateUri + "{endDate}" + ReportRoutes.SendMailUri)]
        public async Task<ActionResult> SendDetailedProviderReport(Guid providerId,string consumerCompanyName, DateTime startDate, DateTime endDate)
        {
            byte[] content = await _detailedProviderReport.GenerateProviderDetailedReportAsync(providerId, consumerCompanyName, startDate, endDate);
            string email = await _reportSenderService.SendReportFileAsync(providerId, content);
            return Ok(string.Format(SuccessMailMessage, email));
        }
    }
}
