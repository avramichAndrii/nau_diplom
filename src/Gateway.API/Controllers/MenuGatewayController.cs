using System;
using System.Linq;
using System.Threading.Tasks;
using Gateway.Application.Abstractions;
using Company.API.Contracts.Request;
using Company.API.Contracts.Response;
using Menu.API.Contracts.Requests;
using Menu.API.Contracts.Responses;
using Menu.API.Contracts.Routs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using UserManagement.Domain.Models;

namespace Gateway.API
{
    [Authorize]
    [ApiController]
    [Route("gateway")]
    public class MenuGatewayController : ControllerBase
    {
        private readonly IMenuRedirectorService _menuRedirectorService;
        private readonly ICompanyRedirectorService _companyRedirectorService;

        public MenuGatewayController(IMenuRedirectorService menuRedirectorService, ICompanyRedirectorService redirectorService)
        {
            _menuRedirectorService = menuRedirectorService;
            _companyRedirectorService = redirectorService;
        }
        
        private async Task UpdateContractStatusWhenMenuChanges(Guid menuId, string status)
        {
            var getContractsResponse = await _companyRedirectorService.GetContractsAsync();
            var contracts = getContractsResponse.ContractResponses
                .Where(c => c.MenuId == menuId)
                .Where(c => c.ContractStatus == "Active").ToList<ContractResponse>();
            foreach (var contract in contracts)
            {
                ContractRequest contractRequest = new ContractRequest()
                {
                    ContractNumber = contract.ContractNumber,
                    ContractStatus = status,
                    ConsumerCompanyId = contract.ConsumerCompanyId,
                    ProviderCompanyId = contract.ProviderCompanyId,
                    MenuId = contract.MenuId
                };
                await _companyRedirectorService.UpdateContractAsync(contract.Id, contractRequest);
            }
        }
        #region menu

        [Authorize(Roles = Role.Admin)]
        [HttpGet(MenuRouting.BaseMenuURL)]
        public async Task<ActionResult<GetAllMenusResponse>> GetAllMenusAsync()
        {
            return await _menuRedirectorService.GetMenusAsync();
        }

        [HttpGet(MenuRouting.BaseMenuURLSlash + "{menuId}")]
        public async Task<ActionResult<MenuResponse>> GetMenuByIdAsync(Guid menuId)
        {
            return await _menuRedirectorService.GetMenuByIdAsync(menuId);
        }

        [HttpGet(MenuRouting.BaseMenuURLSlash + "{menuId}" + "/dishes")]
        public async Task<ActionResult<GetAllDishesResponse>> GetDishesByMenuAsync(Guid menuId)
        {
            return await _menuRedirectorService.GetDishesByMenuAsync(menuId);
        }

        [HttpGet(MenuRouting.GetByProviderURL + "{providerId}")]
        public async Task<ActionResult<GetAllMenusResponse>> GetMenuesByProviderAsync(Guid providerId)
        {
            return await _menuRedirectorService.GetByProviderAsync(providerId);
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Provider)]
        [HttpPost(MenuRouting.BaseMenuURL)]
        public async Task<ActionResult<MenuResponse>> CreateMenuAsync([FromBody] MenuRequest request)
        {
            return await _menuRedirectorService.CreateMenuAsync(request);
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Provider)]
        [HttpPut(MenuRouting.BaseMenuURLSlash + "{menuId}")]
        public async Task<ActionResult<MenuResponse>> UpdateMenuAsync(Guid menuId, [FromBody] MenuRequest request)
        {
            var menuResponse = await _menuRedirectorService.UpdateMenuAsync(menuId, request);
            await UpdateContractStatusWhenMenuChanges(menuId, "Changed");
            return menuResponse;
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Provider)]
        [HttpDelete(MenuRouting.BaseMenuURLSlash + "{menuId}")]
        public async Task<IActionResult> DeleteMenuAsync(Guid menuId)
        {
            await _menuRedirectorService.DeleteMenuAsync(menuId);
            await UpdateContractStatusWhenMenuChanges(menuId, "Cancelled");
            return Ok();
        }

        [HttpGet(MenuRouting.BaseMenuCompanyURL + "{companyId}")]
        public async Task<ActionResult<GetAllMenusResponse>> GetAllMenusByContractsAsync(Guid companyId)
        {
            GetContractsResponse contracts = await _companyRedirectorService.GetContractsByStatusAndCompanyTypeAndIdAsync(companyId, "Consumer", "Active");
            return await _menuRedirectorService.GetAllMenusByContractsAsync(contracts);
        }
        #endregion

        #region dish

        [Authorize(Roles = Role.Admin)]
        [HttpGet(MenuRouting.BaseDishURL)]
        public async Task<ActionResult<GetAllDishesResponse>> GetAllDishesAsync()
        {
            return await _menuRedirectorService.GetDishesAsync();
        }
        [HttpGet(MenuRouting.BaseDishURLSlash + "{dishId}")]
        public async Task<ActionResult<DishResponse>> GetDishByIdAsync(Guid dishId)
        {
            return await _menuRedirectorService.GetDishByIdAsync(dishId);
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Provider)]
        [HttpPost(MenuRouting.BaseDishURL)]
        public async Task<ActionResult<DishResponse>> CreateDishAsync([FromBody] DishRequest request)
        {
            var dishResponse = await _menuRedirectorService.CreateDishAsync(request);
            await UpdateContractStatusWhenMenuChanges(dishResponse.MenuID, "Changed");
            return dishResponse;
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Provider)]
        [HttpPut(MenuRouting.BaseDishURLSlash + "{dishId}")]
        public async Task<ActionResult<DishResponse>> UpdateDishAsync(Guid dishId, [FromBody] DishRequest request)
        {
            var dishResponse = await _menuRedirectorService.UpdateDishAsync(dishId, request);
            await UpdateContractStatusWhenMenuChanges(dishResponse.MenuID, "Changed");
            return dishResponse;
        }

        [HttpPatch(MenuRouting.BaseDishURLSlash + "{dishId}")]
        public async Task<ActionResult<DishResponse>> UpdateDishIsActiveAsync(Guid dishId, [FromBody] UpdateDishIsActiveRequest request)
        {
            var dishResponse = await _menuRedirectorService.UpdateDishIsActiveAsync(dishId, request);
            return dishResponse;
        }

        [HttpDelete(MenuRouting.BaseDishURLSlash + "{dishId}")]
        public async Task<IActionResult> DeleteDishAsync(Guid dishId)
        {
            await _menuRedirectorService.DeleteDishAsync(dishId);
            //await UpdateContractStatusWhenMenuChanges(menuId, "Changed"); TO DO - need to get menuId!
            return Ok();
        }
        #endregion

        #region marketplace

        [Authorize(Roles = Role.Admin + ", " + Role.Provider + ", " + Role.Manager)]
        [HttpGet(MenuRouting.BaseMarketplaceURL)]
        public async Task<ActionResult<GetAllMenusResponse>> GetAllOffersAsync()
        {
            return await _menuRedirectorService.GetOffersAsync();
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Provider + ", " + Role.Manager)]
        [HttpGet(MenuRouting.BaseMarketplaceURLSlash + "{offerId}")]
        public async Task<ActionResult<MenuResponse>> GetOfferAsync(Guid offerId)
        {
            return await _menuRedirectorService.GetOffersByIdAsync(offerId);
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Provider)]
        [HttpPut(MenuRouting.BaseMarketplaceURLSlash + "{menuId}")]
        public async Task<ActionResult<MenuResponse>> UpdateIsInMarketplaceAsync(Guid menuId, [FromBody] MarketplacePutRequest request)
        {
            var menuResponse = await _menuRedirectorService.UpdateIsInMarketplaceAsync(menuId, request);
            await UpdateContractStatusWhenMenuChanges(menuId, "Changed");
            return menuResponse;
        }

        #endregion
    }
}