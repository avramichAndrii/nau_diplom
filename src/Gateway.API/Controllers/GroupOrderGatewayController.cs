using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Gateway.Application.Abstractions;
using Gateway.Application.Services;
using GroupOrder.API.Contracts;
using GroupOrder.API.Contracts.Requests;
using GroupOrder.API.Contracts.Responses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UserManagement.Domain.Models;

namespace Gateway.API
{
    [Authorize]
    [ApiController]
    [Route("gateway")]
    public class GroupOrderGatewayController : ControllerBase
    {
        private readonly IGroupOrderRedirectorService _groupOrderRedirectorService;
        private readonly IOrderProcessingService _orderProcessingService;
        
        public GroupOrderGatewayController(IGroupOrderRedirectorService groupOrderRedirectorService, 
            IOrderProcessingService orderProcessingService)
        {
            _groupOrderRedirectorService = groupOrderRedirectorService;
            _orderProcessingService = orderProcessingService;
        }
        #region GroupOrder

        [Authorize(Roles = Role.Admin)]
        [HttpGet(GroupOrderRoutes.GroupOrderUri)]
        public async Task<ActionResult<GetAllGroupOrdersResponse>> GetAllGroupOrdersAsync()
        {
            return await _groupOrderRedirectorService.GetGroupOrdersAsync();
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Provider + ", " + Role.Manager)]
        [HttpGet(GroupOrderRoutes.GroupOrderUri + "/" + GroupOrderRoutes.ProviderUri + "/{" + GroupOrderRoutes.ProviderUri + "Id}")]
        public async Task<ActionResult<GetAllGroupOrdersResponse>> GetGroupOrdersByProviderIdAsync(Guid providerId)
        {
            return await _groupOrderRedirectorService.GetGroupOrdersByProviderIdAsync(providerId);
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Manager)]
        [HttpGet(GroupOrderRoutes.GroupOrderUri + "/" + GroupOrderRoutes.LunchGroupUri + "/{" + GroupOrderRoutes.LunchGroupUri + "Id}")]
        public async Task<ActionResult<GetAllGroupOrdersResponse>> GetGroupOrdersByLunchGroupIdAsync(Guid lunchGroupId)
        {
            return await _groupOrderRedirectorService.GetAllByLunchGroupsIdAsync(lunchGroupId);
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Provider + ", " + Role.Manager)]
        [HttpGet(GroupOrderRoutes.GroupOrderUri + "/{id}")]
        public async Task<ActionResult<GroupOrderResponse>> GetGroupOrderByIdAsync(Guid id)
        {
            return await _groupOrderRedirectorService.GetGroupOrderByIdAsync(id);
        }

        [HttpPost(GroupOrderRoutes.GroupOrderUri)]
        public async Task<ActionResult<GroupOrderResponse>> CreateGroupOrderAsync([FromBody] GroupOrderRequest request)
        {
            return await _groupOrderRedirectorService.CreateGroupOrderAsync(request);
        }

        [HttpDelete(GroupOrderRoutes.GroupOrderUri + "/{id}")]
        public async Task<ActionResult> DeleteGroupOrderAsync(Guid id)
        {
            await _groupOrderRedirectorService.DeleteGroupOrderAsync(id);
            return Ok();
        }
        #endregion

        #region Order

        [Authorize(Roles = Role.Admin)]
        [HttpGet(GroupOrderRoutes.OrderUri)]
        public async Task<ActionResult<GetAllOrdersResponse>> GetAllOrdersAsync()
        {
            return await _groupOrderRedirectorService.GetOrdersAsync();
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Provider + ", " + Role.Manager)]
        [HttpGet(GroupOrderRoutes.OrderUri + "/" + GroupOrderRoutes.ProviderUri + "/{" + GroupOrderRoutes.ProviderUri + "Id}")]
        public async Task<ActionResult<GetAllOrdersResponse>> GetOrdersByProviderIdAsync(Guid providerId)
        {
            return await _groupOrderRedirectorService.GetOrdersByProviderIdAsync(providerId);
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Manager + ", " + Role.Worker)]
        [HttpGet(GroupOrderRoutes.OrderUri + "/" + GroupOrderRoutes.WorkerUri + "/{" + GroupOrderRoutes.WorkerUri + "Id}")]
        public async Task<ActionResult<GetAllOrdersResponse>> GetOrdersByWorkerIdAsync(Guid workerId)
        {
            return await _groupOrderRedirectorService.GetOrdersByWorkerIdAsync(workerId);
        }

        [HttpGet(GroupOrderRoutes.OrderUri + "/{id}")]
        public async Task<ActionResult<OrderResponse>> GetOrderByIdAsync(Guid id)
        {
            return await _groupOrderRedirectorService.GetOrderByIdAsync(id);
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Manager + ", " + Role.Worker)]
        [HttpPost(GroupOrderRoutes.OrderUri)]
        public async Task<ActionResult<OrderResponse>> CreateOrderAsync([FromBody] OrderRequest request)
        {
            return await _orderProcessingService.CreateOrderByWorker(request);
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Manager + ", " + Role.Worker)]
        [HttpDelete(GroupOrderRoutes.OrderUri + "/{id}")]
        public async Task<ActionResult> DeleteOrderAsync(Guid orderId)
        {
            await _groupOrderRedirectorService.DeleteOrderAsync(orderId);
            return Ok();
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Manager + ", " + Role.Worker)]
        [HttpPut(GroupOrderRoutes.OrderUri)]
        public async Task<ActionResult<OrderResponse>> UpdateOrderAsync([FromBody]OrderRequest request)
        {
            return await _groupOrderRedirectorService.UpdateOrderAsync(request);
        }

        #endregion

        #region OrderTemplate

        [Authorize(Roles = Role.Admin)]
        [HttpGet(GroupOrderRoutes.OrderTemplateUri)]
        public async Task<ActionResult<GetAllOrderTemplatesResponse>> GetAllOrderTemplatesAsync()
        {
            return await _groupOrderRedirectorService.GetOrderTemplatesAsync();
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Worker)]
        [HttpGet(GroupOrderRoutes.OrderTemplateUri + "/" + GroupOrderRoutes.ProviderUri + "/{" + GroupOrderRoutes.ProviderUri + "Id}")]
        public async Task<ActionResult<GetAllOrderTemplatesResponse>> GetOrderTemplatesByProviderIdAsync(Guid providerId)
        {
            return await _groupOrderRedirectorService.GetOrderTemplatesByProviderIdAsync(providerId);
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Worker)]
        [HttpGet(GroupOrderRoutes.OrderTemplateUri + "/" + GroupOrderRoutes.WorkerUri + "/{" + GroupOrderRoutes.WorkerUri + "Id}")]
        public async Task<ActionResult<GetAllOrderTemplatesResponse>> GetOrderTemplatesByWorkerIdAsync(Guid workerId)
        {
            if (!CheckUserId(workerId))
            {
                return Forbid();
            }
            return await _groupOrderRedirectorService.GetOrderTemplatesByWorkerIdAsync(workerId);
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Worker)]
        [HttpGet(GroupOrderRoutes.OrderTemplateUri + "/{id}")]
        public async Task<ActionResult<OrderTemplateResponse>> GetOrderTemplateByIdAsync(Guid id)
        {
            return await _groupOrderRedirectorService.GetOrderTemplateByIdAsync(id);
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Worker)]
        [HttpPost(GroupOrderRoutes.OrderTemplateUri)]
        public async Task<ActionResult<OrderTemplateResponse>> CreateOrderTemplateAsync([FromBody]OrderTemplateRequest request)
        {
            return await _groupOrderRedirectorService.CreateOrderTemplateAsync(request);
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Worker)]
        [HttpDelete(GroupOrderRoutes.OrderTemplateUri + "/{id}")]
        public async Task<ActionResult> DeleteOrderTemplateAsync(Guid id)
        {
            await _groupOrderRedirectorService.DeleteOrderTemplateAsync(id);
            return Ok();
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Worker)]
        [HttpPatch(GroupOrderRoutes.OrderTemplateUri + "/{id}/name")]
        public async Task<ActionResult> UpdateOrderTemplateNameAsync(Guid id, [FromBody] OrderTemplateUpdateNameRequest request)
        {
            await _groupOrderRedirectorService.UpdateOrderTemplateNameAsync(id, request);
            return Ok();
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Worker)]
        [HttpPatch(GroupOrderRoutes.OrderTemplateUri + "/{id}/dishes")]
        public async Task<ActionResult> UpdateOrderTemplateDishesAsync(Guid id, [FromBody] OrderTemplateUpdateDishesRequest request)
        {
            await _groupOrderRedirectorService.UpdateOrderTemplateDishesAsync(id, request);
            return Ok();
        }

        #endregion

        #region Cart

        [Authorize(Roles = Role.Admin + ", " + Role.Worker)]
        [HttpGet(GroupOrderRoutes.CartUri + "/" + GroupOrderRoutes.WorkerUri + "/{" + GroupOrderRoutes.WorkerUri +  "Id}")]
        public async Task<ActionResult<CartResponse>> GetCartByWorkerIdAsync(Guid workerId)
        {
            return await _groupOrderRedirectorService.GetCartByWorkerIdAsync(workerId);
        }

        [HttpPost(GroupOrderRoutes.CartUri)]
        public async Task<ActionResult<CartResponse>> CreateCartAsync([FromBody] CartRequest request)
        {
            return await _groupOrderRedirectorService.CreateCartAsync(request);
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Worker)]
        [HttpPut(GroupOrderRoutes.CartUri)]
        public async Task<ActionResult<CartResponse>> UpdateCartAsync([FromBody] CartRequest request)
        {
            return await _groupOrderRedirectorService.UpdateCartAsync(request);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpDelete(GroupOrderRoutes.CartUri + "/{id}")]
        public async Task<ActionResult> DeleteCartAsync(Guid id)
        {
            await _groupOrderRedirectorService.DeleteCartAsync(id);
            return Ok();
        }
        #endregion

        #region Helpers

        /// <summary>
        /// Defines whether the current user is able to impact user with the requested id
        /// </summary>
        /// <param name="id">Requested user id</param>
        /// <returns></returns>
        private bool CheckUserId(Guid id)
        {
            string role = User.FindFirst(x => x.Type == ClaimsIdentity.DefaultRoleClaimType).Value;

            if (role == Role.Admin)
            {
                // Admin can do anything
                return true;
            }

            Guid currentUserId = Guid.Parse(User.FindFirst(x => x.Type == ClaimsIdentity.DefaultNameClaimType).Value);

            // Other roles can impact only themselves
            return currentUserId == id;
        }

        #endregion
    }
}