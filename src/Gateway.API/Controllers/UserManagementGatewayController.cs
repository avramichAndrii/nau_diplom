using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Gateway.Application;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UserManagement.API.Contracts.Requests;
using UserManagement.API.Contracts.Responses;
using UserManagement.API.Contracts.Routs;
using UserManagement.Domain.Models;

namespace Gateway.API
{
    [Authorize]
    [ApiController]
    [Route("gateway")]
    public class UserManagementGatewayController : ControllerBase
    {
        private readonly IUserManagementRedirectorService _userManagementRedirectorService;

        public UserManagementGatewayController(IUserManagementRedirectorService UMRedirector)
        {
            _userManagementRedirectorService = UMRedirector;
        }
        
        [AllowAnonymous]
        [HttpPost(UserManagementRouting.AuthenticateUri)]
        public async Task<AuthenticateResponse> AuthenticateAsync([FromBody] AuthenticateRequest model)
        {
            return await _userManagementRedirectorService.AuthenticateAsync(model);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpGet(UserManagementRouting.UsersUri)]
        public async Task<ActionResult<UsersCollectionResponse>> UsersAsync()
        {
            return await _userManagementRedirectorService.GetUsersAsync();
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Manager)]
        [HttpGet(UserManagementRouting.UsersUri + "/" + UserManagementRouting.CompanyUri + "/{companyId}/" + UserManagementRouting.RoleUri + "/{role}")]
        public async Task<ActionResult<UsersCollectionResponse>> GetUsersByCompanyAndRoleAsync(Guid companyId, string role)
        {
            return await _userManagementRedirectorService.GetUsersByCompanyAndRoleAsync(companyId, role);
        }

        [AllowAnonymous]
        [HttpPost(UserManagementRouting.RegisterUri)]
        public async Task<ActionResult<UserResponse>> RegisterAsync([FromBody] RegisterRequest model)
        {
            return await _userManagementRedirectorService.RegisterAsync(model);
        }

        [HttpGet("users/{id}")]
        public async Task<ActionResult<UserResponse>> GetByIdAsync(Guid id)
        {
            if (!CheckUserId(id))
            {
                return Forbid();
            }

            return await _userManagementRedirectorService.GetByIdAsync(id);
        }

        [HttpDelete("users/{id}")]
        public async Task<ActionResult<Guid>> DeleteByIdAsync(Guid id)
        {
            if (!CheckUserId(id))
            {
                return Forbid();
            }

            return await _userManagementRedirectorService.DeleteByIdAsync(id);
        }

        [HttpPut("users/{id}")]
        public async Task<ActionResult<UserResponse>> UpdateByIdAsync(Guid id, [FromBody] UpdateRequest model)
        {
            if (!CheckUserId(id))
            {
                return Forbid();
            }

            return await _userManagementRedirectorService.UpdateByIdAsync(id, model);
        }

        [HttpPatch("users/{id}")]
        public async Task SetCompanyForUserAsync(Guid id, [FromBody] SetCompanyRequest request)
        {
            await _userManagementRedirectorService.SetCompanyForUserAsync(id, request);
        }

        [HttpPut("user/change-password/{id}")]
        public Task UpdatePasswordIdAsync(Guid id, [FromBody] UpdatePasswordRequest model)
        {
            return _userManagementRedirectorService.UpdatePasswordAsync(id, model);
        }

        [HttpPost("refresh-token/{token}")]
        public async Task<ActionResult<AuthenticateResponse>> RefreshTokenAsync(Guid token)
        {
            return await _userManagementRedirectorService.RefreshToken(token);
        }


        [HttpPost("user/forgot-password")]
        public async Task<HandleForgottenPasswordResponse> HandleForgottenPasswordAsync([FromBody] ForgotPasswordRequest request)
        {
            return await _userManagementRedirectorService.HandleForgottenPassword(request);
        }
        [HttpPost("user/reset-password")]
        public async Task<UserResponse> ResetPassword([FromBody] ResetPasswordRequest request)
        {
            return await _userManagementRedirectorService.ResetPassword(request);
        }

        /// <summary>
        /// Defines whether the current user is able to impact user with the requested id
        /// </summary>
        /// <param name="id">Requested user id</param>
        /// <returns></returns>
        private bool CheckUserId(Guid id)
        {
            string role = User.FindFirst(x => x.Type == ClaimsIdentity.DefaultRoleClaimType).Value;

            if (role == Role.Admin)
            {
                // Admin can do anything
                return true;
            }

            Guid currentUserId = Guid.Parse(User.FindFirst(x => x.Type == ClaimsIdentity.DefaultNameClaimType).Value);

            // Other roles can impact only themselves
            return currentUserId == id;
        }
    }
}