using System;
using System.Threading.Tasks;
using Gateway.Application.Abstractions;
using Company.API.Contracts;
using Company.API.Contracts.Request;
using Company.API.Contracts.Response;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using UserManagement.Domain.Models;

namespace Gateway.API
{
    [Authorize]
    [ApiController]
    [Route("gateway")]
    public class CompanyGatewayController : ControllerBase
    {
        private readonly ICompanyRedirectorService _companyRedirectorService;

        public CompanyGatewayController(ICompanyRedirectorService redirectorService)
        {
            _companyRedirectorService = redirectorService;
        }

        #region Company

        [Authorize(Roles = Role.Admin)]
        [HttpGet(CompanyRoutes.BaseCompanyUri)]
        public async Task<ActionResult<GetCompaniesResponse>> GetAllCompanies()
        {
            return await _companyRedirectorService.GetCompaniesAsync();
        }

        [HttpGet(CompanyRoutes.BaseCompanyUri + "/{id}")]
        public async Task<ActionResult<CompanyResponse>> GetCompany(Guid id)
        {
            return await _companyRedirectorService.GetCompanyAsync(id);
        }

        [HttpGet(CompanyRoutes.BaseCompanyUri + "/" + CompanyRoutes.CompanyTypeSlash + "{type}")]
        public async Task<ActionResult<GetCompaniesResponse>> GetCompanyByType(string type)
        {
            return await _companyRedirectorService.GetCompaniesByTypeAsync(type);
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Provider + ", " + Role.Manager)]
        [HttpPost(CompanyRoutes.BaseCompanyUri)]
        public async Task<ActionResult<CompanyResponse>> CreateCompany([FromBody] CompanyRequest request)
        {
            return await _companyRedirectorService.CreateCompanyAsync(request);
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Provider + ", " + Role.Manager)]
        [HttpPut(CompanyRoutes.BaseCompanyUri + "/{id}")]
        public async Task<ActionResult<CompanyResponse>> UpdateCompany(Guid id, [FromBody] CompanyRequest request)
        {
            return await _companyRedirectorService.UpdateCompanyAsync(id, request);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpDelete(CompanyRoutes.BaseCompanyUri + "/{id}")]
        public async Task<ActionResult<DeleteCompanyResponse>> DeleteCompany(Guid id)
        {
            return await _companyRedirectorService.RemoveCompanyAsync(id);
        }

        #endregion

        #region Contract

        [Authorize(Roles = Role.Admin + ", " + Role.Provider + ", " + Role.Manager)]
        [HttpGet(CompanyRoutes.BaseContractUri)]
        public async Task<ActionResult<GetContractsResponse>> GetContracts()
        {
            return await _companyRedirectorService.GetContractsAsync();
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Provider + ", " + Role.Manager)]
        [HttpGet(CompanyRoutes.BaseContractUri + "/{id}")]
        public async Task<ActionResult<ContractResponse>> GetContractById(Guid id)
        {
            return await _companyRedirectorService.GetContractByIdAsync(id);
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Provider + ", " + Role.Manager)]
        [HttpGet(CompanyRoutes.BaseContractUri + "/{type}/" + "{companyId}")]
        public async Task<ActionResult<GetContractsResponse>> GetContractsByCompanyTypeAndId(Guid companyId, string type)
        {
            return await _companyRedirectorService.GetContractsByCompanyTypeAndIdAsync(companyId, type);
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Provider + ", " + Role.Manager)]
        [HttpGet(CompanyRoutes.BaseContractUri + "/{status}/" + "{type}/" + "{companyId}")]
        public async Task<ActionResult<GetContractsResponse>> GetContractsByStatusAndCompanyTypeAndId
            (Guid companyId, string type, string status)
        {
            return await _companyRedirectorService.GetContractsByStatusAndCompanyTypeAndIdAsync(companyId, type, status);
        }
        
        [HttpGet(CompanyRoutes.BaseContractUri + "/{status}/" + "{type}/" + "{companyId}/" + "{menuId}")]
        public async Task<ActionResult<GetContractsResponse>> GetContractsByMenuId
            (Guid companyId, string type, string status, Guid menuId)
        {
            var contracts = await _companyRedirectorService.GetContractsByStatusAndCompanyTypeAndIdAsync(companyId, type, status);
            var contractsByMenuId =new GetContractsResponse(contracts.ContractResponses.Where(contract => contract.MenuId == menuId).ToList());
            return contractsByMenuId;
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Manager)]
        [HttpPost(CompanyRoutes.BaseContractUri)]
        public async Task<ActionResult<ContractResponse>> CreateContract([FromBody] ContractRequest request)
        {
            return await _companyRedirectorService.CreateContractAsync(request);
        }

        [Authorize(Roles = Role.Admin + ", " + Role.Provider + ", " + Role.Manager)]
        [HttpPut(CompanyRoutes.BaseContractUri + "/{id}")]
        public async Task<ActionResult<ContractResponse>> UpdateContract(Guid id, [FromBody] ContractRequest request)
        {
            return await _companyRedirectorService.UpdateContractAsync(id, request);
        }

        #endregion
    }
}