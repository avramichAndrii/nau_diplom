﻿using System;

namespace Gateway.API.Contracts
{
    public class SendInvitationRequest
    {
        public Guid InviterId { get; set; }
        public string InvitedUserRole { get; set; }
        public string InvitedUserMail { get; set; }
    }
}
