﻿using UserManagement.API.Contracts.Requests;

namespace Gateway.API.Contracts
{
    public class MailRegisterRequest
    {
        public string Token { get; set; }
        public RegisterRequest UserRegisterRequest { get; set; }
    }
}
