﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gateway.API.Contracts
{
    public class CreateDeliveriesByLunchGroupRequest
    {
        public Guid LunchGroupId { get; set; }
    }
}
