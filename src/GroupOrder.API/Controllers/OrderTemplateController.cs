﻿using GroupOrder.API.Contracts.Requests;
using GroupOrder.API.Contracts.Responses;
using GroupOrder.API.Mappers;
using GroupOrder.Domain.Exceptions;
using GroupOrder.Domain.Models;
using GroupOrder.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GroupOrder.API.Contracts;

namespace GroupOrder.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrderTemplateController : ControllerBase
    {
        private readonly IOrderTemplateService _orderTemplateService;

        public OrderTemplateController(IOrderTemplateService orderTemplateService)
        {
            _orderTemplateService = orderTemplateService;
        }

        [HttpGet]
        public async Task<ActionResult<GetAllOrderTemplatesResponse>> GetAllAsync()
        {
            try
            {
                IReadOnlyCollection<OrderTemplateModel> templateModels = await _orderTemplateService.GetAll();
                return Ok(templateModels.MapToCollection());
            }
            catch (OrderTemplateDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<OrderTemplateResponse>> GetByIdAsync(Guid id)
        {
            try
            {
                OrderTemplateModel templateModel = await _orderTemplateService.GetById(id);
                return Ok(templateModel.MapTo());
            }
            catch (OrderTemplateDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpGet(GroupOrderRoutes.ProviderUri + "/{" + GroupOrderRoutes.ProviderUri + "Id}")]
        public async Task<ActionResult<GetAllOrderTemplatesResponse>> GetAllByProviderIdAsync(Guid providerId)
        {
            try
            {
                IReadOnlyCollection<OrderTemplateModel> templateModels = await _orderTemplateService.GetAllByProviderId(providerId);
                return Ok(templateModels.MapToCollection());
            }
            catch (OrderTemplateDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpGet(GroupOrderRoutes.WorkerUri + "/{" + GroupOrderRoutes.WorkerUri + "Id}")]
        public async Task<ActionResult<GetAllOrderTemplatesResponse>> GetAllByWorkerIdAsync(Guid workerId)
        {
            try
            {
                IReadOnlyCollection<OrderTemplateModel> templateModels = await _orderTemplateService.GetAllByWorkerId(workerId);
                return Ok(templateModels.MapToCollection());
            }
            catch (OrderTemplateDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            try
            {
                await _orderTemplateService.Delete(id);
                return Ok();
            }
            catch (OrderTemplateDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpPatch("{id}/name")]
        public async Task<ActionResult<OrderTemplateResponse>> UpdateNameAsync(Guid id, [FromBody] OrderTemplateUpdateNameRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Name))
            {
                return BadRequest("Name of order template must not be null, empty or consist of whitespaces only");
            }
            try
            {
                OrderTemplateModel result = await _orderTemplateService.UpdateName(request.MapFrom(id));
                return Ok(result.MapTo());
            }
            catch (OrderTemplateDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpPatch("{id}/dishes")]
        public async Task<ActionResult<OrderTemplateResponse>> UpdateDishesAsync(Guid id, [FromBody] OrderTemplateUpdateDishesRequest request)
        {
            try
            {
                OrderTemplateModel result = await _orderTemplateService.UpdateDishes(request.MapFrom(id));
                return Ok(result.MapTo());
            }
            catch (OrderTemplateDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpPost]
        public async Task<ActionResult<OrderTemplateResponse>> CreateAsync([FromBody] OrderTemplateRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new
                {
                    message = ModelState.Select(x => x.Value.Errors)
                                        .Where(y => y.Count > 0)
                                        .ToList()
                });
            }
            try
            {
                OrderTemplateModel response = await _orderTemplateService.Create(request.MapFrom());
                return Ok(response.MapTo());
            }
            catch (OrderTemplateDomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}
