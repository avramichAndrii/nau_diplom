﻿using GroupOrder.API.Contracts.Requests;
using GroupOrder.API.Contracts.Responses;
using GroupOrder.API.Mappers;
using GroupOrder.Domain.Exceptions;
using GroupOrder.Domain.Models;
using GroupOrder.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GroupOrder.API.Contracts;

namespace GroupOrder.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public OrderController( IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpPost]
        public async Task<ActionResult<OrderResponse>> CreateAsync([FromBody] OrderRequest order)
        {
            try
            {
                OrderModel model = await _orderService.Create(order.MapFrom(), order.LunchGroupId);
                return Ok(model.MapTo());
            }
            catch (InvalidDataGroupOrderDomainException idodEx)
            {
                return BadRequest(new { message = idodEx.Message });
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            try
            {
                await _orderService.Delete(id);
                return Ok();
            }
            catch (InvalidDataGroupOrderDomainException nfodEx)
            {
                return BadRequest(new { message = nfodEx.Message });
            }
            catch (NotFoundGroupOrderDomainException nfodEx)
            {
                return BadRequest(new { message = nfodEx.Message });
            }
        }

        [HttpGet]
        public async Task<ActionResult<GetAllOrdersResponse>> GetAllAsync()
        {
            IReadOnlyCollection<OrderModel> orderModels = await _orderService.GetAll();
            return Ok(orderModels.MapToCollection());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<OrderResponse>> GetByIdAsync(Guid Id)
        {
            try
            {
                OrderModel response = await _orderService.GetById(Id);
                return Ok(response.MapTo());
            }
            catch (InvalidDataGroupOrderDomainException nfodEx)
            {
                return BadRequest(new { message = nfodEx.Message });
            }
        }

        [HttpGet(GroupOrderRoutes.WorkerUri + "/{" + GroupOrderRoutes.WorkerUri + "Id}")]
        public async Task<ActionResult<GetAllOrdersResponse>> GetAllByWorkerIdAsync(Guid workerId)
        {
            try
            {
                IReadOnlyCollection<OrderModel> orderModels = await _orderService.GetAllByWorkerId(workerId);
                return Ok(orderModels.MapToCollection());
            }
            catch (NotFoundGroupOrderDomainException nfodEx)
            {
                return BadRequest(new { message = nfodEx.Message });
            }
        }

        [HttpGet(GroupOrderRoutes.ProviderUri + "/{" + GroupOrderRoutes.ProviderUri + "Id}")]
        public async Task<ActionResult<GetAllOrdersResponse>> GetByProviderIdAsync(Guid providerId)
        {
            try
            {
                IReadOnlyCollection<OrderModel> orderModels = await _orderService.GetAllByProviderId(providerId);
                return Ok(orderModels.MapToCollection());
            }
            catch (NotFoundGroupOrderDomainException nfodEx)
            {
                return BadRequest(new { message = nfodEx.Message });
            }
        }

        [HttpPut]
        public async Task<ActionResult<OrderResponse>> UpdateOrderAsync([FromBody] OrderRequest request)
        {
            try
            {
                OrderModel res = await _orderService.Update(request.UpdateMapFrom());
                return res.MapTo();
            }
            catch (NotFoundGroupOrderDomainException nfodEx)
            {
                return BadRequest(new { message = nfodEx.Message });
            }
        }
    }
}
