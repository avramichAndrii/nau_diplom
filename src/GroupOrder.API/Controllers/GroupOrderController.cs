﻿using GroupOrder.API.Contracts.Requests;
using GroupOrder.API.Contracts.Responses;
using GroupOrder.API.Mappers;
using GroupOrder.Domain.Exceptions;
using GroupOrder.Domain.Models;
using GroupOrder.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GroupOrder.API.Contracts;

namespace GroupOrder.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GroupOrderController : ControllerBase
    {
        private readonly IGroupOrderService _groupOrderService;
        public GroupOrderController(IGroupOrderService groupOrderService)
        {
            _groupOrderService = groupOrderService;
        }
        
        [HttpPost]
        public async Task<ActionResult<GroupOrderResponse>> CreateAsync([FromBody] GroupOrderRequest groupOrder)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new
                {
                    message = ModelState.Select(x => x.Value.Errors)
                        .Where(y => y.Count > 0)
                        .ToList()
                });
            }
            try
            {
                GroupOrderModel model = await _groupOrderService.Create(groupOrder.MapFrom());
                return Ok(model.MapTo());
            }
            catch (InvalidDataGroupOrderDomainException idgodEx)
            {
                return BadRequest(new { message = idgodEx.Message });
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            try
            {
                await _groupOrderService.Delete(id);
                return Ok();
            }
            catch (NotFoundGroupOrderDomainException idgodEx)
            {
                return BadRequest(new { message = idgodEx.Message });
            }
        }

        [HttpGet]
        public async Task<ActionResult<GetAllGroupOrdersResponse>> GetAllAsync()
        {
            IReadOnlyCollection<GroupOrderModel>  response = await _groupOrderService.GetAll();
            return Ok(response.MapToCollection());
        }
        
        [HttpGet(GroupOrderRoutes.ProviderUri + "/{" + GroupOrderRoutes.ProviderUri + "Id}")]
        public async Task<ActionResult<GetAllGroupOrdersResponse>> GetAllByProviderIdAsync(Guid providerId)
        {
            try
            {
                IReadOnlyCollection<GroupOrderModel> response = await _groupOrderService.GetAllByProviderId(providerId);
                return Ok(response.MapToCollection());
            }
            catch (NotFoundGroupOrderDomainException nfodEx)
            {
                return BadRequest(new { message = nfodEx.Message });
            }
        }

        [HttpGet(GroupOrderRoutes.LunchGroupUri + "/{" + GroupOrderRoutes.LunchGroupUri + "Id}")]
        public async Task<ActionResult<GetAllGroupOrdersResponse>> GetAllByLunchGroupIdAsync(Guid lunchGroupId)
        {
            try
            {
                IReadOnlyCollection<GroupOrderModel> response = await _groupOrderService.GetAllByLunchGroupId(lunchGroupId);
                return Ok(response.MapToCollection());
            }
            catch (NotFoundGroupOrderDomainException nfodEx)
            {
                return BadRequest(new { message = nfodEx.Message });
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<GroupOrderResponse>> GetByIdAsync(Guid id)
        {
            try
            {
                GroupOrderModel response = await _groupOrderService.GetById(id);
                return Ok(response.MapTo());
            }
            catch (NotFoundGroupOrderDomainException nfodEx)
            {
                return BadRequest(new { message = nfodEx.Message });
            }
        }
    }
}
