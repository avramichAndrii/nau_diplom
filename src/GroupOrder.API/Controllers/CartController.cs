﻿using GroupOrder.API.Contracts.Requests;
using GroupOrder.API.Contracts.Responses;
using GroupOrder.API.Mappers;
using GroupOrder.Domain.Exceptions;
using GroupOrder.Domain.Models;
using GroupOrder.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using GroupOrder.API.Contracts;

namespace GroupOrder.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CartController : ControllerBase
    {
        private readonly ICartService _cartService;

        public CartController(ICartService cartService)
        {
            _cartService = cartService;
        }

        [HttpPost]
        public async Task<ActionResult<CartResponse>> CreateAsync([FromBody] CartRequest cart)
        {
            try
            {
                CartModel model = await _cartService.Create(cart.MapFrom());
                return Ok(model.MapTo());
            }
            catch (InvalidDataGroupOrderDomainException idodEx)
            {
                return BadRequest(new { message = idodEx.Message });
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            try
            {
                await _cartService.Delete(id);
                return Ok();
            }
            catch (NotFoundGroupOrderDomainException nfodEx)
            {
                return BadRequest(new { message = nfodEx.Message });
            }
        }   

        [HttpGet(GroupOrderRoutes.WorkerUri + "/{" + GroupOrderRoutes.WorkerUri + "Id}")]
        public async Task<ActionResult<CartResponse>> GetByWorkerIdAsync(Guid workerId)
        {
            try
            {
                CartModel cartModel = await _cartService.GetCartByWorkerId(workerId);
                return Ok(cartModel.MapTo());
            }
            catch (NotFoundGroupOrderDomainException nfodEx)
            {
                return BadRequest(new { message = nfodEx.Message });
            }
        }        

        [HttpPut]
        public async Task<ActionResult<CartResponse>> UpdateAsync([FromBody] CartRequest request)
        {
            try
            {
                CartModel res = await _cartService.Update(request.UpdateMapFrom());
                return res.MapTo();
            }
            catch (NotFoundGroupOrderDomainException nfodEx)
            {
                return BadRequest(new { message = nfodEx.Message });
            }
        }
    }
}
