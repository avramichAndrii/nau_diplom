﻿using System;
using System.Collections.Generic;
using System.Linq;
using GroupOrder.API.Contracts.Requests;
using GroupOrder.API.Contracts.Responses;
using GroupOrder.Domain.Models;

namespace GroupOrder.API.Mappers
{
    public static class GroupOrderMapper
    {
        public static GroupOrderResponse MapTo(this GroupOrderModel response)
        {
            return new GroupOrderResponse(response.Id, response.ProviderId, response.LunchGroupId, OrderHelper(response.Orders), response.Date);
        }

        public static GetAllGroupOrdersResponse MapToCollection(this IReadOnlyCollection<GroupOrderModel> groupOrderModels)
        {
            return new GetAllGroupOrdersResponse(groupOrderModels.Select(groupModel => groupModel.MapTo()).ToList());
        }

        public static GroupOrderModel MapFrom(this GroupOrderRequest request)
        {
            return new GroupOrderModel(Guid.NewGuid(), request.ProviderId, request.LunchGroupId, request.Date);
        }

        private static List<OrderResponse> OrderHelper(IReadOnlyCollection<OrderModel> orders)
        {
            List<OrderResponse> orderList = new List<OrderResponse>();
            if (orders != null & orders.Count != 0)
            {
                foreach (OrderModel order in orders)
                {
                    OrderResponse orderResponse = order.MapTo();
                    orderList.Add(orderResponse);
                }
            }
            return orderList;
        }
    }
}