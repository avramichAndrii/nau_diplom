﻿using GroupOrder.API.Contracts.Requests;
using GroupOrder.API.Contracts.Responses;
using GroupOrder.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GroupOrder.API.Mappers
{
    public static class OrderMapper
    {
        public static GetAllOrdersResponse MapToCollection(this IReadOnlyCollection<OrderModel> orderModels)
        {
            return new GetAllOrdersResponse
            {
                OrdersCollection = orderModels.Select(om => om.MapTo()).ToList()
            };
        }

        public static OrderResponse MapTo(this OrderModel orderModel)
        {
            return new OrderResponse()
            {
                Id = orderModel.Id,
                ChosenDishes = orderModel.ChosenDishes,
                AdditionalInfo = orderModel.AdditionalInfo,
                Date = orderModel.Date,
                WorkerId = orderModel.WorkerId,
                ProviderId = orderModel.ProviderId,
                GroupOrderId = orderModel.GroupOrderId
            };
        }

        public static OrderModel MapFrom(this OrderRequest request)
        {
            return new OrderModel(Guid.NewGuid(), request.Date, request.AdditionalInfo, request.ProviderId, request.WorkerId, Guid.NewGuid(), request.ChosenDishes);
        }

        public static OrderModel UpdateMapFrom(this OrderRequest request)
        {
            return new OrderModel(request.Id, request.Date, request.AdditionalInfo, request.ProviderId, request.WorkerId, request.GroupOrderId, request.ChosenDishes);
        }
    }
}
