﻿using GroupOrder.API.Contracts.Responses;
using GroupOrder.Domain.Models;
using System;
using GroupOrder.API.Contracts.Requests;

namespace GroupOrder.API.Mappers
{
    public static class CartMapper
    {
        public static CartResponse MapTo(this CartModel cartModel)
        {
            return new CartResponse()
            {
                Id = cartModel.Id,
                ChosenDishes = cartModel.ChosenDishes,
                AdditionalInfo = cartModel.AdditionalInfo,
                WorkerId = cartModel.WorkerId,
                ProviderId = cartModel.ProviderId
            };
        }

        public static CartModel MapFrom(this CartRequest request)
        {
            return new CartModel(Guid.NewGuid(), request.AdditionalInfo, request.ProviderId, request.WorkerId, request.ChosenDishes);
        }

        public static CartModel UpdateMapFrom(this CartRequest request)
        {
            return new CartModel(request.Id, request.AdditionalInfo, request.ProviderId, request.WorkerId, request.ChosenDishes);
        }
    }
}
