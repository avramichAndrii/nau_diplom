﻿using GroupOrder.API.Contracts.Requests;
using GroupOrder.API.Contracts.Responses;
using GroupOrder.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GroupOrder.API.Mappers
{
    public static class OrderTemplateMapper
    {
        public static GetAllOrderTemplatesResponse MapToCollection(this IReadOnlyCollection<OrderTemplateModel> templateModels)
        {
            return new GetAllOrderTemplatesResponse(templateModels.Select(tm => tm.MapTo()).ToList());
        }

        public static OrderTemplateResponse MapTo(this OrderTemplateModel templateModel)
        {
            return new OrderTemplateResponse(templateModel.Id, templateModel.Name,
                                             templateModel.WorkerId, templateModel.ProviderId, templateModel.Dishes);
        }

        public static OrderTemplateModel MapFrom(this OrderTemplateRequest request)
        {
            return new OrderTemplateModel(Guid.NewGuid(), request.Name, request.WorkerId, new Guid(), new List<Guid>());
        }

        public static OrderTemplateModel MapFrom(this OrderTemplateUpdateNameRequest request, Guid id)
        {
            return new OrderTemplateModel(id, request.Name, new Guid(), new Guid(), new List<Guid>());
        }

        public static OrderTemplateModel MapFrom(this OrderTemplateUpdateDishesRequest request, Guid id)
        {
            return new OrderTemplateModel(id, string.Empty, new Guid(), request.ProviderId, request.Dishes.ToList());
        }
    }
}
