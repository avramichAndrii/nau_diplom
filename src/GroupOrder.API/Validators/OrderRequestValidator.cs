﻿using FluentValidation;
using GroupOrder.API.Contracts.Requests;

namespace GroupOrder.API.Validators
{
    public class OrderRequestValidator : AbstractValidator<OrderRequest>
    {
        private const string NotEmpty = "must not be empty";

        public OrderRequestValidator()
        {
            RuleFor(template => template.ChosenDishes).NotEmpty().WithMessage("List of dishes for order " + NotEmpty);
            RuleFor(template => template.WorkerId).NotEmpty().WithMessage("Worker id of order " + NotEmpty);
            RuleFor(template => template.ProviderId).NotEmpty().WithMessage("Provider id of order " + NotEmpty);
        }
    }
}
