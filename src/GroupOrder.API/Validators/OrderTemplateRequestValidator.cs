﻿using FluentValidation;
using GroupOrder.API.Contracts.Requests;

namespace GroupOrder.API.Validators
{
    public class OrderTemplateRequestValidator : AbstractValidator<OrderTemplateRequest>
    {
        private const string NotEmpty = "must not be empty";
        private const string NotEmptyOrWhitespace = "must not be empty or consist of whitespaces only";

        public OrderTemplateRequestValidator()
        {
            RuleFor(template => template.Name).NotEmpty().WithMessage("Name of template " + NotEmptyOrWhitespace);
            RuleFor(template => template.WorkerId).NotEmpty().WithMessage("Worker id of template " + NotEmpty);
        }
    }
}
