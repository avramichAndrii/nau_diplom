﻿using System;
using FluentValidation;
using GroupOrder.API.Contracts.Requests;

namespace GroupOrder.API.Validators
{
    public class GroupOrderRequestValidator : AbstractValidator<GroupOrderRequest>
    {
        private const string NotEmpty = "must not be empty";

        public GroupOrderRequestValidator()
        {
            RuleFor(template => template.LunchGroupId).NotEmpty().WithMessage("LunchGroup id of order " + NotEmpty);
            RuleFor(template => template.Date).GreaterThan(DateTime.Today.AddDays(1)).WithMessage("Wrong date");
            RuleFor(template => template.ProviderId).NotEmpty().WithMessage("Provider id of order " + NotEmpty);
        }
    }
}